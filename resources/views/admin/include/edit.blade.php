@extends('layouts.adminLayout')
@section('css-page')
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
   line-height: 20px;
}
.select2-container .select2-selection--single .select2-selection__rendered {

     padding-left: 0px;

}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Packages Include
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Packages Include</a>
        </li>
        <li class="active">Edit Packages Include</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{url('admin/update-include/' .$include->id)}}" enctype="multipart/form-data"  id="editUser" style="margin-top: 15px;">
                <input type="hidden" name="_method" value="post">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                
                <div class="form-group col-md-12" style="padding-left: 0;">
                  <label>Add Packages Include</label>
                  <textarea type="text" name="Include" 
                  rows="5" class="form-control"  required=""><?=$include['package_include']?></textarea> 
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>

                    
            <div class="form-group" style="clear: both;">
                   <label>&nbsp;</label>
                <button type="submit" class="btn btn-default">Submit</button>
                <a href="{{route('admin.listInclude')}}" class="btn btn-default">Back</a>
             </div>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

  