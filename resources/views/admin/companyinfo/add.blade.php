@extends('layouts.adminLayout')
@section('css-page')
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
   line-height: 20px;
}
.select2-container .select2-selection--single .select2-selection__rendered {

     padding-left: 0px;

}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Company
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Company</a></li>
        <li class="active">Add Company</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{route('admin.storeCompinfo')}}" enctype="multipart/form-data"  id="addTour" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">

               <input id="invisible_id" name="createdby" type="hidden" value="admin">

                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Company Name</label>
                  <input type="text" name="comp_name" class="form-control" id="comp_name" required="" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('comp_name')}}</div>
                </div>
               
                <div class="form-group col-md-6" style="padding-right: 0;">
                      <label>Comp Reg Num</label>
                      <input type="text" name="Comp_Reg_Num" class="form-control" id="Comp_Reg_Num" required="" >
                      <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Comp_Reg_Num')}}</div>
                 </div>
                  <div class="form-group col-md-12" style="clear:both;">
                 
                  <label>Company Desc</label>
                  <textarea type="text" name="Aboutus_Desc" rows="5" class="form-control" id="Aboutus_Desc" required="" placeholder="write message here"  ></textarea>
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Aboutus_Desc')}}</div>
                </div>

                
                 <div class="form-group col-md-6" style="clear: both;padding-left: 0;" >
                   <label>Company Logo</label>
                  <input type="file" name="logo" class="form-control" required="">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('logo')}}</div>
                </div>
                 <div class="form-group col-md-6" style="padding-right: 0;">
                      <label>Email</label>
                      <input type="email" name="email" class="form-control" id="email" required="">
                      <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('email')}}</div>
                 </div>

                 <div class="form-group col-md-6" style="padding-left: 0;" >
                   <label>Comp Tax num</label>
                  <input type="text" name="taxnum" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('taxnum')}}</div>
                </div>

                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>Address</label>
                  <input type="text" name="address" id="address" rows="5" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('address')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;" >
                   <label>FaceBook</label>
                  <input type="text" name="facebook" id="facebook" rows="5" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('facebook')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>Twitter</label>
                  <input type="text" name="twitter" id="twitter" rows="5" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('twitter')}}</div>
                </div>
                 <div class="form-group col-md-6" style="padding-left: 0;" >
                   <label>Instagram</label>
                  <input type="text" name="instagram" id="instagram" rows="5" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('instagram')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>YouTube</label>
                  <input type="text" name="youtube" id="youtube" rows="5" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('youtube')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;" >
                   <label>LinkedIn</label>
                  <input type="text" name="tikTook" id="tikTook" rows="5" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tikTook')}}</div>
                </div>
                
                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>Phone</label>
                  <input type="text" name="phone" class="form-control" required="">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('phone')}}</div>
                </div>

                <div class="form-group col-md-6" style="padding-left: 0;">
                   <label>Country name</label>
                   <select name="country_name" class="form-control" id="country_name"  style="width: 100%" required="">

                       <option selected disabled="">Choose One</option>
                      @foreach($countries as $country)
                       <option value="{{$country->id}}">{{$country->name}}</option>
                      @endforeach

                  </select> 
                </div>
              
                 <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>State</label>
                   <select name="state_name" class="form-control" id="state_name"  style="width: 100%" required="">
                        <option selected disabled="">Choose One</option>
                        

                  </select> 
                </div>

                
                <div class="form-group col-md-6" style="padding-left: 0;">

                  <label>City</label>
                  <select class="form-control" name="city" id="city_id">
                      <option selected disabled="">Choose One</option>
                     
                   
                  </select>
                </div>

                 <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                  <input type="radio" name="radio" value="1"> is enabled
                                  </div>
                  <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                  <input type="radio" name="radio" value="0"> is disabled
                </div>



                <div class="form-group" style="clear: both;">
                 <label>&nbsp;</label>
                      <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{route('admin.listCompinfo')}}" class="btn btn-default">Back</a>
              </div>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

      <script type="text/javascript">
              $( "#country_name" ).change(function() 
                {
                  var get =$("#country_name").val();
                  var link = "{{url('admin/country-id')}}/"+get;
                
                  $.ajax({
                    
                      type: "get",
                      url: link,
                      dataType:"json",
                  })
                  .done(function( msg ) {
               
                  $("#state_name").empty();

                    $.each( msg, function(i,val) {
                  $("#state_name").append("<option value='"+val['id']+"'>"+val['name']+"</option>");
                });

                    
                 });

                });
      </script>

            <script type="text/javascript">
              $( "#state_name" ).change(function() 
                {
                  var getcity =$("#state_name").val();
                  var link = "{{url('admin/city-id')}}/"+getcity;
                  $.ajax({
                      type: "get",
                      url: link,
                      dataType:"json",
                  }).done(function( msg ) {

                  $("#city_id").empty();

                    $.each( msg, function(i,val) {
                  $("#city_id").append("<option value='"+val['id']+"'>"+val['name']+"</option>");
                });

                    
                  });

                });
            </script>  


   @endsection

    <!-- /.content-wrapper -->
   