@extends('layouts.adminLayout')
@section('css-page')
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
   line-height: 20px;
}
.select2-container .select2-selection--single .select2-selection__rendered {

     padding-left: 0px;

}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Company
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Company</a></li>
        <li class="active">Add Company</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{url('admin/update-compinfo/'.$companies->id)}}" enctype="multipart/form-data"  id="addCompany" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">

               <input id="invisible_id" name="createdby" type="hidden" value="admin">

                <div class="form-group col-md-12" style="padding-left: 0;">
                  <label>Company Name</label>
                  <input type="text" name="comp_name" class="form-control" id="comp_name" value="{{$companies->comp_name}}" required=""  >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('comp_name')}}</div>
                </div>
                <div class="form-group col-md-12" style="padding-left: 0;">
                  <label>Company Desc</label>
                   <textarea type="text" required="" name="description" rows="5" class="form-control" id="description" placeholder="write message here" value="{{$companies->Aboutus_Desc}}"  ><?=$companies['Aboutus_Desc']?></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('description')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                      <label>Comp Reg Num</label>
                      <input type="text" name="Comp_Reg_Num" class="form-control" id="Comp_Reg_Num" value="{{$companies->Comp_Reg_Num}}" required="" >
                      <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Comp_Reg_Num')}}</div>
                 </div>

                 <div class="form-group col-md-6" style="padding-right: 0;">
                      <label>Email</label>
                      <input type="email" name="email" class="form-control" id="email" required="" value="{{$companies->Comp_Email}}">
                      <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('email')}}</div>
                 </div>

                 <div class="form-group col-md-6" style="clear: both;padding-left: 0;" >
                   <label>Company Logo</label>
                      @if( !empty($companies->Logo_URL))
                        <div><img src="{{ url('/images/logo/'.$companies->Logo_URL)}}" height="155px" width="155px">
                            <a href="{{ url('admin/destroy-logo/'.$companies->id)}}">Delete it</a></div>
                        @endif
                        <br>
                  <input type="file" name="logo" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('logo')}}</div>
                </div>

                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>Address</label>
                  <input type="text" name="address" id="address" class="form-control" value="{{$companies->Comp_Address}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('address')}}</div>
                </div>
                
                
                <div class="form-group col-md-6" style="padding-right:  0;" >
                   <label>Comp Tax num</label>
                  <input type="text" name="taxnum" class="form-control" value="{{$companies->Comp_Tax_num}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('taxnum')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>FaceBook</label>
                  <input type="text" name="facebook" id="facebook" rows="5" class="form-control" value="{{$companies->facebook}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('facebook')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right:  0;" >
                   <label>Twitter</label>
                  <input type="text" name="twitter" id="twitter" rows="5" class="form-control" value="{{$companies->twitter}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('twitter')}}</div>
                </div>
                 <div class="form-group col-md-6" style="padding-left:  0;" >
                   <label>Instagram</label>
                  <input type="text" name="instagram" id="instagram" rows="5" class="form-control" value="{{$companies->instagram}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('instagram')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right:  0;" >
                   <label>YouTube</label>
                  <input type="text" name="youtube" id="youtube" rows="5" class="form-control" value="{{$companies->youtube}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('youtube')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left:  0;" >
                   <label>LinkedIn</label>
                  <input type="text" name="tikTook" id="tikTook" rows="5" class="form-control" value="{{$companies->tiktok}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tikTook')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>Phone</label>
                  <input type="text" name="phone" class="form-control" value="{{$companies->Comp_Phone}}">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('phone')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;" >
                   <label>Mobile</label>
                    <input type="text" name="mobile" class="form-control" value="{{$companies->mobile}}" aria-label="..." placeholder="check if you want to display it on frontend">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('mobile')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>Country</label>
                  <select name="country_name" class="form-control" id="country_name"  style="width: 100%">
                      @foreach($countries as $country)
                       <option value="{{$country->id}}"{{$companies->Comp_Country_name == $country->id ? 'selected' : ''}}>{{$country->name}}</option>
                      @endforeach
                      </select>   
                </div>
                 <div class="form-group col-md-6" style="padding-left:  0; clear: both;">
                  <label>State</label>
                   <select name="state_name" class="form-control" id="state_name"  style="width: 100%" required="">
                    @foreach($states as $state)
                       <option value="{{$state->id}}" {{$companies->Comp_Stat_name == $state->id ? 'selected' : ''}}>{{$state->name}}</option>
                    @endforeach
                  </select>   
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>City</label>
                  <select name="city_name" class="form-control" id="city_id"  style="width: 100%" required="">
                   @foreach($cities as $city)
                       <option value="{{$city->id}}" {{$companies->Comp_City_name == $city->id ? 'selected' : ''}}>{{$city->name}}</option>
                    @endforeach
                  </select>
                </div>
                   <!--  <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                        <input type="radio" name="radio" value="1" {{ ($companies->is_enabled=="1")? "checked" : "" }} > is enabled
                    </div>
                    <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                        <input type="radio" name="radio" value="0" {{ ($companies->is_enabled=="0")? "checked" : "" }}> is disabled
                    </div> -->
                  


                <div class="form-group" style="clear: both;">
                 <label>&nbsp;</label>
                      <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{route('admin.listCompinfo')}}" class="btn btn-default">Back</a>
                </div>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

      <script type="text/javascript">
              $( "#country_name" ).change(function() 
                {
                  var get =$("#country_name").val();
                  var link = "{{url('admin/country-id')}}/"+get;
                
                  $.ajax({
                    
                      type: "get",
                      url: link,
                      dataType:"json",
                  })
                  .done(function( msg ) {
               
                  $("#state_name").empty();

                    $.each( msg, function(i,val) {
                  $("#state_name").append("<option value='"+val['id']+"'>"+val['name']+"</option>");
                });

                    
                 });

                });
      </script>

            <script type="text/javascript">
              $( "#state_name" ).change(function() 
                {
                  var getcity =$("#state_name").val();
                  var link = "{{url('admin/city-id')}}/"+getcity;
                  $.ajax({
                      type: "get",
                      url: link,
                      dataType:"json",
                  }).done(function( msg ) {

                  $("#city_id").empty();

                    $.each( msg, function(i,val) {
                  $("#city_id").append("<option value='"+val['id']+"'>"+val['name']+"</option>");
                });

                    
                  });

                });
            </script>  

   @endsection

   
    