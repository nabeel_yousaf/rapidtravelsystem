@extends('layouts.adminLayout')
@section('css-page')
<style>
.checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
     margin-top: 0;
    margin-left: 1px;
}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Country
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Country</a></li>
        <li class="active">Add Country</li>
      </ol>
    </section>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{route('admin.storeCountry')}}"  id="addRole" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Add Country</label>
                  <input type="text" name="country" class="form-control" id="country" required="">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <br>
    
               <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{url('admin/list-country')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
