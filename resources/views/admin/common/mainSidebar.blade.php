<!-- Left side column. contains the logo and sidebar -->

{{-- {{dd(Auth::user()->hasAccess('user_management'))}} --}}
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                &nbsp; @if(\Illuminate\Support\Facades\Auth::user()->profile_image!='' && file_exists( public_path() . '/media/images/users/' . \Illuminate\Support\Facades\Auth::user()->profile_image))
                <img src="{{asset('media/images/users/'.\Illuminate\Support\Facades\Auth::user()->profile_image)}}" style="width: 45px;height: 45px;" class="img-circle" alt="User Image"> @else
                <img src="{{asset('media/images/users/'.\Illuminate\Support\Facades\Auth::user()->profile_image)}}" style="width: 45px;height: 45px;" class="img-circle" alt="User Image"> @endif

            </div>
            <div class="pull-left info">
                <p>{{\Illuminate\Support\Facades\Auth::user()->first_name.' '.\Illuminate\Support\Facades\Auth::user()->last_name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu"> 

            
           @if(!Auth::user()->Admin->isEmpty())
           
            @include('admin.common.mainSideBar.user_management')
            @include('admin.common.mainSideBar.tour_management')
            @include('admin.common.mainSideBar.configuration')
            @include('admin.common.mainSideBar.booking_management')
            @include('admin.common.mainSideBar.About_management')
            
            @endif

            @if(Auth::user()->Admin->isEmpty() && Auth::user()->hasAccess('user_management'))
            @include('admin.common.mainSideBar.user_management')
            @endif

            @if(Auth::user()->Admin->isEmpty() && Auth::user()->hasAccess('tour_management'))
            @include('admin.common.mainSideBar.tour_management')
            @endif

            @if(Auth::user()->Admin->isEmpty() && Auth::user()->hasAccess('booking_management'))
            @include('admin.common.mainSideBar.booking_management')
            @endif

            @if(Auth::user()->Admin->isEmpty() && Auth::user()->hasAccess('configuration_management'))
            @include('admin.common.mainSideBar.configuration')
            @endif
            @if(Auth::user()->Admin->isEmpty() && Auth::user()->hasAccess('About_management'))
            @include('admin.common.mainSideBar.About_management')
            @endif

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>