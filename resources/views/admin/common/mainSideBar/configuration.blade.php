<li class="treeview">
    <a href="#">
      <i class="fa fa-edit"></i> <span>Add Country</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{route('admin.addCountry')}}"><i class="fa fa-circle-o"></i>Add New Country</a></li>
      <li><a href="{{route('admin.listCountry')}}"><i class="fa fa-circle-o"></i> List Countries</a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
      <i class="fa fa-edit"></i> <span>Add State</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{route('admin.addState')}}"><i class="fa fa-circle-o"></i>Add New State</a></li>
      <li><a href="{{route('admin.listState')}}"><i class="fa fa-circle-o"></i> List States</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
      <i class="fa fa-edit"></i> <span>Add City</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{route('admin.addCity')}}"><i class="fa fa-circle-o"></i>Add New City</a></li>
      <li><a href="{{route('admin.listCity')}}"><i class="fa fa-circle-o"></i> List Cities</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Company Management</span>
        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
    </a>
    <ul class="treeview-menu">
        <!-- <li><a href="{{route('admin.addCompinfo')}}"><i class="fa fa-circle-o"></i>Add New Company</a></li> -->
        <li><a href="{{route('admin.listCompinfo')}}"><i class="fa fa-circle-o"></i> List Company</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>NavBar Management</span>
        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
    </a>
    <ul class="treeview-menu">
    <!--     <li><a href="{{route('admin.addComponent')}}"><i class="fa fa-circle-o"></i>Add New Module </a></li> -->
        <li><a href="{{route('admin.listComponent')}}"><i class="fa fa-circle-o"></i> List Module</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Attraction Management</span>
        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{route('admin.addAttraction')}}"><i class="fa fa-circle-o"></i>Add Attraction </a></li>
        <li><a href="{{route('admin.listAttraction')}}"><i class="fa fa-circle-o"></i> List Attraction</a></li>
    </ul>
</li>