<li class="treeview">
    <a href="#">
        <i class="fa fa-user"></i> <span>User Management</span>
        <span class="pull-right-container">
         <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{route('admin.addUser')}}"><i class="fa fa-circle-o"></i>Add New User</a></li>
        <li><a href="{{route('admin.listUsers')}}"><i class="fa fa-circle-o"></i> List User</a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Role Management</span>
         <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{route('admin.addRole')}}"><i class="fa fa-circle-o"></i>Add Role</a></li>
        <li><a href="{{route('admin.listRoles')}}"><i class="fa fa-circle-o"></i> List Roles</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Permission Management</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        {{-- <li><a href="{{route('admin.addPermission')}}"><i class="fa fa-circle-o"></i>Add New Permission</a></li> --}}
        <li><a href="{{route('admin.listPermissions')}}"><i class="fa fa-circle-o"></i> List Permissions</a></li>
    </ul>
</li>