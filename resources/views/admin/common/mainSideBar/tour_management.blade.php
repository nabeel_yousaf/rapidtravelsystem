{{-- <li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Tour Packages Include</span>
        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{route('admin.addInclude')}}"><i class="fa fa-circle-o"></i>Add New Packages Include </a></li>
        <li><a href="{{route('admin.listInclude')}}"><i class="fa fa-circle-o"></i> List Packages Include</a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Tour Packages Exclude</span>
        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{route('admin.addExclude')}}"><i class="fa fa-circle-o"></i>Add New Packages Exclude </a></li>
        <li><a href="{{route('admin.listExclude')}}"><i class="fa fa-circle-o"></i> List Packages Exclude</a></li>
    </ul>
</li> --}}
<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Tour Type Management</span>
        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{route('admin.addTourType')}}"><i class="fa fa-circle-o"></i>Add New Tour Type </a></li>
        <li><a href="{{route('admin.listTourType')}}"><i class="fa fa-circle-o"></i> List TourType</a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Tour Management</span>
        <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{route('admin.addTours')}}"><i class="fa fa-circle-o"></i>Add New Tour</a></li>
        <li><a href="{{route('admin.listTours')}}"><i class="fa fa-circle-o"></i> List Tour</a></li>
    </ul>
</li>