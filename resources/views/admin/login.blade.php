<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin Login | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
  <link rel="stylesheet" href="{{asset('css/formValidation.css')}}"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="javascript:void(0)"><b>Have Fun Tour</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form action="{{route('admin.postLogin')}}" id="loginForm" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
{{--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>--}}
    <!-- /.social-auth-links -->
   <div id="form-errors" class="loginError" style="display: none;"></div>
    <a href="admin/password/reset">I forgot my password</a><br>
   {{-- <a href="register.html" class="text-center">Register a new membership</a>--}}

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<style>
.loginError{
    display: block;
    margin-top: 5px;
    margin-bottom: 5px;
    color: #dd4b39;
    font-weight: 400;
    }
</style>


<!-- jQuery 2.2.3 -->
<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
 <script type="text/javascript" src="{{asset('js/formValidation.js')}}"></script>
 <script type="text/javascript" src="{{asset('js/framework/bootstrap.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

  });
  $(document).ready(function() {
      $('#loginForm')
          .formValidation({
              icon: {
               //   valid: 'glyphicon glyphicon-ok',
                //  invalid: 'glyphicon glyphicon-remove',
                //  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                  email: {
                      validators: {
                          notEmpty: {
                              message: 'The email address is required and can\'t be empty'
                          },
                          emailAddress: {
                              message: 'The input is not a valid email address'
                          }
                      }
                  },
                  password: {
                      validators: {
                          notEmpty: {
                              message: 'The password is required and can\'t be empty'
                          }
                      }
                  }
              }
          })
          .on('success.form.fv', function(e) {
              // Prevent form submission
              e.preventDefault();
         //   var $btn = $(this).button('loading');
              // Get the form instance
              var $form = $(e.target);
              var errorDiv = $('#form-errors').hide();

              // Get the FormValidation instance
              var bv = $form.data('formValidation');
            $.ajax({
                    type: "POST",
                    url : $form.attr('action'),
                    data : $form.serialize(),
                    success : function(data){
                       // alert(data);
                       if(data.error == 'false'){
                          setTimeout(function(){
                             //  $btn.button('success');
                             $('#loginForm button[type=submit]').css('background', '#449d44');
                             //  alert("{{route('admin.dashboard')}}");
                            window.location.href = '{{route('admin.dashboard')}}';
                               }, 2000);

                       } else if (data.error == 'true') {

                       setTimeout(function(){
                       var errors = data.message;
                       //console.log(errors);
                           errorsHtml = errors;
                           $( '#form-errors' ).html(errorsHtml).show(); //appending to a <div id="form-errors"></div> inside form
                          // $btn.button('reset');
                          $('#loginForm button[type=submit]').removeClass('disabled').removeAttr('disabled');

                           }, 1500);

                       }
                    },
                    error : function(data){
                    console.log(data.responseText);
                         $( '#form-errors' ).html('Too many login attempts. Please try again after 5 min').show();
                    }

                },"json");
              // Use Ajax to submit form data
            /*  $.post($form.attr('action'), $form.serialize(), function(result) {
                  console.log(result);
              }, 'json');*/
          });
  });
</script>
</body>
</html>
