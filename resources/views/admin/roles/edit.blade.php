@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Role
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Roles</a></li>
        <li class="active">Edit Role</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{url('admin/update-role/'.$role->id)}}"  id="editRoles" style="margin-top: 15px;">
                <input type="hidden" name="_method" value="put">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Role Title</label>
                  <input type="text" name="role_title" value="{{$role->role_title}}" class="form-control" id="role_title" required="" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>Slug</label>
                  <input type="text" name="role_slug" class="form-control" id="role_slug" value="{{$role->role_slug}}" required="" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{last($errors->all())}}</div>
                </div>

              <div class="form-group">
                <label style="float: left; ">Permissions: </label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="col-md-9" style="clear: both;margin-bottom:15px;">
                @foreach($permissions as $permission)
                 <label class="checkbox-inline col-md-2">
                 <input type="checkbox" id="permissions[]" @if(in_array($permission->id,array_pluck($role->permissions,'id'))) checked @endif name="permissions[]" value="{{$permission->id}}" >{{$permission->permission_title}}</label>
                @endforeach
                </div>
              </div>
              <div class="form-group" style="clear: both;margin-top:25px;">
                  <label>Role Description</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." name="role_description" id="role_description">
                  {{$role->role_description}}
                  </textarea>
                </div>
              <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{route('admin.listRoles')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
  @section('js-page')
  <script type="text/javascript" src="{{asset('js/formValidation.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/framework/bootstrap.js')}}"></script>
 <script>
 $(document).ready(function() {
       $('#editRoles')
                 .formValidation({
                     icon: {
                      //   valid: 'glyphicon glyphicon-ok',
                       //  invalid: 'glyphicon glyphicon-remove',
                       //  validating: 'glyphicon glyphicon-refresh'
                     },
                     fields: {
                         role_title: {
                             validators: {
                                 notEmpty: {
                                     message: 'Role title is required'
                                 }

                             }
                         },
                         role_slug: {
                             validators: {
                                 notEmpty: {
                                     message: "Role slug is required"
                                 }
                             }
                         }

                     }
                 })
   });
        $(document).ready(function() {
          $('#role_title').keyup(function(){
          var str = $('#role_title').val();
          str = str.replace(/^\s+|\s+$/g, ''); // trim
          str = str.toLowerCase();

          // remove accents, swap ñ for n, etc
          var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
          var to   = "aaaaeeeeiiiioooouuuunc------";
          for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
          }

          str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes
          $('#role_slug').val(str);
        //  return true;
            });
         });
        </script>
 <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
 <script>
   $(function () {
     // Replace the <textarea id="editor1"> with a CKEditor
     // instance, using default configuration.
     CKEDITOR.replace('role_description');

   });
 </script>
  @append