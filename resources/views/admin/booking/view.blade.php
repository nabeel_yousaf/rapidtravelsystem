@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List Bookings Details
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Bookings Details</a></li>
        <li class="active">List Bookings Details</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body" style="margin-top: 15px;">
            @if (session('message'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            @endif
               <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap button_class">

                    <div class="row">
                    <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th style="width: 8%">SR#</th>
                            <th style="width: 15%">Name</th>
                            <th style="width: 15%">Email</th>
                            <th style="width: 15%">Persons</th>
                            <th style="width: 15%">Phone NO</th>
                            <th style="width: 15%">Booking request</th>
                            <th style="width: 15%">Booking Time</th>
                           
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1; ?>

                         @foreach($bookings as $booking)

                        <tr role="row" class="odd">
                          <td>{{ $i}}</td>
                          
                          <td>{{$booking->name }}</td>
                          <td>{{$booking->email}}</td>
                          <td>{{$booking->persons}}</td>
                           <td>{{$booking->phone}}</td>
                          <td>{{$booking->request}}</td>
                          <td>{{date('l jS \of F Y h:i:s A', strtotime($booking->created_at))}}</td>
                  
                        </tr>
                        <?php $i++ ?>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                  </div>
                </div>
            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
  @section('js-page')
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
  });
</script>
  @append