@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')

<style type="text/css">
  table{
    width:100%;
}
#example_filter{
    float:right;
}
#example_paginate{
    float:right;
}
label {
    display: inline-flex;
    margin-bottom: .5rem;
    margin-top: .5rem;
   
}

</style>


 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List Cities
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Cities</a></li>
        <li class="active">List Cities</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body" style="margin-top: 15px;">
            @if (session('message'))
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            @endif

                 <div class="row">
                  <div class="col-sm-12">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                       <thead>
                          <tr role="row">
                              <th>
                                <input type="checkbox" onclick="checkAll(this)">
                              </th>
                              <th>Country Name</th>
                              <th>State Name</th>
                              <th>City Name</th>
                              <th>Actions</th>
                              
                          </tr>
                       </thead>
                    <tbody>
                         <?php $i=1; ?>

                         @foreach($cities as $city)
                            <tr role="row" class="odd">
                              <td>{{ $i}}</td>
                             
                                @foreach($countries as $country)
                                  @if($country->id == $city->country_id)
                                     <td>{{$country->name}}</td>
                                  @endif
                                @endforeach
                              
                              
                                @foreach($states as $state)
                                  @if($state->id == $city->state_id)
                                    <td>{{$state->name}}</td>
                                  @endif
                                @endforeach
                              
                              <td>
                                <p>{{$city->name}}</p>
                              </td>
                              <td>
                                <a class="btn btn-info" href="{{url('admin/edit-city/' .$city->id)}}">
                               <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                  </a>
                               <br><br> 
                               <!-- <a class="btn btn-danger" href="<?php //echo url('admin/delete-city/' .$city->id);?>" onClick="return confirm('Are you sure? you want to delete this entry!')">
                                <i class="glyphicon glyphicon-trash icon-white"></i>Delete</a>
                              </td> -->
                             
                        </tr>
                        <?php $i++ ?>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                  </div>
                </div>
            </div>

            <!-- /.box-body -->

          </div>

        </div>

   @endsection