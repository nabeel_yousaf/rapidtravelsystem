@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit City  
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Cities</a></li>
        <li class="active">Edit City</li>
      </ol>
    </section>
    <div class="col-md-12">
          <!-- general form elements -->
        <div class="box box-warning">
          <div class="box-body">
            <form method="post" action="{{url('admin/update-city/'.$cities->id)}}" id="addRole" style="margin-top: 15px;">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Country name</label>
                   <select name="country_name" class="form-control" id="country_name" required=""  style="width: 100%">

                      
                      @foreach($countries as $country)
                      <option value="{{$country->id}}" {{$cities->country_id == $country->id ? 'selected' : ''}}>{{$country->name}}</option>
                      @endforeach 

                  </select> 
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>State name</label>
                  <select name="state_name" class="form-control" id="state_name"  style="width: 100%" required="">
                    @foreach($states as $state)
                       <option value="{{$state->id}}" {{$cities->state_id == $state->id ? 'selected' : ''}}>{{$state->name}}</option>
                    @endforeach
                  </select> 
                </div>
                <div class="form-group">
                  <label>City Name</label>
                  <input type="text" name="name" class="form-control" id="name" required="" value="{{$cities->name}}">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{last($errors->all())}}</div>
                </div>
               <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{route('admin.listCity')}}" class="btn btn-default">Back</a>
            </form>

          </div>

            <!-- /.box-body -->

      </div>

    </div>
   </div>

    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

      <script type="text/javascript">
              $( "#country_name" ).change(function() 
                {
                  var get =$("#country_name").val();
                  var link = "{{url('/admin/country-id')}}/"+get;
                  $.ajax({
                      type: "get",
                      url: link,
                      dataType:"json",
                  }).done(function( msg ) {

                  $("#state_name").empty();

                    $.each( msg, function(i,val) {
                  $("#state_name").append("<option value='"+val['id']+"'>"+val['name']+"</option>");
                });

                    
                  });

                });
      </script>

   @endsection
