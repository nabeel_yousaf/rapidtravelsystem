@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Component
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Component</a></li>
        <li class="active">Edit Component</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{url('admin/update-component/'.$components->id)}}"  id="components" style="margin-top: 15px;">
                
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit home</label>
                  <input type="text" name="home" value="{{$components->home}}" class="form-control" id="home" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit tour</label>
                  <input type="text" name="tour" value="{{$components->tour}}" class="form-control" id="tour" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit safari</label>
                  <input type="text" name="safari" value="{{$components->safari}}" class="form-control" id="safari" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit service</label>
                  <input type="text" name="service" value="{{$components->service}}" class="form-control" id="service" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit guide</label>
                  <input type="text" name="guide" value="{{$components->guide}}" class="form-control" id="guide">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit contact</label>
                  <input type="text" name="contact" value="{{$components->contact}}" class="form-control" id="contact" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit about</label>
                  <input type="text" name="about" value="{{$components->about}}" class="form-control" id="about" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit travel</label>
                  <input type="text" name="travel" value="{{$components->travel}}" class="form-control" id="travel">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>

                


               <br>

              <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{route('admin.listComponent')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection
