@extends('layouts.adminLayout')
@section('css-page')
<style>
.checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
     margin-top: 0;
    margin-left: 1px;
}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Component
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Component</a></li>
        <li class="active">Add Component</li>
      </ol>
    </section>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{route('admin.storeComponent')}}"  id="addRole" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Home</label>
                  <input type="text" name="home" class="form-control" id="home" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Tour</label>
                  <input type="text" name="tour" class="form-control" id="tour" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Safari</label>
                  <input type="text" name="safari" class="form-control" id="safari" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Service</label>
                  <input type="text" name="service" class="form-control" id="service">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Guide</label>
                  <input type="text" name="guide" class="form-control" id="guide" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Contact US</label>
                  <input type="text" name="contact" class="form-control" id="contact" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>About US</label>
                  <input type="text" name="about" class="form-control" id="about" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                 <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Travel</label>
                  <input type="text" name="travel" class="form-control" id="travel" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <br>
    
               <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{url('admin/list-component')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
