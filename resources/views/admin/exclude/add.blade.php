@extends('layouts.adminLayout')
@section('css-page')
<style> 
.checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
     margin-top: 0;
    margin-left: 1px;
}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Packages Exclude
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Packages Exclude</a></li>
        <li class="active">Add Packages Exclude</li>
      </ol>
    </section>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{route('admin.storeExclude')}}"  id="addRole" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
              
                <div class="form-group col-md-12" style="padding-left: 0;">
                  <label>Add Packages Exclude</label>
                  <textarea type="text"  required="" name="Exclude" rows="5" class="form-control" id="Exclude" placeholder="write message here"  ></textarea> 
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <br>
    
               <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{url('admin/list-exclude')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
