@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List Attractions
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Attractions</a></li>
        <li class="active">List Attractions</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body" style="margin-top: 15px;">
            @if (session('message'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            @endif
               <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap button_class">

                    <div class="row">
                    <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                            <th style="width: 8%">SR#</th>
                            <th style="width: 15%">Name</th>
                            <th style="width: 15%">Description</th>
                            <th style="width: 15%">Logo</th>
                            
                            <th style="width: 15%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1; ?>

                         @foreach($attractions as $attraction)

                        <tr role="row" class="odd">
                          <td>{{ $i}}</td>
                          
                          <td>{{$attraction->name }}</td>
                          <td>
                            <div style="width: 900px; height: 100px; overflow: auto">
                              {{$attraction->description }}
                            </div>
                          </td>
                          <td>
                              <div class="row">
                                <div class="col-xs-6 col-md-3">

                                  <img src="{{ url('/images/attractions/'.$attraction->logo)}}" height="100px" width="150px">

                                </div>
                              </div>
                          </td>
                          
                         

                          
                          
                          <td>
                          <a class="btn btn-info" href="{{ url('admin/edit-attraction/' .$attraction->id) }}">
                                 <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                  </a>
                    <!-- <a class="btn btn-danger" href="<?php //echo url('admin/delete-exclude/'.$exclude->id);?>" onClick="return confirm('Are you sure? you want to delete this entry!')">
                    <i class="glyphicon glyphicon-trash icon-white"></i>Delete</a> -->
                          </td>
                        </tr>
                        <?php $i++ ?>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                  </div>
                </div>
            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
  @section('js-page')
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
  });
</script>
  @append