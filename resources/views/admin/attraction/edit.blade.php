@extends('layouts.adminLayout')
@section('css-page')
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
   line-height: 20px;
}
.select2-container .select2-selection--single .select2-selection__rendered {

     padding-left: 0px;

}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Attractions
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Attractions</a>
        </li>
        <li class="active">Edit Attractions</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{url('admin/update-attraction/' .$attractions->id)}}" enctype="multipart/form-data"  id="editUser" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                   
                  
                    <div class="form-group col-md-12" style="padding-left: 0;">
                        <label>Attraction</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{$attractions->name}}" required="">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('name')}}</div>
                    </div>

                    <div class="form-group col-md-12" style="padding-left:  0;">
                        <label>Description</label>
                        <textarea type="text" required="" name="description" rows="5" class="form-control" value="{{$attractions->description}}" id="description"><?=$attractions['description']?></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('description')}}</div>
                    </div>
                     <div class="form-group col-md-12" style="clear: both;padding-left: 0;">
                        <label>Profile Thumbnail</label>
                        @if( !empty($attractions->logo))
                        <div><img src="{{ url('/images/attractions/'.$attractions->logo)}}" height="155px" width="155px">
                            <a href="{{ url('admin/destroy-img/'.$attractions->id)}}">Delete it</a></div>
                        @endif
                        <br>
                        <input type="file" name="logo" class="form-control">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('logo')}}</div>
                    </div>
                    
                    <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                        <input type="radio" name="radio" value="1"  {{ ($attractions->is_enabled=="1")? "checked" : "" }} > is enabled
                    </div>
                    <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                        <input type="radio" name="radio" value="0"  {{ ($attractions->is_enabled=="0")? "checked" : "" }} > is disabled
                    </div>
                   
                    <div class="form-group" style="clear: both;">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <a href="{{route('admin.listAttraction')}}" class="btn btn-default">Back</a>
                    </div>
            </form>
            <div class="col-md-2">

            </div>
                <div class="row">
                   <div class="col-md-12">
                                        <label>Upload Multiple Images</label>
                                       
                    <form class="text-center" method="post" action="{{url('admin/Multiple-update-img/'.$attractions->id)}}" enctype="multipart/form-data" id="edit_gal" style="margin-top:15px">

                       <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <input type="file" name="multiple_images[]" id="multiple_images" class="form-control" id="file" accept="gif|jpg|png" multiple required="">
                                            <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('multiple_images')}}</div>

                                            <br>
                                            <button class="btn btn-primary">Submit</button>
                                           
                     </form>
                   </div>
                </div>
                    
                    <div class="col-md-12">
                       <h3><strong>Delete Your Images</strong></h3>
                        <span><small>Select Images and Click Delete Button</small></span>

                        <form method="post" action="{{url('admin/delete-img/'.$attractions->id)}}"  enctype="multipart/form-data" style="margin-top:15px">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <?php $i=1;  ?>
                                @foreach($imgs as $img)
                                      
                                  <div class="col-md-2">
                                    <input type="checkbox" name="image_gal[{{$img->id}}]" id="image_gal{{$i}}" value="{{$img->id}}" >
                                    <label style=" cursor: pointer;" for="image_gal{{$i}}">
                                        <img src="{{ url('/images/logo/attractions/'.$img->attraction_imgs)}}" height="150px" width="150px" />
                                    </label>
                                  </div>
                                <?php $i++; ?>
                                 
                                @endforeach

                            <div class="col-md-12 text-center">
                                <button class="btn btn-danger">Delete</button>
                                        
                            </div>
                            
                        </form>

                    </div>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

  