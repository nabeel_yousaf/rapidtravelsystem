@extends('layouts.adminLayout') @section('css-page')
<style>
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 20px;
    }
    
    .select2-container .select2-selection--single .select2-selection__rendered {
        padding-left: 0px;
    }
</style>
@append @section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Attraction
        <small>Control panel</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Manage Attraction</a></li>
            <li class="active">Add Attraction</li>
        </ol>
    </section>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <form method="post" action="{{route('admin.storeAttraction')}}" enctype="multipart/form-data" id="Attraction" style="margin-top: 15px;">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                   
                  
                    <div class="form-group col-md-12" style="padding-left: 0;">
                        <label>Attraction</label>
                        <input type="text" name="name" class="form-control" id="name" required=" ">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('name')}}</div>
                    </div>

                    <div class="form-group col-md-12" style="padding-left:  0;">
                        <label>Description</label>
                        <textarea type="text" required="" name="description" rows="5" class="form-control" id="description" placeholder="write message here"  ></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('description')}}</div>
                    </div>
                    <div class="form-group col-md-12" style="clear: both;padding-left: 0;">
                        <label>Profile Thumbnail</label>
                        <input type="file" name="image" class="form-control" required="">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_image')}}</div>
                    </div>
                    <div class="form-group col-md-12" style="clear: both;padding-left: 0;">
                        <label>Upload Multiple Images</label>
                        <input type="file" name="multiple_images[]" id="multiple_images" class="form-control" accept="gif|jpg|png" multiple required="">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('multiple_images')}}</div>
                    </div>
                    <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                        <input type="radio" name="radio" value="1"> is enabled
                    </div>
                    <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                        <input type="radio" name="radio" value="0"> is disabled
                    </div>
                   
                    <div class="form-group" style="clear: both;">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <a href="{{route('admin.listTours')}}" class="btn btn-default">Back</a>
                    </div>
                </form>

            </div>

            <!-- /.box-body -->

        </div>

    </div>
</div>

@endsection

