@extends('layouts.adminLayout') @section('css-page')
<style>
    .checkbox-inline+.checkbox-inline,
    .radio-inline+.radio-inline {
        margin-top: 0;
        margin-left: 1px;
    }
</style>
@append @section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Tour Type
        <small>Control panel</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Manage Tour Type</a></li>
            <li class="active">Add Tour Type</li>
        </ol>
    </section>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <form method="post" action="{{route('admin.storeTourType')}}" id="addRole" style="margin-top: 15px;">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group col-md-12" style="padding-left: 0;">
                        <label>Add Tour Type</label>
                        <input type="text" name="tourtype" class="form-control" id="tourtype" required="">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                    </div>
                    <div class="form-group col-md-12" style="padding-left: 0;">
                        <label>Description</label>
                        <textarea type="text" name="description" placeholder="write message here"   rows="10" class="form-control" id="description" required=""></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                    </div>
                     <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                        <input type="radio" name="radio" value="1"> is enabled
                    </div>
                    <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                        <input type="radio" name="radio" value="0"> is disabled
                    </div>
                    <br>
                    <div class="form-group" style="clear: both;">
                        <label>&nbsp;</label>
                    <button type="submit" class="btn btn-default">Submit</button>
                    <a href="{{url('admin/list-tourtype')}}" class="btn btn-default">Back</a>
                    </div>
                </form>

            </div>

            <!-- /.box-body -->

        </div>

    </div>
</div>

@endsection

<!-- /.content-wrapper -->