@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List Tour
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Tour</a></li>
        <li class="active">List Tour</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body" style="margin-top: 15px;">
            @if (session('message'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            @endif
               <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap button_class">

                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                               

                                    <thead>
                                        <tr role="row">
                                          <th style="width: 1%">SR#</th>
                                            <th  style="width: 5%">Tour Types Name</th>
                                           <th  style="width: 25%">Tour Types Description</th>
                                            <th style="width: 1%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; ?>
                                            @foreach($tourtypes as $tourtype)
                                            <tr role="row" class="odd">
                                                <td>{{ $i}}</td>
                                                <td>{{$tourtype->tourtypeName}}</td>
                                                <td>
                                                   <div style="width: 1200px; height: 100px; overflow: auto">
                                                  {{$tourtype->tourtypeDes}}
                                                   </div>
                                                </td>

                                               
                                                <td>
                                                   @if($tourtype->id !==1)
                                                    <a class="btn btn-info" href="{{url('admin/edit-tourtype/' .$tourtype->id)}}">
                                                        <i class="glyphicon glyphicon-edit icon-white"></i> Edit
                                                    </a>
                                                   <br><br>
                                                    <!-- <a class="btn btn-danger" href="<?php //echo url('admin/delete-tourtype/' .$tourtype->id);?>" onClick="return confirm('Are you sure? you want to delete this entry!')">
                                                        <i class="glyphicon glyphicon-trash icon-white"></i>Delete</a> -->
                                                    @endif
                                                </td>
                                              
                                            </tr>
                                            <?php $i++ ?>
                                                @endforeach
                                    </tbody>
                                  
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
  @section('js-page')
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
   
  });
</script>
  @append
