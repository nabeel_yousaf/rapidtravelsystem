@extends('layouts.adminLayout')
@section('css-page')
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
   line-height: 20px;
}
.select2-container .select2-selection--single .select2-selection__rendered {

     padding-left: 0px;

} 
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add User
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Users</a></li>
        <li class="active">Add User</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{route('admin.storeUser')}}" enctype="multipart/form-data"  id="addUser" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group col-md-6" style="padding-left: 0;">

                <!-- <input id="invisible_id" name="createdby" type="hidden" value="admin">
                 <input id="compid" name="compid" type="hidden" value="1"> -->
                 

                  <label>First Name</label>
                  <input type="text" name="first_name" class="form-control" id="first_name" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('first_name')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>Last Name</label>
                  <input type="text" name="last_name" class="form-control" id="last_name" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('last_name')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                      <label>Email</label>
                      <input type="email" name="email" class="form-control" id="email" >
                      <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('email')}}</div>
                 </div>


                <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>Role</label>
                  <select class="form-control" name="role" required="">
                  @foreach($roles as $role)

                    <option value="{{$role->id}}">{{$role->role_title}}</option>
                    @endforeach
                  </select>
                </div>
                
                 <div class="form-group col-md-6" style="clear: both;padding-left: 0;" >
                   <label>Profile Thumbnail</label>
                  <input type="file" name="profile_image" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('profile_image')}}</div>
                </div>
                
                
                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>Post Code</label>
                  <input type="text" name="post_code" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('post_code')}}</div>
                </div>

                <div class="form-group col-md-6" style="padding-left: 0; clear: both">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" id="password" >
                    <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('password')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;">
                    <label>Confirm Password</label>
                    <input type="password" name="confirm_password" class="form-control" id="confirm_password" >
                    <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('confirm_password')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;clear:both;" >
                   <label>Phone</label>
                  <input type="text" name="phone" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('phone')}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;" >
                   <label>Address</label>
                  <input type="text" name="address" id="address" class="form-control">
                   <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('address')}}</div>
                </div>
                <div class="radio form-group col-md-6">
                  <input type="radio" name="radio" value="1">is enabled
                                  </div>
                  <div class="radio form-group col-md-6">
                  <input type="radio" name="radio" value="0">is disabled
                </div>

                <div class="form-group" style="clear: both;">
                 <label>&nbsp;</label>
              <button type="submit" class="btn btn-default">Submit</button>
              <a href="{{route('admin.listUsers')}}" class="btn btn-default">Back</a>
              </div>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
  @section('js-page')
  <script type="text/javascript" src="{{asset('js/formValidation.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/framework/bootstrap.js')}}"></script>
 <script>
 $(document).ready(function() {
  $("#country").select2()
         .on("select2:select", function (e) {
                  var select_val = $(e.currentTarget).val();
                  $.ajax({
                        url: '{{url('/get-states/')}}',
                        data: {
                           code: select_val
                        },

                        dataType: 'json',
                        success: function(data) {
                           //alert(data);
                           $("#state").html(data);
                           var title = $("#billing_state option:first").text();
                           $('#select2-billing_state-container').attr('title', title);
                           $('#select2-billing_state-container').text(title);
                        },
                        type: 'GET'
                     });
              });
               $("#state").select2();

       $('#addUser')
                 .formValidation({
                     icon: {
                      //   valid: 'glyphicon glyphicon-ok',
                       //  invalid: 'glyphicon glyphicon-remove',
                       //  validating: 'glyphicon glyphicon-refresh'
                     },
                     fields: {
                         first_name: {
                             validators: {
                                 notEmpty: {
                                     message: 'First name is required'
                                 }

                             }
                         },
                         last_name: {
                             validators: {
                                 notEmpty: {
                                     message: "Last name is required"
                                 }
                             }
                         },
                          email: {
                               validators: {
                                   notEmpty: {
                                       message: 'The email address is required and can\'t be empty'
                                   },
                                   emailAddress: {
                                       message: 'The input is not a valid email address'
                                   }
                               }
                           },
                          password: {
                             validators: {
                                 notEmpty: {
                                     message: 'The new password is required'
                                   },
                                   regexp: {
                                   regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/,
                                   message: 'The password must contain one uppercase, one digit and min length of 6'
                               },

                             }
                            },
                            confirm_password: {
                             validators: {
                                 notEmpty: {
                                       message: 'The confirm password is required'
                                   },
                                 identical: {
                                     field: 'password',
                                     message: 'The passwords entered must match'
                                 }
                             }
                            }
                     }
                 })
   });

        </script>
  @append