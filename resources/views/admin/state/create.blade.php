@extends('layouts.adminLayout')
@section('css-page')
<style>
.checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
     margin-top: 0;
    margin-left: 1px;
}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add State
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage States</a></li>
        <li class="active">Add States</li>
      </ol>
    </section>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{route('admin.storeState')}}"  id="addRole" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Country Name</label>
                  <select name="name" class="form-control" id="name"  style="width: 100%">

                       <option selected disabled="">Choose One</option>
                      @foreach($countries as $country)
                       <option value="{{$country->id}}">{{$country->name}}</option>
                      @endforeach

                  </select>  
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;"> {{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Add State</label>
                  <input type="text" name="state" class="form-control" id="state" required="">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <br>
    
               <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{url('admin/list-state')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
