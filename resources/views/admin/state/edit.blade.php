@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit States
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage States</a></li>
        <li class="active">Edit State</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{url('admin/update-state/'.$states->id)}}"  id="state" style="margin-top: 15px;">
                
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Country Name</label>
                  <select name="name" class="form-control" id="name"  style="width: 100%">

                       
                      @foreach($countries as $country)
                       <option value="{{$country->id}}"{{$states->country_id == $country->id ? 'selected' : ''}}>{{$country->name}}</option>
                      @endforeach   

                  </select>  
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;"> {{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Edit States</label>
                  <input type="text" name="state" value="{{$states->name}}" class="form-control" id="state" required="">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>

               <br>

              <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{route('admin.listState')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

