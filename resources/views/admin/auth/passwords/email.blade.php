<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>MSJ Communications | Password Reset</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
  <link rel="stylesheet" href="{{asset('css/formValidation.css')}}"/>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="javascript:void(0)"><b>MSJ Admin Panel</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><b>Reset Password</b></p>
                     @if (session('status'))
                           <div class="alert alert-success">
                           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                               {{ session('status') }}
                           </div>
                       @endif

                       <form role="form" method="POST" action="{{ url('admin/password/email') }}">
                           {{ csrf_field() }}

                           <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                               <label for="email" >E-Mail Address</label>


                                   <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                   @if ($errors->has('email'))
                                       <span class="help-block">
                                           <strong>{{ $errors->first('email') }}</strong>
                                       </span>
                                   @endif

                           </div>

                           <div class="form-group">
                               <div class="col-md-6 col-md-offset-4">
                                   <button type="submit" class="btn btn-primary">
                                       <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                                   </button>
                               </div>
                           </div>
                       </form>
{{--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>--}}
    <!-- /.social-auth-links -->
   <div id="form-errors" class="loginError" style="display: none;"></div>
    <a href="javascript:void(0)">&nbsp;</a><br>
   {{-- <a href="register.html" class="text-center">Register a new membership</a>--}}

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<style>
.loginError{
    display: block;
    margin-top: 5px;
    margin-bottom: 5px;
    color: #dd4b39;
    font-weight: 400;
    }
    .login-box-body, .register-box-body {
        background: #fff;
        padding: 35px;
        border-top: 0;
        color: #666;
    }
    .login-box, .register-box {
        width: 400px;
        margin: 7% auto;
    }
</style>



<!-- jQuery 2.2.3 -->
<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
 <script type="text/javascript" src="{{asset('js/formValidation.js')}}"></script>
 <script type="text/javascript" src="{{asset('js/framework/bootstrap.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

  });

</script>
</body>
</html>
