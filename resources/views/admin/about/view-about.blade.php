@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
<style type="text/css">
    table {
        width: 100%;
    }
    
    #example_filter {
        float: right;
    }
    
    #example_paginate {
        float: right;
    }
    
    label {
        display: inline-flex;
        margin-bottom: .5rem;
        margin-top: .5rem;
    }
</style>


 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Abuot Us
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage About</a></li>
        <li class="active">About List</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements --> 
          <div class="box box-warning">
            <div class="box-body" style="margin-top: 15px;">
            @if (session('message'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {{ session('message') }}
                </div>
            @endif
               
                  <div class="row">
                    <div class="col-sm-12">
                     <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                         <tr role="row">
                         

                            <th>ID</th>
                            
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php $i=1; ?>
                         @foreach ($abouts as $about)

                        <tr role="row" class="odd">
                          <td>{{ $i}}</td>
                          
     <td>{{$about->title}}</td>
     <td>{{$about->slug}}</td>
     <td>
                                                <div style="width:100px; height: 100px; overflow: auto">
                                                   {{$about->description}} 
                                                </div>
                                            </td>
    
                         
     <td>
                          <a class="btn btn-info" href="{{url('admin/edit-about/' .$about->id)}}">
                                 <i class="glyphicon glyphicon-edit icon-white"></i>
                                        Edit
                                  </a>

                                  <a  href="{{url('admin/delete-about/' .$about->id)}}">
                                 <button class="btn btn-danger">  Delete</button>
                                    </a>
                                  
                               
              
                 
               
                               <br>
                               <br>
                                 <!-- <a class="btn btn-danger" href="<?php //echo url('admin/delete-country/' .$country->id);?>" onClick="return confirm('Are you sure? you want to delete this entry!')">
                                <i class="glyphicon glyphicon-trash icon-white"></i>Delete</a> -->
                          </td>
       


                        </tr>
                        <?php $i++ ?>
                        @endforeach
                        </tbody>
                
                    </table>
                    </div>
                  </div>
                
            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection
   @section('js-page')
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
  });
</script>
  @append