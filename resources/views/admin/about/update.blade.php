@extends('layouts.adminLayout')
@section('css-page')
<style>
.checkbox-inline+.checkbox-inline, .radio-inline+.radio-inline {
     margin-top: 0;
    margin-left: 1px;
}
</style>
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
         About Us
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>About Us</a></li>
        <li class="active">Update About</li>
      </ol> 
    </section>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
            <form method="post" id="addAbout" action="{{url('admin/update-about/'.$about->id)}}"  style="margin-top: 2px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
              

               <div class="form-group col-md-12" style="padding-left: 0;">
                  <label>Add Title</label>
                  <input type="text" name="title" class="form-control" value="{{$about->title}}"  id="title" required="">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-12" style="padding-left: 0;">
                  <label>Slug</label>
                  <input type="text" name="slug" class="form-control"  value="{{$about->slug}}" id="slug" required="">
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-12" style="padding-left: 0;">
                        <label> Description</label>
                          <textarea  type="text" rows="5" name="description" class="form-control summernote" id="description"  required=""><?=$about->description?></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                          $('.summernote').summernote();
                        });
                    </script>
                    
                    <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                        <input type="radio" name="radio" value="1"  {{ ($about->is_enabled=="1")? "checked" : "" }} > is enabled
                    </div>
                    <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                        <input type="radio" name="radio" value="0"  {{ ($about->is_enabled=="0")? "checked" : "" }} > is disabled
                    </div>
                   
               
              
                    <div class="form-group" style="clear: both;">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <a href="{{route('admin.view-about')}}" class="btn btn-default">Back</a>
                    </div>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
