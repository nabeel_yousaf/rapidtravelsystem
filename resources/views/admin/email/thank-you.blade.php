@extends('layout.layout') @section('content')
 <div class="page-title-container">
                <div class="container">
                    <div class="page-title pull-left">
                        <h2 class="entry-title">Thank you</h2>
                    </div>
                    <ul class="breadcrumbs pull-right">
                        <li><a href="{{url('/')}}">HOME</a></li>

                        <li class="active">Thank you</li>
                    </ul>
                </div>
            </div>
            <section id="content">
                <div class="container">
                    <div id="main">
                        <div class="tab-container style1 travelo-policies">

                            <div class="tab-content">
                                <div id="terms-services" class="tab-pane fade in active">

                                    <div class="policy">
                                        <h2>Dear Customer,
                                            <br />
                                            <br />
                                            Thank you for contacting {{sitename()}}.</h2>
                                            <p>Our team will get back to you shortly</p>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
 @endsection