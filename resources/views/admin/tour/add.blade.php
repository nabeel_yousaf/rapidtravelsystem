@extends('layouts.adminLayout') @section('css-page')
<style>
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 20px;
    }
    
    .select2-container .select2-selection--single .select2-selection__rendered {
        padding-left: 0px;
    }
</style>
@append @section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) --> 
    <section class="content-header">
        <h1>
        Add Tour
        <small>Control panel</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Manage Tours</a></li>
            <li class="active">Add Tour</li>
        </ol>
    </section>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <form method="post" action="{{route('admin.storeTours')}}" enctype="multipart/form-data" id="addTour" style="margin-top: 15px;">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group col-md-6" style="padding-left: 0;">
                        <label>Tour_type_name</label>
                        <select name="Tour_type_name" class="selectpicker form-control" id="Tour_type_name" style="width: 100%" required="">

                            <option selected=></option>
                            @foreach($tourtypes as $tourtype)
                                 <option value="{{$tourtype->id}}">{{$tourtype->tourtypeName}}</option>
                            @endforeach
                        </select>
                    </div>
                
                    <input id="invisible_id" name="createdby" type="hidden" value="admin">
                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Tour Name</label>
                        <input type="text" name="tour_name" class="form-control" id="tour_name" required=" ">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_name')}}</div>
                    </div>

                    <div class="form-group col-md-12" style="padding-left: 0;">
                        <label>Tour Desc</label>
                          <textarea  type="text" rows="5" name="tour_Desc" class="form-control summernote" id="summernote"  required=""></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_Desc')}}</div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                          $('.summernote').summernote();
                        });
                    </script>
                    <div class="form-group col-md-12" style="clear: both;padding-left: 0;">
                        <label>Profile Thumbnail</label>
                        <input type="file" name="tour_image" class="form-control" required="">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_image')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="clear: both;padding-left: 0;">
                        <label>Upload Multiple Images</label>
                        <input type="file" name="multiple_images[]" id="multiple_images" class="form-control" accept="gif|jpg|png" multiple required="">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('multiple_images')}}</div>
                    </div>
                 
                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Compny Name</label>
                        <select class="form-control" readonly="" name="comp_name" required="">
                            @foreach($companies as $comp)
                            <option value="{{$comp->id}}">{{$comp->comp_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0;clear:both;">
                        <label>Tour Adult price</label>
                        <input type="number" name="tour_adult_price" class="form-control" id="tour_adult_price" id="adult">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_adult_price')}}</div>
                    </div>

                   

                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Tour Adult disc price</label>
                        <input type="number" name="Tour_Adult_disc_price" class="form-control" id="Tour_Adult_disc_price" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Adult_disc_price')}}</div>
                    </div>

                    <div class="form-group col-md-6" style="padding-left: 0;">
                        <label>Tour Child price</label>
                        <input type="number" name="Tour_Child_price" class="form-control" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Child_price')}}</div>
                    </div>

                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Tour Child disc price</label>
                        <input type="number" name="Tour_Child_disc_price" id="Tour_Child_disc_price" class="form-control" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Child_disc_price')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0;clear:both;">
                        <label>Tour_Duration</label>
                        <input type="text" name="Tour_Duration" class="form-control" id="Tour_Duration">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Duration')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Select Currency </label>
                        <select class="selectpicker form-control" id="currency" style="width: 100%" name="currency" >
                            <option value="PKR">PKR</option>
                            <option value="AED">AED</option>
                            <option value="USD">USD</option>
                            <option value="SAR">SAR</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0;clear:both;">
                        <label>Include Package </label>
                        <textarea type="text" name="includes" class="form-control summernote" id="includes"></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('includes')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Exclude Package </label>
                        <textarea type="text" name="excludes" class="form-control summernote" id="excludes"></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('excludes')}}</div>
                    </div>
                    <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                        <input type="radio" name="radio" value="1"> is enabled
                    </div>
                    <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                        <input type="radio" name="radio" value="0"> is disabled
                    </div>
                   
                    <div class="form-group" style="clear: both;">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <a href="{{route('admin.view-about')}}" class="btn btn-default">Back</a>
                    </div>
                </form>

            </div>

            <!-- /.box-body -->

        </div>

    </div>
</div>

@endsection

<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker();
    })
</script>
<script type="text/javascript">
    $(function() {
    $('#adult').hide(); 
    $('#Tour_type_name').change(function(){
        if($('#Tour_type_name').val() == 'service') {
            $('#adult').show(); 
        } else {
            $('#adult').hide(); 
        } 
    });
});

    function previewImages() {

      var $preview = $('#preview').empty();
      if (this.files) $.each(this.files, readAndPreview);

      function readAndPreview(i, file) {
        
        if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
          return alert(file.name +" is not an image");
        } // else...
        
        var reader = new FileReader();

        $(reader).on("load", function() {
          $preview.append($("<img/>", {src:this.result, height:100}));
        });

        reader.readAsDataURL(file);
        
      }

    }

    $('#multiple_images').on("change", previewImages);
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>