@extends('layouts.adminLayout') @section('css-page')
<style>
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 20px;
    }
    
    .select2-container .select2-selection--single .select2-selection__rendered {
        padding-left: 0px;
    }
</style>
@append @section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Edit Tour
        <small>Control panel</small>
      </h1>

        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Manage Tours</a></li>
            <li class="active">Edit Tour</li>
        </ol>
    </section>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-warning">
            <div class="box-body">
                <form method="post" action="{{url('admin/update-tour/'.$tours->id)}}" enctype="multipart/form-data" id="addTour" style="margin-top: 15px;">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group col-md-6" style="padding-left: 0;">

                        <input id="invisible_id" name="createdby" type="hidden" value="admin">

                        <label>Tour Name</label>
                        <input type="text" name="tour_name" class="form-control" id="tour_name" value="{{$tours->Tour_name}}" required="">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_name')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Tour_type_name</label>
                       <select class="form-control" name="Tour_type_name">
                            <option selected disabled="">Choose One</option>
                            @foreach($tourtypes as $tourtype)

                            <option value="{{$tourtype->id}}" {{$tours->Tour_type_name == $tourtype->id ? 'selected' : ''}}> {{$tourtype->tourtypeName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12" style="padding-left: 0;">
                        <label>Tour Desc</label>
                          <textarea  type="text" rows="200" cols="100" name="tour_Desc" class="form-control summernote" id="summernote"  required=""><?=$tours['Tour_Desc']?></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_Desc')}}</div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                          $('.summernote').summernote();
                        });
                    </script>
                    <div class="form-group col-md-12" style="clear: both;padding-left: 0;">
                        <label>Profile Thumbnail</label>
                        @if( !empty($tours->profile_image))
                        <div><img src="{{ url('/images/logo/'.$tours->profile_image)}}" height="155px" width="155px">
                            <a href="{{ url('admin/destroy-pic/'.$tours->id)}}">Delete it</a></div>
                        @endif
                        <br>
                        <input type="file" name="tour_image" class="form-control">
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_image')}}</div>
                    </div>



                    <div class="form-group col-md-6" style="padding-left: 0;clear:both;">
                        <label>Tour Adult price</label>
                        <input type="number" name="tour_adult_price" class="form-control" id="tour_adult_price" value="{{$tours->Tour_Adult_price}}" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('tour_adult_price')}}</div>
                    </div>

                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Tour Adult disc price</label>
                        <input type="number" name="Tour_Adult_disc_price" class="form-control" id="Tour_Adult_disc_price" value="{{$tours->Tour_Adult_disc_price}}" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Adult_disc_price')}}</div>
                    </div>

                    <div class="form-group col-md-6" style="padding-left: 0;">
                        <label>Tour Child price</label>
                        <input type="number" name="Tour_Child_price" class="form-control" value="{{$tours->Tour_Child_price}}" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Child_price')}}</div>
                    </div>

                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Tour Child disc price</label>
                        <input type="number" name="Tour_Child_disc_price" id="Tour_Child_disc_price" class="form-control" value="{{$tours->Tour_Child_disc_price}}" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Child_disc_price')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0;clear:both;">
                        <label>Tour_Duration</label>
                        <input type="text" name="Tour_Duration" class="form-control" id="Tour_Duration" value="{{$tours->Tour_Duration}}" >
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('Tour_Duration')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Select Currency </label>
                        <select class="selectpicker form-control" id="currency" style="width: 100%" name="currency" >
                            <option value="PKR" {{$tours->currency=="PKR" ? 'selected' : '' }}>PKR</option>
                            <option value="AED" {{$tours->currency=="AED" ? 'selected' : '' }}>AED</option>
                            <option value="USD" {{$tours->currency=="USD" ? 'selected' : '' }}>USD</option>
                            <option value="SAR" {{$tours->currency=="SAR" ? 'selected' : '' }}>SAR</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6" style="padding-left: 0;clear:both;">
                        <label>Include Package </label>
                        <textarea type="text" name="includes" class="form-control summernote" id="includes"><?=$tours['includes']?></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('includes')}}</div>
                    </div>
                    <div class="form-group col-md-6" style="padding-right: 0;">
                        <label>Exclude Package </label>
                        <textarea type="text" name="excludes" class="form-control summernote" id="excludes"><?=$tours['excludes']?></textarea>
                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('excludes')}}</div>
                    </div>
                    <div class=" form-group col-md-2" style="padding-left: 0;clear:both;">
                        <input type="radio" name="radio" value="1" {{ ($tours->is_enabled=="1")? "checked" : "" }} > is enabled
                    </div>
                    <div class=" form-group col-md-2" style="clear: both: padding-right 0;">
                        <input type="radio" name="radio" value="0" {{ ($tours->is_enabled=="0")? "checked" : "" }}> is disabled
                    </div>

                    <div class="form-group" style="clear: both;">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <a href="{{route('admin.listTours')}}" class="btn btn-default">Back</a>
                    </div>
                </form>

            </div>
                <div class="box-body">
                    <h3><strong>Update Tour Images</strong></h3>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select images to delete</div>
                            <div class="panel-body">
                                <form method="post" action="{{url('admin/delete-imgs/'.$tours->id)}}"  enctype="multipart/form-data" style="margin-top:15px">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <?php $i=1;  ?>
                                        @foreach($tourimgs as $tourimg)
                                            
                                            <label class="block-check" for="image_gal{{$i}}">
                                                <img class="thumbnail" src="{{ url('/images/logo/tourimgs/'.$tourimg->tour_images)}}" height="150px" width="150px" />
                                                <input type="checkbox" name="image_gal[{{$tourimg->id}}]" id="image_gal{{$i}}" value="{{$tourimg->id}}">
                                                <span class="checkmark"></span>
                                            </label>
                                            
                                        <?php $i++; ?>
                                        @endforeach
                                    <hr>
                                    <button class="btn btn-danger btn-large">Delete</button>             
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Select new images for tour</div>
                            <div class="panel-body">   
                                <form class="text-center" method="post" action="{{url('admin/Multiple-update-imgs/'.$tours->id)}}" enctype="multipart/form-data" id="edit_gal" style="margin-top:15px">
                                   <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <input type="file" name="multiple_images[]" id="multiple_images" class="form-control" id="file" accept="gif|jpg|png" multiple>
                                    <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{$errors->first('multiple_images')}}</div>
                                    <br>
                                    <button class="btn btn-default">Upload</button>                
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                    
                    
                     

           

        </div>

    </div>
</div>

@endsection

                    

                  