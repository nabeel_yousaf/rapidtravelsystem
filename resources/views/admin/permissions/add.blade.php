@extends('layouts.adminLayout')
@section('css-page')
@append
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Permission
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Manage Permissions</a></li>
        <li class="active">Add Permission</li>
      </ol>
    </section>
   <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-warning">
            <div class="box-body">
               <form method="post" action="{{route('admin.storePermission')}}"  id="addRole" style="margin-top: 15px;">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="form-group col-md-6" style="padding-left: 0;">
                  <label>Permission Title</label>
                  <input type="text" name="permission_title" class="form-control" id="permission_title" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}}</div>
                </div>
                <div class="form-group col-md-6" style="padding-right: 0;">
                  <label>Slug</label>
                  <input type="text" name="permission_slug" class="form-control" id="permission_slug" >
                  <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{last($errors->all())}}</div>
                </div>
                <div class="form-group">
                  <label>Permission Description</label>
                  <textarea class="form-control" rows="3" placeholder="Enter ..." name="permission_description" id="permission_description"></textarea>
                </div>
               <button type="submit" class="btn btn-default">Submit</button>
                   <a href="{{route('admin.listPermissions')}}" class="btn btn-default">Back</a>
            </form>

            </div>

            <!-- /.box-body -->

          </div>

        </div>
   </div>

   @endsection

    <!-- /.content-wrapper -->
  @section('js-page')
  <script type="text/javascript" src="{{asset('js/formValidation.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/framework/bootstrap.js')}}"></script>
 <script>
 $(document).ready(function() {
       $('#addPermissions')
                 .formValidation({
                     icon: {
                      //   valid: 'glyphicon glyphicon-ok',
                       //  invalid: 'glyphicon glyphicon-remove',
                       //  validating: 'glyphicon glyphicon-refresh'
                     },
                     fields: {
                         permission_title: {
                             validators: {
                                 notEmpty: {
                                     message: 'Permission title is required'
                                 }

                             }
                         },
                         permission_slug: {
                             validators: {
                                 notEmpty: {
                                     message: "Permission slug is required"
                                 }
                             }
                         }

                     }
                 })
   });
        $(document).ready(function() {
          $('#permission_title').keyup(function(){
          var str = $('#permission_title').val();
          str = str.replace(/^\s+|\s+$/g, ''); // trim
          str = str.toLowerCase();

          // remove accents, swap ñ for n, etc
          var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
          var to   = "aaaaeeeeiiiioooouuuunc------";
          for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
          }

          str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes
          $('#permission_slug').val(str);
        //  return true;
            });
         });
        </script>
 <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
 <script>
   $(function () {
     // Replace the <textarea id="editor1"> with a CKEditor
     // instance, using default configuration.
     CKEDITOR.replace('permission_description');

   });
 </script>
  @append