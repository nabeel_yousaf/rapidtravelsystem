<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
       <title>{{sitename()}}</title>
    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css')}}">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ url('css/animate.min.css')}}">

    <!-- Current Page Styles -->
    <link rel="stylesheet" type="text/css" href="{{ url('components/revolution_slider/css/settings.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ url('components/revolution_slider/css/style.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ url('components/jquery.bxslider/jquery.bxslider.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ url('components/flexslider/flexslider.css')}}" media="screen" />
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="{{ url('css/style.css')}}">
    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{ url('css/updates.css')}}">
    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{ url('css/custom.css')}}">
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="{{ url('css/responsive.css')}}">
    <!-- Car Styles -->
    <link rel="stylesheet" href="{{ url('css/car.css')}}">
 <!-- Social Icons -->
   

    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->


    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="#" />

    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6175655-17', 'auto');
  ga('send', 'pageview');

</script>
    
    