<header id="header" class="navbar-static-top">

    <div class="topnav hidden-xs">
        
        <div class="container">           
            <ul class="quick-menu pull-right">
                <li class="{{-- ribbon currency --}}">
                   {{-- <a href="{{url('/')}}" title="{{sitename()}} UAE">
                        <img id="ctl00_Template_Header_Image1" src="{{ url('uploads/flag/White/uae.jpg')}}" style="border-width:0px;" />&nbsp; MY  Next  Trip UAE</a>
                    <ul class="menu mini">
                        <li>
                            <a href="{{url('http://www.havefuntour.com/')}}" target="_blank" title="Have Fun Tour Pakistan">
                            <img id="ctl00_Template_Header_Image2" src="{{ url('uploads/flag/Black/pak.png')}}" style="border-width:0px;" /> &nbsp; Have Fun Tour Pakistan</a>
                        </li>                        
                    </ul> --}}
                    <a href="{{phone()}}">{{phone()}}</a>

                </li>
                <li>
                    <a href="tel:{{mobile()}}">{{mobile()}}</a>
                </li>
                <li>
                    <a href="mailto:{{siteEmail()}}">{{siteEmail()}}</a>
                </li>
            </ul>
            <ul class="quick-menu pull-left">
                <li>
                    <a title="" href="#" data-toggle="tooltip" target="_blank" data-original-title="twitter"><i class="soap-icon-twitter"></i></a>
                </li>
                <li>
                    <a title="" href="#" data-toggle="tooltip" target="_blank" data-original-title="facebook"><i class="soap-icon-facebook"></i></a>
                </li>
                <li>
                    <a title="" href="#" data-toggle="tooltip" target="_blank" data-original-title="instagram"><i class="soap-icon-instagram"></i></a>
                </li>
                <li>
                    <a title="" href="#" data-toggle="tooltip" target="_blank" data-original-title="youtube"><i class="soap-icon-youtube"></i></a>
                </li>
            </ul>
        </div>

    </div>

    <div class="main-header" style="background-color: #fff0 !important;">

        <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">Mobile Menu Toggle
        </a>

        <div class="container">
            <nav id="main-menu" role="navigation">
                <ul class="menu">
                    <li class="pull-right" style="width: 103px; height: 103px;">
                        <h1 class="logo navbar-brand">
                <a href="{{url('/')}}" title="{{sitename()}}">
                    <img src="{{ url('/images/logo/' .$companyData[0]->Logo_URL)}}" alt="{{sitename()}}" />
                </a>
            </h1>
                    </li>
                    <li class="menu-item-has-children pull-left">
                        <a href="{{url('/aboutus/')}}">{{$components[0]->about}}</a>
                        <ul>
                            <li><a href="{{url('/aboutus/')}}">Company Profile</a></li>
                            <li><a href="{{url('/contactus/')}}">Contact Us</a></li>
   
                        </ul>
                    </li>
                    <li id="ctl00_Template_Header_li_TravelGuide" class="menu-item-has-children pull-left"><a href="{{url('/attractions/')}}">{{$components[0]->guide}}</a>

                       <!--  <ul>
                            <li><a href="{{url('/abudabhi/')}}">Abu Dhabi</a></li>
                            <li><a href="{{url('/ajman/')}}">Ajman</a></li>
                            <li><a href="{{url('/dubai/')}}">Dubai</a></li>
                            <li><a href="{{url('/fujairah/')}}">Fujairah</a></li>
                            <li><a href="{{url('/khaimah/')}}">Ras Al Khaimah</a></li>
                            <li><a href="{{url('/sharjah/')}}">Sharjah</a></li>
                        </ul> -->
                    </li>
                    <li id="ctl00_Template_Header_li_tours" class="menu-item pull-left"><a id="ctl00_Template_Header_HyperLink1" href="{{url('/service/')}}">{{$components[0]->service}}</a>     
                     </li>
                     <li id="ctl00_Template_Header_li_tours" class="menu-item pull-left"><a id="ctl00_Template_Header_HyperLink1" href="{{url('/safari/')}}">{{$components[0]->safari}}</a></li>
                    <li id="ctl00_Template_Header_li_home" class="menu-item pull-left"><a href="{{url('/')}}">{{$components[0]->home}}</a> </li>
                   
                    
                    

                     
                   
                      
                    
                </ul>
            </nav>
        </div>

        <nav id="mobile-menu-01" class="mobile-menu collapse">
            <ul id="mobile-primary-menu" class="menu">
                <li class="menu-item"> <a href="{{url('/')}}">{{$components[0]->home}}</a></li>
                <li class="menu-item"> <a href="{{url('/safari/')}}">{{$components[0]->safari}}</a></li>
                <li class="menu-item"> <a href="{{url('/car/')}}">Car hiring</a></li>
                <li class="menu-item"> <a href="{{url('/groups/')}}">Groups</a></li>
                <li class="menu-item"> <a href="{{url('/attractions/')}}">{{$components[0]->guide}}</a></li>
                <li class="menu-item"> <a href="{{url('/aboutus/')}}">{{$components[0]->about}}</a></li>
                <li class="menu-item"> <a href="{{url('/contactus/')}}">Contact Us</a></li>
                
                 
               
            </ul>

            

        </nav>
    </div>
   
</header>