@extends('layout.layout') @section('content')

<div id="slideshow">
    <div class="fullwidthbanner-container">
        <div class="revolution-slider" style="height: 0; overflow: hidden;">
            <ul>
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner1.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Makkah : May God Bless Us to Visit the Holy Makkah
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="{{url('/safari/')}}">BOOK NOW
                        </a>

                </li>
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner2.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Madinah : May God Bless Us to Visit the Holy Madinah                        
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="{{url('/service/')}}">BOOK NOW
                        </a>

                </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner3.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Madinah : May God Bless Us to Visit the Holy Madinah
                        <!-- <br />To Get On Board -->
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner4.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Rent A Car Was Never Easy before.
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner5.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        The world's tallest observation wheel, Ain Dubai
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner6.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        The Sheikh Zayed Grand Mosque stands out as one of the world's largest Mosque.
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner7.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Ferrari World!
                        <br />Where Speed Has No Limits
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner8.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Warner Bros!
                        <br />There's still time To Get On Board
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner9.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Sharjah Creek
                        <br />Enjoy the Time!
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="100">
                    <!-- MAIN IMAGE -->
                    <img src="{{url('uploads/sliders/Banner11.jpg')}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption large_bold_white_med_2 lfr stl tp-resizeme" data-x="0" data-y="550" data-speed="1500" data-start="1800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                        Fujairah
                        <br />Luxury beyond Limits
                    </div>

                    <!-- LAYER NR. 4 -->
                    <a class="tp-caption largewhitebg_button1 sfl stl tp-resizeme" data-x="55" data-y="445" data-speed="1500" data-start="2200" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;" href="#" target=" _blank ">
                        </a>
                    </li>

                </ul>

            </div>
        </div>
    </div>
    <br>
    <section id="content ">

        <div class="container section no-padding ">
            <h2>Explore Our Best Deals</h2>
            <?php $row=0;?>
            <div class="row image-box style10 ">
                  @foreach($tr as $t)
                    @if($t->is_enabled !==0)
                 <div class='col-sms-6 col-sm-6 col-md-3'>
                        <article class='box'>
                            <figure class='animated' data-animation-type='fadeInDown' data-animation-duration='2'>
                        <a class='hover-effect' title='{{$t->Tour_name}}' href="{{url( '/holiday/' .$t->id)}}">

                    <img width='270' height='160'  src="{{ url('/images/logo/'.$t->profile_image)}}"></a>
                    </figure>
                    <div class='details'>
                        <a href="{{url( '/holiday/' .$t->id)}}" class='button btn-mini'>BOOK NOW</a>
                        <h4 class='box-title'>{{$t->Tour_name}}<!-- <small>{{$t->Tour_Duration}}</small> --></h4>
                    </div>
                    </article>
                 </div>
                 @endif

                <?php $row++;
                if ($row==4)
                    echo '</div><div class="row image-box style10 ">';
                 ?>
                 @endforeach
            </div>
        </div>

 <div class="global-map-area1 section parallax" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="description text-center">
                    <h1>Discover Our Amazing Attractions</h1>                    
                </div>
                <div class="image-carousel style3 flex-slider" data-item-width="170" data-item-margin="30">
                    <ul class="slides image-box style9">
                       @foreach($attractions as $attraction)
                    @if($attraction->is_enabled !==0) 
                        <li>

                  
                                <article class='box'>
                                    <figure>
                                        <a href="{{url('/view-attraction/' .$attraction->id)}}" title='{{$attraction->name}}' class='hover-effect'><img src="{{ url('/images/attractions/'.$attraction->logo)}}" alt='' width='170' height='161' /></a>
                                    </figure>
                                    <div class='details'>
                                        <h4 class='box-title'>{{$attraction->name}}</h4>
                                        <a href="{{url('/view-attraction/' .$attraction->id)}}" title='{{$attraction->name}}' class='button'>Explore</a>
                                    </div>
                                </article>
                              
                            </li>
                      @endif            
                   @endforeach   
                    </ul>
                </div>
            </div>
        </div>


<div class="section container">
    <div class="row image-box style4">
        <div class="col-sm-3">
            <article class="box animated" data-animation-type="fadeInLeft" data-animation-delay="0">
                <figure>
                    <a title="" href="{{url('/car/')}}" class="hover-effect">
                        <img width="370" height="172" alt="" src="{{url('uploads/carhiring.jpg')}}"></a>
                </figure>
                <div class="details">
                    <h4 class="box-title">Car Hiring</h4>
                    <a class="goto-detail" href="{{url('/car/')}}"><span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </article>
        </div>
        <div class="col-sm-3">
            <article class="box animated" data-animation-type="fadeInLeft" data-animation-delay="0.3">
                <figure>
                    <a title="" href="{{url('/safari/')}}" class="hover-effect">
                        <img width="370" height="172" alt="" src="{{url('uploads/dicoverBG.jpg')}}"></a>
                </figure>
                <div class="details">
                    <h4 class="box-title">{{$components[0]->safari}}</h4>
                    <a class="goto-detail" href="{{url('/safari/')}}"><span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </article>
        </div>
        <div class="col-sm-3">
            <article class="box animated" data-animation-type="fadeInLeft" data-animation-delay="0.3">
                <figure>
                    <a title="" href="{{url('/groups/')}}" class="hover-effect">
                        <img width="370" height="172" alt="" src="{{url('uploads/group.jpg')}}"></a>
                </figure>
                <div class="details">
                    <h4 class="box-title">Groups</h4>
                    <a class="goto-detail" href="{{url('/groups/')}}"><span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </article>
        </div>
        <div class="col-sm-3">
            <article class="box animated" data-animation-type="fadeInLeft" data-animation-delay="0.3">
                <figure>
                    <a title="" href="{{url('/attractions/')}}" class="hover-effect">
                        <img width="370" height="172" alt="" src="{{url('uploads/tours.jpg')}}"></a>
                </figure>
                <div class="details">
                    <h4 class="box-title">{{$components[0]->guide}}</h4>
                    <a class="goto-detail" href="{{url('/attractions/')}}"><span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </article>
        </div>
        

       
    </div>
</div>
<div class="global-map-area section parallax" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="text-center description">
            <h1>We Provide You An Ultimate Travel Experience</h1>

        </div>
        <br />
        <div class="row image-box style8">
            <div class="col-md-4">
                <article class="box animated" data-animation-type="fadeInUp">
                    <figure>
                        <span class="image-wrapper middle-block">

                                          <img src="{{url('uploads/HomecarHiring.jpg')}}" alt="" class="middle-item" width="100" height="172" />
                                        <span class="opacity-wrapper"></span>
                        </span>
                    </figure>
                    <div class="details">
                        <h2 class="box-title">Car Hiring</h2>
                        <p>
                                {{sitename()}} successfully introduces the charm and culture of the region to visitors from around the globe every year with state of the art luxury coaches and 4-wheel drive vehicles...

                            <br />
                            <br /><a href="{{url('/car/')}}" title="" class="button">MORE</a>

                        </p>
                    </div>
                </article>
            </div>
            <div class="col-md-4">
                <article class="box animated" data-animation-type="fadeInUp">
                    <figure class="middle-block">
                        <img src="{{url('uploads/HomeTravelInsurance.jpg')}}" alt="" class="middle-item" width="100" height="172" />
                        <span class="opacity-wrapper"></span>
                    </figure>
                    <div class="details">
                        <h2 class="box-title">Travel Group</h2>
                        <p>
                            It's the final countdown to your next big adventure. You’ve got your passport and tickets, every pair of socks you own, and enough mosquito repellent to cover an elephant...
                            <br />
                            <br /><a href="{{url('/groups/')}}" title="" class="button">MORE</a>
                        </p>
                    </div>
                </article>
            </div>
            <div class="col-md-4">
                <article class="box animated" data-animation-type="fadeInUp">
                    <figure class="middle-block">
                        <img src="{{url('uploads/group.jpg')}}" alt="" class="middle-item" width="100" height="172" />
                        <span class="opacity-wrapper"></span>
                    </figure>
                    <div class="details">
                        <h2 class="box-title">Travel Guidelines</h2>
                        <p>
                                {{sitename()}} Holidays, the outgoing arm of the Group, builds on the group’s extensive years of experience and far reaching network of partners that enables it to penetrate virtually...
                            <br />
                            <br /><a href="{{url('/attractions/')}}" title="" class="button">MORE</a>
                        </p>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>

</section>

@endsection