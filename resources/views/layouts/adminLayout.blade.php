<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="_token" content="{{ csrf_token() }}">
  @section('title')
  			<title>MSJ Communication | Admin Panel</title>
  @show
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  @include('admin.common.css')
  {{--PAGE CSS--}}

  @section('css-page')
  @show

  <!--[if lt IE 9]>
  <script src="{{asset('js/html5shiv.min.js')}}"></script>
  <script src="{{asset('js/respond.min.js')}}"></script>
  <![endif]-->

  <!-- include summernote css/js -->
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
</head>
<body class="hold-transition skin-blue sidebar-mini">
 <div class="wrapper">

        {{--HEADER--}}
        @section('header')
            @include('admin.common.header')
        @show
        @section('mainSidebar')
          @include('admin.common.mainSidebar')
        @show

        {{--PAGE CONTENT--}}
         @yield('content')

        {{--FOOTER--}}
        @section('footer') 
            @include('admin.common.footer')
        @show
        @section('controlSidebar')
              @include('admin.common.controlSidebar')
        @show
        {{--MASTER JS--}}
        @include('admin.common.js')
        {{--PAGE SCRIPTS--}}
        @section('js-page')
        @show
    </div>
    <!-- <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
   <script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
  };
</script> -->
@yield('js');
    </body>
</html>