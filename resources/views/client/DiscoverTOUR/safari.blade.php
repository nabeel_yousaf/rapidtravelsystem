@extends('layout.layout') @section('content')

<div class="page-title-container">
        <div class="container">
            {{-- <div class="page-title pull-left">
                <h2 class="entry-title">{{$components[0]->safari}}</h2>
            </div> --}}
            <ul class="breadcrumbs pull-right">
                <li><a href="{{url('/')}}">HOME</a></li>
                
                <li class="active">{{$components[0]->safari}}</li>
            </ul>
        </div>
    </div>
     <section id="content">
        <div class="container">
            <div id="main">
                @foreach($groups as $group => $groups)
                <div>
                    <h2>{{App\TourTypeName::find($group)->tourtypeName}}</h2>
                </div>
                <div class="tour-packages row add-clearfix image-box">
                    @foreach($groups as $group)
                    <div class='col-sm-6 col-md-4'>
                        <article class='box'>
                            <figure style="border-radius: 6%;"> 
                                <a href="{{url('/holiday/' .$group->id)}}">
                                    <img src="{{ url('/images/logo/'.$group->profile_image)}}" alt='{{$components[0]->safari}}'></a>
                                <figcaption>
                                    @if($group->Tour_Adult_price == 0)
                                    <span class='price'>Call for price</span>                                    
                                    @else
                                    <span class='price'>Starting from {{$group->Tour_Adult_price}} {{$group->currency}}</span>
                                    @endif
                                    <h4 class='caption-title'>{{$group->Tour_name}}</h4>
                                </figcaption>
                            </figure>
                        </article>
                    </div> 
                    @endforeach             
                </div>
                @endforeach
                {{-- {!! $tours->render() !!} --}}
            </div>
             
        </div>

    </section>

@endsection