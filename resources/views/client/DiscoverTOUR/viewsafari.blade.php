@extends('layout.layout') @section('content')


            <div class="page-title-container">
                <div class="container">
                    {{-- <div class="page-title pull-left">
                        <h2 class="entry-title">
                {{$tour->Tour_name}}</h2>
                    </div> --}}
                    <ul class="breadcrumbs pull-right">
                        <li><a href="{{url('/')}}">HOME</a></li>
                        <li><a href="{{url('/safari')}}">{{$components[0]->safari}}</a></li>
                        <li class="active">
                           {{$tour->Tour_name}}</li>
                    </ul>
                </div>
            </div>
            <section id="content">
                <div class="container">
                    <div class="row">
                        <div id="main" class="col-sm-8 col-md-9">
                            <div class="post">

                                <div class="flexslider photo-gallery style3">
                                    <ul class="slides">
                                        @foreach ($tour->tourimg as $ti)
                                        <li>
                                            <img src="{{ url('/images/logo/tourimgs/'.$ti->tour_images)}}" alt='{{$components[0]->safari}}'>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="details">
                                    <h1 class="entry-title skin-color">
                            {{$tour->Tour_name}}</h1>
                                    <div class="post-content">
                                        <table class="table table-bordered  table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center">Duration</th>
                                                    
                                                    <th style="text-align: center">Adult Price</th>
                                                    <th style="text-align: center">Child Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center">
                                                       {{$tour->Tour_Duration}}</td>
                                                    
                                                    <td style="text-align: center">
                                                        {{$tour->currency}}  {{$tour->Tour_Adult_price}}</td>
                                                    <td style="text-align: center">
                                                        {{$tour->currency}}  {{$tour->Tour_Child_price}}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                      
                                        <p class='text-justify'>{{$tour->Tour_Desc}}</p>
                                    </div>

                                </div>

                                <div class="tour-google-map block"></div>

                                <div class="post-comment block">
                                    <h2 class="reply-title">Request Tour</h2>
                                    
                                <form name="form2" method="post" action="{{url('/request/')}}" id="">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                    <div class="travelo-box">
                                        <div class="comment-form">
                                       
                                            <div class="form-group row">
                                                <div class="col-xs-6">
                                                    <label>Your Name <span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator1" style="color:Red;display:none;">Required</span></label>

                                                    <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Name" type="text" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Name" class="input-text full-width" required="" />
                                                     <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label>Your Email <span id="ctl00_ContentPlaceHolder1_Tour_Details_RegularExpressionValidator1" style="color:Red;display:none;">Invalid email address</span><span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator2" style="color:Red;display:none;">{{array_first($errors->all())}}</span></label>
                                                    <input name="asd"  type="email" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Email" class="input-text full-width" required="" value="" />
                                                    <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-xs-6">
                                                    <label>Your Phone <span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator3" style="color:Red;display:none;">Required</span></label>
                                                    <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Phone" type="tel" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Phone" class="input-text full-width" required="" />
                                                    <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <label>Number of Persons <span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator4" style="color:Red;display:none;">Required</span></label>
                                                    <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Persons" type="number" min="0" value="1" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Persons" class="input-text full-width" required="" />
                                                </div>
                                                <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Special Request</label>
                                                <textarea name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Request" rows="5" cols="20" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Request" class="input-text full-width" required="" placeholder="Request...."></textarea>
                                                <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                            </div>

                                             <div id="ReCaptchContainer"></div>
                                          <br/>
                                       <input type="submit" name="ctl00$ContentPlaceHolder1$btn_Send" value="SEND REQUEST" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btn_Send&quot;, &quot;&quot;, true, &quot;SendTours&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btn_Send" class="buttonCustome btn-large" />
                                        
                                        </div>
                                    </div>
                                </form>
                                    
                                </div>
                                

                            </div>
                        </div>
                        <div class="sidebar col-sm-4 col-md-3">

                          <div class="travelo-box search-stories-box">
                            <h4>Search Stories</h4>
                              <form name="form2" method="get" action="{{url('/safari/')}}" id="">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div>
                                 <div class="with-icon full-width">
                                    <input type="text" class="input-text full-width" placeholder="story name or category">
                                    <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
                                 </div>
                                </div>
                               </form>
                          </div>


                            <div class="travelo-box book-with-us-box">
                                <h4>Why Book with us?</h4>
                                <ul>
                                    <li>
                                        <i class="soap-icon-hotel-1 circle"></i>
                                        <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                                        <p>We are dealing with more than 135,000 around the world.</p>
                                    </li>
                                    <li>
                                        <i class="soap-icon-savings circle"></i>
                                        <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                                        <p>Guaranteed low rates with attractive payment methods.</p>
                                    </li>
                                    <li>
                                        <i class="soap-icon-support circle"></i>
                                        <h5 class="title"><a href="#">Excellent Support</a></h5>
                                        <p>24/7 emergency and customer support</p>
                                    </li>
                                </ul>
                            </div>
                             @foreach($details as $detail)
                            <div class="travelo-box contact-box">
                                <h4 class="box-title">Need {{sitename()}} Help?</h4>
                                <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                                <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>{{$detail->Comp_Phone}}</span>
        <br />
        <a href="#" class="contact-email">{{$detail->Comp_Email}}</a>
    </address>
                            </div>
                              @endforeach
                        </div>
                    </div>
                </div>

                <script src="https://www.google.com/recaptcha/api.js?onload=renderRecaptcha&render=explicit" async defer></script>
                <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

                <script type="text/javascript">
                    var site_key = '6LeSSroUAAAAAMQ7JIf9tzNRcFYYsa7_9AamV3ub';
                    var renderRecaptcha = function() {
                        grecaptcha.render('ReCaptchContainer', {
                            'sitekey': site_key,
                            'callback': reCaptchaCallback,
                            theme: 'light', //light or dark    
                            type: 'image', // image or audio    
                            size: 'normal' //normal or compact    
                        });
                    };

                    var reCaptchaCallback = function(response) {
                        if (response !== '') {
                            jQuery('#lblMessage').css('color', 'green').html('Success');
                        }
                    };

                    jQuery('button[type="button"]').click(function(e) {
                        var message = 'Please check the checkbox';
                        if (typeof(grecaptcha) != 'undefined') {
                            var response = grecaptcha.getResponse();
                            (response.length === 0) ? (message = 'Captcha verification failed') : (message = 'Success!');
                        }
                        //jQuery('#lblMessage').html(message);
                        //jQuery('#lblMessage').css('color', (message.toLowerCase() == 'success!') ? "green" : "red");
                    });
                </script>

            </section>

@endsection