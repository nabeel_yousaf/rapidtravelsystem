@extends('layout.layout') @section('content')
<div class="page-title-container">
            <div class="container">
                {{-- <div class="page-title pull-left">
                    <h2 class="entry-title">{{$components[0]->service}}</h2>
                </div> --}}
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{url('/')}}">HOME</a></li>
                  
                    <li class="active">{{$components[0]->service}}</li>
                </ul>
            </div>
        </div>

        <section id="content">
        <div class="container">
            <div id="main">
                

                <div class="tour-packages row add-clearfix image-box">
                    @foreach($ser as $se)
                   
                    @if($se->is_enabled !==0)

                   
                    <div class='col-sm-6 col-md-4'>
                        <article class='box'>
                            <figure style="border-radius: 6%;" >
                                <a href="{{url('/holiday/' .$se->id)}}">
                                    <img src="{{ url('/images/logo/'.$se->profile_image)}}" alt='Dubai City Tour'></a>
                                <figcaption>
                                   
                                    <h2 class='caption-title'>{{$se->Tour_name}}</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                    @endif
                @endforeach

                
                </div>
               {!! $ser->render() !!}

            </div>
        </div>
    </section>

@endsection