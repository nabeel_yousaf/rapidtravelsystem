@extends('layout.layout') @section('content')

<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">About Us</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            
            <li class="active">About Us</li>
        </ul>
    </div>
</div> 

        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="large-block image-box style6">
                    @foreach($abouts as $about)
                    @if($about->is_enabled !==0)
                        <article class="box">
                        <figure class="col-md-5 pull-right middle-block">
                            <a href="#" title=""><img class="middle-item" src="uploads/about/23.jpg" alt="" width="476" height="318" /></a>
                        </figure>
                        
                        

                        <div class="details col-md-offset-5">
                      
                            <h3 style="font-weight: bold; font-size: 25px;" class="box-title">{{$about->title}}</h3>
                            <ul><li>
                            <div class='text-align: justify;' style="font-size: 14px;"><?=$about->description;?></div>

                            </li></ul>
                                         
                        </div> 
                        
   
                        </article>
                        @endif
                        @endforeach  
                      
                      
                                                           
                     
                     
                    </div>

                   




                </div>
            </div>
        </section>
        <script src="../code.jquery.com/jquery-3.2.1.min.js"></script>

@endsection