@extends('layout.layout') @section('content')

<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">About Us</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            
            <li class="active">About Us</li>
        </ul>
    </div>
</div>

        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="large-block image-box style6">
                        <article class="box">
                        <figure class="col-md-5 pull-right middle-block">
                            <a href="#" title=""><img class="middle-item" src="uploads/about/23.jpg" alt="" width="476" height="318" /></a>
                        </figure>
                        
                        @foreach($abouts as $about)
                        <div class="details col-md-offset-5">
                      
                            <h3 class="box-title">{{$about->title}}</h3>
                            <p class='text-justify'> {{$about->description}} 
                          
                            </p>                        
                        </div>
                        @endforeach  
   
                        </article>

                        <article class="box">
                            <figure class="col-md-5 pull-right middle-block">
                                <a href="#" title=""><img class="middle-item" src="uploads/about/3.jpg" alt="" width="476" height="318" /></a>
                            </figure>
                            <div class="details col-md-7">
                                <h3 class="box-title">Vision?</h3>
                                <p class='text-justify'>To offer exceptional and diversified services provided by our talented manpower through our advanced systems and global partnerships, while ensuring the highest standards at all levels and at the most competitive rates. We aim to provide the best and most memorable client experience, which in turn empowers us to reach and conquer the global market. Client Happiness: Focus, respect, care, and a genuine understanding of their needs: these are the keys to client satisfaction and happiness.</p>
                            </div>
                        </article>
                        <article class="box">
                        <figure class="col-md-5">
                                <a href="#" title="" class="middle-block"><img class="middle-item" src="uploads/about/33.jpg" alt="" width="476" height="318" /></a>
                            </figure>                                
                        <div class="details col-md-offset-5">
                                <h3 class="box-title">Our Services?</h3>
                                <p class='text-justify'>With {{ sitename() }} you will always find the perfect accommodation option. A huge number of hotels, apartments and guest houses around UAE are guaranteed to provide places for our tourists at any time.
                                Comfort is the key to enjoyable travel, and {{ sitename() }} fleet of transport vehicles’ provide it. From coaches and mini-buses to VIP and 4-wheel drive vehicles, the fleet guarantees safe, attractive ground transport for any size group.
                                {{ sitename() }} is composed of a team of a very well qualified, multilingual staff that will always provide our clients with the lowest rates in the market and organize hotel accommodations and car-hires with extremely competitive discounts. In addition to their training, our staff is a group of experienced travellers that will be able to offer the most accurate advice with hindsight of their personal experience.
                                {{ sitename() }} seeks to make the visitor feel at ease and welcome in what is for many, a brief glimpse of a world very different from their own.
                                </p>
                            </div> 
                        </article>
                        <article class="box">
                            <figure class="col-md-5">
                                <a href="#" title="" class="middle-block"><img class="middle-item" src="uploads/about/1.jpg" alt="" width="476" height="318" /></a>
                            </figure>                            
                            <div class="details col-md-offset-5">
                                <h3 class="box-title">24/7 Support?</h3>
                                <p class='text-justify'>Specialists of {{ sitename() }} own offices in Dubai will help with solving any travel issues - from meeting at the airport and conducting excursions to assistance in the event of an insured event, and call centre staff will provide the most prompt support for tourists.</p>
                            </div>
                        </article>
                    </div>

                    <div class="row large-block">
                        <div class="col-md-6">
                            <h2>Know More About Us</h2>
                            <div class="toggle-container box" id="accordion1">
                                <div class="panel style1">
                                    <h4 class="panel-title">
                                        <a href="#acc1" data-toggle="collapse" data-parent="#accordion1">Reservations</a>
                                    </h4>
                                    <div class="panel-collapse collapse in" id="acc1">
                                        <div class="panel-content">
                                            <p class='text-justify'>Process all reservations from Tour operators within the stipulated time-frame. Arrange for visas and other ground serlllvices booked by the tour operator Liaise with the Hotel representatives and assist in-resort clients to the best of their ability and hence ensure that the clients have a satisfactory holiday</p>
                                        </div><!-- end content -->
                                    </div>
                                </div>
                                
                                <div class="panel style1">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#acc2" data-toggle="collapse" data-parent="#accordion1">Limousine /Private car /SIC transfer Reservation</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="acc2">
                                        <div class="panel-content">
                                            <p class='text-justify'>{{ sitename() }} immaculately maintained fleet makes a statement of class, comfort and safety. Our impeccable team of chauffeurs are trained to offer personalized service. We are a well-orchestrated team, providing you with a hassle-free, value-added experience. {{ sitename() }} has its own fleet stations in the following Emirates covering the entire UAE</p>
                                        </div><!-- end content -->
                                    </div>
                                </div>
                                
                                <div class="panel style1">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#acc3" data-toggle="collapse" data-parent="#accordion1">24 Hours Service</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="acc3">
                                        <div class="panel-content">
                                            <p class='text-justify'>{{ sitename() }} is assuring for the clients high profile of service ,among which is providing the assistance and being reachable through the below means: Regular daily presence in all the major hotels. Arrival welcome pack: Each clients is receiving an information welcome pack includes contact numbers(24 – hours multilingual assistance) an invitation for the information meeting , country map, excursion brochure, departure details and pick up time.</p>
                                        </div><!-- end content -->
                                    </div>
                                </div>


                                <div class="panel style1">
                                    <h4 class="panel-title">
                                        <a class="collapsed" href="#acc4" data-toggle="collapse" data-parent="#accordion1">Group Travel</a>
                                    </h4>
                                    <div class="panel-collapse collapse" id="acc4">
                                        <div class="panel-content">
                                            <p class='text-justify'>In {{ sitename() }} we are dedicated exclusively to the provision of outstanding destination management services. Our knowledgeable, multinational team has long been providing specialised services to the groups of many of leading incentive houses and groups and even corporate companies based in the UAE. As the region's market leader, we take great pride in our creative approach and scrupulous attention to detail - and, with our extensive experience there are few situations we have not encountered and little we cannot organize. Our specialised Groups and Incentives Department comprises a team of professionals who are able to create and manage innovative programmes and events and deliver arrangements that cost-effectively achieve your objectives.</p>
                                        </div><!-- end content -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h2>Our Core Values</h2>
                            <div class="tab-container">
                                <ul class="tabs">
                                    <li><a href="#satisfied-customers" data-toggle="tab">Transfers</a></li>
                                    <li class="active"><a href="#tours-suggestions" data-toggle="tab">VIP Services</a></li>
                                    <li><a href="#careers" data-toggle="tab">Tours</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="satisfied-customers">
                                        <!-- <img src="uploads/about/5.jpg" alt="" style="float: left;" /> -->
                                        <h4>Transfers</h4>
                                        <p class='text-justify'>{{ sitename() }} also has a sound infrastructure of our own fleet of vehicles -50 seater luxury buses, coaches and mini vans, luxury cars and four wheel drive vehicles. Our field and office staff speaks several languages to suite the customer. {{sitename()}} has in all the airports own dedicated VIP parking areas just across the terminal exit, where the passengers arriving and departing will save efforts and time walking with their luggage to far parking areas. Cars amenities can be offered to the clienteles with their first steps into the vehicle; newspapers, water and refreshing towels. All of the above services can be booked with very reasonable rates through our online system, marketing material and very informative fact sheet will be available on request.</p>
                                    </div>
                                    <div class="tab-pane fade in active" id="tours-suggestions">
                                        <img src="uploads/about/4.jpg" alt="" style="float: left;" />
                                        <h4>VIP Services</h4>
                                        <p class='text-justify'>VIP Service is an extension of a first-class flight or five star hotel experience– except that it is on board a luxury limousine. Your experience of {{ sitename() }} VIP arrangements will begin the moment you are escorted by our multilingual airport representatives or by other VIP escort services at the airport, multilingual chauffeur to the sophisticated, gleaming black limousine of your choice. A black two-piece business suit, a black cap, a warm smile and a polite greeting are your first impressions of the Prime Limousine chauffeur. Sinking into the plush, genuine leather seating and watching a DVD movie or play a CD. Enjoy an Emarati date, nibbling a chocolate, sipping a cold drink. There are newspapers and magazines you to browse and a telephone with cordless receiver. From a refreshing towel to first aid services, our chauffeurs will cater to the extraordinary requirements of extraordinary guests.</p>
                                    </div>
                                    <div class="tab-pane fade" id="careers">
                                        <h4>Tours</h4>
                                        <p class='text-justify'>{{ sitename() }} is offering wide collections of different VIP tours and safaris which mostly based on private basis. Private Tour means that the tour is designed specifically as per the clientele requirements. It means you are not stuck in a tour group with strangers. Some DMCs only offers private tours.
                                        The benefits of a private tour are multifold. With each private tour, a private tour guide and private vehicle with driver. They are there just for the clientele’s wishes and will cater to him every need. The private tour guide will be informed ahead of time of the needs and preferences. He or she will be waiting for arrival at the airport. The vehicles we offer are chosen carefully. They are air conditioned, clean and expertly maintained. All private drivers are licensed, insured, and extremely experienced. Helicopter tours and transfers ,Stretch limousine – tours, private camping with white-glove service , are just some of the VIP tailored itineraries which being offered by our tours and safaris department as per the VIP’s needs.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </section>


@endsection