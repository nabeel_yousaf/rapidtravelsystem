@extends('layout.layout') @section('content')
<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">{{$attractions->name}}</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li><a href="{{url('/attractions')}}">Attractions</a></li>
            <li class="active">
                {{$attractions->name}}</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <div class="flexslider photo-gallery style3">
                                    <ul class="slides">
                                        @foreach ($attractions->imgs as $img)
                                       
                                        <li>
                                            <img src="{{ url('/images/logo/attractions/'.$img->attraction_imgs)}}" alt=''>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                    <div class="details">
                        <h1 class="entry-title">
                           {{$attractions->name}}</h1>
                        <div class="post-content">
                            <div class="row" style="padding: 12px;"><?=$attractions->description?></div>



                         </div>
                         
                    </div>
                     
                    <h2>Discover Attractions</h2>
                    <div class="travelo-box">
                        <div class="suggestions image-carousel style2" data-animation="slide" data-item-width="150" data-item-margin="22">
                            <ul class="slides">
                               @foreach($views as $view)
                                
                                <li>
                                    <a href="{{url('/view-attraction/' .$view->id)}}" class='hover-effect' style="height: 150px;">
                                        <img src="{{ url('/images/attractions/'.$view->logo)}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>{{$view->name}}</h5>
                                </li>
                                
                                @endforeach                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                
<div class="travelo-box search-stories-box">
    <h4>Search Stories</h4>
    <form name="form2" method="get" action="{{url('/safari/')}}" id="">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div>
        <div class="with-icon full-width">
            <input type="text" class="input-text full-width" placeholder="story name or category">
            <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
        </div>
    </div>
</form>
</div>

                
<div class="travelo-box book-with-us-box">
    <h4>Why Book with us?</h4>
    <ul>
        <li>
            <i class="soap-icon-hotel-1 circle"></i>
            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
            <p>We are dealing with more than 135,000 around the world.</p>
        </li>
        <li>
            <i class="soap-icon-savings circle"></i>
            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
            <p>Guaranteed low rates with attractive payment methods.</p>
        </li>
        <li>
            <i class="soap-icon-support circle"></i>
            <h5 class="title"><a href="#">Excellent Support</a></h5>
            <p>24/7 emergency and customer support</p>
        </li>
    </ul>
</div>

                

            </div>
        </div>
    </div>
</section>


@endsection