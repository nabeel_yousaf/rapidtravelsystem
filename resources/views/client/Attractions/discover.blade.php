@extends('layout.layout') @section('content')
<div class="page-title-container">
            <div class="container">
                {{-- <div class="page-title pull-left">
                    <h2 class="entry-title">Attractions</h2>
                </div> --}}
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{url('/')}}">HOME</a></li>
                  
                    <li class="active">Attractions</li>
                </ul>
            </div>
        </div>

        <section id="content">
        <div class="container">
            <div id="main">
                

                <div class="tour-packages row add-clearfix image-box">
                    @foreach($attractions as $attraction)
                   
                    @if($attraction->is_enabled !==0)

                   
                    <div class='col-sm-6 col-md-4'>
                        <article class='box'>
                            <figure style="border-radius: 6%;">
                                <a href="{{url('/view-attraction/' .$attraction->id)}}">
                                    <img src="{{ url('/images/attractions/'.$attraction->logo)}}" alt=''></a>
                                <figcaption>
                                   
                                    <h2 class='caption-title'>{{$attraction->name}}</h2>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                    @endif
                @endforeach

                
                </div>
               {!! $attractions->render() !!}

            </div>
        </div>
    </section>

@endsection