@extends('layout.layout') @section('content')
<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">Car Hiring</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>

            <li class="active">Car Hiring</li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <img src="{{url('uploads/carhiring.jpg')}}" alt="" />
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">Car Hiring</h1>
                        <div class="post-content">
                            <p>{{sitename()}} successfully introduces the charm and culture of the region to visitors from around the globe every year with state of the art luxury coaches and 4-wheel drive vehicles, We also provide our clients the quality of services and care for the needs for individual or group travellers. From airline tickets to worldwide accommodations, hotels or apartments, luxury cruises & car hires and providing travel services to wonderful and exotic destinations. </p>
                            <p>{{sitename()}} Holidays hold solid and flexible relations with our exclusive partners and suppliers in order to offer you steadfast services with excellence.</p>

                        </div>

                    </div>
                    <form name="form1" method="post" action="{{url('/request/')}}" id="">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="post-comment block">
                        <h2 class="reply-title">Send your Request</h2>
                        <div class="travelo-box">
                            <div class="comment-form">
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <label>Your Name <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" style="color:Red;display:none;">Required</span></label>

                                        <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Name" type="text" id="ctl00_ContentPlaceHolder1_txt_Name" class="input-text full-width" required="" />
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Your Email <span id="ctl00_ContentPlaceHolder1_RegularExpressionValidator1" style="color:Red;display:none;">Invalid email address</span><span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator2" style="color:Red;display:none;">Required</span></label>
                                        <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Email" type="text" id="ctl00_ContentPlaceHolder1_txt_Email" class="input-text full-width" required="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <label>Your Phone <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator3" style="color:Red;display:none;">Required</span></label>
                                        <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Phone" type="Number" min="0" id="ctl00_ContentPlaceHolder1_txt_Phone" class="input-text full-width" required="" />
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Number of Persons <span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator4" style="color:Red;display:none;">Required</span></label>
                                        <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Persons" type="Number" min="0" value="1" id="ctl00_ContentPlaceHolder1_txt_Persons" class="input-text full-width" required="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Special Request</label>
                                    <textarea name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Request" rows="5" cols="20" id="ctl00_ContentPlaceHolder1_txt_Request" class="input-text full-width" placeholder="write message here"   required=""></textarea>
                                </div>

                                <input type="submit" name="ctl00$ContentPlaceHolder1$btn_Send" value="SEND REQUEST" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btn_Send&quot;, &quot;&quot;, true, &quot;SendTours&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btn_Send" class="buttonCustome btn-large" />

                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">

                

                <div class="travelo-box book-with-us-box">
                    <h4>Why Book with us?</h4>
                    <ul>
                        <li>
                            <i class="soap-icon-hotel-1 circle"></i>
                            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                            <p>We are dealing with more than 135,000 around the world.</p>
                        </li>
                        <li>
                            <i class="soap-icon-savings circle"></i>
                            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                            <p>Guaranteed low rates with attractive payment methods.</p>
                        </li>
                        <li>
                            <i class="soap-icon-support circle"></i>
                            <h5 class="title"><a href="#">Excellent Support</a></h5>
                            <p>24/7 emergency and customer support</p>
                        </li>
                    </ul>
                </div>

                
            </div>
        </div>
    </div>
</section>

@endsection