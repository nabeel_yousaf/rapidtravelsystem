@extends('layout.layout') @section('content')

<div class="page-title-container">
  

    <div class="container">
  
        {{-- <div class="page-title pull-left">
           <h2 class="entry-title">
           {{$tour->Tour_name}}
            </h2>
        </div> --}}
        
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
           
            <li class="active">{{$tour->Tour_name}}</li>
        </ul>
    </div>

</div>
 
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-md-9">
                <div class="tab-container style1" id="hotel-main-content">
                    <div class="tab-content">
                        <div id="photos-tab" class="tab-pane fade in active">
                            <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">                            
                                <ul class="slides">
                                    @foreach ($tour->tourimg as $ti)
                                    <li>                
                                        <img src="{{ url('/images/logo/tourimgs/' .$ti->tour_images)}}" alt='' height="450" width="450" />                               
                                    </li>
                                    @endforeach
                                </ul>     
                            </div>
                            <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                                <ul class="slides">
                                     @foreach($tour->tourimg as $ti)
                                    <li> 
                                        <img src="{{ url('/images/logo/tourimgs/'.$ti->tour_images)}}" alt='' />
                                    </li>
                                     @endforeach

                                </ul>
                                 
                            </div>
                        </div>
                    </div>
                </div>
               

                <div id="hotel-features" class="tab-container">
                    <ul class="tabs">
                        <li class="active"><a href="#hotel-description" data-toggle="tab">Itinerary </a></li>

                        <li><a href="#outgoing_Include" data-toggle="tab">Include</a></li>
                        <li><a href="#outgoing_Excludes" data-toggle="tab">Exclude</a></li>

                       
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="hotel-description">
                            <div class="intro table-wrapper full-width hidden-table-sms">
                                <div class="col-sm-12 col-lg-12 features table-cell">
                                    <ul>
                                        <li>
                                            <label>DESTINATION:</label>{{$tour->Tour_name}} </li>
                                        {{-- <li>
                                            <label>Airline:</label>Emirates</li> --}}
                                        <li>
                                            <label>DURATION:</label>{{$tour->Tour_Duration}}
                                          
                                        </li>
                                        
                                        <li>
                                            <label>PRICE STARTING FROM:</label>{{$tour->Tour_Adult_price}} {{$tour->currency}} </li>
                                        
                                        
                                    </ul>
                                </div>
                                <div class="col-sm-12 col-lg-12 table-cell testimonials">
                                    <div class="testimonial style1">
                                        <ul class="slides ">
                                            <li>
                                                <div class="row" style="padding: 12px;"><?=$tour->Tour_Desc?></div>
                                                
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                          
                        </div>
                       
                        <div class="tab-pane fade" id="outgoing_Include">
                            <div class="long-description">
                                <h2 class="skin-color">Package Includes</h2>
                                <ul class='discover triangle hover row'>
                                    @foreach($tour->Tour_packages as $tp)
                                    
                                        @if($tp->pkg_include_id != 0)
                                            <li><?=$tour->includes?></li>
                                        @endif
                                    @endforeach
                                </ul>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="outgoing_Excludes">
                            <div class="long-description">
                                <h2 class="skin-color">Package Excludes</h2>
                                <ul class='discover triangle hover row'>
                                    
                                    @foreach($tour->Tour_packages as $tp)
                                    
                                        @if($tp->pkg_exclude_id != 0)
                                            <li><?=$tour->exludes?></li>
                                        @endif
                                     @endforeach
                                
                                    
                                </ul>

                                

                            </div>

                        </div>
                        
                      
                        
                    </div>

                </div>
                
                    <div class="travelo-box" style="margin-top: 30px;">
                        <h2 class="box-title skin-color">Book Now</h2>
                        
                        <form name="form2" method="post" action="{{url('/request/')}}" id="">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="travelo-box">
                                <div class="comment-form">
                                    <div class="form-group row">
                                        <input style="display: none;" name="tourname" value="{{$tour->Tour_name}}" type="text" id="" class="input-text full-width" />
                                        <div class="col-xs-6">
                                            <label>Booking Date <span id="" style="color:Red;display:none;">Required</span></label>

                                            <input name="booking_date" type="date" id="" class="input-text full-width" required="" />
                                             <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                        <div class="col-xs-6">
                                            
                                            <br />
                                            <input type="radio" id="private" name="transport" value="private">
                                            <label for="private">Private Basis</label>
                                            <input type="radio" id="sharing" name="transport" value="sharing">
                                            <label for="sharing">Sharing Basis</label>
                                             <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xs-6">
                                            <label>Pick-up Location <span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator1" style="color:Red;display:none;">Required</span></label>

                                            <input name="pickup" type="text" id="pickup" class="input-text full-width" required="" />
                                             <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Drop-off Location <span id="dropoff" style="color:Red;display:none;">Required</span></label>

                                            <input name="dropoff" type="text" id="dropoff" class="input-text full-width" required="" />
                                             <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xs-6">
                                            <label>Your Name <span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator1" style="color:Red;display:none;">Required</span></label>

                                            <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Name" type="text" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Name" class="input-text full-width" required="" />
                                             <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Your Email <span id="ctl00_ContentPlaceHolder1_Tour_Details_RegularExpressionValidator1" style="color:Red;display:none;">Invalid email address</span><span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator2" style="color:Red;display:none;">{{array_first($errors->all())}}</span></label>
                                            <input name="asd"  type="email" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Email" class="input-text full-width" required="" value="" />
                                            <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xs-6">
                                            <label>Nationality <span id="" style="color:Red;display:none;">Required</span></label>

                                            <input name="nationality" type="text" id="" class="input-text full-width" required="" />
                                             <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Your Phone <span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator3" style="color:Red;display:none;">Required</span></label>
                                            <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Phone" type="tel" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Phone" class="input-text full-width" required="" placeholder="+971xxxxxxxx" />
                                            <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-xs-6">
                                            <label>Number of Pax <span id="ctl00_ContentPlaceHolder1_Tour_Details_RequiredFieldValidator4" style="color:Red;display:none;">Required</span></label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Persons" type="number" min="0" value="" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Persons" class="input-text full-width" required="" placeholder="adults" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <input name="childs" type="number" min="0" value="" id="" class="input-text full-width" placeholder="childs" />
                                                </div>
                                            </div>
                                        </div>
                                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Special Request</label>
                                        <textarea name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Request" rows="5" cols="20" id="ctl00_ContentPlaceHolder1_Tour_Details_txt_Request" class="input-text full-width" placeholder="Request...."></textarea>
                                        <div style="color: #dd4b39;display: block; margin-top: 5px; margin-bottom: 10px;font-size: 85%;">{{array_first($errors->all())}} </div>
                                    </div>

                                     <div id="ReCaptchContainer"></div>
                                  <br/>
                               <input type="submit" name="ctl00$ContentPlaceHolder1$btn_Send" value="SEND REQUEST" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btn_Send&quot;, &quot;&quot;, true, &quot;SendTours&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btn_Send" class="buttonCustome btn-large" />
                                
                                </div>
                            </div>
                        </form>
                        
                    </div>
                
            </div>
            <div class="sidebar col-md-3">
                
                
                   @foreach($details as $detail)
                <div class="travelo-box contact-box">
                    <h4 class="box-title">Need {{sitename()}} Help?</h4>
                    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                    <address class="contact-details">
                        <a href="tel:{{$detail->Comp_Phone}}" class="contact-phone"><i class="soap-icon-phone"></i> {{$detail->Comp_Phone}}
                        <br />
                        <a href="tel:{{$detail->mobile}}" class="contact-phone"><i class="soap-icon-phone"></i> {{$detail->mobile}}
                        <br />
                        <a href="mailto:{{$detail->Comp_Email}}" class="contact-email">{{$detail->Comp_Email}}</a>
                    </address>
                </div>
                @endforeach
                <div class="travelo-box book-with-us-box">
                    <h4>Why Book with us?</h4>
                    <ul>
                        <li>
                            <i class="soap-icon-hotel-1 circle"></i>
                            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                        <li>
                            <i class="soap-icon-savings circle"></i>
                            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                        <li>
                            <i class="soap-icon-support circle"></i>
                            <h5 class="title"><a href="#">Excellent Support</a></h5>
                            <p>Nunc cursus libero pur congue arut nimspnty.</p>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <script src="../www.google.com/recaptcha/api8c9b.js?onload=renderRecaptcha&amp;render=explicit" async defer></script>
    <script src="../code.jquery.com/jquery-3.2.1.min.js"></script>

    <script type="text/javascript">
        var site_key = '6LeSSroUAAAAAMQ7JIf9tzNRcFYYsa7_9AamV3ub';
        var renderRecaptcha = function() {
            grecaptcha.render('ReCaptchContainer', {
                'sitekey': site_key,
                'callback': reCaptchaCallback,
                theme: 'light', //light or dark    
                type: 'image', // image or audio    
                size: 'normal' //normal or compact    
            });
        };

        var reCaptchaCallback = function(response) {
            if (response !== '') {
                jQuery('#lblMessage').css('color', 'green').html('Success');
            }
        };

        jQuery('button[type="button"]').click(function(e) {
            var message = 'Please check the checkbox';
            if (typeof(grecaptcha) != 'undefined') {
                var response = grecaptcha.getResponse();
                (response.length === 0) ? (message = 'Captcha verification failed') : (message = 'Success!');
            }
            //jQuery('#lblMessage').html(message);
            //jQuery('#lblMessage').css('color', (message.toLowerCase() == 'success!') ? "green" : "red");
        });
    </script>
</section>

@endsection