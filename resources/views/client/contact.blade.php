@extends('layout.layout') @section('content')
 <div class="page-title-container">
            <div class="container">
                {{-- <div class="page-title pull-left">
                    <h2 class="entry-title">Contact Us</h2>
                </div> --}}
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{url('/')}}">HOME</a></li>
                 
                    <li class="active">Contact Us</li>
                </ul>
            </div>
        </div>

        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="travelo-google-map block">
                        <img src="{{url('/images/location.jpg')}}" style="width: 100%;  height: auto;">
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-3">
                            @foreach($details as $detail)
                            <div class="travelo-box contact-us-box">
                                <h4>Contact us</h4>
                                <ul class="contact-address">
                                    <li class="address">
                                        <i class="soap-icon-address circle"></i>
                                        <h5 class="title">Address</h5>
                                        <p>{{$detail->Comp_Address}}</p>
										
                                    </li>
                                    <li class="phone">
                                        <i class="soap-icon-phone circle"></i>
                                        <h5 class="title">Phone</h5>
                                        <p>{{phone()}}</p>
                                  
                                    </li>
                                    <li class="phone">
                                        <i class="soap-icon-phone circle"></i>
                                        <h5 class="title">Mobile</h5>
                                        <p>{{mobile()}}</p>
                                  
                                    </li>
                                    <li class="email">
                                        <i class="soap-icon-message circle"></i>
                                        <h5 class="title">Email</h5>
                                        <p>{{siteEmail()}}</p>
                                        
                                    </li>
                                </ul>
                               
                            </div>
                            @endforeach
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <div class="travelo-box">
                            <form name="form1" method="post" action="{{url('/request/')}}" id="">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="contact-form">
                                    <h4 class="box-title">Send us a Message</h4>

                                    <div class="row form-group">
                                        <div class="col-xs-6">
                                            <label>Your Name<span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator1" style="color:Red;display:none;" >Required</span></label>
                                           
                                      <input name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Name" type="text" required="" id="ctl00_ContentPlaceHolder1_txt_Name" class="input-text full-width" required="" />
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Your Email<span id="asd" style="color:Red;display:none;">Required</span></label >
                                           
                                            <input name="asd" type="email" required=""  id="ctl00_ContentPlaceHolder1_txt_Email" class="input-text full-width" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Your Message<span id="ctl00_ContentPlaceHolder1_RequiredFieldValidator3" style="color:Red;display:none;">Required</span></label>
                                       <textarea name="ctl00$ContentPlaceHolder1$Tour_Details$txt_Request" rows="5" cols="20" id="ctl00_ContentPlaceHolder1_txt_Request" class="input-text full-width" required="" placeholder="write message here"  ></textarea>
                                    </div>

                                    <div id="ReCaptchContainer"></div>
                                <br />
                                       <input type="submit" name="ctl00$ContentPlaceHolder1$btn_Send" value="SEND REQUEST" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ContentPlaceHolder1$btn_Send&quot;, &quot;&quot;, true, &quot;SendTours&quot;, &quot;&quot;, false, false))" id="ctl00_ContentPlaceHolder1_btn_Send" class="buttonCustome btn-large" />
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        
        <script type="text/javascript">
        tjq(".travelo-google-map").gmap3({
            map: {
                options: {
                    center: [25.2364173, 55.3027937],
                    zoom: 12
                }
            },
            marker:{
                values: [
                    { latLng: [25.2364173, 55.3027937], data: "Travco Travel" }

                ],
                options: {
                    draggable: false
                },
            }
        });
    </script>

        

@endsection

