@extends('layout.layout') @section('content')

<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">Fujairah</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li><a href="{{url('/discover/')}}">Travel Guide</a></li>
            <li class="active">
                Fujairah</li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <a href="#">
                            <img src="{{url('uploads/Destinations/882/35.jpg')}}" alt='' /></a>
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">
                            Fujairah</h1>
                        <div class="post-content">
                            <p class='text-justify'>Fujairah is one of the smallest of the seven Emirates that make up the United Arab Emirates, with a population of just 130,000, mostly concentrated in its main city, also called Fujairah. It&rsquo;s the only Emirate with a coastline on the Gulf of Oman in the country's east instead of the Persian Gulf, and unlike the other Emirates, Fujairah is quite mountainous. It boasts a higher than average yearly rainfall, although it is warm and dry for most of the year. Daytime temperatures from October to March average around 25 &deg;C and rarely reach above 30 &deg;C, while in the summer temperatures may reach over 40 &deg;C. Only 90 minutes from the cosmopolitan world city of Dubai, Fujairah is quite an interesting alternative Arabian holiday destination. The UAE has a unique spirit that seems to have successfully blended the Bedouin lifestyle with western modernity, and Fujairah features the stark beauty of the desert and mountains alongside camel racing, traditional Bedouin towns and glorious beaches with modern luxury resorts. It&rsquo;s the perfect place for visitors seeking sun, sea or adventure through unforgettable diving holidays, lazing on a fabulous beach or exploring the desert and the Hajaar Mountains.</p>

<p class='text-justify'>Diving &amp; Snorkelling in Fujairah</p>

<p class='text-justify'>Diving in Fujairah is considered the best in the whole of the UAE and with a reef trailing 55 miles off the coast, most spots are no more than a few hundred feet from the shore. Fujairah is a pretty good place to discover whether you will enjoy throwing yourself to the bottom of the ocean with weights on your hips without trying to get a refund for a full-blooded PADI course. Fujairah is the only Emirate facing the Indian Ocean giving you a chance to sample its blood warm waters. Most diving courses only last a day and will give you a taste of the Scuba diving experience.</p>

<p class='text-justify'>Fujairah Dive Sites</p>

<p class='text-justify'>Can be as varied and as interesting as those found in the Red Sea, but are certainly less busy. Here are just some of the most popular ones:</p>

<p class='text-justify'><strong>Snoopy Island:</strong>&nbsp;the favourite for divers &amp; snorkellers. There are plenty of hard &amp; soft corals as well as anemones. Colourful fish abound with morays, barracuda &amp; the odd reef shark at the back of the island.&nbsp;<br />
<strong>Dibba Rock:</strong>&nbsp;is the place to see turtles and goby. There are also lots of anemones with their resident clownfish. This is one of the best snorkelling sites around, especially if you want to see turtles. They are prolific on the seaward side of the island where there are lots of coral reefs.&nbsp;<br />
<strong>InchCape 1:</strong>&nbsp;a wreck sitting upright at a depth of around 30 meters depending on the tide. The wreck is small and it is possible to go around it several times during a dive.&nbsp;<br />
<strong>Shark Island:</strong>&nbsp;about 20 minutes by boat from Fujairah in the Bay of Khorfakkan, and is a treat for snorkellers with plenty of colourful fish varieties to see. For divers the site offers beautiful rock formations at between 6-15 metres. There is little coral formation, although the area is home to moray eels and rays as well as puffers and batfish.&nbsp;<br />
<strong>Coral Garden:</strong>&nbsp;about 50 metres east of Shark Island. Provides deep dives between 24-30 metres, which is suitable for advanced divers only. The dive is a gradual descent to a garden of soft coral, abundant gorgoneas,and horn coral. Other underwater life includes morays.</p>

<p class='text-justify'>Desert Adventures in Fujairah</p>

<p class='text-justify'>These adventures come in all shapes and sizes. Take a ride on &lsquo;the ship of the desert&rsquo; and experience how the Bedouins travelled on top of a camel, or tour the contrasting landscapes of the lush valley of Wadi Hatta and the UAE desert. Wadi Hatta, situated at the foothills of the Hajaar Mountains, is a green oasis offering spectacular views of the desert as well as trips to the old Hatta village. The UAE desert can be explored with a 4WD vehicle, bouncing over the seemingly endless dunes like a roller coaster ride.</p>

<p class='text-justify'>Desert feasts, with entertainment by belly dancers and visits to Bedouin campsites can all be arranged. Visits to the oasis of Al Dhaid can be combined with trips to Khorfakkan, situated at the foothills of the Khorfakkan Mountains where you can enjoy fishing, snorkelling and wind surfing. Small local souks are the places to buy locally harvested fresh fruits and vegetables, rugs, carpets and locally made pottery.</p>

<p class='text-justify'>Fujairah Hotels</p>

<p class='text-justify'>The Iberotel Miramar Al Aqah Beach Resort is approximately 90 minutes from Dubai International Airport and just 45 km from Fujairah National Airport. This stunning five-star resort overlooks the sea and its own pristine private beach with the impressive Hajaar Mountains as a backdrop. Tastefully designed in a low-rise Arabian/Moroccan style architecture that never rises above three floors, the centre piece of the hotel is the very large free form swimming pool with lots of sun loungers, umbrellas and shade available.</p>

<p class='text-justify'>The luxury Iberotel Miramar Al Aqah Beach Resort has well appointed rooms, either mountain facing or sea and pool facing, and an excellent range of bars and restaurants. The standard of food is extremely high and the all inclusive plan in the hotel represents outstandingly good value for money. A selection of shops, a health club and gym, beauty salon and hairdresser, heated outdoor and indoor pools and day and night animation programmes are also readily available. Free shuttle buses run to and from Dubai from Fujairah City if you want to sample the more cosmopolitan delights of Dubai itself. You can also book boat and fishing trips, desert safaris, mountain and coastal discovery tours. Guests can indulge in a variety of water sports including windsurfing, diving, snorkelling and kayaking.</p>

                         </div>
                         
                    </div>
                     
                    <h2>Discover United Arab Emirates</h2>
                    <div class="travelo-box">
                        <div class="suggestions image-carousel style2" data-animation="slide" data-item-width="150" data-item-margin="22">
                            <ul class="slides">
                                <li>
                                    <a href="{{url('/abudabhi/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/32-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Abu Dhabi</h5>
                                </li><li>
                                    <a href="{{url('/ajman/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/33-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ajman</h5>
                                </li><li>
                                    <a href="{{url('/dubai/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/34-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Dubai</h5>
                                </li><li>
                                    <a href="{{url('/khaimah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/36-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ras Al Khaimah</h5>
                                </li><li>
                                    <a href="{{url('/sharjah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/37-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Sharjah</h5>
                                </li>
                                
                               
                                
                                
                               
                            </ul>
                        </div>
                    </div>
                 

                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                
<div class="travelo-box search-stories-box">
    <h4>Search Stories</h4>
    <div>
        <div class="with-icon full-width">
            <input type="text" class="input-text full-width" placeholder="story name or category">
            <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
        </div>
    </div>
</div>

                
<div class="travelo-box book-with-us-box">
    <h4>Why Book with us?</h4>
    <ul>
        <li>
            <i class="soap-icon-hotel-1 circle"></i>
            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
            <p>We are dealing with more than 135,000 around the world.</p>
        </li>
        <li>
            <i class="soap-icon-savings circle"></i>
            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
            <p>Guaranteed low rates with attractive payment methods.</p>
        </li>
        <li>
            <i class="soap-icon-support circle"></i>
            <h5 class="title"><a href="#">Excellent Support</a></h5>
            <p>24/7 emergency and customer support</p>
        </li>
    </ul>
</div>

                
<div class="travelo-box contact-box">
    <h4 class="box-title">Need {{sitename()}} Help?</h4>
    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
    <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>(+971) 4 336 6643</span>
        <br />
        <a href="#" class="contact-email">dubai@mynexttrip.com</a>
    </address>
</div>
            </div>
        </div>
    </div>
</section>

@endsection