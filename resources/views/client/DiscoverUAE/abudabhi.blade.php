@extends('layout.layout') @section('content')
<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">Abu Dhabi</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li><a href="{{url('discover')}}">Travel Guide</a></li>
            <li class="active">
                Abu Dhabi</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <a href="#">
                            <img src="{{url('uploads/Destinations/882/32.jpg')}}" alt='' /></a>
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">
                            Abu Dhabi</h1>
                        <div class="post-content">
                            <p class='text-justify'>Abu Dhabi, the capital of the United Arab Emirates, has marked its territory as one of the top tourist destinations due to its world class infrastructure and interesting attractions. Until recently, Abu Dhabi boasted of a city of conventions and exhibitions; but today, it has also gained popularity for its entertainment options and shopping venues. iVarious areas and islands in Abu Dhabi are developed to promote tourism including the Corniche, and Yas and Saadiyat Island.s To provide in-depth information on the various things to do in Abu Dhabi, we have compiled a must visit list of top landmarks, shopping outlets, dining options and a variety of activities. Also, travellers will be informed on the various means of transportation and communication options in Abu Dhabi.</p>

<p class='text-justify'>Abu Dhabi is geographically located on the north-eastern part of the Persian Gulf in the Arabian Peninsula. The island city is located just 250 metres from the mainland which consists of many other suburbs linked to the emirate, Abu Dhabi. A special feature of the city includes the Abu Dhabi Corniche which offers the chance to walk, cycle or jog along the island&rsquo;s coastline which has a breathtaking view.</p>

                         </div>
                         
                    </div>
                     
                    <h2>Discover United Arab Emirates</h2>
                    <div class="travelo-box">
                        <div class="suggestions image-carousel style2" data-animation="slide" data-item-width="150" data-item-margin="22">
                            <ul class="slides">
                                <li>
                                    <a href="{{url('/ajman/')}}" class='hover-effect'>
                                        <img src="{{url('/uploads/Destinations/171/33-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ajman</h5>
                                </li><li>
                                    <a href="{{url('/dubai/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/34-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Dubai</h5>
                                </li><li>
                                    <a href="{{url('/fujairah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/35-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Fujairah</h5>
                                </li><li>
                                    <a href="{{url('/khaimah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/36-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ras Al Khaimah</h5>
                                </li><li>
                                    <a href="{{url('/sharjah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/37-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Sharjah</h5>
                                </li>
                                
                               
                                
                                
                               
                            </ul>
                        </div>
                    </div>
                 

                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                
<div class="travelo-box search-stories-box">
    <h4>Search Stories</h4>
    <div>
        <div class="with-icon full-width">
            <input type="text" class="input-text full-width" placeholder="story name or category">
            <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
        </div>
    </div>
</div>

                
<div class="travelo-box book-with-us-box">
    <h4>Why Book with us?</h4>
    <ul>
        <li>
            <i class="soap-icon-hotel-1 circle"></i>
            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
            <p>We are dealing with more than 135,000 around the world.</p>
        </li>
        <li>
            <i class="soap-icon-savings circle"></i>
            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
            <p>Guaranteed low rates with attractive payment methods.</p>
        </li>
        <li>
            <i class="soap-icon-support circle"></i>
            <h5 class="title"><a href="#">Excellent Support</a></h5>
            <p>24/7 emergency and customer support</p>
        </li>
    </ul>
</div>

                
<div class="travelo-box contact-box">
    <h4 class="box-title">Need {{sitename()}} Help?</h4>
    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
    <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>(+971) 4 336 6643</span>
        <br />
        <a href="#" class="contact-email">dubai@mynexttrip.com</a>
    </address>
</div>
            </div>
        </div>
    </div>
</section>
@endsection