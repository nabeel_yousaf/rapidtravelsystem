@extends('layout.layout') @section('content')
<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">Sharjah</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li><a href="{{url('/discover/')}}">Travel Guide</a></li>
            <li class="active">
                Sharjah</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <a href="#">
                            <img src="{{url('uploads/Destinations/882/37.jpg')}}" alt='' /></a>
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">
                            Sharjah</h1>
                        <div class="post-content">
                            <p class='text-justify'>Sharjah is the third largest of the seven emirates that make up the United Arab Emirates (UAE) and is the only one to have land on both the Arabian Gulf Coast and the Gulf of Oman. It is an emirate of contrasts where visitors can enjoy a holiday in the sun, discover traditional markets (souks) or visit modern shopping malls, explore the many heritage sites and museums, admire the majestic mosques, stroll around the lagoons, and experience the natural beauty of true Arabian deserts, mountains and seas.</p>

<p class='text-justify'>Sharjah covers approximately 2,600 square kilometres. In addition to Sharjah city, which lies on the shores of the Arabian Gulf, the emirate has three regions on the scenic east coast; Dibba Al Hisn, Khor Fakkan and Kalba. Sharjah is a close neighbour to Dubai (it&rsquo;s just a 15 minute drive from the centre of Sharjah to Dubai International Airport).</p>

<p class='text-justify'>Sharjah&rsquo;s location is significant both geographically and historically. The city overlooks the Arabian Gulf coastline that stretches for more than 16 kilometres, consisting of sandy beaches and built up areas. The central region combines lush green oases, with gravel plains and rolling red sand dunes. To the East the emirate reaches the Gulf of Oman coast where the landscape changes to a spectacular rocky coastline backed by impressively high mountains. Historically, the east coast is full of archaeological sites dating back to as early as the 2nd millennium, and forts built by the Portuguese in the 16th century were used to control the spice trade. In more recent times, Sharjah opened the first airport in the region in 1932. The Emirate is located midway between Europe and the Far East and is within 8 hours flying time in either direction.</p>

                         </div>
                         
                    </div>
                     
                    <h2>Discover United Arab Emirates</h2>
                    <div class="travelo-box">
                        <div class="suggestions image-carousel style2" data-animation="slide" data-item-width="150" data-item-margin="22">
                            <ul class="slides">
                                <li>
                                    <a href="{{url('/abudabhi/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/32-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Abu Dhabi</h5>
                                </li><li>
                                    <a href="{{url('/ajman/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/33-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ajman</h5>
                                </li><li>
                                    <a href="{{url('/dubai/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/34-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Dubai</h5>
                                </li><li>
                                    <a href="{{url('/fujairah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/35-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Fujairah</h5>
                                </li><li>
                                    <a href="{{url('/khaimah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/36-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ras Al Khaimah</h5>
                                </li>
                                
                               
                                
                                
                               
                            </ul>
                        </div>
                    </div>
                 

                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                
<div class="travelo-box search-stories-box">
    <h4>Search Stories</h4>
    <div>
        <div class="with-icon full-width">
            <input type="text" class="input-text full-width" placeholder="story name or category">
            <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
        </div>
    </div>
</div>

                
<div class="travelo-box book-with-us-box">
    <h4>Why Book with us?</h4>
    <ul>
        <li>
            <i class="soap-icon-hotel-1 circle"></i>
            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
            <p>We are dealing with more than 135,000 around the world.</p>
        </li>
        <li>
            <i class="soap-icon-savings circle"></i>
            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
            <p>Guaranteed low rates with attractive payment methods.</p>
        </li>
        <li>
            <i class="soap-icon-support circle"></i>
            <h5 class="title"><a href="#">Excellent Support</a></h5>
            <p>24/7 emergency and customer support</p>
        </li>
    </ul>
</div>

                
<div class="travelo-box contact-box">
    <h4 class="box-title">Need {{sitename()}} Help?</h4>
    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
    <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>(+971) 4 336 6643</span>
        <br />
        <a href="#" class="contact-email">dubai@mynexttrip.com</a>
    </address>
</div>
            </div>
        </div>
    </div>
</section>

@endsection