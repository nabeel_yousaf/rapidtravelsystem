@extends('layout.layout') @section('content')
<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">Ajman</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li><a href="{{url('discover')}}">Travel Guide</a></li>
            <li class="active">
                Ajman</li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <a href="#">
                            <img src="{{url('uploads/Destinations/882/33.jpg')}}" alt='' /></a>
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">
                            Ajman</h1>
                        <div class="post-content">
                            <p class='text-justify'>Located near the cities of Sharjah and Umm Al Quwain, Ajman is the smallest of all the emirates. Ajman comprises the two jurisdictions of Masfout and Manama, situated to the north of the Masafi-Dhaid main artery. Though the biggest draw in the area is Sharjah, Ajman encompasses some interesting attractions and sites for tourists. One of the main attractions in Ajman is the long expanse of powdery white sandy beach, situated along the Persian Gulf Coast, stretching for almost 20 miles.</p>

<p class='text-justify'>Prominent in any Ajman travel guide is the eighteenth century fort located in the heart of the city. The fort has been renovated and turned into the local museum where the area&rsquo;s history can be examined in more depth. Once serving as the Ruler&rsquo;s palace, it became Ajman&rsquo;s cardinal police station in 1970. Tourists visiting the museum are privy to old manuscripts, odes to traditional daily life in Ajman as well as many ancient artifacts and old weapons once used in battle. The museum is situated along the coast and close to many savory restaurants, and a number of high-end Sharjah hotels if opting to be based outside of the city.</p>

<p class='text-justify'>Ajman is essentially a port city great for shopping and dining. The small city maintains a certain charm the other emirates lack, mostly due to its small size which creates a more intimate feel than many of the large, bustling cities. Though in the past it&rsquo;s held less appeal than the other emirates, Ajman does feature a handful of interesting attractions. The naturally formed creek diffuses the inland and remains a major focal point in the city. The section of Ajman called Masfout is an important agricultural area providing much of the local produce found throughout the region. Masfout is noted for its vibrant marble and is bordered by the Hajar Mountains creating a picturesque backdrop.</p>

                         </div>
                         
                    </div>
                     
                    <h2>Discover United Arab Emirates</h2>
                    <div class="travelo-box">
                        <div class="suggestions image-carousel style2" data-animation="slide" data-item-width="150" data-item-margin="22">
                            <ul class="slides">
                                <li>
                                    <a href="{{url('/abudabhi/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/32-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Abu Dhabi</h5>
                                </li><li>
                                    <a href="{{url('/dubai/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/34-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Dubai</h5>
                                </li><li>
                                    <a href="{{url('/fujairah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/35-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Fujairah</h5>
                                </li><li>
                                    <a href="{{url('/khaimah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/36-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ras Al Khaimah</h5>
                                </li><li>
                                    <a href="{{url('/sharjah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/37-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Sharjah</h5>
                                </li>
                                
                               
                                
                                
                               
                            </ul>
                        </div>
                    </div>
                 

                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                
<div class="travelo-box search-stories-box">
    <h4>Search Stories</h4>
    <div>
        <div class="with-icon full-width">
            <input type="text" class="input-text full-width" placeholder="story name or category">
            <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
        </div>
    </div>
</div>

                
<div class="travelo-box book-with-us-box">
    <h4>Why Book with us?</h4>
    <ul>
        <li>
            <i class="soap-icon-hotel-1 circle"></i>
            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
            <p>We are dealing with more than 135,000 around the world.</p>
        </li>
        <li>
            <i class="soap-icon-savings circle"></i>
            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
            <p>Guaranteed low rates with attractive payment methods.</p>
        </li>
        <li>
            <i class="soap-icon-support circle"></i>
            <h5 class="title"><a href="#">Excellent Support</a></h5>
            <p>24/7 emergency and customer support</p>
        </li>
    </ul>
</div>

                
<div class="travelo-box contact-box">
    <h4 class="box-title">Need {{sitename()}} Help?</h4>
    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
    <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>(+971) 4 336 6643</span>
        <br />
        <a href="#" class="contact-email">dubai@mynexttrip.com</a>
    </address>
</div>
            </div>
        </div>
    </div>
</section>
@endsection