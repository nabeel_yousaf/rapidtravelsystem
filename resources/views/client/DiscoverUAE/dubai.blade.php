@extends('layout.layout') @section('content')
<div class="page-title-container">
    <div class="container">
        <{{-- div class="page-title pull-left">
            <h2 class="entry-title">Dubai</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li><a href="{{url('/discover')}}">Travel Guide</a></li>
            <li class="active">
                Dubai</li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <a href="#">
                            <img src="{{url('uploads/Destinations/882/34.jpg')}}"  alt='' /></a>
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">
                            Dubai</h1>
                        <div class="post-content">
                            <p class='text-justify'>Dubai is located on the Persian Gulf, to the northeast of the United Arab Emirates. Dubai is the second largest emirate with an urban area of 3885 sq km and the city is roughly 35 sq km. However it will expand to twice that size with the addition of the man-made islands; the Waterfront, the three Palms, the World, the Universe, Dubailand, as well as the construction in the desert.</p>

<p class='text-justify'>One of the most fascinating geographical aspects of Dubai is its Creek, which divides the city into two regions. Dubai Creek is made up of a natural 9.5 mile inlet in the Persian Gulf, around which the city&rsquo;s trade developed. North of the Creek is called Deira, and Bur Dubai refers to the south where it joins the tourist and residential developments of Jumeirah along the coast.</p>

<p class='text-justify'>Dubai also has the highest population, sharing its borders with Abu Dhabi to the south, Sharjah to the northeast and the Sultanate of Oman to the southeast.</p>

<p class='text-justify'>Due to the city&rsquo;s unique geographical location it enjoys a strategic position which allows it to connect to all local Gulf States, as well as to East Africa and South Asia</p>

<p class='text-justify'>Dubai Holidays</p>

<p class='text-justify'>Dubai is one the most popular of the United Arab Emirates (UAE) for tourism and that&rsquo;s little wonder given all that this cosmopolitan place has to offer. Its increase in popularity in the last few years has been unbelievable, changing from an occasionally used stop- over point convenient for shopping to a thriving full on tourist resort with plenty to do. Dubai is one of the world&rsquo;s most exciting and dynamic cities. It&rsquo;s a remarkable oasis in the desert combining ultra-luxurious accommodation, sandy beaches, unbelievable duty-free shopping and the allure of traditional Arabic culture. The city is packed with man-made wonders, like the self-styled &ldquo;Seven Star&rdquo; Burj al-Arab built on a huge artificial island and costing a staggering $650 million to build. Dubai is an Emirate and a world class city that is made for superlatives. From the man-made Palm Islands to the world's tallest building, the Burj Khalifa, which stands 828 meters high this place is exuberant, excessive and despite the global economic downturn, still seems to be going strong.</p>

<p class='text-justify'>It&rsquo;s seemingly impossible to keep up with the constant changes in this great city. They say you need to come back to Dubai every six months to check what new amenities, hotels and shopping has been added since the last trip. The UAE isn&rsquo;t all man-made wonders, retail therapy to the extreme and 4&times;4 thrashes through desert dunes though! The inhabitants of the Gulf States have had a long relationship with camels, and the prize racing beasts are very highly valued. Many of the seven species of marine turtles in the world can be seen in the waters of the UAE: the green turtle, the hawksbill turtle, the loggerhead turtle and (if you are fortunate) the huge leatherback turtle. The green and hawksbill turtles nest on beaches of the UAE as well as regularly feeding in its waters.</p>

<p class='text-justify'>Dubai: Retail Paradise</p>

<p class='text-justify'>There are more outlets for more global brands in Dubai than almost anywhere else in the world. The Dubai Mall, as is ever the case in Dubai, the biggest of its kind in the world with 600 shops open now and 600 to follow. If it&rsquo;s a shopping mall with the most expensive stores and brands you&rsquo;re after, then it must be Wafi near the Creek. It&rsquo;s in the shape of a pyramid and sphinx, and houses Chanel and Escada as well as newcomers like Kitson. to name a few. The flagship store is Salam with a wealth of prestige international brands.</p>

<p class='text-justify'>Karama is the place for fake watches, handbags and clothing that are almost indistinguishable from the real thing. The famous Gold Souk is an area of small jewellery stores selling yellow gold on the edge of the Creek. It&rsquo;s best to arrive in the late morning when the stores are less busy and be prepared for the typical Middle Eastern practice of haggling. Next door is the Spice Souk which sells exotic spices and at very good value prices.</p>

<p class='text-justify'>The Mall of the Emirates is also a must see, where you can shop or go skiing at the biggest indoor ski slope in the Middle East. It is also worth taking a 30 minute taxi ride out of town to the Outlet Mall. It&rsquo;s one million square feet of pure discount. Granted, it's last year&rsquo;s models and not a great selection of sizes, but the prices are a fraction of the original.</p>

<p class='text-justify'>There are two big sale promotion events in Dubai: Dubai Shopping Festival during the winter and Dubai Summer Surprises. They are both pretty intense, but if you&rsquo;re prepared to haggle, even in the upmarket boutiques, you may get some additional discounts. Dubai is not the place for cheap bargains (with the possible exception of the Outlet Mall), but you will get well known and established brands and high quality locally produced merchandise at prices that represent outstandingly good value for money.</p>

<p class='text-justify'>Sand, Sun and Fun In Dubai</p>

<p class='text-justify'>There are a plethora of things to do in Dubai. Most hotels offer comprehensive facilities within their own buildings of grounds, but further afield the choice of activities is virtually limitless.</p>

<p class='text-justify'>Water Theme Parks</p>

<p class='text-justify'>You can get a day pass to the Water Park which is attached to the Atlantis Palm Resort. The main feature of the Park is a Pyramid-like structure called the Ziggurat, and from this there&rsquo;s a choice of about five or six water activities, depending on how brave you feel! Freefall water chutes with a near vertical 90 foot slide or a lazy river where you sit in oversized donut rings and float down the river and through waterfalls.</p>

<p class='text-justify'>Dune Bashing in Dubai</p>

<p class='text-justify'>Four wheel drive safaris are popular where you are taken to the dunes and thrown about a bit! Tyres are deflated for the mountains of sand while you drive up to the top of a very steep dune and take it in turns to descend. For a moment, you&rsquo;ll just be hanging there, looking down a very steep slope only being held by your seatbelt , and then it&rsquo;s down the dune very fast, which is exciting and incredibly scary at the same time. You can combine driving around the dunes with a visit to a camel farm on the way back to the city.</p>

<p class='text-justify'>Camel Racing</p>

<p class='text-justify'>There are around 15 dedicated race tracks across the seven Emirates. The largest and best equipped is Nad Al Sheba just outside of Dubai. The race season runs from October to early April every year with races taking place on Thursdays, Fridays and Saturdays, and on national holidays. Partially to combat the illegal use of children as jockeys in these races, and partly because it&rsquo;s such a cool idea, Camel races today use electronic, wireless-controlled robot jockeys. There are still some human riders, but progressively the robots are taking over. The devices are simply strapped to the camel&rsquo;s back and controlled by their owners over a wireless radio link. These robots weigh only 4 kilograms (compared with the 45 kilogram weight of a jockey) so they&rsquo;re easier on the Camel too!</p>

                         </div>
                         
                    </div>
                     
                    <h2>Discover United Arab Emirates</h2>
                    <div class="travelo-box">
                        <div class="suggestions image-carousel style2" data-animation="slide" data-item-width="150" data-item-margin="22">
                            <ul class="slides">
                                <li>
                                    <a href="{{url('/abudabhi/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/32-2.jpg')}}"  alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Abu Dhabi</h5>
                                </li><li>
                                    <a href="{{url('/ajman/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/33-2.jpg')}}"  alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ajman</h5>
                                </li><li>
                                    <a href="{{url('/fujairah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/35-2.jpg')}}"  alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Fujairah</h5>
                                </li><li>
                                    <a href="{{url('/khaimah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/36-2.jpg')}}"  alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ras Al Khaimah</h5>
                                </li><li>
                                    <a href="{{url('/sharjah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/37-2.jpg')}}"  alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Sharjah</h5>
                                </li>
                                
                               
                                
                                
                               
                            </ul>
                        </div>
                    </div>
                 

                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                
<div class="travelo-box search-stories-box">
    <h4>Search Stories</h4>
    <div>
        <div class="with-icon full-width">
            <input type="text" class="input-text full-width" placeholder="story name or category">
            <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
        </div>
    </div>
</div>

                
<div class="travelo-box book-with-us-box">
    <h4>Why Book with us?</h4>
    <ul>
        <li>
            <i class="soap-icon-hotel-1 circle"></i>
            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
            <p>We are dealing with more than 135,000 around the world.</p>
        </li>
        <li>
            <i class="soap-icon-savings circle"></i>
            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
            <p>Guaranteed low rates with attractive payment methods.</p>
        </li>
        <li>
            <i class="soap-icon-support circle"></i>
            <h5 class="title"><a href="#">Excellent Support</a></h5>
            <p>24/7 emergency and customer support</p>
        </li>
    </ul>
</div>

                
<div class="travelo-box contact-box">
    <h4 class="box-title">Need Travco Help?</h4>
    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
    <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>(+971) 4 336 6643</span>
        <br />
        <a href="#" class="contact-email">dubai@travco.com</a>
    </address>
</div>
            </div>
        </div>
    </div>
</section>

@endsection