@extends('layout.layout') @section('content')
<div class="page-title-container">
    <div class="container">
        {{-- <div class="page-title pull-left">
            <h2 class="entry-title">Ras Al Khaimah</h2>
        </div> --}}
        <ul class="breadcrumbs pull-right">
            <li><a href="{{url('/')}}">HOME</a></li>
            <li><a href="{{url('/discover/')}}">Travel Guide</a></li>
            <li class="active">
                Ras Al Khaimah</li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="post">
                    <figure class="image-container">
                        <a href="#">
                            <img src="{{url('uploads/Destinations/882/36.jpg')}}" alt='' /></a>
                    </figure>
                    <div class="details">
                        <h1 class="entry-title">
                            Ras Al Khaimah</h1>
                        <div class="post-content">
                            <p class='text-justify'>Ras Al Khaimah is a unique destination for visitors of all ages, thanks to its diverse landscapes and distinctive excursions that have enticed guests over the years. This rising emirate boasts beautiful and dramatic mountains, red sandy deserts and lush green plains indented by a series of creeks and lagoons. It has a rich heritage, dating back 7,000 years, which manifests in numerous historical sites, forts and abandoned villages.</p>

<p class='text-justify'>Ras Al Khaimah offers a variety of entertainment and relaxation facilities including high-end hotels &amp; resorts, international restaurants and selective spas, all at great value for money. Located on the beaches, mountains as well as the desert, these luxurious resorts give full justice to the natural beauty of Ras Al Khaimah. The emirate is also a haven for exceptional Spas that are prominent in resorts like the Banyan Tree Al Wadi, Hilton Resort &amp; Spa and Golden Tulip Khatt Springs. The cherry on the cake is the natural hot springs in Khatt, the only one in the UAE, where guests can take a relaxing therapeutic bath.</p>

<p class='text-justify'>With a wide range of adventure and sports activities covering Mountain adventures, Desert camps, Golf courses and Watersports, the emirate of Ras Al Khaimah presents an ultimate outdoor experience for tourists. Whether shopping in the welcoming old Souq (market), soaking up the sun on one of the beaches or taking a ride on the world&rsquo;s tallest water slide, Ras Al Khaimah has something for everyone. A special excursion worth mentioning is the Land of Pearls, which takes guests through the journey of a Pearl from the operation room to the mouth of the oyster itself!</p>

<p class='text-justify'>This rising emirate, with its rich history and culture, offers a number of historical sites and ancient ruins dating back to the 13th century AD. The forts and the museums display the proud heritage of the Emiratis here, who are still renowned for their warm hospitality.</p>

<p class='text-justify'>Located just 45 minutes from the Dubai International airport and 30 minutes from Oman as well as being well connected by Airport, Ports and the National Highway, Ras Al Khaimah enables easy stopovers for guests. With its splendid natural treasure and recreational choices, Ras Al Khaimah is a picture-perfect leisure destination that is a must for travellers, looking to be smitten by the real essence of Arabia.</p>

                         </div>
                         
                    </div>
                     
                    <h2>Discover United Arab Emirates</h2>
                    <div class="travelo-box">
                        <div class="suggestions image-carousel style2" data-animation="slide" data-item-width="150" data-item-margin="22">
                            <ul class="slides">
                                <li>
                                    <a href="{{url('/abudabhi/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/32-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Abu Dhabi</h5>
                                </li><li>
                                    <a href="{{url('/ajman/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/33-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Ajman</h5>
                                </li><li>
                                    <a href="{{url('/dubai/')}}" class='hover-effect'>
                                        <img src='uploads/Destinations/171/34-2.jpg' alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Dubai</h5>
                                </li><li>
                                    <a href="{{url('/fujairah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/35-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Fujairah</h5>
                                </li><li>
                                    <a href="{{url('/sharjah/')}}" class='hover-effect'>
                                        <img src="{{url('uploads/Destinations/171/37-2.jpg')}}" alt='' class='middle-item' />
                                    </a>
                                    <h5 class='caption'>Sharjah</h5>
                                </li>
                                
                               
                                
                                
                               
                            </ul>
                        </div>
                    </div>
                 

                </div>
            </div>
            <div class="sidebar col-sm-4 col-md-3">
                
<div class="travelo-box search-stories-box">
    <h4>Search Stories</h4>
    <div>
        <div class="with-icon full-width">
            <input type="text" class="input-text full-width" placeholder="story name or category">
            <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
        </div>
    </div>
</div>

                
<div class="travelo-box book-with-us-box">
    <h4>Why Book with us?</h4>
    <ul>
        <li>
            <i class="soap-icon-hotel-1 circle"></i>
            <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
            <p>We are dealing with more than 135,000 around the world.</p>
        </li>
        <li>
            <i class="soap-icon-savings circle"></i>
            <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
            <p>Guaranteed low rates with attractive payment methods.</p>
        </li>
        <li>
            <i class="soap-icon-support circle"></i>
            <h5 class="title"><a href="#">Excellent Support</a></h5>
            <p>24/7 emergency and customer support</p>
        </li>
    </ul>
</div>

                
<div class="travelo-box contact-box">
    <h4 class="box-title">Need {{sitename()}} Help?</h4>
    <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
    <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>(+971) 4 336 6643</span>
        <br />
        <a href="#" class="contact-email">dubai@mynexttrip.com</a>
    </address>
</div>
            </div>
        </div>
    </div>
</section>
@endsection