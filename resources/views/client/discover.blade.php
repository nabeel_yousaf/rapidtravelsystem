@extends('layout.layout') @section('content')
<div class="page-title-container">
                <div class="container">
                    {{-- <div class="page-title pull-left">
                        <h2 class="entry-title">Discover United Arab Emirates</h2>
                    </div> --}}
                    <ul class="breadcrumbs pull-right">
                        <li><a href="{{url('/')}}">HOME</a></li>

                        <li class="active">Discover United Arab Emirates</li>
                    </ul>
                </div>
            </div>
             <section id="content">
                <div class="container">
                    <div class="row">
                        <div id="main" class="col-sm-8 col-md-9">
                            <div class="post">
                                <figure class="image-container">
                                    <a href="#">
                                        <img src="{{url('uploads/uae.jpg')}}" alt="" /></a>
                                </figure>
                                <div class="details">
                                    <h1 class="entry-title">Discover United Arab Emirates</h1>
                                    <div class="post-content">
                                        <p class='text-justify'>The United Arab Emirates (UAE) is today an inspiring success story. Here, Bedouin and sleek urban living come together to form a nation that has mountains, beaches, deserts, and the world renowned duty free shopping in one package. The phenomenal growth of the UAE property market has seen the country emerge as the commercial centre of the Middle East. The rapid economic growth of the UAE, which can in turn be attributed to the UAE’s position as one of the world’s most preferred tourist destinations has also fuelled the demand for UAE property.</p>
                                        <p class='text-justify'>Property in the UAE, whether apartments, villas, or commercial property continues to witness increasing demand and has made the country the centre of all the property action in the world today</p>
                                    </div>

                                </div>

                                <div class="items-container row image-box style9">
                                    <div class="col-sm-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="{{url('/abudabhi/')}}">
                                                    <img width="370" height="191" alt="" src="{{url('uploads/destinations/overview/32.jpg')}}"></a>
                                            </figure>
                                            <div class="details">
                                                <h4 class="box-title">Abu Dhabi</h4>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="{{url('/ajman/')}}">
                                                    <img width="370" height="191" alt="" src="{{url('uploads/destinations/overview/33.jpg')}}"></a>
                                            </figure>
                                            <div class="details">
                                                <h4 class="box-title">Ajman</h4>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="{{url('/dubai/')}}">
                                                    <img width="370" height="191" alt="" src="{{url('uploads/destinations/overview/34.jpg')}}"></a>
                                            </figure>
                                            <div class="details">
                                                <h4 class="box-title">Dubai</h4>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="{{url('/fujairah/')}}">
                                                    <img width="370" height="191" alt="" src="{{url('uploads/destinations/overview/35.jpg')}}"></a>
                                            </figure>
                                            <div class="details">
                                                <h4 class="box-title">Fujairah</h4>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="{{url('/khaimah/')}}">
                                                    <img width="370" height="191" alt="" src="{{url('uploads/destinations/overview/36.jpg/')}}"></a>
                                            </figure>
                                            <div class="details">
                                                <h4 class="box-title">Ras Al Khaimah</h4>
                                            </div>
                                        </article>
                                    </div>
                                    <div class="col-sm-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="{{url('/sharjah/')}}">
                                                    <img width="370" height="191" alt="" src="{{url('uploads/destinations/overview/37.jpg/')}}"></a>
                                            </figure>
                                            <div class="details">
                                                <h4 class="box-title">Sharjah</h4>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar col-sm-4 col-md-3">

                            <div class="travelo-box search-stories-box">
                                <h4>Search Stories</h4>
                                <div>
                                    <div class="with-icon full-width">
                                        <input type="text" class="input-text full-width" placeholder="story name or category">
                                        <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="travelo-box book-with-us-box">
                                <h4>Why Book with us?</h4>
                                <ul>
                                    <li>
                                        <i class="soap-icon-hotel-1 circle"></i>
                                        <h5 class="title"><a href="#">135,00+ Hotels</a></h5>
                                        <p>We are dealing with more than 135,000 around the world.</p>
                                    </li>
                                    <li>
                                        <i class="soap-icon-savings circle"></i>
                                        <h5 class="title"><a href="#">Low Rates &amp; Savings</a></h5>
                                        <p>Guaranteed low rates with attractive payment methods.</p>
                                    </li>
                                    <li>
                                        <i class="soap-icon-support circle"></i>
                                        <h5 class="title"><a href="#">Excellent Support</a></h5>
                                        <p>24/7 emergency and customer support</p>
                                    </li>
                                </ul>
                            </div>

                            <div class="travelo-box contact-box">
                                <h4 class="box-title">Need {{sitename()}} Help?</h4>
                                <p>We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.</p>
                                <address class="contact-details">
        <span class="contact-phone"><i class="soap-icon-phone"></i>(+971) 4 336 6643</span>
        <br />
        <a href="#" class="contact-email">dubai@mynexttrip.com</a>
    </address>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


@endsection