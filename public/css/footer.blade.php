<footer id="footer">

    <div class="footer-wrapper" style="background: url(../public/images/bg.jpg);">

        <div class="container">

            <div class="row"> 

                <div class="col-sm-3 col-md-3">

                     <ul class="social-icons clearfix">

                        <li class="twitter"><a title="twitter" href="{{$companyData[0]->twitter}}" data-toggle="tooltip" target="_blank"><i class="soap-icon-twitter"></i></a></li>

                        <li class="facebook"><a title="facebook" href="{{$companyData[0]->facebook}}" data-toggle="tooltip" target="_blank"><i class="soap-icon-facebook"></i></a></li>

                        <li class="youtube"><a title="youtube" href="{{$companyData[0]->youtube}}" data-toggle="tooltip" target="_blank"><i class="soap-icon-youtube"></i></a></li>

                        <li class="instagram"><a title="instagram" href="{{$companyData[0]->instagram}}" data-toggle="tooltip" target="_blank"><i class="soap-icon-instagram"></i></a></li>

                         <li class="linkedin"><a title="linkedin" href="{{$companyData[0]->tiktok}}" data-toggle="tooltip" target="_blank"><i class="soap-icon-linkedin"></i></a></li>

                    </ul>

                </div>

                <div class="col-sm-4 col-md-4">

                    <p>Sign up for our mailing list to get latest updates and offers.</p>

                </div>

            <form name="form" method="post" action="{{url('/request/')}}"  id="">

                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="col-sm-5 col-md-5">

                    

                    <span id="" style="color:Red;display:none;">invalid email address</span>

                    <span id="" style="color:Red;display:none;">*</span>

                        <input name="asd" type="type" id="" class="input-text" placeholder="your email" required="" style="width: 60%;" />

                     <input type="submit" name="ctl00$Template_Footer$btn_booknow" value="GO"  class="buttonCustome" />

                   

                    <br />

                    <span>We respect your privacy</span>

                </div>

            </form>

            </div>

            <hr>

            <div class="row">

                <div class="col-sm-4 col-md-4">

                    <h2>Discover</h2>

                    <ul class="discover triangle hover row">

                    

                        <li class="col-xs-6"><a href="{{url('/safari')}}">{{$components[0]->safari}}</a></li>

                        

                      <li class="col-xs-6"><a href="{{url('/car/')}}">Car  Hiring</a></li>

                        <li class="col-xs-6"><a href="{{url('/groups')}}">Groups</a></li>

                        <li class="col-xs-6"><a href="{{url('/attractions')}}">{{$components[0]->guide}}</a></li>

                        <li class="col-xs-6"><a href="{{url('/aboutus/')}}">{{$components[0]->about}}</a></li>

                        <li class="col-xs-6"><a href="{{url('/contactus/')}}">Contact Us</a></li>

                       

                      

                    </ul>

                </div>

                  

                        <div class="col-sm-4 col-md-4">

                            <h2>{{sitename()}}</h2> <h2> News</h2>

                            <ul class="travel-news">



                                <li><div class='thumb'><a href=# target='_blank'><img src='#' alt='' width='63' height='63' /></a>

                                        </div><div class='description'><h5 class='s-title'><a href='#' target='_blank'>Hospitality Company of My Next Trip Group Rebranded to Jaz Hotel Group </a></h5></div></li><li><div class='thumb'><a href='#' target='_blank'><img src='#' alt='' width='63' height='63' /></a>

                                        </div><div class='description'><h5 class='s-title'><a href='#' target='_blank'>New Updates to MY Next Trip’s Bus Fleet </a></h5></div></li>





                              

                            </ul>

                        </div>



                

                

                <div class="col-sm-4 col-md-4">

                    <h2>About My Next Trip</h2>

                    <p>My Next Trip Dubai is a leading in-bound Tour Operator in the Middle East region, and offer travellers to the UAE the benefits of one of the most experienced travel organizations in the country</p>

                    <br />

                    <address class="contact-details">

                        <span class="contact-phone"><i class="soap-icon-phone"></i>{{$companyData[0]->Comp_Phone}}</span>

                        <br />

                        <a href="#" class="contact-email">{{$companyData[0]->Comp_Email}}</a>

                    </address>

                    

                </div>

            </div>

        </div>

    </div>

    <div class="bottom gray-area">

        <div class="container">

            <div class="logo pull-left">

              

            </div>

            <div class="pull-right">

                <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>

            </div>

            <div class="copyright pull-right">

                <p><a href="#">Privacy Statement</a>  - &copy; 2019 My Next Trip LLC</p>

            </div>

        </div>

    </div>

</footer> 

    <!-- Javascript -->

    <script type="text/javascript" src="{{url('js/jquery-1.11.1.min.js')}}"></script>

    <script type="text/javascript" src="{{url('js/jquery.noconflict.js')}}"></script>

    <script type="text/javascript" src="{{url('js/modernizr.2.7.1.min.js')}}"></script>

    <script type="text/javascript" src="{{url('js/jquery-migrate-1.2.1.min.js')}}"></script>

    <script type="text/javascript" src="{{url('js/jquery.placeholder.js')}}"></script>

    <script type="text/javascript" src="{{url('js/jquery-ui.1.10.4.min.js')}}"></script>



    <!-- Twitter Bootstrap -->

    <script type="text/javascript" src="{{url('js/bootstrap.js')}}"></script>



    <!-- load revolution slider scripts -->

    <script type="text/javascript" src="{{url('components/revolution_slider/js/jquery.themepunch.plugins.min.js')}}"></script>

    <script type="text/javascript" src="{{url('components/revolution_slider/js/jquery.themepunch.revolution.min.js')}}"></script>



    <!-- Flex Slider -->

    <script type="text/javascript" src="{{url('components/flexslider/jquery.flexslider-min.js')}}"></script>



    <!-- load BXSlider scripts -->

    <script type="text/javascript" src="{{url('components/jquery.bxslider/jquery.bxslider.min.js')}}"></script>



    <!-- parallax -->

    <script type="text/javascript" src="{{url('js/jquery.stellar.min.js')}}"></script>



    <!-- waypoint -->

    <script type="text/javascript" src="{{url('js/waypoints.min.js')}}"></script>



    <!-- load page Javascript -->

    <script type="text/javascript" src="{{url('js/theme-scripts.js')}}"></script>

    <script type="text/javascript" src="{{url('js/scripts.js')}}"></script>



    <script type="text/javascript">

        tjq(document).ready(function () {

            tjq('.revolution-slider').revolution(

            {

                dottedOverlay: "none",

                delay: 9000,

                startwidth: 1200,

                startheight: 646,

                onHoverStop: "on",

                hideThumbs: 10,

                fullWidth: "on",

                forceFullWidth: "on",

                navigationType: "none",

                shadow: 0,

                spinner: "spinner4",

                hideTimerBar: "on",

            });

        });

    </script>

    <script type="text/javascript">

        tjq(document).ready(function () {

            // calendar panel

            var cal = new Calendar();

            var unavailable_days = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];

            var price_arr = { 3: '$170', 4: '$170', 5: '$170', 6: '$170', 7: '$170', 8: '$170', 9: '$170', 10: '$170', 11: '$170', 12: '$170', 13: '$170', 14: '$170', 15: '$170', 16: '$170', 17: '$170' };



            var current_date = new Date();

            var current_year_month = (1900 + current_date.getYear()) + "-" + (current_date.getMonth() + 1);

            tjq("#select-month").find("[value='" + current_year_month + "']").prop("selected", "selected");

            cal.generateHTML(current_date.getMonth(), (1900 + current_date.getYear()), unavailable_days, price_arr);

            tjq(".calendar").html(cal.getHTML());



            tjq("#select-month").change(function () {

                var selected_year_month = tjq("#select-month option:selected").val();

                var year = parseInt(selected_year_month.split("-")[0], 10);

                var month = parseInt(selected_year_month.split("-")[1], 10);

                cal.generateHTML(month - 1, year, unavailable_days, price_arr);

                tjq(".calendar").html(cal.getHTML());

            });





            tjq(".goto-writereview-pane").click(function (e) {

                e.preventDefault();

                tjq('#hotel-features .tabs a[href="#hotel-write-review"]').tab('show')

            });



            // editable rating

            tjq(".editable-rating.five-stars-container").each(function () {

                var oringnal_value = tjq(this).data("original-stars");

                if (typeof oringnal_value == "undefined") {

                    oringnal_value = 0;

                } else {

                    //oringnal_value = 10 * parseInt(oringnal_value);

                }

                tjq(this).slider({

                    range: "min",

                    value: oringnal_value,

                    min: 0,

                    max: 5,

                    slide: function (event, ui) {



                    }

                });

            });

        });



        tjq('a[href="#map-tab"]').on('shown.bs.tab', function (e) {

            var center = panorama.getPosition();

            google.maps.event.trigger(map, "resize");

            map.setCenter(center);



            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);

            map.setStreetView(panorama);

        });

        tjq('a[href="#steet-view-tab"]').on('shown.bs.tab', function (e) {

            fenway = panorama.getPosition();

            panoramaOptions.position = fenway;

            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);

            map.setStreetView(panorama);

        });

        var map = null;

        var panorama = null;

        var fenway = new google.maps.LatLng(48.856614, 2.352222);

        var mapOptions = {

            center: fenway,

            zoom: 12

        };

        var panoramaOptions = {

            position: fenway,

            pov: {

                heading: 34,

                pitch: 10

            }

        };

        function initialize() {

            tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);

            map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);

            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);

            map.setStreetView(panorama);

        }

        google.maps.event.addDomListener(window, 'load', initialize);

    </script>





    

<script type="text/javascript">

//<![CDATA[

var Page_Validators =  new Array(document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1"), document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1"), document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2"), document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3"), document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4"), document.getElementById("ctl00_Template_Footer_RegularExpressionValidator1"), document.getElementById("ctl00_Template_Footer_RequiredFieldValidator1"));

//]]>

</script>



<script type="text/javascript">

//<![CDATA[

var ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1 = document.all ? document.all["ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1"] : document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1");

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1.controltovalidate = "ctl00_ContentPlaceHolder1_Outgoing_Details_txt_Name";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1.focusOnError = "t";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1.errormessage = "Required";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1.display = "Dynamic";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1.validationGroup = "SendTours";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator1.initialvalue = "";

var ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1 = document.all ? document.all["ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1"] : document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1");

ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1.controltovalidate = "ctl00_ContentPlaceHolder1_Outgoing_Details_txt_Email";

ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1.focusOnError = "t";

ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1.errormessage = "Invalid email address";

ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1.display = "Dynamic";

ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1.validationGroup = "SendTours";

ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";

ctl00_ContentPlaceHolder1_Outgoing_Details_RegularExpressionValidator1.validationexpression = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

var ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2 = document.all ? document.all["ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2"] : document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2");

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2.controltovalidate = "ctl00_ContentPlaceHolder1_Outgoing_Details_txt_Email";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2.focusOnError = "t";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2.errormessage = "Required";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2.display = "Dynamic";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2.validationGroup = "SendTours";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator2.initialvalue = "";

var ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3 = document.all ? document.all["ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3"] : document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3");

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3.controltovalidate = "ctl00_ContentPlaceHolder1_Outgoing_Details_txt_Phone";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3.focusOnError = "t";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3.errormessage = "Required";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3.display = "Dynamic";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3.validationGroup = "SendTours";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator3.initialvalue = "";

var ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4 = document.all ? document.all["ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4"] : document.getElementById("ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4");

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4.controltovalidate = "ctl00_ContentPlaceHolder1_Outgoing_Details_txt_arrival_date";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4.focusOnError = "t";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4.errormessage = "Required";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4.display = "Dynamic";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4.validationGroup = "SendTours";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";

ctl00_ContentPlaceHolder1_Outgoing_Details_RequiredFieldValidator4.initialvalue = "";

var ctl00_Template_Footer_RegularExpressionValidator1 = document.all ? document.all["ctl00_Template_Footer_RegularExpressionValidator1"] : document.getElementById("ctl00_Template_Footer_RegularExpressionValidator1");

ctl00_Template_Footer_RegularExpressionValidator1.controltovalidate = "ctl00_Template_Footer_txt_emailaddress";

ctl00_Template_Footer_RegularExpressionValidator1.focusOnError = "t";

ctl00_Template_Footer_RegularExpressionValidator1.errormessage = "invalid email address";

ctl00_Template_Footer_RegularExpressionValidator1.display = "Dynamic";

ctl00_Template_Footer_RegularExpressionValidator1.validationGroup = "newsletter";

ctl00_Template_Footer_RegularExpressionValidator1.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";

ctl00_Template_Footer_RegularExpressionValidator1.validationexpression = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

var ctl00_Template_Footer_RequiredFieldValidator1 = document.all ? document.all["ctl00_Template_Footer_RequiredFieldValidator1"] : document.getElementById("ctl00_Template_Footer_RequiredFieldValidator1");

ctl00_Template_Footer_RequiredFieldValidator1.controltovalidate = "ctl00_Template_Footer_txt_emailaddress";

ctl00_Template_Footer_RequiredFieldValidator1.focusOnError = "t";

ctl00_Template_Footer_RequiredFieldValidator1.errormessage = "*";

ctl00_Template_Footer_RequiredFieldValidator1.display = "Dynamic";

ctl00_Template_Footer_RequiredFieldValidator1.validationGroup = "newsletter";

ctl00_Template_Footer_RequiredFieldValidator1.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";

ctl00_Template_Footer_RequiredFieldValidator1.initialvalue = "";

//]]>

</script>





<script type="text/javascript">

//<![CDATA[



var Page_ValidationActive = false;

if (typeof(ValidatorOnLoad) == "function") {

    ValidatorOnLoad();

}



function ValidatorOnSubmit() {

    if (Page_ValidationActive) {

        return ValidatorCommonOnSubmit();

    }

    else {

        return true;

    }

}

        //]]>

</script>