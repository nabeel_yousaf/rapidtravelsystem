-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 10, 2020 at 04:03 AM
-- Server version: 10.3.22-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `havefuntour_production`
--

-- --------------------------------------------------------

--
-- Table structure for table `attractions`
--

DROP TABLE IF EXISTS `attractions`;
CREATE TABLE `attractions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attractions`
--

INSERT INTO `attractions` (`id`, `name`, `description`, `logo`, `is_enabled`, `created_at`, `updated_at`) VALUES
(11, 'Dubai', 'Dubai is located on the Persian Gulf, to the northeast of the United Arab Emirates. Dubai is the second largest emirate with an urban area of 3885 sq km and the city is roughly 35 sq km. However it will expand to twice that size with the addition of the man-made islands; the Waterfront, the three Palms, the World, the Universe, Dubailand, as well as the construction in the desert.\r\n\r\nOne of the most fascinating geographical aspects of Dubai is its Creek, which divides the city into two regions. Dubai Creek is made up of a natural 9.5 mile inlet in the Persian Gulf, around which the city’s trade developed. North of the Creek is called Deira, and Bur Dubai refers to the south where it joins the tourist and residential developments of Jumeirah along the coast.\r\n\r\nDubai also has the highest population, sharing its borders with Abu Dhabi to the south, Sharjah to the northeast and the Sultanate of Oman to the southeast.\r\n\r\nDue to the city’s unique geographical location it enjoys a strategic position which allows it to connect to all local Gulf States, as well as to East Africa and South Asia\r\n\r\nDubai Holidays\r\n\r\nDubai is one the most popular of the United Arab Emirates (UAE) for tourism and that’s little wonder given all that this cosmopolitan place has to offer. Its increase in popularity in the last few years has been unbelievable, changing from an occasionally used stop- over point convenient for shopping to a thriving full on tourist resort with plenty to do. Dubai is one of the world’s most exciting and dynamic cities. It’s a remarkable oasis in the desert combining ultra-luxurious accommodation, sandy beaches, unbelievable duty-free shopping and the allure of traditional Arabic culture. The city is packed with man-made wonders, like the self-styled “Seven Star” Burj al-Arab built on a huge artificial island and costing a staggering $650 million to build. Dubai is an Emirate and a world class city that is made for superlatives. From the man-made Palm Islands to the world\'s tallest building, the Burj Khalifa, which stands 828 meters high this place is exuberant, excessive and despite the global economic downturn, still seems to be going strong.\r\n\r\nIt’s seemingly impossible to keep up with the constant changes in this great city. They say you need to come back to Dubai every six months to check what new amenities, hotels and shopping has been added since the last trip. The UAE isn’t all man-made wonders, retail therapy to the extreme and 4×4 thrashes through desert dunes though! The inhabitants of the Gulf States have had a long relationship with camels, and the prize racing beasts are very highly valued. Many of the seven species of marine turtles in the world can be seen in the waters of the UAE: the green turtle, the hawksbill turtle, the loggerhead turtle and (if you are fortunate) the huge leatherback turtle. The green and hawksbill turtles nest on beaches of the UAE as well as regularly feeding in its waters.', '1581161331.jpg', 1, '2020-02-08 06:28:51', '2020-02-08 08:38:37'),
(12, 'Ajman', 'Located near the cities of Sharjah and Umm Al Quwain, Ajman is the smallest of all the emirates. Ajman comprises the two jurisdictions of Masfout and Manama, situated to the north of the Masafi-Dhaid main artery. Though the biggest draw in the area is Sharjah, Ajman encompasses some interesting attractions and sites for tourists. One of the main attractions in Ajman is the long expanse of powdery white sandy beach, situated along the Persian Gulf Coast, stretching for almost 20 miles.\r\n\r\nProminent in any Ajman travel guide is the eighteenth century fort located in the heart of the city. The fort has been renovated and turned into the local museum where the area’s history can be examined in more depth. Once serving as the Ruler’s palace, it became Ajman’s cardinal police station in 1970. Tourists visiting the museum are privy to old manuscripts, odes to traditional daily life in Ajman as well as many ancient artifacts and old weapons once used in battle. The museum is situated along the coast and close to many savory restaurants, and a number of high-end Sharjah hotels if opting to be based outside of the city.\r\n\r\nAjman is essentially a port city great for shopping and dining. The small city maintains a certain charm the other emirates lack, mostly due to its small size which creates a more intimate feel than many of the large, bustling cities. Though in the past it’s held less appeal than the other emirates, Ajman does feature a handful of interesting attractions. The naturally formed creek diffuses the inland and remains a major focal point in the city. The section of Ajman called Masfout is an important agricultural area providing much of the local produce found throughout the region. Masfout is noted for its vibrant marble and is bordered by the Hajar Mountains creating a picturesque backdrop.', '1581161422.jpg', 1, '2020-02-08 06:30:22', '2020-02-08 08:41:00'),
(13, 'Kartarpur', 'The Kartarpur Corridor is a visa-free border crossing and secure corridor connecting the Gurdwara Darbar Sahib in Pakistan to the border with India. The crossing allows Sikh devotees from India to visit the gurdwara in Kartarpur, 4.7 kilometres (2.9 miles) from the India–Pakistan border without a visa, creating a link which allows pilgrims holding Indian passports to easily visit both the Kartarpur shrine and Gurdwara Dera Baba Nanak on the Indian side of the border. Pakistani Sikhs are unable to use the border crossing, and cannot access the site without first obtaining an Indian visa or unless they work there.\r\n\r\nThe Kartarpur Corridor was first proposed in early 1999 by Atal Bihari Vajpayee and Nawaz Sharif, the prime ministers of India and Pakistan respectively, as part of the Delhi–Lahore Bus diplomacy.\r\n\r\nOn 26 November 2018, the foundation stone was laid down on the Indian side; two days later, on 28 November 2018, Pakistani Prime Minister Imran Khan did the same for the Pakistani side. The corridor was completed for the 550th birth anniversary of Guru Nanak on 12 November 2019. Khan said \"Pakistan believes that the road to prosperity of region and bright future of our coming generation lies in peace\", adding that \"Pakistan is not only opening the border but also their hearts for the Sikh community\". Indian Prime Minister Narendra Modi compared the decision by the two countries to go ahead with the corridor to the fall of the Berlin Wall in November 1989, saying that the project could help in easing tensions between the two countries.\r\n\r\nPreviously, Sikh pilgrims from India had to take a bus to Lahore to get to Kartarpur, which is a 125 kilometres (78 miles) journey, even though people on the Indian side of the border could also physically see Gurdwara Darbar Sahib Kartarpur from the Indian side, where an elevated observation platform was constructed.', '1582642723.jpg', 1, '2020-02-08 08:22:40', '2020-02-25 19:58:43'),
(14, 'Swat', 'The history of Swat valley goes back to around 2000 years ago. It was known as Udyana and later the name was changed to Suvastu. The valley was a very peaceful area for living until the 11th century. The game of power led to the disruption the valley and it was first conquered by Mahmud of Ghazni. The ritual of acquiring the lands continued and Swat valley was then taken over by the Yusufzais.\r\n\r\nIn the time period of 19th century, swat valley was under Akhund Sahib who believed in the Muslim law. At this time, the economy of Swat valley flourished due to agriculture and it became one of the significant trading areas for businesses.\r\n\r\nThe population of Swat valley is around 1,257,602 and they have a number of cultural group living in which the most prominent ones are Pakhtuns, Yusufzais, Kohistanis, Gujars and Awans.\r\n\r\nSwat Valley is known as the “mini Switzerland”. Its landscapes are a proof of natural beauty and it was one of the most visited areas and had a major tourism industry. There were events which led to the downfall of the tourism which was one of the major sources of income of the Swats economy. The conflicts between the Taliban and the Pakistani army have effected the valleys attractions. The whole conflict started in 1990s when Sufi Muhammad, a cleric figure tried to impose the sharia law on the people of swat valley. In 2007, his son in law tried to follow his foot step by imposing the religion with strength of arms. The Pakistan army took control of the situation and in 2008 the war of Swat was put to an end. Another factor that also contributed in diminishing the tourism industry include the floods in 2010 in which most of the infrastructure was destroyed including the roads and bridges leading to difficulties in traveling to Swat valley.', '1582631993.jpg', 1, '2020-02-08 08:23:05', '2020-02-25 16:59:53'),
(15, 'Naran', 'Naran is a medium sized town situated in upper kaghan valley which is a part of Khyber Pakhtun khwa province of Pakistan. It is one of the most beautiful part of northern areas in pakistan which is elevated 2500 meters above sea level. Its beauty captures a lot of people towards itself and thus it is a famous resort for tourists and trekkers. The weather of Naran is very cold. The ice on the moutain tops never melts, even in months of June and Jully there are glaciers and mountains are covered with snow.\r\nThe road to naran valley travells alongside with River Kunhar which Starts from the glaciers of Kaghan and flow deep down in mansehra.  The local languages of Naran Valley are Hindko and Gojri but every individual overthere can speak and understand urdu. The dress is Shalwar kameez with long bottom shalwars. Most of the people are tall. Average height of males was about 5”8’ or 5”9’.\r\nThe main items of Naran valley are dry fruits and Handicrafts. One of the handicrafts you should buy are the artistically carved Walnuts and another famous thing is the Namdas, the woolen felt rugs while woolen shawls, embroidered shawls and shirts are also avalibale.These places have cottage industries running and you can find yourself having excellent bargains. Hand made articles are displaced in the shops at the main market.', '1582632455.jpg', 1, '2020-02-08 08:23:31', '2020-02-25 17:07:35'),
(16, 'Skardu', 'The name \"Skardu\" is believed to be derived from the Balti word meaning \"a low land between two high places.\"[3] The two referenced “high places\" are Shigar city, and the high-altitude Satpara Lake.\r\n\r\nThe first mention of Skardu dates to the first half of the 16th century. Mirza Haidar (1499–1551) described Askardu in the 16th-century text Tarikh-i-Rashidi Baltistan as one of the districts of the area. The first mention of Skardu in European literature was made by Frenchman François Bernier (1625–1688), who mentions the city by the name of Eskerdou. After his mention, Skardu was quickly drawn into Asian maps produced in Europe, and was first mentioned as Eskerdow the map \"Indiae orientalis nec non insularum adiacentium nova descriptio\" by Dutch engraver Nicolaes Visscher II, published between 1680–1700.', '1582633060.jpg', 1, '2020-02-08 08:29:51', '2020-02-25 17:17:40'),
(17, 'Abu Dhabi', 'Abu Dhabi, the capital of the United Arab Emirates, has marked its territory as one of the top tourist destinations due to its world class infrastructure and interesting attractions. Until recently, Abu Dhabi boasted of a city of conventions and exhibitions; but today, it has also gained popularity for its entertainment options and shopping venues. iVarious areas and islands in Abu Dhabi are developed to promote tourism including the Corniche, and Yas and Saadiyat Island.s To provide in-depth information on the various things to do in Abu Dhabi, we have compiled a must visit list of top landmarks, shopping outlets, dining options and a variety of activities. Also, travellers will be informed on the various means of transportation and communication options in Abu Dhabi.\r\n\r\nAbu Dhabi is geographically located on the north-eastern part of the Persian Gulf in the Arabian Peninsula. The island city is located just 250 metres from the mainland which consists of many other suburbs linked to the emirate, Abu Dhabi. A special feature of the city includes the Abu Dhabi Corniche which offers the chance to walk, cycle or jog along the island’s coastline which has a breathtaking view.', '1581169225.png', 1, '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(18, 'Nathiagali', 'During British rule Nathia Gali, then part of Abbottabad tehsil of Hazara District, served as the summer headquarters of the Chief Commissioner of the (then) Peshawar division of the Punjab. The town along with Dunga Gali constituted a notified area under the Punjab Municipalities Act, 1891. The income in 1903-4 was Rs. 3,000 chiefly derived from a house tax, whilst expenditure was Rs. 1,900.\r\n\r\nA British Kashmiri family were once driven up to the highest point in Nathiagali only realising at the last moment that their driver and guide had never driven in snow before. This revelation occurred at the exact moment the minibus they were travelling in began sliding on the ice towards the edge of the mountain. Nathiagali has no road safety barriers after Muree, travellers are advised not to attempt to ascend in winter months at night without a competent driver and experienced guide.\r\n\r\nThe weather of Nathiagali remains cool, pleasant and foggy in summers (1 May to 31 August). During the monsoon season (1 July to 16 September), rain is expected almost every day. Cold winds start to chill the weather in autumn. winters (1 November to 28 February) are very cold and chilly. In December and January, heavy snowfall occurs here. The weather remains cold in spring. Here most comfortable weather is the summer season.[5] Frequent rainfall occurs here annually. Rainfall lies between 1650mm-1850mm annually.in winter temperature can drop to _10*C and in summer it rise to a high of 30*C.', '1582631622.jpg', 1, '2020-02-25 16:53:42', '2020-02-25 16:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `attraction_imgs`
--

DROP TABLE IF EXISTS `attraction_imgs`;
CREATE TABLE `attraction_imgs` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(11) DEFAULT NULL,
  `attraction_imgs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attraction_imgs`
--

INSERT INTO `attraction_imgs` (`id`, `attraction_id`, `attraction_imgs`, `created_at`, `updated_at`) VALUES
(22, 11, 'as0hc-112851-tHb.jpg', '2020-02-08 06:28:51', '2020-02-08 06:28:51'),
(23, 11, 'a16Mz-112851-nup.png', '2020-02-08 06:28:51', '2020-02-08 06:28:51'),
(24, 11, 'DMctu-112851-FX7.png', '2020-02-08 06:28:51', '2020-02-08 06:28:51'),
(25, 12, '0xWYW-113022-aiJ.jpg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(26, 12, 'COGZx-113022-Myt.jpeg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(27, 12, '3oL5k-113022-HTz.jpeg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(28, 12, 'd9rrM-113022-eem.jpg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(42, 17, 'i62QJ-014025-JQ9.jpg', '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(43, 17, 'uVbaB-014025-HnC.jpg', '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(44, 17, 'lhd63-014025-7xJ.png', '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(45, 17, '7pJDu-014025-sWI.png', '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(46, 18, 'Wykow-115342-dtN.jpg', '2020-02-25 16:53:42', '2020-02-25 16:53:42'),
(47, 18, 'YkjNd-115342-KWj.jpg', '2020-02-25 16:53:42', '2020-02-25 16:53:42'),
(48, 18, 'MiSpE-115342-cCF.jpg', '2020-02-25 16:53:42', '2020-02-25 16:53:42'),
(49, 18, 'S9IJQ-115342-3Ju.jpg', '2020-02-25 16:53:42', '2020-02-25 16:53:42'),
(50, 18, 'Y1Tcv-115342-lIf.jpg', '2020-02-25 16:53:42', '2020-02-25 16:53:42'),
(51, 15, 'jX8yG-120737-GJS.jpg', '2020-02-25 17:07:37', '2020-02-25 17:07:37'),
(52, 15, 'zGuBt-120737-wuL.jpg', '2020-02-25 17:07:37', '2020-02-25 17:07:37'),
(53, 15, 'BfR78-120737-cF1.jpg', '2020-02-25 17:07:37', '2020-02-25 17:07:37'),
(54, 15, 'pABbf-120737-P8c.jpg', '2020-02-25 17:07:37', '2020-02-25 17:07:37'),
(55, 15, 'SwUJy-120737-ChT.jpg', '2020-02-25 17:07:37', '2020-02-25 17:07:37'),
(56, 16, 'JiNU9-121701-PTE.jpg', '2020-02-25 17:17:01', '2020-02-25 17:17:01'),
(57, 16, 'nrq5v-121701-4Lu.jpg', '2020-02-25 17:17:01', '2020-02-25 17:17:01'),
(58, 16, 'YdJUy-121701-jk4.jpg', '2020-02-25 17:17:01', '2020-02-25 17:17:01'),
(59, 16, 'KNqTw-121701-HnS.jpg', '2020-02-25 17:17:01', '2020-02-25 17:17:01'),
(60, 16, '8rzsE-121701-qJ6.webp', '2020-02-25 17:17:01', '2020-02-25 17:17:01'),
(61, 16, '9Q2Y7-121701-rQC.jpg', '2020-02-25 17:17:01', '2020-02-25 17:17:01'),
(62, 16, 'isNvE-121701-WWK.jpg', '2020-02-25 17:17:01', '2020-02-25 17:17:01'),
(64, 13, 'daC6h-025602-i69.jpg', '2020-02-25 19:56:02', '2020-02-25 19:56:02'),
(65, 13, '09Owu-025602-w24.jpg', '2020-02-25 19:56:02', '2020-02-25 19:56:02'),
(66, 13, '9TClS-025602-Xz9.jpg', '2020-02-25 19:56:02', '2020-02-25 19:56:02'),
(67, 13, 'qXJwl-025910-Yhy.jpg', '2020-02-25 19:59:10', '2020-02-25 19:59:10');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `state_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 'Model  Town', '2020-01-29 07:35:44', '2020-01-29 07:35:44'),
(2, 2, 4, 'Behriya town', '2020-01-29 08:50:58', '2020-01-29 08:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `company_infornations`
--

DROP TABLE IF EXISTS `company_infornations`;
CREATE TABLE `company_infornations` (
  `id` int(10) UNSIGNED NOT NULL,
  `comp_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_live_dte` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Reg_Num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_City_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Stat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Country_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Comp_Tax_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) DEFAULT NULL,
  `CreatedBy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Logo_URL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Aboutus_Desc` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiktok` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_infornations`
--

INSERT INTO `company_infornations` (`id`, `comp_name`, `Comp_live_dte`, `Comp_Reg_Num`, `Comp_City_name`, `Comp_Stat_name`, `Comp_Country_name`, `Comp_Address`, `Comp_Phone`, `mobile`, `Comp_Tax_num`, `Comp_Email`, `is_enabled`, `CreatedBy`, `Logo_URL`, `Aboutus_Desc`, `created_at`, `updated_at`, `facebook`, `instagram`, `twitter`, `youtube`, `tiktok`) VALUES
(2, 'Lets Go Have Fun Tours', '2020-02-25 17:12:50', '001', '1', '4', '2', '310 Asad Plaza International Market M Block Model Town Lahore', '+92 42 32186439', '+92 313 487 6968', '1110s', 'info@havefuntour.com', NULL, 'admin', 'Logo_URL-2.png', 'This is Tour Company ....', '2020-01-29 08:57:59', '2020-02-25 22:12:50', 'https://www.facebook.com/LETSGOHAVEFUN007/', 'https://www.instagram.com/letsgohavefuntour/?hl=en', 'https://twitter.com/LetsGoHaveFunT2', 'http://www.youtube.com/c/mateenkhan007', 'tiktook');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

DROP TABLE IF EXISTS `components`;
CREATE TABLE `components` (
  `id` int(10) UNSIGNED NOT NULL,
  `home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `safari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guide` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `travel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `components`
--

INSERT INTO `components` (`id`, `home`, `tour`, `safari`, `service`, `guide`, `contact`, `about`, `travel`, `created_at`, `updated_at`) VALUES
(1, 'home', 'tour', 'Tours', 'Services', 'Attractions', 'Contact us', 'About Us', 'Travel', '2020-02-05 07:51:46', '2020-02-20 16:24:11');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'UAE', '2020-01-29 07:13:23', '2020-01-29 07:13:23'),
(2, 'Pakistan', '2020-01-29 07:34:39', '2020-01-29 07:34:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_08_27_133805_create_roles_table', 1),
(4, '2016_08_27_133922_create_permissions_table', 1),
(5, '2016_08_27_134037_create_permission_role_table', 1),
(6, '2016_08_27_134135_create_role_user_table', 1),
(7, '2020_01_11_192304_add_newFields_to_users_table', 1),
(8, '2020_01_12_081816_create_company_infornations_table', 1),
(9, '2020_01_12_164547_create_tours_table', 1),
(10, '2020_01_13_071443_create_tourimages_table', 1),
(11, '2020_01_15_102136_create_tour_type_names_table', 1),
(12, '2020_01_17_135046_create_package_ins_table', 1),
(13, '2020_01_17_145846_create_package_exes_table', 1),
(14, '2020_01_18_094125_create_tour_packages_table', 1),
(15, '2020_01_22_125755_create_post_requests_table', 2),
(16, '2020_01_23_114726_create_jobs_table', 3),
(17, '2020_01_29_120514_create_countries_table', 4),
(18, '2020_01_29_121443_create_states_table', 5),
(19, '2020_01_29_123155_create_cities_table', 6),
(20, '2020_01_31_142339_add_softdelete_to_tours_table', 7),
(21, '2020_02_01_095808_add_softdelete_to_table', 8),
(22, '2020_02_05_100523_add_socialAccounts_to_company_table', 9),
(23, '2020_02_05_110428_create_modules_table', 10),
(24, '2020_02_05_123050_create_components_table', 11),
(26, '2020_02_05_152219_create_attractions_table', 12),
(27, '2020_02_05_160607_create_attraction_imgs_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `package_exes`
--

DROP TABLE IF EXISTS `package_exes`;
CREATE TABLE `package_exes` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_exclude` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_exes`
--

INSERT INTO `package_exes` (`id`, `package_exclude`, `created_at`, `updated_at`) VALUES
(4, 'Pick up from your Hotel or Residence in Dubai or Sharjah', '2020-01-24 07:02:49', '2020-01-24 07:02:49'),
(5, 'Tanura performance', '2020-01-24 07:03:16', '2020-01-24 07:03:16'),
(6, 'Morning breakfast, tea and coffee', '2020-01-24 07:03:27', '2020-01-24 07:03:27'),
(7, 'Drop off at your Hotel', '2020-01-24 07:03:36', '2020-01-24 07:03:36');

-- --------------------------------------------------------

--
-- Table structure for table `package_ins`
--

DROP TABLE IF EXISTS `package_ins`;
CREATE TABLE `package_ins` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_include` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_ins`
--

INSERT INTO `package_ins` (`id`, `package_include`, `created_at`, `updated_at`) VALUES
(3, 'Dune Bashing in 4x4 vehicles', '2020-01-24 07:01:03', '2020-01-24 07:01:03'),
(4, 'Sunset Point (photograph opportunity)', '2020-01-24 07:01:23', '2020-01-24 07:01:23'),
(5, 'BBQ Dinner for Vegetarian and non-vegitarian', '2020-01-24 07:01:43', '2020-01-24 07:01:43'),
(6, 'Arabic Sweets and Fresh Fruits for desert', '2020-01-24 07:01:58', '2020-01-24 07:01:58'),
(7, 'Dune Bashing in 4x4 vehicles', '2020-01-24 07:02:18', '2020-01-24 07:02:18'),
(8, 'Packages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Air Ticket + Visa\r\n• Hotel\r\n• Transport', '2020-02-24 17:29:45', '2020-02-24 17:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permission_title`, `permission_slug`, `permission_description`, `created_at`, `updated_at`) VALUES
(1, 'Tour Management', 'tour_management', '<p>This is used to create Tour</p>', '2020-01-20 08:34:09', '2020-02-03 04:05:40'),
(2, 'User Management', 'user_management', '<p>This is used to update User</p>', '2020-01-20 08:34:09', '2020-02-03 04:05:55'),
(3, 'Booking management', 'booking_management', '<p>This is Only Create Booking&nbsp;</p>', '2020-02-01 05:16:13', '2020-02-03 04:06:09'),
(4, 'Configuration Management', 'configuration_management', '<p>Configuration Management</p>', '2020-02-03 04:05:08', '2020-02-03 04:05:08'),
(6, 'nothing', 'nothing', '<p>asd</p>', '2020-02-04 04:22:37', '2020-02-04 04:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(19, 1, 1, '2020-02-03 04:10:50', '2020-02-03 04:10:50'),
(32, 3, 3, '2020-02-04 04:23:22', '2020-02-04 04:23:22'),
(33, 4, 3, '2020-02-04 04:23:22', '2020-02-04 04:23:22'),
(34, 2, 6, '2020-02-04 04:24:32', '2020-02-04 04:24:32'),
(35, 3, 6, '2020-02-04 04:24:32', '2020-02-04 04:24:32'),
(36, 4, 6, '2020-02-04 04:24:32', '2020-02-04 04:24:32'),
(43, 2, 2, '2020-02-13 14:25:27', '2020-02-13 14:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `post_requests`
--

DROP TABLE IF EXISTS `post_requests`;
CREATE TABLE `post_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_date` date NOT NULL,
  `transport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pickup` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dropoff` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persons` int(11) DEFAULT NULL,
  `childs` int(11) NOT NULL,
  `request` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_requests`
--

INSERT INTO `post_requests` (`id`, `booking_date`, `transport`, `pickup`, `dropoff`, `name`, `nationality`, `email`, `phone`, `persons`, `childs`, `request`, `created_at`, `updated_at`) VALUES
(66, '2020-03-28', 'private', 'Lahore', 'lahore', 'Mateen', 'Pakistan', 'mateenkhan007@hotmail.com', '03214322086', 1, 1, 'Free', '2020-03-07 17:26:08', '2020-03-07 17:26:08');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_title`, `role_slug`, `role_description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '<p>This is test</p>', '2020-01-20 08:34:09', '2020-02-03 04:10:50'),
(2, 'Guest User', 'customer', '<p>This is test</p>', '2020-01-20 08:34:09', '2020-02-03 04:10:58'),
(3, 'Developer', 'developer', '<p>This is Developer</p>', '2020-02-01 05:09:54', '2020-02-01 09:30:39'),
(6, 'QA', 'qa', '<p>this is QA</p>', '2020-02-04 04:24:32', '2020-02-04 04:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-01-20 08:34:09', '2020-02-27 14:31:14'),
(2, 2, 2, '2020-01-20 08:34:09', '2020-02-01 07:32:41'),
(3, 3, 3, '2020-02-01 07:45:14', '2020-02-01 07:45:14'),
(4, 6, 4, '2020-02-04 04:29:19', '2020-02-04 04:29:43'),
(7, 2, 8, '2020-02-10 06:58:54', '2020-02-10 06:58:54'),
(8, 2, 9, '2020-02-13 14:22:31', '2020-02-17 13:27:20');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Abu Dahbi', '2020-01-29 07:20:43', '2020-01-29 07:22:19'),
(2, 1, 'Dubai', '2020-01-29 07:25:07', '2020-01-29 07:25:07'),
(3, 2, 'Lahore', '2020-01-29 07:34:57', '2020-01-29 07:34:57'),
(4, 2, 'Islamabad', '2020-01-29 08:50:11', '2020-01-29 08:50:11');

-- --------------------------------------------------------

--
-- Table structure for table `tourimages`
--

DROP TABLE IF EXISTS `tourimages`;
CREATE TABLE `tourimages` (
  `id` int(10) UNSIGNED NOT NULL,
  `comp_id` int(11) DEFAULT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `tour_images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tourimages`
--

INSERT INTO `tourimages` (`id`, `comp_id`, `tour_id`, `tour_images`, `created_at`, `updated_at`) VALUES
(58, 1, 68, 'sPOb5-121341-PPn.jpg', '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(59, 1, 68, 'mOinE-121341-mVg.jpg', '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(60, 1, 68, 'crUG6-121341-tsJ.jpg', '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(61, 1, 68, 'ft3ng-121342-GSp.jpg', '2020-01-24 07:13:42', '2020-01-24 07:13:42'),
(62, 1, 68, 'AWIGC-121342-hoP.jpg', '2020-01-24 07:13:42', '2020-01-24 07:13:42'),
(63, 1, 69, 'I62P5-121652-1Ot.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(64, 1, 69, 'xaijU-121652-CQb.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(65, 1, 69, 'RgnpB-121652-MP8.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(66, 1, 69, 'lN6YD-121652-L4H.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(67, 1, 69, 'cihNE-121652-5N7.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(68, 1, 69, 'C42hk-121652-Axg.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(69, 1, 69, 'XYunK-121652-1Lo.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(70, 1, 69, 'ZfGFo-121652-LPU.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(71, 1, 69, 'TieSG-121652-Wmh.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(72, 1, 69, 'LebHZ-121652-e6i.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(73, 1, 69, 'FcyXS-121652-dgU.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(74, 1, 69, 'Iz3cJ-121652-1jw.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(75, 1, 69, 'w6yPb-121652-R1I.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(76, 1, 70, 'RJcg6-121927-vGZ.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(77, 1, 70, 'B9WsH-121927-Iyj.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(78, 1, 70, 'oJPCK-121927-rAn.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(79, 1, 70, 'HBnEP-121927-pju.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(80, 1, 70, '3e6WT-121927-Qzv.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(81, 1, 70, 'POGTF-121927-IDx.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(82, 1, 70, 'ayPt1-121927-BKs.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(83, 1, 73, '055MB-123735-eeg.jpg', '2020-01-24 07:37:35', '2020-01-24 07:37:35'),
(84, 1, 73, 'h6R61-123735-ZgD.jpg', '2020-01-24 07:37:35', '2020-01-24 07:37:35'),
(85, 1, 77, 'mLjbc-010309-2X1.jpg', '2020-01-24 08:03:09', '2020-01-24 08:03:09'),
(144, 1, 89, 'wgbaD-071425-G22.jpg', '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(145, 1, 89, 'M83Lc-071425-bgU.jpg', '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(146, 1, 89, 'OevVM-071426-iMb.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(147, 1, 89, 'vEsrR-071426-nYf.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(148, 1, 89, '5gvPZ-071426-wjp.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(149, 1, 89, 'GdrXf-071426-SaW.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(150, 1, 90, 'YfIEV-071516-D4h.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(151, 1, 90, 'dE4yU-071516-pdv.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(152, 1, 90, 'v3BQT-071516-rX0.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(153, 1, 90, 'pH7sF-071516-1PC.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(154, 1, 90, 'cLE5d-071516-FPV.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(155, 1, 90, 'R7o0x-071516-XU8.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(156, 1, 91, 'rYDst-071601-V35.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(157, 1, 91, 'RxxoT-071601-UFC.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(158, 1, 91, 'spaRr-071601-CU5.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(159, 1, 91, 'voXFz-071601-Mnv.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(160, 1, 91, 'wBP7I-071601-cZt.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(161, 1, 92, 'fVERh-071701-L0e.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(162, 1, 92, 'cBizq-071701-db8.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(163, 1, 92, 'sugcM-071701-PBQ.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(164, 1, 92, 'XLtEu-071701-MX5.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(165, 1, 92, 'WTVh8-071701-4sG.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(166, 1, 93, 'Ah0eg-071744-nAr.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(167, 1, 93, 'OyPdc-071744-SqJ.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(168, 1, 93, '1S9eg-071744-71J.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(169, 1, 93, 'IqgfE-071744-IRu.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(170, 1, 93, 'pndAx-071744-XJA.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(171, 1, 93, 'T2oZe-071744-qHt.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(172, 1, 94, '5Oc9p-071829-rqT.jpg', '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(173, 1, 94, 'rx4bv-071829-gLC.jpg', '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(174, 1, 94, 'WmBOw-071830-EEA.jpg', '2020-01-27 02:18:30', '2020-01-27 02:18:30'),
(175, 1, 94, 'wdYKd-071830-WPC.jpg', '2020-01-27 02:18:30', '2020-01-27 02:18:30'),
(176, 1, 94, 'TuQiU-071830-3oi.jpg', '2020-01-27 02:18:30', '2020-01-27 02:18:30'),
(177, 1, 95, '8GI7t-071917-Z60.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(178, 1, 95, 'Yr9uJ-071917-2S8.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(179, 1, 95, '52WQV-071917-s7W.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(180, 1, 95, 'WE3tu-071917-IKh.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(181, 1, 95, 'Q0u2A-071917-Oky.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(182, 1, 95, 'H4ud4-071917-drq.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(183, 1, 95, 'Qs1Ga-071917-sDW.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(184, 1, 95, 'IT5f0-071917-3k6.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(185, 1, 96, 'YKbLo-072006-xJS.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(186, 1, 96, 'Tasak-072006-jdu.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(187, 1, 96, 'AkkQy-072006-iKN.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(188, 1, 96, 'nvX5c-072006-D0A.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(189, 1, 96, 'S2Mro-072006-W1x.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(190, 1, 97, 'nM6lw-072056-gtt.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(191, 1, 97, 'EEBeg-072056-zTR.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(192, 1, 97, 'lJnWP-072056-kHB.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(193, 1, 97, 'rVfj5-072056-kdo.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(194, 1, 97, 'dHoEi-072056-6pj.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(195, 1, 97, 'kIfNq-072056-GGu.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(196, 1, 97, 'Tkz64-072056-Cyt.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(197, 1, 98, '0AomK-072147-8gm.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(198, 1, 98, 'diHdP-072147-iDf.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(199, 1, 98, '2DjNc-072147-5p9.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(200, 1, 98, 'lqRJ9-072147-uTe.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(201, 1, 98, 'mipuB-072147-5Sk.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(202, 1, 98, 'j4aJp-072147-Prn.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(203, 1, 99, 'erUFb-072241-KAB.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(204, 1, 99, 'T1Z3x-072241-dhN.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(205, 1, 99, '29JDY-072241-5YU.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(206, 1, 99, 'dYZ47-072241-GVM.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(207, 1, 99, 'oNkGi-072241-6IF.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(208, 1, 99, 'CleEU-072242-v0j.jpg', '2020-01-27 02:22:42', '2020-01-27 02:22:42'),
(209, 1, 100, 't4yAy-072332-46p.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(210, 1, 100, 'D9kqa-072332-4u8.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(211, 1, 100, 'nMx6N-072332-lNR.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(212, 1, 100, 'YxtTU-072332-esI.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(213, 1, 100, 'gTyJg-072332-3iM.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(214, 1, 100, 'CLsQE-072332-LGG.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(215, 1, 100, 'nAGT1-072332-btj.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(216, 1, 101, 'LNuXJ-072430-TcG.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(217, 1, 101, 'F1n7N-072430-cDb.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(218, 1, 101, 'iLK9o-072430-u0h.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(219, 1, 101, 'ZsVni-072430-o0l.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(220, 1, 101, 'CkRCd-072430-yZ9.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(221, 1, 101, 'KlhNT-072430-1Iz.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(222, 1, 101, 'uy1Vx-072431-kOv.jpg', '2020-01-27 02:24:31', '2020-01-27 02:24:31'),
(223, 1, 101, 'GIlBT-072431-2tb.jpg', '2020-01-27 02:24:31', '2020-01-27 02:24:31'),
(224, 1, 102, 'Skk9O-072525-WDv.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(225, 1, 102, 'WWhQ5-072525-B71.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(226, 1, 102, 'WiilP-072525-kMB.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(227, 1, 102, 'Samqu-072525-mjF.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(228, 1, 102, '6nZTb-072525-TXX.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(229, 1, 103, 'KG5LU-072617-JMD.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(230, 1, 103, 'U5GOd-072617-itM.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(231, 1, 103, 'JjCWe-072617-mns.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(232, 1, 103, 'o4cfI-072617-31Y.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(233, 1, 103, 'DsCsl-072617-yYz.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(234, 1, 104, '69amN-072732-AjH.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(235, 1, 104, 'lA0ox-072732-QDw.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(236, 1, 104, 'ISzHJ-072732-5Wy.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(237, 1, 104, 'EASoq-072732-aUO.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(238, 1, 104, 'q3nnq-072732-3OS.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(239, 1, 104, 'SEZGo-072732-xEP.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(240, 1, 104, 'SHoEC-072732-euw.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(241, 1, 104, 'PWkju-072732-99y.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(242, 1, 104, 'QegL1-072732-75e.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(243, 1, 104, '9siJl-072732-ZkK.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(244, 1, 104, '4Q39K-072732-n7Y.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(245, 1, 104, '9TjFz-072732-Rvp.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(246, 1, 104, 'OnS4f-072732-lir.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(247, 1, 104, 'k7g40-072732-LD3.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(248, 1, 105, 'gih7I-072826-qxR.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(249, 1, 105, 'fw4kX-072826-SzT.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(250, 1, 105, 'vpTGa-072826-RYS.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(251, 1, 105, 'q4fvJ-072826-if1.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(252, 1, 105, 'WhLIm-072826-GkW.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(253, 1, 106, 'gtthg-072953-eOE.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(254, 1, 106, 'Hfh9N-072953-GdQ.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(255, 1, 106, '62kPA-072953-ihy.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(256, 1, 106, 'iLtG0-072953-bFt.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(257, 1, 106, 'Soe03-072953-Cu5.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(258, 1, 106, 'pIvc6-072953-TL8.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(259, 1, 107, 'u55Xf-073056-9wR.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(260, 1, 107, 'bkPPU-073056-bzp.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(261, 1, 107, 'H2mu9-073056-VCZ.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(262, 1, 107, 'FUfRj-073056-9em.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(263, 1, 107, 'kMPYM-073056-IFo.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(264, 1, 107, '9jriB-073056-7nW.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(265, 1, 108, 'K1jS1-073145-wj8.jpg', '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(266, 1, 108, 'LPdF6-073145-FiB.jpg', '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(267, 1, 108, 'bsuAU-073145-Hkb.jpg', '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(268, 1, 108, 'krYNq-073146-AtP.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(269, 1, 108, 'qzhPW-073146-y3C.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(270, 1, 108, 'O8Y8U-073146-e4b.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(271, 1, 108, 'RukfR-073146-73g.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(272, 1, 108, '7lwEr-073146-XO9.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(273, 1, 108, '04gzS-073146-ja1.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(274, 1, 108, 'JSIU7-073146-4cJ.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(275, 1, 109, 'iV4VP-073312-rVf.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(276, 1, 109, '9hpwh-073312-nMU.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(277, 1, 109, 'QNbRs-073312-jbS.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(278, 1, 109, 'Heg8M-073312-wy3.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(279, 1, 110, 'qT9xm-073456-QFW.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(280, 1, 110, '5TLU0-073456-UeL.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(281, 1, 110, 'AlHYK-073456-1aq.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(282, 1, 110, '2zI3q-073456-on9.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(283, 1, 110, 'wq8cp-073456-wWN.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(284, 1, 110, 'lDylO-073456-DJv.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(285, 1, 110, '9fBEv-073456-TJJ.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(286, 1, 110, 'r5Gzb-073456-fRk.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(287, 1, 111, 'iutXk-073549-y1q.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(288, 1, 111, 'CaX49-073549-oyl.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(289, 1, 111, '8YHF0-073549-Lxp.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(290, 1, 111, 'iB175-073549-at6.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(291, 1, 111, 'PXE6X-073549-2bm.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(292, 1, 111, 'SGOow-073549-YP4.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(293, 1, 111, 'tBslp-073549-YX2.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(294, 1, 111, 'ZZGwm-073549-jPL.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(295, 1, 112, 'n5hTR-073635-K38.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(296, 1, 112, '2lbY1-073635-GUw.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(297, 1, 112, 'LZkIA-073635-lFS.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(298, 1, 112, 'XRpQs-073635-Hrk.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(299, 1, 112, 'E2GuH-073635-AMG.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(300, 1, 112, 'pmiMU-073635-HzU.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(301, 1, 112, '0gGuI-073635-EIS.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(302, 1, 112, 'UZI6p-073635-zzE.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(303, 1, 113, 'QLpkk-073720-zWB.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(304, 1, 113, '0uSYQ-073720-ETp.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(305, 1, 113, 'OgTqY-073720-tI8.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(306, 1, 113, 'fpgqC-073720-ThW.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(307, 1, 113, 'dXTUp-073720-qFi.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(308, 1, 113, 'IaKjU-073720-qCI.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(309, 1, 113, '5hVqz-073720-Gr3.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(310, 1, 113, 'SeD78-073720-i5i.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(311, 1, 114, 'P1EnL-073805-FY8.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(312, 1, 114, 'u0MFx-073805-Xp3.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(313, 1, 114, 'H5bSd-073805-7po.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(314, 1, 114, 'M1s1Y-073805-osa.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(315, 1, 114, 'Zvtg2-073805-R8W.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(316, 1, 114, 'q8Yde-073805-nvg.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(317, 1, 115, 'eMyuY-073856-Atf.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(318, 1, 115, 'eexBS-073856-yLc.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(319, 1, 115, '1Q6pI-073856-N8H.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(320, 1, 115, 'VHkkQ-073856-8tI.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(321, 1, 115, 'wy889-073856-C2o.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(322, 1, 115, 'zDQBD-073856-iY8.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(323, 1, 115, 'edRiM-073856-47v.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(324, 1, 115, 'gMMJl-073856-qou.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(325, 1, 115, 'jRMVV-073856-UfZ.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(326, 1, 115, 'V0MOn-073856-oEY.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(327, 1, 115, 'rZkOQ-073856-3z7.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(328, 1, 116, '5OAbq-073944-IF3.jpg', '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(329, 1, 116, 'qnlyv-073944-Cwe.jpg', '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(330, 1, 116, 'FjUFF-073945-Eur.jpg', '2020-01-27 02:39:45', '2020-01-27 02:39:45'),
(331, 1, 116, 'GAm8m-073945-QWT.jpg', '2020-01-27 02:39:45', '2020-01-27 02:39:45'),
(332, 1, 116, 'qISp7-073945-LXX.jpg', '2020-01-27 02:39:45', '2020-01-27 02:39:45'),
(344, 1, 119, 'UYoc2-055904-s1V.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(345, 1, 119, 'PDIXk-055904-Xcl.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(346, 1, 119, 'Mhplb-055904-HvR.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(347, 1, 119, '1qGN2-055904-xwo.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(348, 1, 119, 'UO9yd-055904-jIa.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(349, 1, 119, 'ZzZao-055904-dUz.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(350, 1, 119, 'Ywi7R-055904-h5k.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(351, 1, 119, 'VnzSk-055904-HaJ.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(352, 1, 119, 'u6Cqg-055904-azE.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(353, 1, 119, 'UvbVp-055904-0EG.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(354, 1, 119, 'xdWG1-055904-v5S.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(355, 1, 119, 'ZED7v-055904-xz6.png', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(356, 1, 119, 'ykNOj-055904-0x6.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(357, 1, 119, '2j9qk-055904-nxH.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(358, 1, 119, 'eKWDK-055904-InV.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(359, 1, 119, 'osNmS-055904-LJo.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(360, 1, 120, 'yEu4Q-103449-J13.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(361, 1, 120, 'DXs2N-103449-Imr.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(362, 1, 120, 'Vbx3e-103449-iLV.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(363, 1, 120, 'j914L-103449-RFh.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(364, 1, 120, 'Y7FCw-103449-3nB.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(365, 1, 120, 'peCoC-103449-T7W.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(366, 1, 120, 'oHQ3P-103449-XVq.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(367, 1, 120, 'Ac4AT-103449-zQ2.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(368, 1, 120, 'f4dMd-103449-cCA.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(369, 1, 120, 'KWerm-103449-VDm.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(370, 1, 120, 'nVq9w-103449-CVR.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(371, 1, 120, 'T2zLG-103449-5iA.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(372, 1, 120, 'YP2PX-103449-Nqc.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(373, 1, 120, 'PjxLn-103449-pDW.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(374, 1, 120, 'nk2QU-103449-lNI.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(375, 1, 120, 'jwCs9-103449-Bul.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(376, 1, 120, 'lynmf-103449-lfL.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(377, 1, 120, 'kO7r1-103449-QAX.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(378, 1, 120, 'bcyUy-103449-i3s.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(379, 1, 118, 'J9waP-034625-Umf.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(380, 1, 118, 'o0z0Y-034625-mkS.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(381, 1, 118, 'bNgTC-034625-Lpg.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(382, 1, 118, 'FbKLr-034625-mvv.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(383, 1, 118, 'wXTZl-034625-fDJ.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(384, 1, 118, 'eeqCE-034625-xKt.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(385, 1, 118, 'oAsxS-034625-cBO.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(386, 1, 118, 'UfgSM-034625-EZw.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(387, 1, 118, '1IMaN-034625-VIq.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(388, 1, 118, 'kgTyO-034625-fsd.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(389, 2, 121, 'P8YGA-035009-74I.png', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(390, 2, 121, 'qG0Ek-035009-nKq.jpg', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(391, 2, 121, 'XQdKG-035009-kMF.jpg', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(392, 2, 121, 'cdhL4-035009-Jwt.jpg', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(394, 2, 122, '5iqAU-081806-MN9.png', '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(395, 2, 123, 'zI8QJ-112633-OiC.png', '2020-02-20 16:26:33', '2020-02-20 16:26:33'),
(402, NULL, 82, '4gljB-010937-3DQ.jpg', '2020-02-24 18:09:37', '2020-02-24 18:09:37'),
(403, NULL, 82, 'cKVNM-010937-rpy.jpg', '2020-02-24 18:09:37', '2020-02-24 18:09:37'),
(404, NULL, 82, 'W2S7s-010937-R7g.jpg', '2020-02-24 18:09:37', '2020-02-24 18:09:37'),
(412, 1, 84, 'ksxo1-013900-ilg.jpg', '2020-02-24 18:39:00', '2020-02-24 18:39:00'),
(413, 1, 84, 'I2rOh-013900-9qG.jpg', '2020-02-24 18:39:00', '2020-02-24 18:39:00'),
(414, 1, 84, 'P6Lyf-013900-yTz.jpg', '2020-02-24 18:39:00', '2020-02-24 18:39:00'),
(415, NULL, 83, '1beYH-014407-e0C.jpg', '2020-02-24 18:44:07', '2020-02-24 18:44:07'),
(416, NULL, 83, 'diLL3-014407-8WP.jpg', '2020-02-24 18:44:07', '2020-02-24 18:44:07'),
(417, NULL, 83, 'T8f47-014407-wL2.jpg', '2020-02-24 18:44:07', '2020-02-24 18:44:07'),
(418, NULL, 83, 'ZdYhT-014407-Jk0.jpg', '2020-02-24 18:44:07', '2020-02-24 18:44:07'),
(419, NULL, 83, 'QNcXO-014407-Bu8.jpg', '2020-02-24 18:44:07', '2020-02-24 18:44:07'),
(420, NULL, 83, 'BuxM2-014407-f85.jpg', '2020-02-24 18:44:07', '2020-02-24 18:44:07'),
(421, 2, 125, 'zUTY6-014821-4sS.jpg', '2020-02-24 18:48:21', '2020-02-24 18:48:21'),
(422, 2, 125, 'mIJsm-014821-yBp.jpg', '2020-02-24 18:48:21', '2020-02-24 18:48:21'),
(423, 2, 125, 'PQOvp-014821-Zk3.jpg', '2020-02-24 18:48:21', '2020-02-24 18:48:21'),
(424, 2, 125, 'ULQof-014821-hGX.jpg', '2020-02-24 18:48:21', '2020-02-24 18:48:21'),
(425, 2, 125, 'J9Fcn-014821-G2w.jpg', '2020-02-24 18:48:21', '2020-02-24 18:48:21'),
(426, 2, 126, 'yGDCI-015743-E8T.jpg', '2020-02-24 18:57:43', '2020-02-24 18:57:43'),
(427, 2, 126, 'DKGQP-015743-fCy.jpg', '2020-02-24 18:57:43', '2020-02-24 18:57:43'),
(428, 2, 126, 'Saa7m-015743-Kwd.jpg', '2020-02-24 18:57:43', '2020-02-24 18:57:43'),
(429, 2, 126, 'ppqT5-015743-bjO.jpg', '2020-02-24 18:57:43', '2020-02-24 18:57:43'),
(430, 2, 126, 'IXgLM-015743-mwk.jpg', '2020-02-24 18:57:43', '2020-02-24 18:57:43'),
(431, 2, 127, 'fKgeL-021737-Vxp.jpg', '2020-02-24 19:17:37', '2020-02-24 19:17:37'),
(432, 2, 127, '6ZH3q-021737-V52.jpg', '2020-02-24 19:17:37', '2020-02-24 19:17:37'),
(433, 2, 127, 'tGnab-021737-PaO.jpg', '2020-02-24 19:17:37', '2020-02-24 19:17:37'),
(434, 2, 127, 'hDu1c-021737-9LC.jpg', '2020-02-24 19:17:37', '2020-02-24 19:17:37'),
(435, 2, 127, 'UYbhb-021737-1o7.jpg', '2020-02-24 19:17:37', '2020-02-24 19:17:37'),
(436, 2, 128, 'x7pye-021945-zaH.jpg', '2020-02-24 19:19:45', '2020-02-24 19:19:45'),
(437, 2, 128, 'ZtG8e-021945-MKA.jpg', '2020-02-24 19:19:45', '2020-02-24 19:19:45'),
(438, 2, 128, 'oZu8y-021945-nSA.jpg', '2020-02-24 19:19:45', '2020-02-24 19:19:45'),
(439, 2, 128, 'KBWks-021945-WaC.jpg', '2020-02-24 19:19:45', '2020-02-24 19:19:45'),
(440, NULL, 117, 'avWR3-033630-mmP.jpg', '2020-02-24 20:36:30', '2020-02-24 20:36:30'),
(441, NULL, 117, 'JsG6y-033630-dxp.jpg', '2020-02-24 20:36:31', '2020-02-24 20:36:31'),
(442, NULL, 117, 'nlUQc-033631-o4T.jpg', '2020-02-24 20:36:31', '2020-02-24 20:36:31'),
(443, NULL, 117, 'E7auc-033631-wY6.jpg', '2020-02-24 20:36:31', '2020-02-24 20:36:31'),
(444, 2, 129, '4f1TF-034328-Z2T.jpg', '2020-02-24 20:43:28', '2020-02-24 20:43:28'),
(445, 2, 129, 'lJGuE-034328-l1W.jpg', '2020-02-24 20:43:28', '2020-02-24 20:43:28'),
(446, 2, 129, 'OW7Lp-034328-ZSO.jpg', '2020-02-24 20:43:28', '2020-02-24 20:43:28'),
(447, 2, 129, 'N6wYG-034328-Tc5.jpg', '2020-02-24 20:43:28', '2020-02-24 20:43:28'),
(448, 2, 130, 'wDsAj-035521-8bb.jpg', '2020-02-24 20:55:22', '2020-02-24 20:55:22'),
(449, 2, 130, '9W0mY-035522-b0g.jpg', '2020-02-24 20:55:22', '2020-02-24 20:55:22'),
(450, 2, 130, 'q9Sfa-035522-QF5.jpg', '2020-02-24 20:55:22', '2020-02-24 20:55:22'),
(458, NULL, 78, 'DJGwy-044908-pSt.jpg', '2020-02-24 21:49:08', '2020-02-24 21:49:08'),
(459, NULL, 78, 'GFFmn-044908-8CJ.jpg', '2020-02-24 21:49:08', '2020-02-24 21:49:08'),
(460, NULL, 78, 'j3Tap-044908-aUI.jpg', '2020-02-24 21:49:08', '2020-02-24 21:49:08'),
(461, NULL, 78, 'tZ1ay-044908-6DJ.jpg', '2020-02-24 21:49:08', '2020-02-24 21:49:08'),
(462, NULL, 78, '9ZohG-044908-NAl.jpg', '2020-02-24 21:49:08', '2020-02-24 21:49:08'),
(463, NULL, 78, 'z7wpy-044908-vkF.jpg', '2020-02-24 21:49:08', '2020-02-24 21:49:08'),
(464, NULL, 78, 'Ek1ZL-044908-KaL.jpg', '2020-02-24 21:49:08', '2020-02-24 21:49:08'),
(465, NULL, 79, 'jH05B-074348-H4x.jpg', '2020-02-25 12:43:48', '2020-02-25 12:43:48'),
(466, NULL, 79, 'jaPZR-074348-W5m.jpg', '2020-02-25 12:43:48', '2020-02-25 12:43:48'),
(467, NULL, 79, 'AXOS7-074348-BHz.jpg', '2020-02-25 12:43:48', '2020-02-25 12:43:48'),
(468, NULL, 79, 'yPV6G-074348-BaL.jpg', '2020-02-25 12:43:49', '2020-02-25 12:43:49'),
(469, NULL, 79, '8foAN-074349-7Xv.jpg', '2020-02-25 12:43:49', '2020-02-25 12:43:49'),
(470, NULL, 80, 'rzV1m-080620-2Ra.webp', '2020-02-25 13:06:20', '2020-02-25 13:06:20'),
(471, NULL, 80, 'tH1js-080620-Pxm.jpg', '2020-02-25 13:06:20', '2020-02-25 13:06:20'),
(472, NULL, 80, '4KgSc-080620-mtP.jpg', '2020-02-25 13:06:20', '2020-02-25 13:06:20'),
(473, NULL, 80, 'Oj5ku-080620-iE2.jpg', '2020-02-25 13:06:20', '2020-02-25 13:06:20'),
(474, 2, 131, 'KSif6-100131-4QT.jpg', '2020-02-25 15:01:31', '2020-02-25 15:01:31'),
(475, 2, 131, 'nSZyl-100131-8ce.jpg', '2020-02-25 15:01:31', '2020-02-25 15:01:31'),
(476, 2, 131, 'VVSYz-100131-TmP.jpg', '2020-02-25 15:01:31', '2020-02-25 15:01:31'),
(477, 2, 131, 'hqn2s-100131-wfq.jpg', '2020-02-25 15:01:31', '2020-02-25 15:01:31'),
(478, 2, 131, 'EKoUl-100131-4X0.jpg', '2020-02-25 15:01:31', '2020-02-25 15:01:31'),
(479, 2, 131, 'hnlro-100131-FUI.jpg', '2020-02-25 15:01:31', '2020-02-25 15:01:31'),
(480, 2, 132, 'ElEbb-110151-ZHO.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(481, 2, 132, 'RSyMc-110151-f14.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(482, 2, 132, 'jFPjq-110151-gIJ.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(483, 2, 132, 'IRt9X-110151-u1t.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(484, 2, 132, '6kfvW-110151-IyW.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(485, 2, 132, 'yNJOG-110151-1Va.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(486, 2, 132, 'epqrv-110151-N2s.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(487, 2, 132, 'FOx0R-110151-5Qt.webp', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(488, 2, 132, 'tbYJS-110151-ZOH.jpg', '2020-02-25 16:01:51', '2020-02-25 16:01:51'),
(489, 2, 133, 'UX4l0-010225-9lx.jpg', '2020-02-25 18:02:25', '2020-02-25 18:02:25'),
(490, 2, 133, 'qzL1h-010225-Ecp.jpg', '2020-02-25 18:02:25', '2020-02-25 18:02:25'),
(491, 2, 133, 'YCgrd-010225-ePS.jpg', '2020-02-25 18:02:25', '2020-02-25 18:02:25'),
(492, 2, 133, 'gZ4gu-010225-316.jpg', '2020-02-25 18:02:25', '2020-02-25 18:02:25'),
(493, 2, 133, 'CH1m3-010225-74f.jpg', '2020-02-25 18:02:25', '2020-02-25 18:02:25'),
(494, 2, 133, 'ZOfcZ-010225-MG6.jpg', '2020-02-25 18:02:25', '2020-02-25 18:02:25'),
(498, NULL, 134, 'ycoyy-035153-Ju0.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(499, NULL, 134, 'egJZ0-035153-mwn.webp', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(500, NULL, 134, 'XjP5d-035153-efK.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(501, NULL, 134, 'zC6Xq-035153-3KA.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(502, NULL, 134, '4kaSC-035153-TMA.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(503, NULL, 134, 'AheBb-035153-jHo.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(504, NULL, 134, '74Dnt-035153-AeK.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(505, NULL, 134, 'R9vgq-035153-4qr.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(506, NULL, 134, 'GIzNc-035153-wBz.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(507, NULL, 134, 'zMZbU-035153-ByV.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(508, NULL, 134, 'M4hMD-035153-T0v.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(509, NULL, 134, '9o4m3-035153-sB4.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(510, NULL, 134, 'bvseU-035153-RY2.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(511, NULL, 134, 'ls7mK-035153-Ndv.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(512, NULL, 134, 'mgVCo-035153-Kuq.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(513, NULL, 134, '7hQ6E-035153-N1T.jpg', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(514, NULL, 134, 'WVSmV-035153-Qby.webp', '2020-02-25 20:51:53', '2020-02-25 20:51:53'),
(527, NULL, 88, '8bUKc-041052-o1v.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(528, NULL, 88, 'Hjo5P-041052-bWt.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(529, NULL, 88, 'iI7OZ-041052-XrA.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(530, NULL, 88, 'oZOna-041052-vJO.webp', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(531, NULL, 88, 'kWEEI-041052-tBd.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(532, NULL, 88, 'v5nzD-041052-bVW.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(533, NULL, 88, 'OXa88-041052-XI8.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(534, NULL, 88, 'M0xDU-041052-wk9.webp', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(535, NULL, 88, 'lmFeM-041052-WD9.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(536, NULL, 88, '4ESXn-041052-q4A.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(537, NULL, 88, '9Epnv-041052-6nG.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(538, NULL, 88, 'yWzCc-041052-FEu.jpg', '2020-02-25 21:10:52', '2020-02-25 21:10:52'),
(539, 2, 135, 'jDP6Y-014559-GnN.jpg', '2020-02-26 18:45:59', '2020-02-26 18:45:59'),
(540, 2, 135, 'c1C3D-014559-BfO.jpg', '2020-02-26 18:45:59', '2020-02-26 18:45:59'),
(541, 2, 135, 'ULbZP-014559-PXw.jpg', '2020-02-26 18:45:59', '2020-02-26 18:45:59'),
(542, 2, 135, 'rIYEI-014559-OQl.jpg', '2020-02-26 18:45:59', '2020-02-26 18:45:59'),
(543, 2, 135, 'eRqw6-014559-9aP.jpg', '2020-02-26 18:45:59', '2020-02-26 18:45:59'),
(544, 2, 136, 'qMfdP-015932-AuP.jpg', '2020-02-26 18:59:32', '2020-02-26 18:59:32'),
(545, 2, 136, 'Ui1hF-015932-iJd.jpg', '2020-02-26 18:59:32', '2020-02-26 18:59:32'),
(546, 2, 136, 'QJZsA-015932-yrS.jpg', '2020-02-26 18:59:32', '2020-02-26 18:59:32'),
(547, 2, 136, 'G3u9c-015932-WNW.jpg', '2020-02-26 18:59:32', '2020-02-26 18:59:32'),
(548, 2, 136, 'rRTMf-015932-lnI.jpeg', '2020-02-26 18:59:32', '2020-02-26 18:59:32'),
(552, 2, 140, '3p01x-022534-126.jpg', '2020-02-26 19:25:34', '2020-02-26 19:25:34'),
(553, 2, 141, 'PFd9x-023708-M5S.png', '2020-02-26 19:37:08', '2020-02-26 19:37:08'),
(554, 2, 142, 'EL9XV-023906-91Y.png', '2020-02-26 19:39:06', '2020-02-26 19:39:06'),
(556, 2, 144, 'yjSHb-024720-Ymn.png', '2020-02-26 19:47:20', '2020-02-26 19:47:20'),
(559, 2, 147, '5E6OD-030629-Z7Q.png', '2020-02-26 20:06:29', '2020-02-26 20:06:29'),
(561, 2, 138, 'WMG7p-035654-Asq.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(562, 2, 138, 'GIKje-035654-yxl.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(563, 2, 138, 'nQMDo-035654-hg5.png', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(564, 2, 138, 'j8CVu-035654-N4y.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(565, 2, 138, 'ahEU2-035654-91c.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(566, 2, 138, 'UJMrn-035654-AMv.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(567, 2, 138, 'BsL35-035654-glv.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(568, 2, 138, 'FYk3E-035654-Iwt.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(569, 2, 138, 'PlFNh-035654-Xdy.jpg', '2020-02-27 20:56:54', '2020-02-27 20:56:54'),
(570, NULL, 139, '8i9f1-050941-MEW.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(571, NULL, 139, 'aYRSf-050941-0iG.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(572, NULL, 139, 'Lkv4E-050941-kXs.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(573, NULL, 139, 'OISyR-050941-WOh.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(574, NULL, 139, 'G0mZ3-050941-Z4x.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(575, NULL, 139, 'GBKdQ-050941-paM.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(576, NULL, 139, 'Rqeui-050941-Ngh.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(577, NULL, 139, 'sU0iS-050941-CDE.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(578, NULL, 139, 'pCkXq-050941-wyT.jpg', '2020-02-27 22:09:41', '2020-02-27 22:09:41'),
(579, NULL, 143, 'Fne1R-054648-lJd.jpg', '2020-02-27 22:46:48', '2020-02-27 22:46:48'),
(580, NULL, 143, 'yxsQo-054648-uSJ.jpg', '2020-02-27 22:46:48', '2020-02-27 22:46:48'),
(581, NULL, 143, 'LMf7m-054648-3Yk.jpg', '2020-02-27 22:46:49', '2020-02-27 22:46:49'),
(582, NULL, 143, 'eR56F-054649-Veg.jpg', '2020-02-27 22:46:49', '2020-02-27 22:46:49'),
(583, NULL, 143, 'cMsQr-054649-TL4.jpg', '2020-02-27 22:46:49', '2020-02-27 22:46:49'),
(584, NULL, 143, 'oHX0d-054649-BPo.jpg', '2020-02-27 22:46:49', '2020-02-27 22:46:49'),
(585, NULL, 146, 'Q9P2n-074932-Imq.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(586, NULL, 146, '7nGzI-074932-Mrd.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(587, NULL, 146, 'm6abK-074932-HVB.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(588, NULL, 146, '0EEkA-074932-k5x.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(589, NULL, 146, 'TqeZW-074932-zVr.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(590, NULL, 146, 'hU3I0-074932-1fk.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(591, NULL, 146, 'iguG0-074932-4iW.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(592, NULL, 146, 'ySuoA-074932-le7.jpg', '2020-02-28 00:49:32', '2020-02-28 00:49:32'),
(593, NULL, 145, '2J8YO-075503-zyS.jpg', '2020-02-28 00:55:03', '2020-02-28 00:55:03'),
(594, NULL, 145, 'lEiuj-075503-G82.jpg', '2020-02-28 00:55:03', '2020-02-28 00:55:03'),
(595, NULL, 145, 'JQTcn-075503-Pq1.jpg', '2020-02-28 00:55:03', '2020-02-28 00:55:03'),
(596, NULL, 145, 'pxhLc-075503-eHd.jpg', '2020-02-28 00:55:03', '2020-02-28 00:55:03'),
(597, NULL, 145, 'Ew9qp-075503-R6c.jpg', '2020-02-28 00:55:03', '2020-02-28 00:55:03'),
(598, NULL, 145, '8WbXc-075503-OPo.jpg', '2020-02-28 00:55:03', '2020-02-28 00:55:03'),
(599, NULL, 148, '089N4-080043-QUw.jpg', '2020-02-28 01:00:43', '2020-02-28 01:00:43'),
(600, NULL, 148, 'ANmuA-080043-85I.jpg', '2020-02-28 01:00:43', '2020-02-28 01:00:43'),
(601, NULL, 148, 'WvOPd-080043-AGU.jpg', '2020-02-28 01:00:43', '2020-02-28 01:00:43'),
(602, NULL, 148, 'bzI86-080043-efm.jpg', '2020-02-28 01:00:43', '2020-02-28 01:00:43'),
(603, NULL, 148, '2tGVJ-080043-2GZ.jpg', '2020-02-28 01:00:43', '2020-02-28 01:00:43'),
(604, NULL, 87, 'PmVuI-031457-R2G.jpg', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(605, NULL, 87, 'FTWPb-031457-nYd.jpg', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(606, NULL, 87, '0iI1q-031457-Xv6.jpg', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(607, NULL, 87, 'zxKIq-031457-uAS.webp', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(608, NULL, 87, 'mhAs3-031457-G3e.jpg', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(609, NULL, 87, '3hmMD-031457-LuH.jpg', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(610, NULL, 87, 'fEKM3-031457-ER7.png', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(611, NULL, 87, '1xS0A-031457-o32.jpg', '2020-03-03 20:14:57', '2020-03-03 20:14:57'),
(612, NULL, 85, 'CdnGC-035054-9F8.jpg', '2020-03-03 20:50:54', '2020-03-03 20:50:54'),
(613, NULL, 85, 'VDJrL-035054-ivn.jpg', '2020-03-03 20:50:54', '2020-03-03 20:50:54'),
(614, NULL, 85, 'NRoWr-035054-aIc.jpg', '2020-03-03 20:50:54', '2020-03-03 20:50:54'),
(615, NULL, 85, '91XrA-035054-TZq.jpg', '2020-03-03 20:50:54', '2020-03-03 20:50:54'),
(616, NULL, 85, 'lHQXM-035054-0fd.jpg', '2020-03-03 20:50:54', '2020-03-03 20:50:54'),
(617, NULL, 85, 'UJXsC-035054-fiM.jpg', '2020-03-03 20:50:54', '2020-03-03 20:50:54'),
(618, NULL, 86, 'RkHUB-040739-IIc.jpg', '2020-03-03 21:07:39', '2020-03-03 21:07:39'),
(619, NULL, 86, 'ZA0NS-040739-3zD.jpg', '2020-03-03 21:07:39', '2020-03-03 21:07:39'),
(620, NULL, 86, 'Wio0r-040739-oDz.jpg', '2020-03-03 21:07:39', '2020-03-03 21:07:39'),
(621, NULL, 86, '9uvOd-040739-bn8.jpg', '2020-03-03 21:07:39', '2020-03-03 21:07:39'),
(622, NULL, 86, 'qVhFl-040739-H2D.jpg', '2020-03-03 21:07:39', '2020-03-03 21:07:39'),
(623, NULL, 86, '8Fax7-040739-H0n.jpg', '2020-03-03 21:07:39', '2020-03-03 21:07:39'),
(624, 2, 149, 'sqpCa-024207-mwi.jpg', '2020-03-05 19:42:07', '2020-03-05 19:42:07'),
(625, 2, 149, 'kuOv8-024207-Zd8.jpg', '2020-03-05 19:42:07', '2020-03-05 19:42:07'),
(626, 2, 149, '9RAtR-024207-7nF.png', '2020-03-05 19:42:07', '2020-03-05 19:42:07'),
(627, 2, 149, 'rzdga-024207-7ew.jpg', '2020-03-05 19:42:07', '2020-03-05 19:42:07'),
(628, 2, 149, 'OLIZx-024207-2vr.jpg', '2020-03-05 19:42:07', '2020-03-05 19:42:07'),
(629, 2, 150, 'CNSap-024612-HVM.jpg', '2020-03-05 19:46:12', '2020-03-05 19:46:12'),
(630, 2, 150, 'AfLR3-024612-xw1.jpg', '2020-03-05 19:46:12', '2020-03-05 19:46:12'),
(631, 2, 150, 'MUsE9-024612-baE.jpg', '2020-03-05 19:46:12', '2020-03-05 19:46:12'),
(632, 2, 150, '1OmiJ-024612-ub3.jpg', '2020-03-05 19:46:12', '2020-03-05 19:46:12'),
(633, 2, 150, 'PwkVV-024612-14o.jpg', '2020-03-05 19:46:12', '2020-03-05 19:46:12'),
(634, 2, 150, 'LmnfW-024612-sU1.jpg', '2020-03-05 19:46:12', '2020-03-05 19:46:12'),
(636, 2, 152, 'gamEy-113459-Kjb.JPG', '2020-03-07 16:34:59', '2020-03-07 16:34:59');

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

DROP TABLE IF EXISTS `tours`;
CREATE TABLE `tours` (
  `id` int(10) UNSIGNED NOT NULL,
  `comp_id` int(11) DEFAULT NULL,
  `Tour_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Tour_Desc` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Tour_Adult_price` int(11) DEFAULT NULL,
  `Tour_Adult_disc_price` int(11) DEFAULT NULL,
  `Tour_Child_price` int(11) DEFAULT NULL,
  `Tour_Child_disc_price` int(11) DEFAULT NULL,
  `Tour_type_name` int(11) DEFAULT NULL,
  `TourType_Code` int(11) DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `Tour_Duration` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `includes` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excludes` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`id`, `comp_id`, `Tour_name`, `Tour_Desc`, `profile_image`, `Tour_Adult_price`, `Tour_Adult_disc_price`, `Tour_Child_price`, `Tour_Child_disc_price`, `Tour_type_name`, `TourType_Code`, `is_enabled`, `Tour_Duration`, `includes`, `excludes`, `currency`, `CreatedBy`, `created_at`, `updated_at`, `deleted_at`) VALUES
(78, NULL, 'Lahore City Tour', '<p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">City of Gardens and Historical Monuments - Lahore is the second largest city of Pakistan with a population of more than 10 million. Being the cultural, historical and art center of Pakistan, this beautiful city features marvelous sights for visitors who love sightseeing. Take our guided tours and avail the opportunity to explore the city\'s treasures.</p><p style=\"font-family: gudearegular; padding: 0px 15px; text-align: justify !important;\"><b style=\"\"><u style=\"background-color: rgb(255, 156, 0);\">MORNING TOUR</u></b></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Start at 09.00 AM -  Duration 04-05 hours</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">BADSHAHI MOSQUE (Built in 1673)</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Considered the world\'s largest living, historical mosque, this Islamic Worship centre was constructed using an attractive blend of white marble and red sand stone. The poet- phillospher \"Iqbal\" is buried at its footsteps.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">LAHORE FORT (Built in 1566 to 1673)</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><br>This is a marvel of Mughal architecture with its fabulous Shish Mahal (Hall of Mirrors) and superbly preserved Hall of Special and Common audience. Also experience the pleasure of walking through Pakistan’s only surviving Elephant -steps.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">JEHANGIR\'S TOMB (Built in 1673)</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Presenting a matchless display of mosaics and inlays, the tomb of the fourth Mughal Emperor is set magnificently in the clam surroundings of symmetrical gardens. An old travelers\'s motel leads to the stately entrance of the tomb.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">LAHORE MUSEUM (Established in 1864)</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; text-align: justify !important;\"><font color=\"#333333\">Displaying a complete cross- section of the culture and history of the region with rare manuscripts miniatures, Islamic calligraphy, contemporary arts and crafts. Pakistan\'s finest museum also contains one of the best collections of Buddhist art from the Gandhara period.</font><br><span style=\"background-color: rgb(255, 156, 0);\"> <b style=\"\"><u style=\"\">AFTERNOON TOUR</u></b></span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Start at 02.30 PM -  Duration: 03-04 hours</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">SHALIMAR GARDENS (Created in 1641)</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">\"Shalimar\" means \"the place of Love and Happiness\". with its triple - terraced gardens decorated with marble of pavilions, this is one of the world\'s finest Persian style gardens, laid down by Emprorer Shah Jahan</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">OLD CITY (Built during 16th Century A.D)</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Walled area, accessible through 12 gates, is certainly the highlight of Lahore. This fascinating area is a maze of interconnecting alleys and multistoried houses. Let us walk through its picturesque and crowded bazaars.</p>', '1582562890.jpg', 39, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-01-27 02:01:26', '2020-03-07 17:04:32', NULL),
(79, NULL, 'Khewra Salt Mine Tour', '<p><b>Package Overview:</b></p><p> The Khewra Salt Mine is located in Khewra, north of Pind Dadan Khan, in the salt range areas  of Jehlum district. </p><p>It is Pakistan\'s largest, the world\'s 2nd largest, and the oldest salt mine in the world.\r\n</p><p>\r\nKhewra Salt Mine is a major tourist attraction, with around 250,000 visitors a year.</p><p> They are taken into the mine on Khewra Salt mines Railways. There are numerous pools of salty water inside. </p><p>Do plan one day trip from Lahore to Khewra Salt mines. </p><p><b>\r\n\r\n Capacity :</b> Minimum 2 Pax</p><p><b>Places Includes:</b></p><ul><li>\r\nKalar Kahar Lake</li><li>Khewra Salt Mine</li></ul><p><b><br></b></p><p><b>Tour Includes:</b>\r\n</p><p>\r\nPick & Drop from Lahore Private Car with Driver\r\nFuel & toll taxes.</p><p><b>Tour Excludes:</b>\r\n</p><p>\r\nEntry Tickets\r\nMeals\r\nAny other expense\r\n\r\nOne day trip from lahore to Khewra Salt Mines itinerary.</p><p>Pick up from Lahore, enroute to Khewra Salt mines via motorway. </p><p>Evening back to Lahore.</p>', '1582616555.jpg', 60, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-01-27 02:02:32', '2020-02-26 19:09:43', NULL),
(80, NULL, 'Wagha Border', 'Amazing energy, amazing feel, amazing place, wonderful soldiers, and the flag ceremony was just wao.\r\n The visit must be early in the morning or in the evening before sunset... you can experience the passionate soldiers having a parade and flag raise ceremony every day from both sides of the border.. its a croudy. \r\nA perfect show of clockwise movement from both the Indian and Pakistan side\'s soldiers face to face in the flag lowering ceremony that take place every evening an hour before the sunset.\r\n\r\nOne day trip from lahore to Wagah Border Includes :\r\n\r\nPick & drop from Lahore\r\nGli car with Driver\r\nFuel & toll taxes\r\nOne day trip from lahore to  Wagah Border  Excludes :\r\n\r\nEntry Tickets\r\nMeals\r\nAny other expense\r\nOne day trip from lahore to Wagah Border itinerary :\r\n\r\nPick up from Lahore, enroute to  Wagah Border via Canal Road. Evening back to Lahore', '1582617915.webp', 39, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-01-27 02:03:39', '2020-02-25 21:25:48', NULL),
(82, NULL, 'Umrah - Ecnomical (15 Days)', 'Umrah Packages\r\nPakistan’s first online travel company is the complete platform which provides you cheap air tickets, discounted Umrah packages, advance booking , hotel and car services for almost 5000+ destinations. We always try to bring reasonable and comfortable deals to our valuable customers because our belief is that best deals and services \r\n\r\nNow it’s a religious announcement to all the followers of Islam that we have introduced two best Umrah packages because we know very well that every single Muslim want to perform Umrah in his/her life-period. Therefore we are here with affordable Umrah packages just for you.\r\n\r\n“Umrah is the holy obligations that can perform any time in the year. Hajjis visit all the sacred places which belong to Muhammad (P.B.U.H), His family and companions.”\r\n\r\nEconomy Package \r\n15 Days Umrah Package\r\nThe Economy Package, in which we provide less fare but distance is (700 M)\r\n\r\nSuperb Declaration:\r\nThe most titanic news is come for you that now just single person can also go for Umrah. Yes you read right, if you have no other partner to go with you than don’t be worry because your travel partner has solution of every problem. We deal single person Umrah services but under some age limitation. We provide him/her full package on the demand.\r\n\r\nMale:\r\nIf male demand to go single on Umrah then he must be 40+ for this demand below 40 don’t go single.\r\n\r\nFemale:\r\nIf any lady has no Mehram to go with her on Umrah then don’t be tense because now she can perform Umrah even with-out Mehram, just follow age limit condition. Female must be 45+ for single traveling.\r\n\r\nPackages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Visa\r\n• Hotel\r\n• Transport\r\nVisa Processing:\r\nAs you know we deal visa for specific international destinations, for Umrah & Hajj we give Visa services. Visa process almost complete with-in 15 days. If you have Pass-port then it must be valid for 8 months.\r\n\r\nPre-Requisite For Umrah Package:\r\n• Passport original with 8 months validity\r\n• 1 CNIC 14*6 cm\r\n• Blue back-ground photos (female pic in hijab, male pic without cap and glasses)\r\n• Vaccination card/ Polio Card for Child\r\n• 100% payment receives in advance for package.\r\n• If you demand cancellation after approval then charge PKR: 10,000 per head\r\nHotel Services:\r\nWe provide 3 star and 4 star hotel services to our customers. Rooms are available on sharing or separate on your demand. All rooms are Air-conditioned with free Wifi while Accommodation closes in Haram. Rooms have four specifications written down.\r\n\r\nQuint Room:\r\nit is for 5 persons. If you are travel in group then you book for family room or this room also available on sharing on your demand.\r\n\r\nQuad Room:\r\nit is available for 4 persons.\r\n\r\nDouble Room:\r\naccording to name we can judge that, only 2 people require for this room and as well sharing facility also available on it.\r\n\r\nTriple Room:\r\nthis required 3 persons or single person also book on sharing facility.', '1582551809.jpg', 39950, NULL, NULL, NULL, 2, NULL, 1, '15 Days', '', '', 'PKR', 0, '2020-01-27 02:05:25', '2020-02-24 20:47:18', NULL),
(83, NULL, 'Umrah - Super Economical (15 Days)', 'Umrah Super Economy \r\nPakistan’s first online travel company is the complete platform which provides you cheap air tickets, discounted Umrah packages, advance booking , hotel and car services for almost 5000+ destinations. We always try to bring reasonable and comfortable deals to our valuable customers because our belief is that best deals and services \r\n\r\nNow it’s a religious announcement to all the followers of Islam that we have introduced two best Umrah packages because we know very well that every single Muslim want to perform Umrah in his/her life-period. Therefore we are here with affordable Umrah packages just for you.\r\n\r\n“Umrah is the holy obligations that can perform any time in the year. Hajjis visit all the sacred places which belong to Muhammad (P.B.U.H), His family and companions.”\r\n\r\nEconomy Package \r\n15 Days Umrah Package\r\nThe Economy Package, in which we provide less fare but distance is 900 M with Shuttle Service.\r\n\r\nSuperb Declaration:\r\nThe most titanic news is come for you that now just single person can also go for Umrah. Yes you read right, if you have no other partner to go with you than don’t be worry because your travel partner has solution of every problem. We deal single person Umrah services but under some age limitation. We provide him/her full package on the demand.\r\n\r\nMale:\r\nIf male demand to go single on Umrah then he must be 40+ for this demand below 40 don’t go single.\r\n\r\nFemale:\r\nIf any lady has no Mehram to go with her on Umrah then don’t be tense because now she can perform Umrah even with-out Mehram, just follow age limit condition. Female must be 45+ for single traveling.\r\n\r\nPackages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Visa\r\n• Hotel\r\n• Transport\r\nVisa Processing:\r\nAs you know we deal visa for specific international destinations, for Umrah & Hajj we give Visa services. Visa process almost complete with-in 15 days. If you have Pass-port then it must be valid for 8 months.\r\n\r\nPre-Requisite For Umrah Package:\r\n• Passport original with 8 months validity\r\n• 1 CNIC 14*6 cm\r\n• Blue back-ground photos (female pic in hijab, male pic without cap and glasses)\r\n• Vaccination card/ Polio Card for Child\r\n• 100% payment receives in advance for package.\r\n• If you demand cancellation after approval then charge PKR: 10,000 per head\r\nHotel Services:\r\nWe provide 3 star and 4 star hotel services to our customers. Rooms are available on sharing or separate on your demand. All rooms are Air-conditioned with free Wifi while Accommodation closes in Haram. Rooms have four specifications written down.\r\n\r\nQuint Room:\r\nit is for 5 persons. If you are travel in group then you book for family room or this room also available on sharing on your demand.\r\n\r\nQuad Room:\r\nit is available for 4 persons.\r\n\r\nDouble Room:\r\naccording to name we can judge that, only 2 people require for this room and as well sharing facility also available on it.\r\n\r\nTriple Room:\r\nthis required 3 persons or single person also book on sharing facility.', '1582550550.jpg', 37000, NULL, NULL, NULL, 2, NULL, 1, '15 Days', '', '', 'PKR', 0, '2020-01-27 02:06:52', '2020-02-24 20:47:26', NULL),
(84, NULL, 'Umrah - Standard (15 Days)', 'Umrah Packages\r\nPakistan’s first online travel company is the complete platform which provides you cheap air tickets, discounted Umrah packages, advance booking , hotel and car services for almost 5000+ destinations. We always try to bring reasonable and comfortable deals to our valuable customers because our belief is that best deals and services \r\n\r\nNow it’s a religious announcement to all the followers of Islam that we have introduced two best Umrah packages because we know very well that every single Muslim want to perform Umrah in his/her life-period. Therefore we are here with affordable Umrah packages just for you.\r\n\r\n“Umrah is the holy obligations that can perform any time in the year. Hajjis visit all the sacred places which belong to Muhammad (P.B.U.H), His family and companions.”\r\n\r\nStandard Package \r\n15 Days Umrah Package\r\nThe Standard Package, in which we provide less fare and distance is (500 M).\r\n\r\nSuperb Declaration:\r\nThe most titanic news is come for you that now just single person can also go for Umrah. Yes you read right, if you have no other partner to go with you than don’t be worry because your travel partner has solution of every problem. We deal single person Umrah services but under some age limitation. We provide him/her full package on the demand.\r\n\r\nMale:\r\nIf male demand to go single on Umrah then he must be 40+ for this demand below 40 don’t go single.\r\n\r\nFemale:\r\nIf any lady has no Mehram to go with her on Umrah then don’t be tense because now she can perform Umrah even with-out Mehram, just follow age limit condition. Female must be 45+ for single traveling.\r\n\r\nPackages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Visa\r\n• Hotel\r\n• Transport\r\nVisa Processing:\r\nAs you know we deal visa for specific international destinations, for Umrah & Hajj we give Visa services. Visa process almost complete with-in 15 days. If you have Pass-port then it must be valid for 8 months.\r\n\r\nPre-Requisite For Umrah Package:\r\n• Passport original with 8 months validity\r\n• 1 CNIC 14*6 cm\r\n• Blue back-ground photos (female pic in hijab, male pic without cap and glasses)\r\n• Vaccination card/ Polio Card for Child\r\n• 100% payment receives in advance for package.\r\n• If you demand cancellation after approval then charge PKR: 10,000 per head\r\nHotel Services:\r\nWe provide 3 star and 4 star hotel services to our customers. Rooms are available on sharing or separate on your demand. All rooms are Air-conditioned with free Wifi while Accommodation closes in Haram. Rooms have four specifications written down.\r\n\r\nQuint Room:\r\nit is for 5 persons. If you are travel in group then you book for family room or this room also available on sharing on your demand.\r\n\r\nQuad Room:\r\nit is available for 4 persons.\r\n\r\nDouble Room:\r\naccording to name we can judge that, only 2 people require for this room and as well sharing facility also available on it.\r\n\r\nTriple Room:\r\nthis required 3 persons or single person also book on sharing facility.', '1582551678.jpg', 46000, NULL, NULL, NULL, 2, NULL, 1, '15 Days', '', '', 'PKR', 0, '2020-01-27 02:07:38', '2020-02-24 20:47:35', NULL),
(85, NULL, 'Mushkpuri Track Tour', 'MushkPuri Tour', '1583250622.jpg', 147, NULL, NULL, NULL, 5, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-01-27 02:08:23', '2020-03-03 20:53:22', NULL),
(86, NULL, 'Nathiyagaali Tour', 'Nathiyagaali Tour', '1583251658.jpg', 246, NULL, NULL, NULL, 5, NULL, 1, '4 Days', '', '', 'USD', 0, '2020-01-27 02:10:10', '2020-03-03 22:40:21', NULL),
(87, NULL, 'Naran Kaghan', 'Naran Kaghan', '1583248448.webp', 310, NULL, NULL, NULL, 5, NULL, 1, '5 Days', '', '', 'USD', 0, '2020-01-27 02:11:50', '2020-03-03 23:08:24', NULL),
(88, NULL, 'Rent A Car', 'Rent A Car', '1582646945.webp', 0, NULL, NULL, NULL, 1, NULL, 1, '0', '', '', 'PKR', 0, '2020-01-27 02:13:15', '2020-02-25 21:09:05', NULL),
(92, 1, 'International VISAs', 'International VISAs', '1580109421.jpg', 500, NULL, NULL, NULL, 1, NULL, 1, NULL, '', '', 'USD', 0, '2020-01-27 02:17:00', '2020-01-27 02:17:01', NULL),
(117, NULL, 'Dubai 5 Start Tour', 'Dubai 5* Tour\r\n- Visa\r\n- 04 Nights stay at 5 STAR HILTON DUBAI AL HABTOOR CITY hotel\r\n   COMPLIMENTARY daily Buffet breakfast at the hotel\r\n   Return Dubai Airport transfers\r\n   (COMPLIMENTRY UPGRADE to private basis)\r\n- Get Free 2 Complimentary LA  PERAL BRONZE tickets once worth USD 98/- Per Person\r\n- Half day Dubai City Tour with Free visit to\r\n  Dubai Mall Shopping Tour\r\n- Desert Safari Tour with BBQ Dinnner,Belly Dance Show,\r\n  Tanoora Dance Show and other camp activities\r\n- Visit to Miracle Garden and Global Village\r\n- Visit to Dubai Frame,the World\'s largest man made frame\r\n- Visit Glow Garden, Art Park and ice Park\r\n- This offer is Valid For 2 Adults only on double sharing basis and minimum 4 Night stay.', '1582558586.jpg', 500, NULL, NULL, NULL, 4, NULL, 1, '5 Days', '', '', 'USD', 0, '2020-01-27 02:40:26', '2020-02-25 21:28:53', NULL),
(125, NULL, 'Umrah - Economical (21 Days)', 'Umrah Packages\r\nPakistan’s first online travel company is the complete platform which provides you cheap air tickets, discounted Umrah packages, advance booking , hotel and car services for almost 5000+ destinations. We always try to bring reasonable and comfortable deals to our valuable customers because our belief is that best deals and services \r\n\r\nNow it’s a religious announcement to all the followers of Islam that we have introduced two best Umrah packages because we know very well that every single Muslim want to perform Umrah in his/her life-period. Therefore we are here with affordable Umrah packages just for you.\r\n\r\n“Umrah is the holy obligations that can perform any time in the year. Hajjis visit all the sacred places which belong to Muhammad (P.B.U.H), His family and companions.”\r\n\r\nEconomical Package \r\n21 Days Umrah Package\r\nThe Standard Package, in which we provide less fare and distance is (700 M).\r\n\r\nSuperb Declaration:\r\nThe most titanic news is come for you that now just single person can also go for Umrah. Yes you read right, if you have no other partner to go with you than don’t be worry because your travel partner has solution of every problem. We deal single person Umrah services but under some age limitation. We provide him/her full package on the demand.\r\n\r\nMale:\r\nIf male demand to go single on Umrah then he must be 40+ for this demand below 40 don’t go single.\r\n\r\nFemale:\r\nIf any lady has no Mehram to go with her on Umrah then don’t be tense because now she can perform Umrah even with-out Mehram, just follow age limit condition. Female must be 45+ for single traveling.\r\n\r\nPackages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Visa\r\n• Hotel\r\n• Transport\r\nVisa Processing:\r\nAs you know we deal visa for specific international destinations, for Umrah & Hajj we give Visa services. Visa process almost complete with-in 15 days. If you have Pass-port then it must be valid for 8 months.\r\n\r\nPre-Requisite For Umrah Package:\r\n• Passport original with 8 months validity\r\n• 1 CNIC 14*6 cm\r\n• Blue back-ground photos (female pic in hijab, male pic without cap and glasses)\r\n• Vaccination card/ Polio Card for Child\r\n• 100% payment receives in advance for package.\r\n• If you demand cancellation after approval then charge PKR: 10,000 per head\r\nHotel Services:\r\nWe provide 3 star and 4 star hotel services to our customers. Rooms are available on sharing or separate on your demand. All rooms are Air-conditioned with free Wifi while Accommodation closes in Haram. Rooms have four specifications written down.\r\n\r\nQuint Room:\r\nit is for 5 persons. If you are travel in group then you book for family room or this room also available on sharing on your demand.\r\n\r\nQuad Room:\r\nit is available for 4 persons.\r\n\r\nDouble Room:\r\naccording to name we can judge that, only 2 people require for this room and as well sharing facility also available on it.\r\n\r\nTriple Room:\r\nthis required 3 persons or single person also book on sharing facility.', '1582552101.jpg', 45000, NULL, NULL, NULL, 2, NULL, 1, '21 Days', '', '', 'PKR', 0, '2020-02-24 18:48:21', '2020-03-03 18:35:25', NULL),
(126, NULL, 'Umrah - Standard (21 Days)', 'Umrah Packages\r\nPakistan’s first online travel company is the complete platform which provides you cheap air tickets, discounted Umrah packages, advance booking , hotel and car services for almost 5000+ destinations. We always try to bring reasonable and comfortable deals to our valuable customers because our belief is that best deals and services \r\n\r\nNow it’s a religious announcement to all the followers of Islam that we have introduced two best Umrah packages because we know very well that every single Muslim want to perform Umrah in his/her life-period. Therefore we are here with affordable Umrah packages just for you.\r\n\r\n“Umrah is the holy obligations that can perform any time in the year. Hajjis visit all the sacred places which belong to Muhammad (P.B.U.H), His family and companions.”\r\n\r\nStandard Package \r\n21 Days Umrah Package\r\nThe Standard Package, in which we provide less fare and distance is (500 M).\r\n\r\nSuperb Declaration:\r\nThe most titanic news is come for you that now just single person can also go for Umrah. Yes you read right, if you have no other partner to go with you than don’t be worry because your travel partner has solution of every problem. We deal single person Umrah services but under some age limitation. We provide him/her full package on the demand.\r\n\r\nMale:\r\nIf male demand to go single on Umrah then he must be 40+ for this demand below 40 don’t go single.\r\n\r\nFemale:\r\nIf any lady has no Mehram to go with her on Umrah then don’t be tense because now she can perform Umrah even with-out Mehram, just follow age limit condition. Female must be 45+ for single traveling.\r\n\r\nPackages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Visa\r\n• Hotel\r\n• Transport\r\nVisa Processing:\r\nAs you know we deal visa for specific international destinations, for Umrah & Hajj we give Visa services. Visa process almost complete with-in 15 days. If you have Pass-port then it must be valid for 8 months.\r\n\r\nPre-Requisite For Umrah Package:\r\n• Passport original with 8 months validity\r\n• 1 CNIC 14*6 cm\r\n• Blue back-ground photos (female pic in hijab, male pic without cap and glasses)\r\n• Vaccination card/ Polio Card for Child\r\n• 100% payment receives in advance for package.\r\n• If you demand cancellation after approval then charge PKR: 10,000 per head\r\nHotel Services:\r\nWe provide 3 star and 4 star hotel services to our customers. Rooms are available on sharing or separate on your demand. All rooms are Air-conditioned with free Wifi while Accommodation closes in Haram. Rooms have four specifications written down.\r\n\r\nQuint Room:\r\nit is for 5 persons. If you are travel in group then you book for family room or this room also available on sharing on your demand.\r\n\r\nQuad Room:\r\nit is available for 4 persons.\r\n\r\nDouble Room:\r\naccording to name we can judge that, only 2 people require for this room and as well sharing facility also available on it.\r\n\r\nTriple Room:\r\nthis required 3 persons or single person also book on sharing facility.', '1582552663.jpg', 52000, NULL, NULL, NULL, 2, NULL, 1, '21 Days', '', '', 'PKR', 0, '2020-02-24 18:57:43', '2020-02-24 20:48:02', NULL),
(127, NULL, 'Umrah - Ecnomical (28 Days)', 'Umrah Packages\r\nPakistan’s first online travel company is the complete platform which provides you cheap air tickets, discounted Umrah packages, advance booking , hotel and car services for almost 5000+ destinations. We always try to bring reasonable and comfortable deals to our valuable customers because our belief is that best deals and services \r\n\r\nNow it’s a religious announcement to all the followers of Islam that we have introduced two best Umrah packages because we know very well that every single Muslim want to perform Umrah in his/her life-period. Therefore we are here with affordable Umrah packages just for you.\r\n\r\n“Umrah is the holy obligations that can perform any time in the year. Hajjis visit all the sacred places which belong to Muhammad (P.B.U.H), His family and companions.”\r\n\r\nEconomical Package \r\n28 Days Umrah Package\r\nThe Economical Package, in which we provide less fare and distance is (700 M).\r\n\r\nSuperb Declaration:\r\nThe most titanic news is come for you that now just single person can also go for Umrah. Yes you read right, if you have no other partner to go with you than don’t be worry because your travel partner has solution of every problem. We deal single person Umrah services but under some age limitation. We provide him/her full package on the demand.\r\n\r\nMale:\r\nIf male demand to go single on Umrah then he must be 40+ for this demand below 40 don’t go single.\r\n\r\nFemale:\r\nIf any lady has no Mehram to go with her on Umrah then don’t be tense because now she can perform Umrah even with-out Mehram, just follow age limit condition. Female must be 45+ for single traveling.\r\n\r\nPackages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Visa\r\n• Hotel\r\n• Transport\r\nVisa Processing:\r\nAs you know we deal visa for specific international destinations, for Umrah & Hajj we give Visa services. Visa process almost complete with-in 15 days. If you have Pass-port then it must be valid for 8 months.\r\n\r\nPre-Requisite For Umrah Package:\r\n• Passport original with 8 months validity\r\n• 1 CNIC 14*6 cm\r\n• Blue back-ground photos (female pic in hijab, male pic without cap and glasses)\r\n• Vaccination card/ Polio Card for Child\r\n• 100% payment receives in advance for package.\r\n• If you demand cancellation after approval then charge PKR: 10,000 per head\r\nHotel Services:\r\nWe provide 3 star and 4 star hotel services to our customers. Rooms are available on sharing or separate on your demand. All rooms are Air-conditioned with free Wifi while Accommodation closes in Haram. Rooms have four specifications written down.\r\n\r\nQuint Room:\r\nit is for 5 persons. If you are travel in group then you book for family room or this room also available on sharing on your demand.\r\n\r\nQuad Room:\r\nit is available for 4 persons.\r\n\r\nDouble Room:\r\naccording to name we can judge that, only 2 people require for this room and as well sharing facility also available on it.\r\n\r\nTriple Room:\r\nthis required 3 persons or single person also book on sharing facility.', '1582553857.jpg', 49000, NULL, NULL, NULL, 2, NULL, 1, '28 Days', '', '', 'PKR', 0, '2020-02-24 19:17:37', '2020-02-24 20:48:20', NULL),
(128, NULL, 'Umrah - Standard (28 Days)', 'Umrah Packages\r\nPakistan’s first online travel company is the complete platform which provides you cheap air tickets, discounted Umrah packages, advance booking , hotel and car services for almost 5000+ destinations. We always try to bring reasonable and comfortable deals to our valuable customers because our belief is that best deals and services \r\n\r\nNow it’s a religious announcement to all the followers of Islam that we have introduced two best Umrah packages because we know very well that every single Muslim want to perform Umrah in his/her life-period. Therefore we are here with affordable Umrah packages just for you.\r\n\r\n“Umrah is the holy obligations that can perform any time in the year. Hajjis visit all the sacred places which belong to Muhammad (P.B.U.H), His family and companions.”\r\n\r\nStandard Package \r\n28 Days Umrah Package\r\nThe Standard Package, in which we provide less fare and distance is (500 M).\r\n\r\nSuperb Declaration:\r\nThe most titanic news is come for you that now just single person can also go for Umrah. Yes you read right, if you have no other partner to go with you than don’t be worry because your travel partner has solution of every problem. We deal single person Umrah services but under some age limitation. We provide him/her full package on the demand.\r\n\r\nMale:\r\nIf male demand to go single on Umrah then he must be 40+ for this demand below 40 don’t go single.\r\n\r\nFemale:\r\nIf any lady has no Mehram to go with her on Umrah then don’t be tense because now she can perform Umrah even with-out Mehram, just follow age limit condition. Female must be 45+ for single traveling.\r\n\r\nPackages Services:\r\nIn latest Umrah Packages you get avail all the facilities which requires on that holy place. We list down all the facilities for your convenience because we always want to give you simple, easy and secure travel.\r\n\r\n• Visa\r\n• Hotel\r\n• Transport\r\nVisa Processing:\r\nAs you know we deal visa for specific international destinations, for Umrah & Hajj we give Visa services. Visa process almost complete with-in 15 days. If you have Pass-port then it must be valid for 8 months.\r\n\r\nPre-Requisite For Umrah Package:\r\n• Passport original with 8 months validity\r\n• 1 CNIC 14*6 cm\r\n• Blue back-ground photos (female pic in hijab, male pic without cap and glasses)\r\n• Vaccination card/ Polio Card for Child\r\n• 100% payment receives in advance for package.\r\n• If you demand cancellation after approval then charge PKR: 10,000 per head\r\nHotel Services:\r\nWe provide 3 star and 4 star hotel services to our customers. Rooms are available on sharing or separate on your demand. All rooms are Air-conditioned with free Wifi while Accommodation closes in Haram. Rooms have four specifications written down.\r\n\r\nQuint Room:\r\nit is for 5 persons. If you are travel in group then you book for family room or this room also available on sharing on your demand.\r\n\r\nQuad Room:\r\nit is available for 4 persons.\r\n\r\nDouble Room:\r\naccording to name we can judge that, only 2 people require for this room and as well sharing facility also available on it.\r\n\r\nTriple Room:\r\nthis required 3 persons or single person also book on sharing facility.', '1582553985.jpg', 59000, NULL, NULL, NULL, 2, NULL, 1, '28 Days', '', '', 'PKR', 0, '2020-02-24 19:19:45', '2020-02-24 20:48:31', NULL),
(129, NULL, 'Dubai 5 Star Tour', 'Dubai 5* Tour\r\n- Visa\r\n- 04 Nights stay at 5 STAR V CURIO COLLECTION BY HILTON hotel\r\n   COMPLIMENTARY daily Buffet breakfast at the hotel\r\n   Return Dubai Airport transfers\r\n   (COMPLIMENTRY UPGRADE to private basis)\r\n- Get Free 2 Complimentary LA  PERAL BRONZE tickets once worth USD 98/- Per Person\r\n- Half day Dubai City Tour with Free visit to\r\n  Dubai Mall Shopping Tour\r\n- Desert Safari Tour with BBQ Dinnner,Belly Dance Show,\r\n  Tanoora Dance Show and other camp activities\r\n- Visit to Miracle Garden and Global Village\r\n- Visit to Dubai Frame,the World\'s largest man made frame\r\n- Visit Glow Garden, Art Park and ice Park\r\n- This offer is Valid For 2 Adults only on double sharing basis and minimum 4 Night stay.', '1582559008.jpg', 515, NULL, NULL, NULL, 4, NULL, 1, '5 Days', '', '', 'USD', 0, '2020-02-24 20:43:28', '2020-02-25 21:26:25', NULL),
(130, NULL, 'Dubai Economical Tour', 'Dubai Tour\r\n- Visa\r\n- 04 Nights stay at Panorama hotel,Bur Dubai \r\n   COMPLIMENTARY daily Buffet breakfast at the hotel\r\n   Return Dubai Airport on private basis\r\n- Half day Dubai City Tour \r\n- Desert Safari Tour with BBQ Dinnner,Belly Dance Show,\r\n  Tanoora Dance Show and other camp activities\r\n- Visit to Miracle Garden and Global Village\r\n- Visit IMG PARK ACCESS\r\n- This offer is Valid For 1 Adults only on double sharing basis and minimum 4 Night stay.', '1582559784.jpg', 280, NULL, NULL, NULL, 4, NULL, 1, '5 Days', '', '', 'USD', 0, '2020-02-24 20:55:21', '2020-02-25 21:26:51', NULL),
(131, NULL, 'Kartarpur Tour', 'a', '1582624891.jpg', 390, NULL, NULL, NULL, 6, NULL, 1, '4 Days', '', '', 'USD', 0, '2020-02-25 15:01:31', '2020-02-25 15:02:50', NULL),
(132, NULL, 'Baisakhi Mela', '6 Days Itinerary\r\n\r\nDay 01: Arrival Lahore\r\nAfter a warm and traditional Punjabi welcome at airport  you will be transferred to hotel and check in. In the afternoon, we will go for half day tour of old part of Lahore city and visit Old Bazaar. Overnight stay in hotel at Lahore.\r\n\r\n \r\n\r\nDay 02: Lahore – \r\nAfter breakfast we will leave for Lahore city tour by visiting Badshahi mosque, museum and Shahi Kila. In the afternoon we will go for half day tour to Gurdwara Dera Sahib Panjvin Patshahi, later on we drive back  Overnight stay at Lahore.\r\n\r\n \r\n\r\nDay 03: Lahore –  Sacha Sauda - Nankana Sahib - Islamabad\r\n we will leave for Nankana Sahib for the darshan of Gurdwara Janamasthan (the birth place of Baba Nanak Ji), en route we will also visit Gurdwara Sacha Sauda. After visiting all the holy Gurdwaras at Nankana Sahib we will proceed to Islamabad, on arrival transfer to hotel and check in. Overnight stay at Islamabad.\r\n\r\n\r\n \r\n\r\nDay 04: Islamabad –  Panja Sahib (Hassan Abdal)\r\nAfter breakfast we will proceed for a full day excursion for Panja Sahib (Hassan Abdal), we will also visit the tomb of Baba Walli Kandhari (a friend of Baba Guru Nanak Dev Ji). If the time allow we will also visit Taxila Museum and sites.\r\n\r\n\r\nThousands of pilgrims are gathered at Panja Saheb to celebrate Baisakhi Mela here at Hassanabd. Overnight stay in hotel at Islamabad.\r\n\r\n \r\n\r\nDay 05: Islamabad\r\nAfter breakfast we will leave for Islamabad city tour by visiting Shakarparian, Lok Virsa museum and Faisal Mosque,Bazaar and Surroundings.\r\nOvernight stay in hotel at Islamabad\r\n \r\n\r\nDay 06: Islamabad  – Departure\r\nAfter breakfast you will be dropped off at airport for our flight back home. End of our services.\r\n\r\n \r\n\r\nPackage Price: 385 USD Per Person\r\nMinimum group size is 2 pax\r\n\r\n \r\n\r\nPackage Includes:\r\nAccommodation on twin sharing basis with 3 star hotels\r\nAir-conditioned luxury transport\r\nPunjabi speaking tour guide\r\nBreakfast will only be complimentary if it is provided the same from hotel', '1582628510.jpg', 385, NULL, NULL, NULL, 6, NULL, 1, '6 Days', '', '', 'USD', 0, '2020-02-25 16:01:50', '2020-02-25 21:28:07', NULL),
(133, NULL, 'Swat', 'Swat is often called The Switzerland of East !And its right called. From lush green valleys to snow peaks, valley\'s landscape is remarkable! :D Join us and see with your own eyes :D \r\n-----------------------------------------------\r\nFast Facts:\r\nTrip Duration: 3 Days and 2 Nights\r\nWeather Conditions: Cold\r\nAccommodation: Hotel\r\n------------------------------------------------\r\nTrip Charges:\r\nCost from Lahore/Islamabad USD 325\r\n\r\nNumber of pax: Minimum 2 pax\r\n\r\nEvent Plan:\r\nDay 0\r\n-Departure from Lahore\r\n\r\nDay 01:\r\n-Breakfast in Mingora\r\n-Continue Drive to Malam Jabba\r\n-Spend Some time at Malam Jabba\r\n-Continue drive to Behrain/Mingora\r\n-Arrival at Behrain/Mingora\r\n-Spend some time at Madyan Bahrain Road to capture some perfect sceneries\r\n-Night stay at Behrain/Fiza Ghatt\r\n\r\nDay 02\r\n-Breakfast in Behrain\r\n-Continue drive to Kalam\r\n-Transfer to jeeps and departure for Ushu Forest \r\n-Arrival at Ushu Forest and free time to explore surroundings\r\n-Coming back to Kalam\r\n-Night Stay at Kalam\r\n\r\nDay 03\r\n-Breakfast at Kalam\r\n-Departure for Lahore\r\n-Visit River Swat Bank\r\n-Arrival Lahore', '1582635744.jpg', 300, NULL, NULL, NULL, 5, NULL, 1, '5 Days', '', '', 'USD', 0, '2020-02-25 18:02:24', '2020-03-06 17:14:29', NULL),
(134, NULL, 'Group Tour', 'Meetings: We arrange the best range of meeting facilities and the latest high-tech Audio Visual equipment for any size of groups.\r\nIncentives: We offer the most superb destinations for incentive ideas and occasions; sightseeing, activities and wild adventures.\r\nConferences: We arrange professional executive conferences at the best venues with all the facilities to suit your needs.\r\nEvents: We arrange Pakistan Northern Areas Tour, Honeymoon Tour, Lahore City Tour,Village Tour and all as per requirement.', '1582645776.jpg', 0, NULL, NULL, NULL, 1, NULL, 1, '0', '', '', 'PKR', 0, '2020-02-25 20:35:13', '2020-02-25 20:52:27', NULL),
(135, 2, 'Dubai Work Visa', '<p><b><u><br></u></b></p><p><b><u>GET DUBAI WORK VISA IN REASONABLE PRICE</u></b></p><ul><li>Work Visa For Packing</li><li>Work Visa For Indoor Helper</li></ul><p><b><u>Salary Package</u></b></p><ul><li><b>1300</b> AED + OVER TIME</li><li>Duty Time:&nbsp;<b>10 Hours + Over Time</b></li></ul><p><b><u>DUBAI WORK VISA INCLUDE</u></b></p><ul><li>FREE TRANSPORT (Pick&amp;Drop)</li><li>FREE ACCOMMODATION&nbsp;</li><li>FREE MEDICAL</li><li>FREE LUNCH</li></ul><p><br></p><p><b><u><br></u></b></p><p><br></p><p><br></p>', '1582724759.jpg', 0, NULL, NULL, NULL, 1, NULL, 1, 'Work Visa', '', '', 'PKR', 0, '2020-02-26 18:45:59', '2020-02-26 18:45:59', NULL),
(136, NULL, 'Japan Work Visa', '<p><span style=\"font-weight: 700;\"><u>GET JAPAN WORK VISA IN REASONABLE PRICE</u></span></p><ul><li>Work Visa&nbsp;</li><li>Business Visa</li></ul><p><b>For More Information Call US AT 0313 487 6968</b></p><p><br></p>', '1582725572.jpg', NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, '', '', 'PKR', 0, '2020-02-26 18:59:32', '2020-02-26 19:02:20', NULL),
(137, NULL, 'Rohtas Fort & Mangla', '<p>1</p>', '1582726976.jpg', NULL, NULL, NULL, NULL, 3, NULL, 0, NULL, '', '', 'PKR', 0, '2020-02-26 19:22:56', '2020-02-27 22:20:11', NULL),
(138, NULL, 'Changa Managa', '<div class=\"vc_tta-panel-heading\" style=\"margin: 0px 0px -1px; padding: 0px; border: 1px solid rgb(240, 240, 240); font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 16px; line-height: inherit; font-family: Roboto, Arial, Tahoma, sans-serif; vertical-align: baseline; transition: background 0.2s ease-in-out 0s; border-radius: 5px 5px 0px 0px; background-color: rgb(248, 248, 248); color: rgb(98, 98, 98);\"><h4 class=\"vc_tta-panel-title vc_tta-controls-icon-position-left\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 21px; line-height: 1; font-family: Roboto, Arial, Tahoma, sans-serif; vertical-align: baseline; color: rgb(202, 1, 53);\"><span style=\"color: rgb(98, 98, 98); font-size: 16px; text-align: center; background-color: rgb(252, 252, 252);\"><br></span></h4><h4 class=\"vc_tta-panel-title vc_tta-controls-icon-position-left\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 21px; line-height: 1; font-family: Roboto, Arial, Tahoma, sans-serif; vertical-align: baseline; color: rgb(202, 1, 53);\"><span style=\"color: rgb(98, 98, 98); font-size: 16px; text-align: center; background-color: rgb(252, 252, 252);\">Once the largest man-made forest in the world, Changa Manga is better known as “one of the oldest hand planted forests in the world” and hosts a wide variety of flora and fauna. The forest is home to 14 species of mammals, 50 species of birds, 6 species of reptiles, 2 species of amphibians and 27 species of insects. Thus, other than producing timber for the local industry, the forest also serves as an important wildlife reserve. Named after two bandit brothers, the Changa Manga forest was originally planted in 1866 by British foresters and its trees were harvested to gather fuel and resources for the engines employed in the Northwestern railway networks. The thieves were a constant source of terror for the “law abiding citizens” of the districts in the 19th century as they would hold up and plunder any passing trader. The robbers had a den in the “secret heart” of the forest where they sought shelter from the British peacekeepers. The robbers were eventually captured by the police and became the inspiration for the name of the forest</span></h4><h4 class=\"vc_tta-panel-title vc_tta-controls-icon-position-left\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 21px; line-height: 1; font-family: Roboto, Arial, Tahoma, sans-serif; vertical-align: baseline; color: rgb(202, 1, 53);\"><span style=\"color: rgb(98, 98, 98); font-size: 16px; text-align: center; background-color: rgb(252, 252, 252);\"><br></span></h4><h4 class=\"vc_tta-panel-title vc_tta-controls-icon-position-left\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 21px; line-height: 1; font-family: Roboto, Arial, Tahoma, sans-serif; vertical-align: baseline; color: rgb(202, 1, 53);\"><span style=\"color: rgb(98, 98, 98); font-size: 16px; text-align: center; background-color: rgb(252, 252, 252);\"><br></span></h4><h4 class=\"vc_tta-panel-title vc_tta-controls-icon-position-left\" style=\"margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: 21px; line-height: 1; font-family: Roboto, Arial, Tahoma, sans-serif; vertical-align: baseline; color: rgb(202, 1, 53);\"><span style=\"color: rgb(98, 98, 98); font-size: 16px; text-align: center; background-color: rgb(252, 252, 252);\">   <b><u> Day 1</u></b></span></h4></div><div class=\"vc_tta-panel-body\" style=\"margin: 0px; padding: 14px 20px; border: 1px solid rgb(240, 240, 240); font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; vertical-align: baseline; box-sizing: content-box; overflow: hidden; transform: translate3d(0px, 0px, 0px); transition: padding 0.2s ease-in-out 0s; border-radius: 0px 0px 5px 5px; min-height: 10px; background-color: rgb(248, 248, 248);\"><div class=\"wpb_text_column wpb_content_element \" style=\"font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\"><div class=\"wpb_wrapper\" style=\"font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; margin: 0px; padding: 0px; border: 0px; vertical-align: baseline;\"><ul style=\"color: rgb(115, 126, 134); font: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 30px; padding: 0px; border: 0px; vertical-align: baseline; list-style: outside none;\"><li style=\"margin: 0px 0px 10px; padding: 0px; border: 0px; font: inherit; vertical-align: baseline; list-style: outside disc;\">Drive from Lahore to Changa Manga</l', '1582819035.jpg', 30, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-02-26 19:23:51', '2020-02-27 21:31:00', NULL),
(139, NULL, 'Khanpur Dam , Taxila and Rawal Dam', 'a', '1582823342.jpg', 50, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-02-26 19:24:43', '2020-02-27 22:17:09', NULL),
(140, NULL, 'Mitchell’s Fruit Farms', '<p>a</p>', '1582727134.jpeg', NULL, NULL, NULL, NULL, 3, NULL, 0, NULL, '', '', 'PKR', 0, '2020-02-26 19:25:34', '2020-03-03 19:12:16', NULL),
(141, NULL, 'Buddhist Taxila Tour', '<p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">TAXILA – Once a Great Centre of Buddhist Civilization</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><img alt=\"\" src=\"http://www.tdcp.gop.pk/image/taxila1.jpg\" style=\"float: right; height: 204px; margin: 4px; width: 319px;\">Taxila or Takshashila (\"City of Cut Stone\" or \"Takṣa Rock\") is one of the subcontinent’s treasures, and was once an important city of the kingdom of Gandhara. The ruins of Taxila are located about 30 km north of Islamabad/Rawalpindi, just off the famous Grand Trunk Road. Taxila was an important Buddhist Centre from 5 th century BC to 6 th Century AD. Ancient Taxila was situated at the pivotal junction of South Asia and Central Asia.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">The renowned archaeologist Sir Alexander Cunningham rediscovered the ruins of Taxila in mid- 19th century. In 1980, Taxila was declared a UNESCO World Heritage Site. In 2006 it was ranked as the top tourist destination in Pakistan by The Guardian newspaper.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Taxila was considered to be amongst the world’s earliest Buddhist university. Other notable sites here are: Bhir Mound, Dharmarajika Stupa, Sikap and Sirsukh cities, Shrine of double-headed Eagle, Jandial Temple, Jaulian Buddhist Monastery, etc.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Taxila Museum is famous for its magnificent collection of Gandharn Art (a blend of Greek and Buddhist art and house rare collection of utensils, jewelry, toys and pottery highlighting daily) life of the inhabitants of ancient Taxila.</p>', '1582727828.png', NULL, NULL, NULL, NULL, 3, NULL, 0, NULL, '', '', 'PKR', 0, '2020-02-26 19:37:08', '2020-02-26 19:49:08', NULL),
(142, NULL, 'Harrapa Ruins Tour', '<p><span style=\"color: rgb(51, 51, 51); font-family: gudearegular; text-align: justify; background-color: rgb(244, 244, 244);\">Harappa is an archaeological site in about 200 km south of Lahore. Harrappa is the foremost discovered site of Indus valley Civilization that flourished during 3500 BC to 1500 BC. The site shows traces of a well settled community living systematically with highhygenic standards at that time. The site takes its name from a modern village located near the former course of the Ravi River. The current village of Harappa is 6 km from the ancient site.</span><br></p>', '1582727946.png', NULL, NULL, NULL, NULL, 3, NULL, 0, NULL, '', '', 'PKR', 0, '2020-02-26 19:39:06', '2020-02-26 19:48:50', NULL);
INSERT INTO `tours` (`id`, `comp_id`, `Tour_name`, `Tour_Desc`, `profile_image`, `Tour_Adult_price`, `Tour_Adult_disc_price`, `Tour_Child_price`, `Tour_Child_disc_price`, `Tour_type_name`, `TourType_Code`, `is_enabled`, `Tour_Duration`, `includes`, `excludes`, `currency`, `CreatedBy`, `created_at`, `updated_at`, `deleted_at`) VALUES
(143, NULL, 'Cholistan Desert', '<p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">CHOLISTAN - Gateway to Adventure</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><img alt=\"\" src=\"http://www.tdcp.gop.pk/image/cholistan1.jpg\" style=\"float: right; margin: 10px; width: 25%;\">Occupying the far south of Punjab, Cholistan is the largest desert in Pakistan, covering over 25,000 sq. kms area. It extends south into the Thar Desert in Sindh, and east into the India\'s Rajisthan Desert. Ahead of you is another world where the 21st century does not intrude. About 1000 years ago, the Cholistan was a fertile plain watered by the Ghaggar River (now called Hakra in Pakistan), Along the 500 km of the ancient river course archaeologists have discovered over 400 old inhabited sites, most dating back to the Indus Valley Civilization, In the middle of the desert stands majestically the impressive Derawar Fort, this originally was the ancestral seat of Amir of Bahawalpur. This is the largest and the best preserved chain of forts, built in the days gone by. Its massive walls, holding forty enormous buttresses (ten per side), are now a permanent feature of the desert landscape, as they rise out of the sand in the setting sun. Nearby, is the elegant Derawar Mosque which is made out of marble and is an exact replica of the Moti Masjid in Delhi\'s Red Fort. it\'s a place where the stark moon presents an unearthly, yet a captivating vision. And before you realize, you have fallen in love with it. About 45 km south of Derawar Fort, is the shrine of Channan Pir (\'Channan\' meaning \'moon-like\') which is an<br>important pilgrim center and the desert comes alive during festivals.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">The desert also hosts an annual Jeep rally, known as Cholistan Desert Jeep Rally. It is the biggest motor sports event in Pakistan.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><br><img alt=\"\" src=\"http://www.tdcp.gop.pk/image/cholistan3.jpg\" style=\"float: right; margin: 4px; width: 25%;\" class=\"note-float-right\">There are several forts in Cholistan Region. These are :</p><ul style=\"margin-top: 10px; margin-bottom: 2px; list-style: none; color: rgb(51, 51, 51); font-family: \" century=\"\" gothic\";\"=\"\"><li style=\"font-family: gudearegular;\">    Derawar Fort</li><li style=\"font-family: gudearegular;\">    Islamgarh Fort</li><li style=\"font-family: gudearegular;\">    Mirgarh Fort</li><li style=\"font-family: gudearegular;\">    Jamgarh Fort</li><li style=\"font-family: gudearegular;\">    Mojgarh Fort</li><li style=\"font-family: gudearegular;\">    Marot Fort</li><li style=\"font-family: gudearegular;\">    Phoolra Fort</li><li style=\"font-family: gudearegular;\">    Khangarh Fort</li><li style=\"font-family: gudearegular;\">    Khairgarh Fort</li><li style=\"font-family: gudearegular;\">    Nawankot Fort</li><li style=\"font-family: gudearegular;\">    Bijnot Fort</li><li style=\"font-family: gudearegular;\"><br></li><li style=\"font-family: gudearegular;\"><br></li></ul>', '1582825549.jpg', 55, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-02-26 19:39:57', '2020-02-28 00:43:45', NULL),
(144, 2, 'Bahawalpur City Tour', '<p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">The foundation of city was laid in 1748AD which was encircled having 7 gates and was declared the capital of state. During last quarter of 19<span style=\"position: relative; font-size: 10.5px; line-height: 0; vertical-align: baseline; top: -0.5em;\">th</span>&nbsp;century abbasi family constructed huge and elegant palaces Noor Mahal, Gulzar Mahal, Darbar Mahal and Sadiq Garh Pal-aces. The projected population of city 2015 is 9 Lac.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">Bahawalpur City Tour</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">The tour takes you to visit Museum having eight galleries, nearby is Central Library which is the second biggest Library of Punjab after Lahore having 3 lacs book collections. Noor Mahal (1972-75) ZOO and Al Sadiq Mosque (1931)</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">Museum:</span></p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Museum timing (9 am – 4 pm) Break (01:00 – 02:00 pm) Friday Haf Day. Remains closed on Monday and Public Holidays. The tour cost will include, transport A/c (Non A/c), Entry Tickets at places of interest, guide services.</p>', '1582728440.png', NULL, NULL, NULL, NULL, 3, NULL, 0, NULL, '', '', 'PKR', 0, '2020-02-26 19:47:20', '2020-02-26 19:47:20', NULL),
(145, NULL, 'Soon Sakasar vellay', '<p>a</p>', '1582833267.jpg', 40, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-02-26 20:03:12', '2020-03-03 18:59:33', NULL),
(146, NULL, 'Rohtas Fort & Mangla Tour', '<p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Situated some 16 km north-west of Jhelum, the imposing&nbsp;<span style=\"font-weight: 700;\">Rohtas Fort</span>&nbsp;is one of the finest castles in the subcontinent. Built by Sher Shah Suri (1540-1545 AD), the ruins and the structure of the Fort speak high about the strength and determination of its builder. For its grandeur and historical value, Rohtas Fort is also included in the World Heritage Sites list.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\">Mangla Dam</span>&nbsp;is one of the largest earth-filled dams in the world. Mangla Lake offers panoramic views with boating opportunities in good weather. The nearby Power House is spectacular for its stunning mural paintings. Mangla Dam and Rohtas Fort make a good weekend excursion from either Islamabad or Lahore..</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\"><u>TOUR ITINERARY</u></span></p><ul style=\"margin-top: 10px; margin-bottom: 2px; list-style: none; color: rgb(51, 51, 51); font-family: \" century=\"\" gothic\";\"=\"\"><li style=\"font-family: gudearegular;\">7.30 am: Departure for Rohtas Fort by G. T. Road. Short break en-route</li><li style=\"font-family: gudearegular;\">10.30 am: Arrival and visit of Rohtas Fort</li><li style=\"font-family: gudearegular;\">12.30 pm: Departure for Dina</li><li style=\"font-family: gudearegular;\">02:00 pm: Departure for Mangla Dam</li><li style=\"font-family: gudearegular;\">02:30 pm: Arrival &amp; Visit of Mangla Dam</li><li style=\"font-family: gudearegular;\">04.30 pm: Departure for Lahore</li><li style=\"font-family: gudearegular;\">07.30 pm: Arrival at Lahore. End</li></ul><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\"><u>Tour Price</u></span>: subject to group size and requirements</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><span style=\"font-weight: 700;\"><u>Items Included:</u></span></p><ul style=\"margin-top: 10px; margin-bottom: 2px; list-style: none; color: rgb(51, 51, 51); font-family: \" century=\"\" gothic\";\"=\"\"><li style=\"font-family: gudearegular;\">A/C Transport</li><li style=\"font-family: gudearegular;\">Entry tickets for Rohtas Fort &amp; Mangla Dam</li><li style=\"font-family: gudearegular;\">Services of tour conducting Officers.</li></ul>', '1582832965.jpg', 39, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-02-26 20:05:15', '2020-03-03 19:07:39', NULL),
(147, 2, 'Thal Jeep Rally', '<p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Due to overwhelming popularity of Cholistan Jeep Rally, it was persistent demand by the people of&nbsp;<em>Muzaffargarh</em>&amp; Layyah to organize a Jeep Rally in the unique landscape of Thal desert. After due consideration, the Government of Punjab organizaed Thal Jeep Rally in District&nbsp;<em>Muzaffargarh</em>&amp; Layyah in<span style=\"position: relative; font-size: 10.5px; line-height: 0; vertical-align: baseline; top: -0.5em;\">&nbsp;</span>2016 which proved to be very successful.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">In 2018, 3rd Thal Jeep Rally was held from ________ November, 2018. The Rally started from Changa Manga Teela, Head Muhammad Wala,&nbsp;<em>Muzaffargarh</em>&nbsp;and &nbsp;after a round circuit of around 180 km ended at the same point with &nbsp;midpoint at tehsil Choubara, District Layyah.The desert stage was set for the thrilling event that really highlighted the regional culture and colours. This event also contributed to the socio-economic uplift of the area.</p>', '1582729589.png', NULL, NULL, NULL, NULL, 3, NULL, 0, NULL, '', '', 'PKR', 0, '2020-02-26 20:06:29', '2020-02-26 20:06:29', NULL),
(148, NULL, 'Festival of Lamps', '<p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Just adjacent to the fabulous Shalamar Gardens in Lahore is the shrine of 16<span style=\"position: relative; font-size: 10.5px; line-height: 0; vertical-align: baseline; top: -0.5em;\">th</span> century famous sufi saint, Shah Hussain, which is the venue of an annual traditional celebration full of cultural zeal and color. The Festival is locally called as “Mela Chiraghan” or the Fesival of Lamps and it is one of the largest traditional events of the Punjab province. Mela Chiraghan is  celebrated during the last week of March and this three day event is full of holy rituals, joy, music and folk dances. One of the most attractive features is the traditional food stalls that are specially designed for this carnival.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\">Thousands of devotees come to Lahore to attend this impressive annual Urs and major part of the Grand Trunk Road near to the shrine are decorated with stalls, toy shops and food outlets. The most significant ritual is the gathering of devotees who carry Chiraghs (light lamps) in the honor of Shah Hussain and some of the pilgrims toss lamps into a huge bonfire. The legend associated with this activity is that prayers of the devotees will be heard soon by the Almighty and they will get the reward.</p><p style=\"font-family: gudearegular; padding: 0px 15px; color: rgb(51, 51, 51); text-align: justify !important;\"><img alt=\"\" src=\"http://www.tdcp.gop.pk/image/mc2.jpg\" style=\"float: right; height: 186px; margin: 4px; width: 246px;\">Shah Hussain is also known as Madhu Lal Hussain who was actually the closest Hindu companion of the saint. Shah Hussain is particularly known for his sufi poetry and his verses in form of short poems are called Kafis of Shah Hussain. A number of renowned singers and Qawwals of Pakistan feel pride to perform Kafis of Shah Hussain. The nights of Mela Chiraghan of Lahore, the Fesival of Lights, are celebrated with Qawwalis and folk dances and these dances are truly spectacular and mirthful.</p>', '1582833600.jpg', 20, NULL, NULL, NULL, 3, NULL, 1, '1 Day', '', '', 'USD', 0, '2020-02-26 20:08:36', '2020-03-03 19:08:38', NULL),
(149, NULL, 'Shogran', '<p>Shogran</p>', '1583419327.jpg', 250, NULL, NULL, NULL, 5, NULL, 1, '4 Days', '', '', 'USD', 0, '2020-03-05 19:42:07', '2020-03-05 19:46:49', NULL),
(150, 2, 'Kashmir', '<p>Kashmir&nbsp;</p>', '1583419572.jpg', 300, NULL, NULL, NULL, 5, NULL, 1, '7 Days', '', '', 'USD', 0, '2020-03-05 19:46:12', '2020-03-05 19:46:12', NULL),
(151, NULL, 'Hunza', '<p>Hunza</p>', '1583497436.jpg', 355, NULL, NULL, NULL, 5, NULL, 1, '7 Days', '', '', 'USD', 0, '2020-03-06 17:22:19', '2020-03-06 17:26:25', NULL),
(153, NULL, 'chitral', '<p>c</p>', '1583583101.jpg', 399, NULL, NULL, NULL, 5, NULL, 1, '9 Days', '', '', 'USD', 0, '2020-03-07 17:07:28', '2020-03-07 17:36:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tour_packages`
--

DROP TABLE IF EXISTS `tour_packages`;
CREATE TABLE `tour_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `pkg_include_id` int(11) DEFAULT NULL,
  `pkg_exclude_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tour_packages`
--

INSERT INTO `tour_packages` (`id`, `tour_id`, `pkg_include_id`, `pkg_exclude_id`, `created_at`, `updated_at`) VALUES
(28, 68, 3, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(29, 68, 4, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(30, 68, 5, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(31, 68, 6, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(32, 68, 7, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(33, 68, NULL, 4, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(34, 68, NULL, 5, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(35, 68, NULL, 6, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(36, 68, NULL, 7, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(37, 69, 3, NULL, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(38, 69, 4, NULL, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(39, 69, 5, NULL, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(40, 69, NULL, 4, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(41, 69, NULL, 5, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(42, 70, 3, NULL, '2020-01-24 07:19:26', '2020-01-24 07:19:26'),
(43, 70, 4, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(44, 70, 5, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(45, 70, 6, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(46, 70, 7, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(47, 70, NULL, 4, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(48, 70, NULL, 5, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(49, 70, NULL, 6, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(65, 78, 3, NULL, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(66, 78, 4, NULL, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(67, 78, 5, NULL, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(68, 78, NULL, 4, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(69, 78, NULL, 5, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(70, 78, NULL, 6, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(71, 79, 3, NULL, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(72, 79, 4, NULL, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(73, 79, 5, NULL, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(74, 79, NULL, 4, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(75, 79, NULL, 5, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(76, 80, 4, NULL, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(77, 80, 5, NULL, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(78, 80, 6, NULL, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(79, 80, NULL, 4, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(80, 80, NULL, 5, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(81, 80, NULL, 7, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(82, 81, 3, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(83, 81, 4, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(84, 81, 5, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(85, 81, 6, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(86, 81, NULL, 4, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(87, 81, NULL, 5, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(88, 81, NULL, 6, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(89, 82, 3, NULL, '2020-01-27 02:05:25', '2020-01-27 02:05:25'),
(90, 82, 4, NULL, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(91, 82, 5, NULL, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(92, 82, NULL, 4, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(93, 82, NULL, 5, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(94, 82, NULL, 6, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(95, 83, 4, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(96, 83, 5, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(97, 83, 6, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(98, 83, 7, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(99, 83, NULL, 5, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(100, 83, NULL, 6, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(101, 83, NULL, 7, '2020-01-27 02:06:53', '2020-01-27 02:06:53'),
(102, 84, 3, NULL, '2020-01-27 02:07:38', '2020-01-27 02:07:38'),
(103, 84, 4, NULL, '2020-01-27 02:07:38', '2020-01-27 02:07:38'),
(104, 84, 5, NULL, '2020-01-27 02:07:38', '2020-01-27 02:07:38'),
(105, 84, NULL, 4, '2020-01-27 02:07:39', '2020-01-27 02:07:39'),
(106, 84, NULL, 5, '2020-01-27 02:07:39', '2020-01-27 02:07:39'),
(107, 84, NULL, 6, '2020-01-27 02:07:39', '2020-01-27 02:07:39'),
(108, 85, 3, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(109, 85, 4, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(110, 85, 5, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(111, 85, 7, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(112, 85, NULL, 4, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(113, 85, NULL, 5, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(114, 85, NULL, 6, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(115, 86, 3, NULL, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(116, 86, 4, NULL, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(117, 86, 5, NULL, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(118, 86, NULL, 4, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(119, 86, NULL, 5, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(120, 86, NULL, 7, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(121, 87, 4, NULL, '2020-01-27 02:11:50', '2020-01-27 02:11:50'),
(122, 87, 5, NULL, '2020-01-27 02:11:50', '2020-01-27 02:11:50'),
(123, 87, 6, NULL, '2020-01-27 02:11:50', '2020-01-27 02:11:50'),
(124, 87, NULL, 4, '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(125, 87, NULL, 5, '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(126, 87, NULL, 6, '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(127, 88, 3, NULL, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(128, 88, 4, NULL, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(129, 88, 5, NULL, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(130, 88, NULL, 4, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(131, 88, NULL, 5, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(132, 88, NULL, 6, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(133, 89, 3, NULL, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(134, 89, 4, NULL, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(135, 89, 5, NULL, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(136, 89, NULL, 4, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(137, 89, NULL, 5, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(138, 89, NULL, 6, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(139, 90, 3, NULL, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(140, 90, 4, NULL, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(141, 90, 5, NULL, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(142, 90, NULL, 4, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(143, 90, NULL, 5, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(144, 90, NULL, 6, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(145, 91, 3, NULL, '2020-01-27 02:16:00', '2020-01-27 02:16:00'),
(146, 91, 4, NULL, '2020-01-27 02:16:00', '2020-01-27 02:16:00'),
(147, 91, 5, NULL, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(148, 91, 6, NULL, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(149, 91, NULL, 4, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(150, 91, NULL, 5, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(151, 91, NULL, 6, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(152, 92, 3, NULL, '2020-01-27 02:17:00', '2020-01-27 02:17:00'),
(153, 92, 4, NULL, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(154, 92, 5, NULL, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(155, 92, 6, NULL, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(156, 92, NULL, 4, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(157, 92, NULL, 5, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(158, 92, NULL, 6, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(159, 93, 3, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(160, 93, 4, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(161, 93, 5, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(162, 93, 6, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(163, 93, 7, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(164, 93, NULL, 4, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(165, 93, NULL, 5, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(166, 93, NULL, 6, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(167, 93, NULL, 7, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(168, 94, 3, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(169, 94, 4, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(170, 94, 6, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(171, 94, 7, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(172, 94, NULL, 5, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(173, 94, NULL, 6, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(174, 95, 3, NULL, '2020-01-27 02:19:16', '2020-01-27 02:19:16'),
(175, 95, 4, NULL, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(176, 95, 5, NULL, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(177, 95, NULL, 4, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(178, 95, NULL, 5, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(179, 95, NULL, 6, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(180, 96, 3, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(181, 96, 4, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(182, 96, 5, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(183, 96, 6, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(184, 96, NULL, 4, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(185, 96, NULL, 5, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(186, 96, NULL, 6, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(187, 97, 3, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(188, 97, 4, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(189, 97, 5, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(190, 97, 6, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(191, 97, NULL, 4, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(192, 97, NULL, 5, '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(193, 97, NULL, 6, '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(194, 97, NULL, 7, '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(195, 98, 3, NULL, '2020-01-27 02:21:46', '2020-01-27 02:21:46'),
(196, 98, 4, NULL, '2020-01-27 02:21:46', '2020-01-27 02:21:46'),
(197, 98, 5, NULL, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(198, 98, 6, NULL, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(199, 98, NULL, 4, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(200, 98, NULL, 5, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(201, 98, NULL, 6, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(202, 99, 3, NULL, '2020-01-27 02:22:40', '2020-01-27 02:22:40'),
(203, 99, 4, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(204, 99, 5, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(205, 99, 6, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(206, 99, 7, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(207, 99, NULL, 4, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(208, 99, NULL, 5, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(209, 99, NULL, 6, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(210, 99, NULL, 7, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(211, 100, 3, NULL, '2020-01-27 02:23:31', '2020-01-27 02:23:31'),
(212, 100, 4, NULL, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(213, 100, 5, NULL, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(214, 100, 7, NULL, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(215, 100, NULL, 4, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(216, 100, NULL, 5, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(217, 100, NULL, 6, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(218, 101, 3, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(219, 101, 4, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(220, 101, 5, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(221, 101, 6, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(222, 101, NULL, 4, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(223, 101, NULL, 5, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(224, 101, NULL, 6, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(225, 102, 3, NULL, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(226, 102, 4, NULL, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(227, 102, NULL, 4, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(228, 102, NULL, 5, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(229, 102, NULL, 7, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(230, 103, 3, NULL, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(231, 103, 4, NULL, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(232, 103, NULL, 4, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(233, 103, NULL, 5, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(234, 103, NULL, 6, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(235, 104, 3, NULL, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(236, 104, 4, NULL, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(237, 104, 5, NULL, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(238, 104, NULL, 4, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(239, 104, NULL, 5, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(240, 104, NULL, 6, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(241, 105, 5, NULL, '2020-01-27 02:28:25', '2020-01-27 02:28:25'),
(242, 105, 6, NULL, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(243, 105, 7, NULL, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(244, 105, NULL, 5, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(245, 105, NULL, 6, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(246, 105, NULL, 7, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(247, 106, 3, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(248, 106, 4, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(249, 106, 5, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(250, 106, 6, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(251, 106, 7, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(252, 106, NULL, 4, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(253, 106, NULL, 5, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(254, 106, NULL, 6, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(255, 106, NULL, 7, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(256, 107, 4, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(257, 107, 5, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(258, 107, 6, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(259, 107, 7, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(260, 107, NULL, 4, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(261, 107, NULL, 5, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(262, 107, NULL, 6, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(263, 108, 3, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(264, 108, 5, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(265, 108, 6, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(266, 108, 7, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(267, 108, NULL, 4, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(268, 108, NULL, 5, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(269, 108, NULL, 6, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(270, 108, NULL, 7, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(271, 109, 4, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(272, 109, 5, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(273, 109, 6, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(274, 109, 7, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(275, 109, NULL, 4, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(276, 109, NULL, 5, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(277, 109, NULL, 6, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(278, 110, 3, NULL, '2020-01-27 02:34:55', '2020-01-27 02:34:55'),
(279, 110, 4, NULL, '2020-01-27 02:34:55', '2020-01-27 02:34:55'),
(280, 110, 5, NULL, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(281, 110, 6, NULL, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(282, 110, 7, NULL, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(283, 110, NULL, 4, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(284, 110, NULL, 5, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(285, 110, NULL, 6, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(286, 110, NULL, 7, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(287, 111, 3, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(288, 111, 4, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(289, 111, 5, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(290, 111, 6, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(291, 111, NULL, 4, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(292, 111, NULL, 5, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(293, 111, NULL, 6, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(294, 111, NULL, 7, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(295, 112, 3, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(296, 112, 4, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(297, 112, 5, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(298, 112, 6, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(299, 112, 7, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(300, 112, NULL, 4, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(301, 112, NULL, 5, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(302, 112, NULL, 6, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(303, 112, NULL, 7, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(304, 113, 3, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(305, 113, 4, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(306, 113, 5, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(307, 113, 6, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(308, 113, NULL, 4, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(309, 113, NULL, 5, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(310, 113, NULL, 6, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(311, 114, 3, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(312, 114, 4, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(313, 114, 5, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(314, 114, 6, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(315, 114, NULL, 4, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(316, 114, NULL, 5, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(317, 114, NULL, 6, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(318, 115, 3, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(319, 115, 4, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(320, 115, 6, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(321, 115, 7, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(322, 115, NULL, 4, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(323, 115, NULL, 5, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(324, 115, NULL, 6, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(325, 116, 3, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(326, 116, 4, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(327, 116, 5, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(328, 116, 6, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(329, 116, 7, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(330, 116, NULL, 4, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(331, 116, NULL, 5, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(332, 116, NULL, 6, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(333, 116, NULL, 7, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(334, 117, 3, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(335, 117, 4, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(336, 117, 5, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(337, 117, 6, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(338, 117, 7, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(339, 117, NULL, 4, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(340, 117, NULL, 5, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(341, 117, NULL, 6, '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(342, 117, NULL, 7, '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(343, 118, 3, NULL, '2020-01-27 02:41:14', '2020-01-27 02:41:14'),
(344, 118, 4, NULL, '2020-01-27 02:41:14', '2020-01-27 02:41:14'),
(345, 118, 5, NULL, '2020-01-27 02:41:14', '2020-01-27 02:41:14'),
(346, 118, 6, NULL, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(347, 118, 7, NULL, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(348, 118, NULL, 4, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(349, 118, NULL, 5, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(350, 118, NULL, 6, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(351, 118, NULL, 7, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(352, 119, 4, NULL, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(353, 119, 5, NULL, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(354, 119, NULL, 4, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(355, 119, NULL, 5, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(356, 120, 3, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(357, 120, 4, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(358, 120, 5, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(359, 120, 6, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(360, 120, NULL, 4, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(361, 120, NULL, 5, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(362, 120, NULL, 6, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(363, 121, 3, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(364, 121, 4, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(365, 121, 6, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(366, 121, 7, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(367, 121, NULL, 4, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(368, 121, NULL, 5, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(369, 121, NULL, 6, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(370, 122, 6, NULL, '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(371, 122, NULL, 4, '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(372, 122, NULL, 5, '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(373, 79, 4, NULL, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(374, 79, 6, NULL, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(375, 79, 7, NULL, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(376, 79, NULL, 5, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(377, 79, NULL, 7, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(378, 124, 3, NULL, '2020-02-20 16:44:16', '2020-02-20 16:44:16'),
(379, 124, NULL, 4, '2020-02-20 16:44:16', '2020-02-20 16:44:16'),
(380, 124, NULL, 5, '2020-02-20 16:44:16', '2020-02-20 16:44:16');

-- --------------------------------------------------------

--
-- Table structure for table `tour_type_names`
--

DROP TABLE IF EXISTS `tour_type_names`;
CREATE TABLE `tour_type_names` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_enabled` tinyint(11) DEFAULT NULL,
  `tourtypeName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tourtypeDes` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tour_type_names`
--

INSERT INTO `tour_type_names` (`id`, `is_enabled`, `tourtypeName`, `tourtypeDes`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Services ', 'Services Available', '2020-01-20 09:00:11', '2020-01-20 09:00:11', NULL),
(2, 1, 'Umrah Packages', NULL, '2020-01-24 07:08:30', '2020-02-20 16:28:39', NULL),
(3, 1, 'Lahore Tours', NULL, '2020-01-24 07:09:42', '2020-02-20 16:28:44', NULL),
(4, 1, 'UAE Tours', NULL, '2020-01-24 07:10:16', '2020-02-20 16:28:50', NULL),
(5, 1, 'Pakistan Northern Tours', NULL, '2020-01-24 07:11:39', '2020-02-20 16:28:55', NULL),
(6, 1, 'Sikh Tayra Tours', NULL, '2020-02-01 05:01:13', '2020-02-03 06:31:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_user` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comp_id` int(11) DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `address`, `city`, `state`, `country`, `profile_image`, `post_code`, `admin_user`, `password`, `remember_token`, `created_at`, `updated_at`, `comp_id`, `is_enabled`) VALUES
(1, 'Admin', 'Khan', 'info@rapidfiresol.com', '+92 3214322086', '310 Asad Center 3rd Floor Model Town International Market', 'Test City', 'Alabama', 'United States', 'user-1.jpg', '53700', 2, '$2y$10$qMJeXM23/T6ebNLZvdiLget4hIRkHNCHTJ.49ngQl4BYGnL6b3UNu', 'bzHF2q8Af9yq1PkoLVUIJfbxRD6X63yT5twJ0A5Q0K5xF5eEJsONhYZsbIsp', NULL, '2020-02-27 14:30:14', 0, 1),
(9, 'Test', '1', 'test@test.com', '000000', 'asdasdas', NULL, NULL, NULL, 'user-9.jpg', '000', 2, '$2y$10$qMJeXM23/T6ebNLZvdiLget4hIRkHNCHTJ.49ngQl4BYGnL6b3UNu', 'bzHF2q8Af9yq1PkoLVUIJfbxRD6X63yT5twJ0A5Q0K5xF5eEJsONhYZsbIsp', '2020-02-13 14:22:31', '2020-02-17 13:27:19', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attractions`
--
ALTER TABLE `attractions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attraction_imgs`
--
ALTER TABLE `attraction_imgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_infornations`
--
ALTER TABLE `company_infornations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_infornations_comp_email_unique` (`Comp_Email`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_exes`
--
ALTER TABLE `package_exes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_ins`
--
ALTER TABLE `package_ins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_requests`
--
ALTER TABLE `post_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_requests_email_unique` (`email`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourimages`
--
ALTER TABLE `tourimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_packages`
--
ALTER TABLE `tour_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_type_names`
--
ALTER TABLE `tour_type_names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attractions`
--
ALTER TABLE `attractions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `attraction_imgs`
--
ALTER TABLE `attraction_imgs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company_infornations`
--
ALTER TABLE `company_infornations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `components`
--
ALTER TABLE `components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `package_exes`
--
ALTER TABLE `package_exes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `package_ins`
--
ALTER TABLE `package_ins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `post_requests`
--
ALTER TABLE `post_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tourimages`
--
ALTER TABLE `tourimages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=638;

--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `tour_packages`
--
ALTER TABLE `tour_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `tour_type_names`
--
ALTER TABLE `tour_type_names`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
