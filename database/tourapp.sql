-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2020 at 11:09 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tourapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `attractions`
--

CREATE TABLE `attractions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attractions`
--

INSERT INTO `attractions` (`id`, `name`, `description`, `logo`, `is_enabled`, `created_at`, `updated_at`) VALUES
(11, 'Dubai', 'Dubai is located on the Persian Gulf, to the northeast of the United Arab Emirates. Dubai is the second largest emirate with an urban area of 3885 sq km and the city is roughly 35 sq km. However it will expand to twice that size with the addition of the man-made islands; the Waterfront, the three Palms, the World, the Universe, Dubailand, as well as the construction in the desert.\r\n\r\nOne of the most fascinating geographical aspects of Dubai is its Creek, which divides the city into two regions. Dubai Creek is made up of a natural 9.5 mile inlet in the Persian Gulf, around which the city’s trade developed. North of the Creek is called Deira, and Bur Dubai refers to the south where it joins the tourist and residential developments of Jumeirah along the coast.\r\n\r\nDubai also has the highest population, sharing its borders with Abu Dhabi to the south, Sharjah to the northeast and the Sultanate of Oman to the southeast.\r\n\r\nDue to the city’s unique geographical location it enjoys a strategic position which allows it to connect to all local Gulf States, as well as to East Africa and South Asia\r\n\r\nDubai Holidays\r\n\r\nDubai is one the most popular of the United Arab Emirates (UAE) for tourism and that’s little wonder given all that this cosmopolitan place has to offer. Its increase in popularity in the last few years has been unbelievable, changing from an occasionally used stop- over point convenient for shopping to a thriving full on tourist resort with plenty to do. Dubai is one of the world’s most exciting and dynamic cities. It’s a remarkable oasis in the desert combining ultra-luxurious accommodation, sandy beaches, unbelievable duty-free shopping and the allure of traditional Arabic culture. The city is packed with man-made wonders, like the self-styled “Seven Star” Burj al-Arab built on a huge artificial island and costing a staggering $650 million to build. Dubai is an Emirate and a world class city that is made for superlatives. From the man-made Palm Islands to the world\'s tallest building, the Burj Khalifa, which stands 828 meters high this place is exuberant, excessive and despite the global economic downturn, still seems to be going strong.\r\n\r\nIt’s seemingly impossible to keep up with the constant changes in this great city. They say you need to come back to Dubai every six months to check what new amenities, hotels and shopping has been added since the last trip. The UAE isn’t all man-made wonders, retail therapy to the extreme and 4×4 thrashes through desert dunes though! The inhabitants of the Gulf States have had a long relationship with camels, and the prize racing beasts are very highly valued. Many of the seven species of marine turtles in the world can be seen in the waters of the UAE: the green turtle, the hawksbill turtle, the loggerhead turtle and (if you are fortunate) the huge leatherback turtle. The green and hawksbill turtles nest on beaches of the UAE as well as regularly feeding in its waters.', '1581161331.jpg', 1, '2020-02-08 06:28:51', '2020-02-08 08:38:37'),
(12, 'Ajman', 'Located near the cities of Sharjah and Umm Al Quwain, Ajman is the smallest of all the emirates. Ajman comprises the two jurisdictions of Masfout and Manama, situated to the north of the Masafi-Dhaid main artery. Though the biggest draw in the area is Sharjah, Ajman encompasses some interesting attractions and sites for tourists. One of the main attractions in Ajman is the long expanse of powdery white sandy beach, situated along the Persian Gulf Coast, stretching for almost 20 miles.\r\n\r\nProminent in any Ajman travel guide is the eighteenth century fort located in the heart of the city. The fort has been renovated and turned into the local museum where the area’s history can be examined in more depth. Once serving as the Ruler’s palace, it became Ajman’s cardinal police station in 1970. Tourists visiting the museum are privy to old manuscripts, odes to traditional daily life in Ajman as well as many ancient artifacts and old weapons once used in battle. The museum is situated along the coast and close to many savory restaurants, and a number of high-end Sharjah hotels if opting to be based outside of the city.\r\n\r\nAjman is essentially a port city great for shopping and dining. The small city maintains a certain charm the other emirates lack, mostly due to its small size which creates a more intimate feel than many of the large, bustling cities. Though in the past it’s held less appeal than the other emirates, Ajman does feature a handful of interesting attractions. The naturally formed creek diffuses the inland and remains a major focal point in the city. The section of Ajman called Masfout is an important agricultural area providing much of the local produce found throughout the region. Masfout is noted for its vibrant marble and is bordered by the Hajar Mountains creating a picturesque backdrop.', '1581161422.jpg', 1, '2020-02-08 06:30:22', '2020-02-08 08:41:00'),
(13, 'Ras Al Khaimah', 'Ras Al Khaimah is a unique destination for visitors of all ages, thanks to its diverse landscapes and distinctive excursions that have enticed guests over the years. This rising emirate boasts beautiful and dramatic mountains, red sandy deserts and lush green plains indented by a series of creeks and lagoons. It has a rich heritage, dating back 7,000 years, which manifests in numerous historical sites, forts and abandoned villages.\r\n\r\nRas Al Khaimah offers a variety of entertainment and relaxation facilities including high-end hotels & resorts, international restaurants and selective spas, all at great value for money. Located on the beaches, mountains as well as the desert, these luxurious resorts give full justice to the natural beauty of Ras Al Khaimah. The emirate is also a haven for exceptional Spas that are prominent in resorts like the Banyan Tree Al Wadi, Hilton Resort & Spa and Golden Tulip Khatt Springs. The cherry on the cake is the natural hot springs in Khatt, the only one in the UAE, where guests can take a relaxing therapeutic bath.\r\n\r\nWith a wide range of adventure and sports activities covering Mountain adventures, Desert camps, Golf courses and Watersports, the emirate of Ras Al Khaimah presents an ultimate outdoor experience for tourists. Whether shopping in the welcoming old Souq (market), soaking up the sun on one of the beaches or taking a ride on the world’s tallest water slide, Ras Al Khaimah has something for everyone. A special excursion worth mentioning is the Land of Pearls, which takes guests through the journey of a Pearl from the operation room to the mouth of the oyster itself!\r\n\r\nThis rising emirate, with its rich history and culture, offers a number of historical sites and ancient ruins dating back to the 13th century AD. The forts and the museums display the proud heritage of the Emiratis here, who are still renowned for their warm hospitality.\r\n\r\nLocated just 45 minutes from the Dubai International airport and 30 minutes from Oman as well as being well connected by Airport, Ports and the National Highway, Ras Al Khaimah enables easy stopovers for guests. With its splendid natural treasure and recreational choices, Ras Al Khaimah is a picture-perfect leisure destination that is a must for travellers, looking to be smitten by the real essence of Arabia.', '1581168160.png', 1, '2020-02-08 08:22:40', '2020-02-08 08:41:26'),
(14, 'Sharjah', 'Sharjah is the third largest of the seven emirates that make up the United Arab Emirates (UAE) and is the only one to have land on both the Arabian Gulf Coast and the Gulf of Oman. It is an emirate of contrasts where visitors can enjoy a holiday in the sun, discover traditional markets (souks) or visit modern shopping malls, explore the many heritage sites and museums, admire the majestic mosques, stroll around the lagoons, and experience the natural beauty of true Arabian deserts, mountains and seas.\r\n\r\nSharjah covers approximately 2,600 square kilometres. In addition to Sharjah city, which lies on the shores of the Arabian Gulf, the emirate has three regions on the scenic east coast; Dibba Al Hisn, Khor Fakkan and Kalba. Sharjah is a close neighbour to Dubai (it’s just a 15 minute drive from the centre of Sharjah to Dubai International Airport).\r\n\r\nSharjah’s location is significant both geographically and historically. The city overlooks the Arabian Gulf coastline that stretches for more than 16 kilometres, consisting of sandy beaches and built up areas. The central region combines lush green oases, with gravel plains and rolling red sand dunes. To the East the emirate reaches the Gulf of Oman coast where the landscape changes to a spectacular rocky coastline backed by impressively high mountains. Historically, the east coast is full of archaeological sites dating back to as early as the 2nd millennium, and forts built by the Portuguese in the 16th century were used to control the spice trade. In more recent times, Sharjah opened the first airport in the region in 1932. The Emirate is located midway between Europe and the Far East and is within 8 hours flying time in either direction.', '1581168185.jpg', 1, '2020-02-08 08:23:05', '2020-02-08 08:41:50'),
(15, 'asdas', 'dasd', '1581168211.jpg', 1, '2020-02-08 08:23:31', '2020-02-08 08:23:31'),
(16, 'asdasasdasdas', 'dasdasdasd', '1581168591.jpg', 1, '2020-02-08 08:29:51', '2020-02-08 08:29:51'),
(17, 'Abu Dhabi', 'Abu Dhabi, the capital of the United Arab Emirates, has marked its territory as one of the top tourist destinations due to its world class infrastructure and interesting attractions. Until recently, Abu Dhabi boasted of a city of conventions and exhibitions; but today, it has also gained popularity for its entertainment options and shopping venues. iVarious areas and islands in Abu Dhabi are developed to promote tourism including the Corniche, and Yas and Saadiyat Island.s To provide in-depth information on the various things to do in Abu Dhabi, we have compiled a must visit list of top landmarks, shopping outlets, dining options and a variety of activities. Also, travellers will be informed on the various means of transportation and communication options in Abu Dhabi.\r\n\r\nAbu Dhabi is geographically located on the north-eastern part of the Persian Gulf in the Arabian Peninsula. The island city is located just 250 metres from the mainland which consists of many other suburbs linked to the emirate, Abu Dhabi. A special feature of the city includes the Abu Dhabi Corniche which offers the chance to walk, cycle or jog along the island’s coastline which has a breathtaking view.', '1581169225.png', 1, '2020-02-08 08:40:25', '2020-02-08 08:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `attraction_imgs`
--

CREATE TABLE `attraction_imgs` (
  `id` int(10) UNSIGNED NOT NULL,
  `attraction_id` int(11) DEFAULT NULL,
  `attraction_imgs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attraction_imgs`
--

INSERT INTO `attraction_imgs` (`id`, `attraction_id`, `attraction_imgs`, `created_at`, `updated_at`) VALUES
(22, 11, 'as0hc-112851-tHb.jpg', '2020-02-08 06:28:51', '2020-02-08 06:28:51'),
(23, 11, 'a16Mz-112851-nup.png', '2020-02-08 06:28:51', '2020-02-08 06:28:51'),
(24, 11, 'DMctu-112851-FX7.png', '2020-02-08 06:28:51', '2020-02-08 06:28:51'),
(25, 12, '0xWYW-113022-aiJ.jpg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(26, 12, 'COGZx-113022-Myt.jpeg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(27, 12, '3oL5k-113022-HTz.jpeg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(28, 12, 'd9rrM-113022-eem.jpg', '2020-02-08 06:30:22', '2020-02-08 06:30:22'),
(29, 13, 'ZdQz8-012240-M2X.jpg', '2020-02-08 08:22:40', '2020-02-08 08:22:40'),
(30, 13, 'pkCvg-012240-htt.jpg', '2020-02-08 08:22:40', '2020-02-08 08:22:40'),
(31, 14, 'ujX96-012305-C6Q.jpg', '2020-02-08 08:23:05', '2020-02-08 08:23:05'),
(32, 14, 'cpdVa-012305-tKV.png', '2020-02-08 08:23:05', '2020-02-08 08:23:05'),
(33, 14, 'Ofuk2-012305-rmh.png', '2020-02-08 08:23:05', '2020-02-08 08:23:05'),
(34, 14, 'UjZmX-012305-sHr.png', '2020-02-08 08:23:05', '2020-02-08 08:23:05'),
(35, 15, 'bJc2Z-012331-TEz.jpg', '2020-02-08 08:23:31', '2020-02-08 08:23:31'),
(36, 15, 'tjVCK-012331-8w7.jpg', '2020-02-08 08:23:31', '2020-02-08 08:23:31'),
(37, 16, 'EAHgK-012951-WYI.png', '2020-02-08 08:29:51', '2020-02-08 08:29:51'),
(38, 16, 'DbBB5-012951-P5B.png', '2020-02-08 08:29:51', '2020-02-08 08:29:51'),
(39, 16, 'cPEnn-012951-1Nb.png', '2020-02-08 08:29:51', '2020-02-08 08:29:51'),
(40, 16, 'NK5xO-012951-Rxj.png', '2020-02-08 08:29:51', '2020-02-08 08:29:51'),
(41, 16, 'CTdpj-012951-9Gz.jpg', '2020-02-08 08:29:51', '2020-02-08 08:29:51'),
(42, 17, 'i62QJ-014025-JQ9.jpg', '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(43, 17, 'uVbaB-014025-HnC.jpg', '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(44, 17, 'lhd63-014025-7xJ.png', '2020-02-08 08:40:25', '2020-02-08 08:40:25'),
(45, 17, '7pJDu-014025-sWI.png', '2020-02-08 08:40:25', '2020-02-08 08:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `state_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 'Model  Town', '2020-01-29 07:35:44', '2020-01-29 07:35:44'),
(2, 2, 4, 'Behriya town', '2020-01-29 08:50:58', '2020-01-29 08:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `company_infornations`
--

CREATE TABLE `company_infornations` (
  `id` int(10) UNSIGNED NOT NULL,
  `comp_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_live_dte` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Reg_Num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_City_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Stat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Country_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Phone` int(15) DEFAULT NULL,
  `Comp_Tax_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Comp_Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) DEFAULT NULL,
  `CreatedBy` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Logo_URL` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Aboutus_Desc` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiktok` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_infornations`
--

INSERT INTO `company_infornations` (`id`, `comp_name`, `Comp_live_dte`, `Comp_Reg_Num`, `Comp_City_name`, `Comp_Stat_name`, `Comp_Country_name`, `Comp_Address`, `Comp_Phone`, `Comp_Tax_num`, `Comp_Email`, `is_enabled`, `CreatedBy`, `Logo_URL`, `Aboutus_Desc`, `created_at`, `updated_at`, `facebook`, `instagram`, `twitter`, `youtube`, `tiktok`) VALUES
(2, 'Lets Go Have Fun Tours', '2020-02-20 17:23:41', '001', '1', '4', '2', '310 Asad Plaza International Market M Block Model Town Lahore', 30120120, '1110s', 'info@havefuntour.com', NULL, 'admin', 'Logo_URL-2.png', 'This is Tour Company ....', '2020-01-29 08:57:59', '2020-02-20 12:23:41', 'https://www.facebook.com/public/Mateen-Ahmad', 'instagarm', 'twitter', 'youtube', 'tiktook');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

CREATE TABLE `components` (
  `id` int(10) UNSIGNED NOT NULL,
  `home` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `safari` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guide` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `travel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `components`
--

INSERT INTO `components` (`id`, `home`, `tour`, `safari`, `service`, `guide`, `contact`, `about`, `travel`, `created_at`, `updated_at`) VALUES
(1, 'home', 'tour', 'Tours', 'Services', 'Attractions', 'Contact us', 'About Us', 'Travel', '2020-02-05 07:51:46', '2020-02-20 16:24:11');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'UAE', '2020-01-29 07:13:23', '2020-01-29 07:13:23'),
(2, 'Pakistan', '2020-01-29 07:34:39', '2020-01-29 07:34:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_08_27_133805_create_roles_table', 1),
(4, '2016_08_27_133922_create_permissions_table', 1),
(5, '2016_08_27_134037_create_permission_role_table', 1),
(6, '2016_08_27_134135_create_role_user_table', 1),
(7, '2020_01_11_192304_add_newFields_to_users_table', 1),
(8, '2020_01_12_081816_create_company_infornations_table', 1),
(9, '2020_01_12_164547_create_tours_table', 1),
(10, '2020_01_13_071443_create_tourimages_table', 1),
(11, '2020_01_15_102136_create_tour_type_names_table', 1),
(12, '2020_01_17_135046_create_package_ins_table', 1),
(13, '2020_01_17_145846_create_package_exes_table', 1),
(14, '2020_01_18_094125_create_tour_packages_table', 1),
(15, '2020_01_22_125755_create_post_requests_table', 2),
(16, '2020_01_23_114726_create_jobs_table', 3),
(17, '2020_01_29_120514_create_countries_table', 4),
(18, '2020_01_29_121443_create_states_table', 5),
(19, '2020_01_29_123155_create_cities_table', 6),
(20, '2020_01_31_142339_add_softdelete_to_tours_table', 7),
(21, '2020_02_01_095808_add_softdelete_to_table', 8),
(22, '2020_02_05_100523_add_socialAccounts_to_company_table', 9),
(23, '2020_02_05_110428_create_modules_table', 10),
(24, '2020_02_05_123050_create_components_table', 11),
(26, '2020_02_05_152219_create_attractions_table', 12),
(27, '2020_02_05_160607_create_attraction_imgs_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `package_exes`
--

CREATE TABLE `package_exes` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_exclude` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_exes`
--

INSERT INTO `package_exes` (`id`, `package_exclude`, `created_at`, `updated_at`) VALUES
(4, 'Pick up from your Hotel or Residence in Dubai or Sharjah', '2020-01-24 07:02:49', '2020-01-24 07:02:49'),
(5, 'Tanura performance', '2020-01-24 07:03:16', '2020-01-24 07:03:16'),
(6, 'Morning breakfast, tea and coffee', '2020-01-24 07:03:27', '2020-01-24 07:03:27'),
(7, 'Drop off at your Hotel', '2020-01-24 07:03:36', '2020-01-24 07:03:36');

-- --------------------------------------------------------

--
-- Table structure for table `package_ins`
--

CREATE TABLE `package_ins` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_include` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_ins`
--

INSERT INTO `package_ins` (`id`, `package_include`, `created_at`, `updated_at`) VALUES
(3, 'Dune Bashing in 4x4 vehicles', '2020-01-24 07:01:03', '2020-01-24 07:01:03'),
(4, 'Sunset Point (photograph opportunity)', '2020-01-24 07:01:23', '2020-01-24 07:01:23'),
(5, 'BBQ Dinner for Vegetarian and non-vegitarian', '2020-01-24 07:01:43', '2020-01-24 07:01:43'),
(6, 'Arabic Sweets and Fresh Fruits for desert', '2020-01-24 07:01:58', '2020-01-24 07:01:58'),
(7, 'Dune Bashing in 4x4 vehicles', '2020-01-24 07:02:18', '2020-01-24 07:02:18');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permission_title`, `permission_slug`, `permission_description`, `created_at`, `updated_at`) VALUES
(1, 'Tour Management', 'tour_management', '<p>This is used to create Tour</p>', '2020-01-20 08:34:09', '2020-02-03 04:05:40'),
(2, 'User Management', 'user_management', '<p>This is used to update User</p>', '2020-01-20 08:34:09', '2020-02-03 04:05:55'),
(3, 'Booking management', 'booking_management', '<p>This is Only Create Booking&nbsp;</p>', '2020-02-01 05:16:13', '2020-02-03 04:06:09'),
(4, 'Configuration Management', 'configuration_management', '<p>Configuration Management</p>', '2020-02-03 04:05:08', '2020-02-03 04:05:08'),
(6, 'nothing', 'nothing', '<p>asd</p>', '2020-02-04 04:22:37', '2020-02-04 04:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(19, 1, 1, '2020-02-03 04:10:50', '2020-02-03 04:10:50'),
(32, 3, 3, '2020-02-04 04:23:22', '2020-02-04 04:23:22'),
(33, 4, 3, '2020-02-04 04:23:22', '2020-02-04 04:23:22'),
(34, 2, 6, '2020-02-04 04:24:32', '2020-02-04 04:24:32'),
(35, 3, 6, '2020-02-04 04:24:32', '2020-02-04 04:24:32'),
(36, 4, 6, '2020-02-04 04:24:32', '2020-02-04 04:24:32'),
(43, 2, 2, '2020-02-13 14:25:27', '2020-02-13 14:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `post_requests`
--

CREATE TABLE `post_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persons` int(11) DEFAULT NULL,
  `request` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_requests`
--

INSERT INTO `post_requests` (`id`, `name`, `email`, `phone`, `persons`, `request`, `created_at`, `updated_at`) VALUES
(15, 'asd', 'waqarhashmi42@gmail.com', '1321', 1, 'asdas', '2020-01-23 09:31:17', '2020-01-23 09:31:17'),
(16, 'asd', 'waqarhashmi42@gmail.com', '1321', 1, 'asdas', '2020-01-23 10:05:12', '2020-01-23 10:05:12'),
(17, 'asd', 'waqarhashmi42@gmail.com', '1321', 1, 'asdas', '2020-01-23 10:06:07', '2020-01-23 10:06:07'),
(18, 'asd', 'waqarhashmi42@gmail.com', '1321', 1, 'asdas', '2020-01-23 10:07:46', '2020-01-23 10:07:46'),
(19, 'mateen ahmad', 'mateen.ahmad@rapidfiresol.com', '030000', 1, 'asdsa', '2020-01-23 10:09:32', '2020-01-23 10:09:32'),
(20, 'sad', 'waqarhashmi42@gmail.com', '030222', NULL, 'dasdasdas', '2020-01-23 10:21:48', '2020-01-23 10:21:48'),
(21, 'sad', 'waqarhashmi42@gmail.com', '030222', NULL, 'dasdasdas', '2020-01-23 10:25:16', '2020-01-23 10:25:16'),
(22, 'sad', 'waqarhashmi42@gmail.com', '030222', NULL, 'dasdasdas', '2020-01-23 10:27:10', '2020-01-23 10:27:10'),
(23, NULL, 'viky@gmail.com', NULL, NULL, NULL, '2020-01-28 09:05:50', '2020-01-28 09:05:50'),
(27, NULL, 'abc@hotmail.com', NULL, NULL, NULL, '2020-01-28 12:48:57', '2020-01-28 12:48:57'),
(28, NULL, 'abc@hotmail.com', NULL, NULL, NULL, '2020-01-28 12:49:10', '2020-01-28 12:49:10'),
(29, NULL, 'mateen.ahmad@rapidfiresol.com', NULL, NULL, NULL, '2020-01-28 12:49:38', '2020-01-28 12:49:38'),
(32, 'viky', 'waqarhashmi42@gmail.com', '030222', 1, 'asdasasdas', '2020-01-29 06:02:15', '2020-01-29 06:02:15'),
(33, 'viky', 'waqarhashmi42@gmail.com', '03021234566546', NULL, 'asdasasd', '2020-01-29 06:06:41', '2020-01-29 06:06:41'),
(34, 'vikyyyyy', 'waqarhashmi42@gmail.com', '033333333333333', 121313546, 'ignore it', '2020-01-29 06:12:31', '2020-01-29 06:12:31'),
(35, 'ali', 'waqarhashmi42@gmail.com', '0322224564654', 151351, '213546879', '2020-01-29 06:15:40', '2020-01-29 06:15:40'),
(36, 'waqar', 'waqarhashmi42@gmail.com', '02133221546', 1121, 'asdasasd', '2020-01-29 06:19:20', '2020-01-29 06:19:20'),
(37, 'waqar', 'waqarhashmi42@gmail.com', '02133221546', 1121, 'asdasasd', '2020-01-29 06:19:45', '2020-01-29 06:19:45'),
(38, NULL, 'viky@gmail.com', NULL, NULL, NULL, '2020-01-29 06:21:50', '2020-01-29 06:21:50'),
(39, NULL, 'viky@gmail,com', NULL, 1, NULL, '2020-01-29 06:25:23', '2020-01-29 06:25:23'),
(40, NULL, 'viky@gmail.com', NULL, 1, NULL, '2020-01-29 06:25:36', '2020-01-29 06:25:36'),
(45, 'waqar', 'viky@gmail.com', '1231546', 321546879, 'asdas', '2020-01-29 10:03:09', '2020-01-29 10:03:09'),
(46, 'waqar', 'viky@gmail.com', '1231546', 321546879, 'asdas', '2020-01-29 10:03:49', '2020-01-29 10:03:49'),
(53, 'sdf', 'sdf@emai.ccc', 'asdas', 1, 'asdas', '2020-02-06 09:03:55', '2020-02-06 09:03:55'),
(54, 'asd', 'asd@email.com', 'asd', 1, 'dasdasd', '2020-02-06 09:04:18', '2020-02-06 09:04:18'),
(55, NULL, 'asdfasdfasd', NULL, NULL, NULL, '2020-02-06 09:22:11', '2020-02-06 09:22:11'),
(56, 'asd', 'asd@d.com', 'asd', 1, 'asdas', '2020-02-06 09:27:27', '2020-02-06 09:27:27'),
(57, 'asd', 'asd@gmail.com', 'asd', 1, 'asdas', '2020-02-06 09:34:42', '2020-02-06 09:34:42'),
(58, 'asd', 'A@v.com', 'asd', NULL, 'asdas', '2020-02-06 11:09:04', '2020-02-06 11:09:04'),
(59, 'muhammamd soban', 'muhammad.soban@ozitechnology.com', '03367030601', NULL, 'lets go have fun by rapid fire.sol', '2020-02-14 22:34:33', '2020-02-14 22:34:33'),
(60, 'awais', 'awasi2asfakln@as.com', NULL, NULL, 'good work', '2020-02-14 22:39:01', '2020-02-14 22:39:01'),
(61, 'asdfasf', 'asf@asfa', 'asfa', 1, 'afa', '2020-02-14 22:40:42', '2020-02-14 22:40:42'),
(62, 'asfa', 'asdfas@asfa', NULL, NULL, 'asfasf', '2020-02-14 22:54:26', '2020-02-14 22:54:26'),
(63, 'asdfa', 'asfa@af.com', NULL, NULL, 'asdgva', '2020-02-14 22:56:23', '2020-02-14 22:56:23'),
(64, 'asfa', 'asf@asf', NULL, NULL, 'asfa', '2020-02-14 22:58:03', '2020-02-14 22:58:03'),
(65, 'NeooMuh', 'tetakhiling2015@mail.ru', NULL, NULL, 'Здесь вы можете заказать копию любого сайта под ключ, недорого и качественно, при этом не тратя свое время на различные программы и фриланс-сервисы. \r\n \r\nКлонированию подлежат сайты как на конструкторах, так и на движках: \r\n- Tilda (Тильда) \r\n- Wix (Викс)', '2020-02-17 15:08:49', '2020-02-17 15:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_title`, `role_slug`, `role_description`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '<p>This is test</p>', '2020-01-20 08:34:09', '2020-02-03 04:10:50'),
(2, 'Guest User', 'customer', '<p>This is test</p>', '2020-01-20 08:34:09', '2020-02-03 04:10:58'),
(3, 'Developer', 'developer', '<p>This is Developer</p>', '2020-02-01 05:09:54', '2020-02-01 09:30:39'),
(6, 'QA', 'qa', '<p>this is QA</p>', '2020-02-04 04:24:32', '2020-02-04 04:24:32');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-01-20 08:34:09', '2020-02-11 11:15:46'),
(2, 2, 2, '2020-01-20 08:34:09', '2020-02-01 07:32:41'),
(3, 3, 3, '2020-02-01 07:45:14', '2020-02-01 07:45:14'),
(4, 6, 4, '2020-02-04 04:29:19', '2020-02-04 04:29:43'),
(7, 2, 8, '2020-02-10 06:58:54', '2020-02-10 06:58:54'),
(8, 2, 9, '2020-02-13 14:22:31', '2020-02-17 13:27:20');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Abu Dahbi', '2020-01-29 07:20:43', '2020-01-29 07:22:19'),
(2, 1, 'Dubai', '2020-01-29 07:25:07', '2020-01-29 07:25:07'),
(3, 2, 'Lahore', '2020-01-29 07:34:57', '2020-01-29 07:34:57'),
(4, 2, 'Islamabad', '2020-01-29 08:50:11', '2020-01-29 08:50:11');

-- --------------------------------------------------------

--
-- Table structure for table `tourimages`
--

CREATE TABLE `tourimages` (
  `id` int(10) UNSIGNED NOT NULL,
  `comp_id` int(11) DEFAULT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `tour_images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tourimages`
--

INSERT INTO `tourimages` (`id`, `comp_id`, `tour_id`, `tour_images`, `created_at`, `updated_at`) VALUES
(58, 1, 68, 'sPOb5-121341-PPn.jpg', '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(59, 1, 68, 'mOinE-121341-mVg.jpg', '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(60, 1, 68, 'crUG6-121341-tsJ.jpg', '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(61, 1, 68, 'ft3ng-121342-GSp.jpg', '2020-01-24 07:13:42', '2020-01-24 07:13:42'),
(62, 1, 68, 'AWIGC-121342-hoP.jpg', '2020-01-24 07:13:42', '2020-01-24 07:13:42'),
(63, 1, 69, 'I62P5-121652-1Ot.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(64, 1, 69, 'xaijU-121652-CQb.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(65, 1, 69, 'RgnpB-121652-MP8.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(66, 1, 69, 'lN6YD-121652-L4H.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(67, 1, 69, 'cihNE-121652-5N7.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(68, 1, 69, 'C42hk-121652-Axg.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(69, 1, 69, 'XYunK-121652-1Lo.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(70, 1, 69, 'ZfGFo-121652-LPU.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(71, 1, 69, 'TieSG-121652-Wmh.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(72, 1, 69, 'LebHZ-121652-e6i.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(73, 1, 69, 'FcyXS-121652-dgU.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(74, 1, 69, 'Iz3cJ-121652-1jw.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(75, 1, 69, 'w6yPb-121652-R1I.jpg', '2020-01-24 07:16:52', '2020-01-24 07:16:52'),
(76, 1, 70, 'RJcg6-121927-vGZ.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(77, 1, 70, 'B9WsH-121927-Iyj.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(78, 1, 70, 'oJPCK-121927-rAn.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(79, 1, 70, 'HBnEP-121927-pju.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(80, 1, 70, '3e6WT-121927-Qzv.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(81, 1, 70, 'POGTF-121927-IDx.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(82, 1, 70, 'ayPt1-121927-BKs.jpg', '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(83, 1, 73, '055MB-123735-eeg.jpg', '2020-01-24 07:37:35', '2020-01-24 07:37:35'),
(84, 1, 73, 'h6R61-123735-ZgD.jpg', '2020-01-24 07:37:35', '2020-01-24 07:37:35'),
(85, 1, 77, 'mLjbc-010309-2X1.jpg', '2020-01-24 08:03:09', '2020-01-24 08:03:09'),
(86, 1, 78, 'IoKnq-070127-jcc.jpg', '2020-01-27 02:01:27', '2020-01-27 02:01:27'),
(87, 1, 78, 'YbTK5-070127-zNN.jpg', '2020-01-27 02:01:27', '2020-01-27 02:01:27'),
(88, 1, 78, 'vArGi-070127-laB.jpg', '2020-01-27 02:01:27', '2020-01-27 02:01:27'),
(89, 1, 78, '5G0cE-070127-M96.jpg', '2020-01-27 02:01:27', '2020-01-27 02:01:27'),
(90, 1, 79, 'X2RZ7-070232-sG5.jpg', '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(91, 1, 79, 'EtxpL-070232-6Tz.jpg', '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(92, 1, 79, '69vs4-070232-AfM.jpg', '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(93, 1, 79, 'nANeb-070233-nfL.jpg', '2020-01-27 02:02:33', '2020-01-27 02:02:33'),
(94, 1, 80, 'EqVcI-070339-gf4.jpg', '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(95, 1, 80, '1LqCY-070339-sLb.jpg', '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(96, 1, 80, 'y5oQ6-070339-yWO.jpg', '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(97, 1, 80, 'IhYAo-070340-KdB.jpg', '2020-01-27 02:03:40', '2020-01-27 02:03:40'),
(98, 1, 80, 'Esner-070340-LPr.jpg', '2020-01-27 02:03:40', '2020-01-27 02:03:40'),
(104, 1, 82, 'tZEoo-070526-QRg.jpg', '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(105, 1, 82, 'e0oBc-070526-knX.jpg', '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(106, 1, 82, 'kHxMD-070526-0Qq.jpg', '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(107, 1, 82, 'iGrl2-070527-TIU.jpg', '2020-01-27 02:05:27', '2020-01-27 02:05:27'),
(108, 1, 82, 'iwapK-070527-qGu.jpg', '2020-01-27 02:05:27', '2020-01-27 02:05:27'),
(109, 1, 83, 'pxARQ-070653-nWX.jpg', '2020-01-27 02:06:53', '2020-01-27 02:06:53'),
(110, 1, 83, 'RhVTQ-070653-bSS.jpg', '2020-01-27 02:06:53', '2020-01-27 02:06:53'),
(111, 1, 83, 'qwUBy-070653-N8E.jpg', '2020-01-27 02:06:53', '2020-01-27 02:06:53'),
(118, 1, 85, 'YCnrd-070824-V2C.jpg', '2020-01-27 02:08:24', '2020-01-27 02:08:24'),
(119, 1, 85, 'b4cSr-070824-Zqp.jpg', '2020-01-27 02:08:24', '2020-01-27 02:08:24'),
(120, 1, 85, 'pTw94-070824-xxf.jpg', '2020-01-27 02:08:24', '2020-01-27 02:08:24'),
(121, 1, 85, 'urULL-070824-3KR.jpg', '2020-01-27 02:08:24', '2020-01-27 02:08:24'),
(122, 1, 85, 'VByNt-070824-2Qx.jpg', '2020-01-27 02:08:24', '2020-01-27 02:08:24'),
(123, 1, 85, '5kY9D-070824-3cR.jpg', '2020-01-27 02:08:24', '2020-01-27 02:08:24'),
(124, 1, 86, 'atX82-071010-ASf.jpg', '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(125, 1, 86, 'bveNB-071010-w2V.jpg', '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(126, 1, 86, 'tfrtQ-071010-fZE.jpg', '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(127, 1, 86, 'mgb3f-071010-1L8.jpg', '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(128, 1, 86, 'jKfez-071010-zPp.jpg', '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(129, 1, 86, 'tJ7FR-071011-lDU.jpg', '2020-01-27 02:10:11', '2020-01-27 02:10:11'),
(130, 1, 87, 'f3twO-071151-1Pb.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(131, 1, 87, 'Edfrs-071151-aHa.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(132, 1, 87, 'LihTD-071151-rMf.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(133, 1, 87, 'Flg0o-071151-Tyi.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(134, 1, 87, 'Ep364-071151-5mX.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(135, 1, 87, 'vk5AK-071151-qLU.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(136, 1, 87, 'LrWUh-071151-LSa.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(137, 1, 87, 'yURas-071151-nwu.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(138, 1, 87, 'vEnWx-071151-DFI.jpg', '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(139, 1, 88, '5dLWp-071315-D95.jpg', '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(140, 1, 88, 'XLZoH-071315-DpL.jpg', '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(141, 1, 88, 'rxSYZ-071315-aoX.jpg', '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(142, 1, 88, 'fiWfn-071316-pQL.jpg', '2020-01-27 02:13:16', '2020-01-27 02:13:16'),
(143, 1, 88, 'PC2Y0-071316-8JT.jpg', '2020-01-27 02:13:16', '2020-01-27 02:13:16'),
(144, 1, 89, 'wgbaD-071425-G22.jpg', '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(145, 1, 89, 'M83Lc-071425-bgU.jpg', '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(146, 1, 89, 'OevVM-071426-iMb.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(147, 1, 89, 'vEsrR-071426-nYf.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(148, 1, 89, '5gvPZ-071426-wjp.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(149, 1, 89, 'GdrXf-071426-SaW.jpg', '2020-01-27 02:14:26', '2020-01-27 02:14:26'),
(150, 1, 90, 'YfIEV-071516-D4h.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(151, 1, 90, 'dE4yU-071516-pdv.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(152, 1, 90, 'v3BQT-071516-rX0.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(153, 1, 90, 'pH7sF-071516-1PC.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(154, 1, 90, 'cLE5d-071516-FPV.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(155, 1, 90, 'R7o0x-071516-XU8.jpg', '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(156, 1, 91, 'rYDst-071601-V35.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(157, 1, 91, 'RxxoT-071601-UFC.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(158, 1, 91, 'spaRr-071601-CU5.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(159, 1, 91, 'voXFz-071601-Mnv.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(160, 1, 91, 'wBP7I-071601-cZt.jpg', '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(161, 1, 92, 'fVERh-071701-L0e.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(162, 1, 92, 'cBizq-071701-db8.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(163, 1, 92, 'sugcM-071701-PBQ.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(164, 1, 92, 'XLtEu-071701-MX5.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(165, 1, 92, 'WTVh8-071701-4sG.jpg', '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(166, 1, 93, 'Ah0eg-071744-nAr.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(167, 1, 93, 'OyPdc-071744-SqJ.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(168, 1, 93, '1S9eg-071744-71J.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(169, 1, 93, 'IqgfE-071744-IRu.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(170, 1, 93, 'pndAx-071744-XJA.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(171, 1, 93, 'T2oZe-071744-qHt.jpg', '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(172, 1, 94, '5Oc9p-071829-rqT.jpg', '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(173, 1, 94, 'rx4bv-071829-gLC.jpg', '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(174, 1, 94, 'WmBOw-071830-EEA.jpg', '2020-01-27 02:18:30', '2020-01-27 02:18:30'),
(175, 1, 94, 'wdYKd-071830-WPC.jpg', '2020-01-27 02:18:30', '2020-01-27 02:18:30'),
(176, 1, 94, 'TuQiU-071830-3oi.jpg', '2020-01-27 02:18:30', '2020-01-27 02:18:30'),
(177, 1, 95, '8GI7t-071917-Z60.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(178, 1, 95, 'Yr9uJ-071917-2S8.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(179, 1, 95, '52WQV-071917-s7W.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(180, 1, 95, 'WE3tu-071917-IKh.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(181, 1, 95, 'Q0u2A-071917-Oky.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(182, 1, 95, 'H4ud4-071917-drq.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(183, 1, 95, 'Qs1Ga-071917-sDW.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(184, 1, 95, 'IT5f0-071917-3k6.jpg', '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(185, 1, 96, 'YKbLo-072006-xJS.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(186, 1, 96, 'Tasak-072006-jdu.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(187, 1, 96, 'AkkQy-072006-iKN.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(188, 1, 96, 'nvX5c-072006-D0A.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(189, 1, 96, 'S2Mro-072006-W1x.jpg', '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(190, 1, 97, 'nM6lw-072056-gtt.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(191, 1, 97, 'EEBeg-072056-zTR.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(192, 1, 97, 'lJnWP-072056-kHB.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(193, 1, 97, 'rVfj5-072056-kdo.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(194, 1, 97, 'dHoEi-072056-6pj.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(195, 1, 97, 'kIfNq-072056-GGu.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(196, 1, 97, 'Tkz64-072056-Cyt.jpg', '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(197, 1, 98, '0AomK-072147-8gm.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(198, 1, 98, 'diHdP-072147-iDf.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(199, 1, 98, '2DjNc-072147-5p9.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(200, 1, 98, 'lqRJ9-072147-uTe.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(201, 1, 98, 'mipuB-072147-5Sk.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(202, 1, 98, 'j4aJp-072147-Prn.jpg', '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(203, 1, 99, 'erUFb-072241-KAB.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(204, 1, 99, 'T1Z3x-072241-dhN.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(205, 1, 99, '29JDY-072241-5YU.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(206, 1, 99, 'dYZ47-072241-GVM.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(207, 1, 99, 'oNkGi-072241-6IF.jpg', '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(208, 1, 99, 'CleEU-072242-v0j.jpg', '2020-01-27 02:22:42', '2020-01-27 02:22:42'),
(209, 1, 100, 't4yAy-072332-46p.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(210, 1, 100, 'D9kqa-072332-4u8.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(211, 1, 100, 'nMx6N-072332-lNR.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(212, 1, 100, 'YxtTU-072332-esI.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(213, 1, 100, 'gTyJg-072332-3iM.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(214, 1, 100, 'CLsQE-072332-LGG.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(215, 1, 100, 'nAGT1-072332-btj.jpg', '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(216, 1, 101, 'LNuXJ-072430-TcG.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(217, 1, 101, 'F1n7N-072430-cDb.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(218, 1, 101, 'iLK9o-072430-u0h.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(219, 1, 101, 'ZsVni-072430-o0l.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(220, 1, 101, 'CkRCd-072430-yZ9.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(221, 1, 101, 'KlhNT-072430-1Iz.jpg', '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(222, 1, 101, 'uy1Vx-072431-kOv.jpg', '2020-01-27 02:24:31', '2020-01-27 02:24:31'),
(223, 1, 101, 'GIlBT-072431-2tb.jpg', '2020-01-27 02:24:31', '2020-01-27 02:24:31'),
(224, 1, 102, 'Skk9O-072525-WDv.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(225, 1, 102, 'WWhQ5-072525-B71.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(226, 1, 102, 'WiilP-072525-kMB.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(227, 1, 102, 'Samqu-072525-mjF.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(228, 1, 102, '6nZTb-072525-TXX.jpg', '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(229, 1, 103, 'KG5LU-072617-JMD.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(230, 1, 103, 'U5GOd-072617-itM.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(231, 1, 103, 'JjCWe-072617-mns.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(232, 1, 103, 'o4cfI-072617-31Y.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(233, 1, 103, 'DsCsl-072617-yYz.jpg', '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(234, 1, 104, '69amN-072732-AjH.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(235, 1, 104, 'lA0ox-072732-QDw.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(236, 1, 104, 'ISzHJ-072732-5Wy.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(237, 1, 104, 'EASoq-072732-aUO.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(238, 1, 104, 'q3nnq-072732-3OS.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(239, 1, 104, 'SEZGo-072732-xEP.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(240, 1, 104, 'SHoEC-072732-euw.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(241, 1, 104, 'PWkju-072732-99y.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(242, 1, 104, 'QegL1-072732-75e.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(243, 1, 104, '9siJl-072732-ZkK.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(244, 1, 104, '4Q39K-072732-n7Y.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(245, 1, 104, '9TjFz-072732-Rvp.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(246, 1, 104, 'OnS4f-072732-lir.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(247, 1, 104, 'k7g40-072732-LD3.jpg', '2020-01-27 02:27:32', '2020-01-27 02:27:32'),
(248, 1, 105, 'gih7I-072826-qxR.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(249, 1, 105, 'fw4kX-072826-SzT.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(250, 1, 105, 'vpTGa-072826-RYS.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(251, 1, 105, 'q4fvJ-072826-if1.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(252, 1, 105, 'WhLIm-072826-GkW.jpg', '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(253, 1, 106, 'gtthg-072953-eOE.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(254, 1, 106, 'Hfh9N-072953-GdQ.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(255, 1, 106, '62kPA-072953-ihy.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(256, 1, 106, 'iLtG0-072953-bFt.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(257, 1, 106, 'Soe03-072953-Cu5.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(258, 1, 106, 'pIvc6-072953-TL8.jpg', '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(259, 1, 107, 'u55Xf-073056-9wR.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(260, 1, 107, 'bkPPU-073056-bzp.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(261, 1, 107, 'H2mu9-073056-VCZ.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(262, 1, 107, 'FUfRj-073056-9em.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(263, 1, 107, 'kMPYM-073056-IFo.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(264, 1, 107, '9jriB-073056-7nW.jpg', '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(265, 1, 108, 'K1jS1-073145-wj8.jpg', '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(266, 1, 108, 'LPdF6-073145-FiB.jpg', '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(267, 1, 108, 'bsuAU-073145-Hkb.jpg', '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(268, 1, 108, 'krYNq-073146-AtP.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(269, 1, 108, 'qzhPW-073146-y3C.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(270, 1, 108, 'O8Y8U-073146-e4b.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(271, 1, 108, 'RukfR-073146-73g.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(272, 1, 108, '7lwEr-073146-XO9.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(273, 1, 108, '04gzS-073146-ja1.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(274, 1, 108, 'JSIU7-073146-4cJ.jpg', '2020-01-27 02:31:46', '2020-01-27 02:31:46'),
(275, 1, 109, 'iV4VP-073312-rVf.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(276, 1, 109, '9hpwh-073312-nMU.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(277, 1, 109, 'QNbRs-073312-jbS.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(278, 1, 109, 'Heg8M-073312-wy3.jpg', '2020-01-27 02:33:12', '2020-01-27 02:33:12'),
(279, 1, 110, 'qT9xm-073456-QFW.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(280, 1, 110, '5TLU0-073456-UeL.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(281, 1, 110, 'AlHYK-073456-1aq.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(282, 1, 110, '2zI3q-073456-on9.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(283, 1, 110, 'wq8cp-073456-wWN.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(284, 1, 110, 'lDylO-073456-DJv.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(285, 1, 110, '9fBEv-073456-TJJ.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(286, 1, 110, 'r5Gzb-073456-fRk.jpg', '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(287, 1, 111, 'iutXk-073549-y1q.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(288, 1, 111, 'CaX49-073549-oyl.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(289, 1, 111, '8YHF0-073549-Lxp.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(290, 1, 111, 'iB175-073549-at6.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(291, 1, 111, 'PXE6X-073549-2bm.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(292, 1, 111, 'SGOow-073549-YP4.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(293, 1, 111, 'tBslp-073549-YX2.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(294, 1, 111, 'ZZGwm-073549-jPL.jpg', '2020-01-27 02:35:49', '2020-01-27 02:35:49'),
(295, 1, 112, 'n5hTR-073635-K38.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(296, 1, 112, '2lbY1-073635-GUw.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(297, 1, 112, 'LZkIA-073635-lFS.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(298, 1, 112, 'XRpQs-073635-Hrk.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(299, 1, 112, 'E2GuH-073635-AMG.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(300, 1, 112, 'pmiMU-073635-HzU.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(301, 1, 112, '0gGuI-073635-EIS.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(302, 1, 112, 'UZI6p-073635-zzE.jpg', '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(303, 1, 113, 'QLpkk-073720-zWB.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(304, 1, 113, '0uSYQ-073720-ETp.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(305, 1, 113, 'OgTqY-073720-tI8.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(306, 1, 113, 'fpgqC-073720-ThW.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(307, 1, 113, 'dXTUp-073720-qFi.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(308, 1, 113, 'IaKjU-073720-qCI.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(309, 1, 113, '5hVqz-073720-Gr3.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(310, 1, 113, 'SeD78-073720-i5i.jpg', '2020-01-27 02:37:20', '2020-01-27 02:37:20'),
(311, 1, 114, 'P1EnL-073805-FY8.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(312, 1, 114, 'u0MFx-073805-Xp3.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(313, 1, 114, 'H5bSd-073805-7po.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(314, 1, 114, 'M1s1Y-073805-osa.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(315, 1, 114, 'Zvtg2-073805-R8W.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(316, 1, 114, 'q8Yde-073805-nvg.jpg', '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(317, 1, 115, 'eMyuY-073856-Atf.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(318, 1, 115, 'eexBS-073856-yLc.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(319, 1, 115, '1Q6pI-073856-N8H.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(320, 1, 115, 'VHkkQ-073856-8tI.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(321, 1, 115, 'wy889-073856-C2o.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(322, 1, 115, 'zDQBD-073856-iY8.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(323, 1, 115, 'edRiM-073856-47v.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(324, 1, 115, 'gMMJl-073856-qou.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(325, 1, 115, 'jRMVV-073856-UfZ.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(326, 1, 115, 'V0MOn-073856-oEY.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(327, 1, 115, 'rZkOQ-073856-3z7.jpg', '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(328, 1, 116, '5OAbq-073944-IF3.jpg', '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(329, 1, 116, 'qnlyv-073944-Cwe.jpg', '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(330, 1, 116, 'FjUFF-073945-Eur.jpg', '2020-01-27 02:39:45', '2020-01-27 02:39:45'),
(331, 1, 116, 'GAm8m-073945-QWT.jpg', '2020-01-27 02:39:45', '2020-01-27 02:39:45'),
(332, 1, 116, 'qISp7-073945-LXX.jpg', '2020-01-27 02:39:45', '2020-01-27 02:39:45'),
(333, 1, 117, 'FymDm-074027-AHV.jpg', '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(334, 1, 117, 'GlPWL-074027-jC0.jpg', '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(335, 1, 117, '4acul-074027-GyK.jpg', '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(336, 1, 117, 'fsJ8o-074027-fBh.jpg', '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(337, 1, 117, 'gFca6-074027-z1m.jpg', '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(338, 1, 117, 'pA1qe-074027-X52.jpg', '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(339, 1, 117, 'eoN7f-074027-dxH.jpg', '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(344, 1, 119, 'UYoc2-055904-s1V.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(345, 1, 119, 'PDIXk-055904-Xcl.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(346, 1, 119, 'Mhplb-055904-HvR.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(347, 1, 119, '1qGN2-055904-xwo.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(348, 1, 119, 'UO9yd-055904-jIa.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(349, 1, 119, 'ZzZao-055904-dUz.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(350, 1, 119, 'Ywi7R-055904-h5k.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(351, 1, 119, 'VnzSk-055904-HaJ.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(352, 1, 119, 'u6Cqg-055904-azE.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(353, 1, 119, 'UvbVp-055904-0EG.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(354, 1, 119, 'xdWG1-055904-v5S.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(355, 1, 119, 'ZED7v-055904-xz6.png', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(356, 1, 119, 'ykNOj-055904-0x6.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(357, 1, 119, '2j9qk-055904-nxH.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(358, 1, 119, 'eKWDK-055904-InV.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(359, 1, 119, 'osNmS-055904-LJo.jpg', '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(360, 1, 120, 'yEu4Q-103449-J13.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(361, 1, 120, 'DXs2N-103449-Imr.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(362, 1, 120, 'Vbx3e-103449-iLV.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(363, 1, 120, 'j914L-103449-RFh.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(364, 1, 120, 'Y7FCw-103449-3nB.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(365, 1, 120, 'peCoC-103449-T7W.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(366, 1, 120, 'oHQ3P-103449-XVq.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(367, 1, 120, 'Ac4AT-103449-zQ2.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(368, 1, 120, 'f4dMd-103449-cCA.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(369, 1, 120, 'KWerm-103449-VDm.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(370, 1, 120, 'nVq9w-103449-CVR.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(371, 1, 120, 'T2zLG-103449-5iA.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(372, 1, 120, 'YP2PX-103449-Nqc.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(373, 1, 120, 'PjxLn-103449-pDW.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(374, 1, 120, 'nk2QU-103449-lNI.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(375, 1, 120, 'jwCs9-103449-Bul.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(376, 1, 120, 'lynmf-103449-lfL.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(377, 1, 120, 'kO7r1-103449-QAX.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(378, 1, 120, 'bcyUy-103449-i3s.jpg', '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(379, 1, 118, 'J9waP-034625-Umf.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(380, 1, 118, 'o0z0Y-034625-mkS.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(381, 1, 118, 'bNgTC-034625-Lpg.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(382, 1, 118, 'FbKLr-034625-mvv.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(383, 1, 118, 'wXTZl-034625-fDJ.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(384, 1, 118, 'eeqCE-034625-xKt.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(385, 1, 118, 'oAsxS-034625-cBO.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(386, 1, 118, 'UfgSM-034625-EZw.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(387, 1, 118, '1IMaN-034625-VIq.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(388, 1, 118, 'kgTyO-034625-fsd.jpg', '2020-01-31 10:46:25', '2020-01-31 10:46:25'),
(389, 2, 121, 'P8YGA-035009-74I.png', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(390, 2, 121, 'qG0Ek-035009-nKq.jpg', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(391, 2, 121, 'XQdKG-035009-kMF.jpg', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(392, 2, 121, 'cdhL4-035009-Jwt.jpg', '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(393, NULL, 78, 'Kn9a5-081502-KWw.jpg', '2020-02-17 13:15:02', '2020-02-17 13:15:02'),
(394, 2, 122, '5iqAU-081806-MN9.png', '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(395, 2, 123, 'zI8QJ-112633-OiC.png', '2020-02-20 16:26:33', '2020-02-20 16:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE `tours` (
  `id` int(10) UNSIGNED NOT NULL,
  `comp_id` int(11) DEFAULT NULL,
  `Tour_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Tour_Desc` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Tour_Adult_price` int(11) DEFAULT NULL,
  `Tour_Adult_disc_price` int(11) DEFAULT NULL,
  `Tour_Child_price` int(11) DEFAULT NULL,
  `Tour_Child_disc_price` int(11) DEFAULT NULL,
  `Tour_type_name` int(11) DEFAULT NULL,
  `TourType_Code` int(11) DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `Tour_Duration` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`id`, `comp_id`, `Tour_name`, `Tour_Desc`, `profile_image`, `Tour_Adult_price`, `Tour_Adult_disc_price`, `Tour_Child_price`, `Tour_Child_disc_price`, `Tour_type_name`, `TourType_Code`, `is_enabled`, `Tour_Duration`, `CreatedBy`, `created_at`, `updated_at`, `deleted_at`) VALUES
(78, NULL, 'Airport Trfs', 'Airport Trfs', '1581927427.jpg', 123, 123, 123, 123, 1, NULL, 0, '123', 0, '2020-01-27 02:01:26', '2020-02-20 16:35:05', NULL),
(79, NULL, 'Inter Hotel Trfs', 'Inter Hotel Trfs', '1580989461.jpg', 1, NULL, NULL, NULL, 5, NULL, 1, NULL, 0, '2020-01-27 02:02:32', '2020-02-20 16:41:50', NULL),
(80, NULL, 'Trio (Standard)', 'Trio (Standard)', '1580108619.jpg', 213, 23, 232, 232, 6, NULL, 0, '2', 0, '2020-01-27 02:03:39', '2020-02-20 16:35:15', NULL),
(82, NULL, 'Trio (Luxury)', 'Trio (Luxury)', '1580108726.jpg', 323, 23432, 234, 234, 9, NULL, 0, '2', 0, '2020-01-27 02:05:25', '2020-02-20 16:35:21', NULL),
(83, NULL, 'Trio (Premium)', 'Trio (Premium)', '1580108813.jpg', 123, 123, 123, 1231, 6, NULL, 0, '123', 0, '2020-01-27 02:06:52', '2020-02-20 16:35:31', NULL),
(84, NULL, 'Dxb City Tour', 'Dxb City Tour', '1581927388.png', 213, 124, 124, 124, 1, NULL, 0, '124', 0, '2020-01-27 02:07:38', '2020-02-20 16:35:45', NULL),
(85, NULL, 'Meraas Tour', 'Meraas Tour', '1580108904.jpg', 2423, 234, 234, 23423, 1, NULL, 1, '234', 0, '2020-01-27 02:08:23', '2020-02-20 16:36:02', NULL),
(86, 1, 'Desert Safari', 'Desert Safari', '1580109010.jpg', 232, 34, 234, 234, 5, NULL, 1, '234', 0, '2020-01-27 02:10:10', '2020-01-27 02:10:10', NULL),
(87, 1, 'Abudhabi Desert Safari', 'Abudhabi Desert Safari', '1580109111.jpg', 5646, 123, 123, 123, 5, NULL, 1, '123', 0, '2020-01-27 02:11:50', '2020-01-27 02:11:51', NULL),
(88, 1, 'Creek Dhow Cruise', 'Creek Dhow Cruise', '1580109195.jpg', 213, 312, 123, 123, 1, NULL, 1, '123', 0, '2020-01-27 02:13:15', '2020-01-27 02:13:15', NULL),
(89, NULL, 'Canal Dhow Cruise', 'Canal Dhow Cruise', '1580109265.jpg', 1231, 123, 123, 123, 7, NULL, 0, '123', 0, '2020-01-27 02:14:25', '2020-01-31 11:35:22', NULL),
(90, 1, 'Marina Dhow Cruise', 'Marina Dhow Cruise', '1580109316.jpg', 123, 24, 1243, 124, 1, NULL, 1, '124', 0, '2020-01-27 02:15:16', '2020-01-27 02:15:16', NULL),
(91, 1, 'Queen Elizabeth 2 Tour', 'Queen Elizabeth 2 Tour', '1580109361.jpg', 432, 234, 234, 234, 6, NULL, 1, '234', 0, '2020-01-27 02:16:00', '2020-01-27 02:16:01', NULL),
(92, 1, 'Flying Cup', 'Flying Cup', '1580109421.jpg', 324, 234, 234, 234, 1, NULL, 1, '23423', 0, '2020-01-27 02:17:00', '2020-01-27 02:17:01', NULL),
(93, 1, 'Chillout Ice Lounge', 'Chillout Ice Lounge', '1580109464.jpg', 423, 23, 4, 23, 1, NULL, 1, '23', 0, '2020-01-27 02:17:43', '2020-01-27 02:17:44', NULL),
(94, 1, 'iFly', 'iFly', '1580109509.jpg', 32, 234, 24, 234, 1, NULL, 1, '23', 0, '2020-01-27 02:18:29', '2020-01-27 02:18:29', NULL),
(95, 1, 'Wild Wadi Water Park', 'Wild Wadi Water Park', '1580109557.jpg', 23, 234, 234, 234, 1, NULL, 1, '234', 0, '2020-01-27 02:19:16', '2020-01-27 02:19:17', NULL),
(96, 1, 'Dubai Frame', 'Dubai Frame', '1580109606.jpg', 234, 23, 234, 234, 6, NULL, 1, '234', 0, '2020-01-27 02:20:06', '2020-01-27 02:20:06', NULL),
(97, 1, 'Ras Al Khaimah Zipline', 'Ras Al Khaimah Zipline', '1580109656.jpg', 324223, 234, 234, 234, 7, NULL, 1, '234', 0, '2020-01-27 02:20:55', '2020-01-27 02:20:56', NULL),
(98, 1, 'Hot Air Balloon Ride', 'Hot Air Balloon Ride', '1580109707.jpg', 234, 234, 23, 234, 9, NULL, 1, '234', 0, '2020-01-27 02:21:46', '2020-01-27 02:21:47', NULL),
(99, 1, 'Yellow Boat Ride', 'Yellow Boat Ride', '1580109761.jpg', 2342, 3234, 432, 42, 6, NULL, 1, '234', 0, '2020-01-27 02:22:40', '2020-01-27 02:22:41', NULL),
(100, 1, 'Wonder Bus', 'Wonder Bus', '1580109812.jpg', 234, 234, 234, 234, 1, NULL, 1, '234', 0, '2020-01-27 02:23:31', '2020-01-27 02:23:32', NULL),
(101, 1, 'Abu Dhabi City Tour', 'Abu Dhabi City Tour', '1580109870.jpg', 123, 123, 123, 12, 1, NULL, 1, '123', 0, '2020-01-27 02:24:30', '2020-01-27 02:24:30', NULL),
(102, 1, 'Seasonal Tour', 'Seasonal Tour', '1580109925.jpg', 234, 244, 43, 456, 6, NULL, 1, '567', 0, '2020-01-27 02:25:25', '2020-01-27 02:25:25', NULL),
(103, 1, 'Abu Dhabi City Tour with Ferrari Park + Yas Waterworld + Warner Bros', 'Abu Dhabi City Tour with Ferrari Park + Yas Waterworld + Warner Bros', '1580109977.jpg', 324, 234, 234, 234, 1, NULL, 1, '423', 0, '2020-01-27 02:26:16', '2020-01-27 02:26:17', NULL),
(104, 1, 'Dubai Mall', 'Dubai Mall', '1580110051.jpg', 324, 23, 43, 34, 1, NULL, 1, '34', 0, '2020-01-27 02:27:31', '2020-01-27 02:27:31', NULL),
(105, 1, 'Dubai Park (Valid from 01 Jan 2020)', 'Dubai Park (Valid from 01 Jan 2020)', '1580110106.jpg', 321, 234, 234, 234, 7, NULL, 1, '234', 0, '2020-01-27 02:28:25', '2020-01-27 02:28:26', NULL),
(106, 1, 'Snow Park', 'Snow Park', '1580110193.jpg', 213, 123, 123, 123, 1, NULL, 1, '123', 0, '2020-01-27 02:29:53', '2020-01-27 02:29:53', NULL),
(107, 1, 'Dolphin Show Creek', 'Dolphin Show Creek', '1580110256.jpg', 213, 123, 213, 3423, 7, NULL, 1, '2342', 0, '2020-01-27 02:30:56', '2020-01-27 02:30:56', NULL),
(108, 1, 'Atlantis', 'Atlantis', '1580110305.jpg', 234, 234, 234, 234, 1, NULL, 1, '23', 0, '2020-01-27 02:31:45', '2020-01-27 02:31:45', NULL),
(109, 1, 'IMG Park', 'IMG Park', '1580110391.jpg', 234, 234, 234, 234, 1, NULL, 1, '23', 0, '2020-01-27 02:33:11', '2020-01-27 02:33:11', NULL),
(110, 1, 'Yacht Ride', 'Yacht Ride', '1580110496.jpg', 123, 234, 23, 234, 1, NULL, 1, '234', 0, '2020-01-27 02:34:55', '2020-01-27 02:34:56', NULL),
(111, 1, 'Limo Ride', 'Limo Ride', '1580110548.jpg', 234, 23, 423, 234, 1, NULL, 1, '234', 0, '2020-01-27 02:35:48', '2020-01-27 02:35:48', NULL),
(112, 1, 'Helicopter', 'Helicopter', '1580110595.jpg', 234, 234, 234, 234, 1, NULL, 1, '234', 0, '2020-01-27 02:36:35', '2020-01-27 02:36:35', NULL),
(113, 1, 'Al Hadheerah - Bab al Shams', 'Al Hadheerah - Bab al Shams', '1580110639.jpg', 234, 234, 23, 23, 7, NULL, 1, '423', 0, '2020-01-27 02:37:19', '2020-01-27 02:37:19', NULL),
(114, 1, 'La Perle', 'La Perle', '1580110685.jpg', 234, 234, 234, 234, 1, NULL, 1, '234', 0, '2020-01-27 02:38:05', '2020-01-27 02:38:05', NULL),
(115, 1, 'Seaplane', 'Seaplane', '1580110736.jpg', 243, 234, 23, 423, 5, NULL, 1, '234', 0, '2020-01-27 02:38:56', '2020-01-27 02:38:56', NULL),
(116, 1, 'Illusion Museum', 'Illusion Museum', '1580110784.jpg', 123, 123, 123, 123, 1, NULL, 1, '123', 0, '2020-01-27 02:39:44', '2020-01-27 02:39:44', NULL),
(117, 1, 'AL AIN TOUR', 'AL AIN TOUR', '1580110827.jpg', 32, 44, 333, NULL, 1, NULL, 1, '234', 0, '2020-01-27 02:40:26', '2020-01-27 02:40:27', NULL),
(118, NULL, 'VISAS', 'VISAS', '1580486081.jpg', 342, 234, 234, 234, 1, NULL, 0, '23', 0, '2020-01-27 02:41:14', '2020-01-31 10:54:41', NULL),
(120, 1, 'Duabi tour', 'Dubai Tour', '1580294089.jpg', 31, 32, 32, 32, 5, NULL, 1, '2 days', 0, '2020-01-29 05:34:49', '2020-01-29 05:34:49', NULL),
(121, 2, 'dubai Tour', 'this is dubai tour', '1581177009.jpg', 123, 123, 123, 123, 5, NULL, 1, '123', 0, '2020-02-08 10:50:09', '2020-02-08 10:50:09', NULL),
(122, 2, 'Umrah', 'asdf', '1581927486.png', 123, 123, 1, 1, 1, NULL, 1, '12', 0, '2020-02-17 13:18:06', '2020-02-17 13:18:06', NULL),
(123, NULL, 'Umrah', 'abc', '1582197993.png', NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, 0, '2020-02-20 16:26:33', '2020-02-20 16:32:26', NULL),
(124, 2, 'Umrah 15 Days', 'abc', '', 43000, 43000, 2525, 0, 11, NULL, 1, '15', 0, '2020-02-20 16:44:16', '2020-02-20 16:49:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tour_packages`
--

CREATE TABLE `tour_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `tour_id` int(11) DEFAULT NULL,
  `pkg_include_id` int(11) DEFAULT NULL,
  `pkg_exclude_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tour_packages`
--

INSERT INTO `tour_packages` (`id`, `tour_id`, `pkg_include_id`, `pkg_exclude_id`, `created_at`, `updated_at`) VALUES
(28, 68, 3, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(29, 68, 4, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(30, 68, 5, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(31, 68, 6, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(32, 68, 7, NULL, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(33, 68, NULL, 4, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(34, 68, NULL, 5, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(35, 68, NULL, 6, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(36, 68, NULL, 7, '2020-01-24 07:13:41', '2020-01-24 07:13:41'),
(37, 69, 3, NULL, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(38, 69, 4, NULL, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(39, 69, 5, NULL, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(40, 69, NULL, 4, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(41, 69, NULL, 5, '2020-01-24 07:16:51', '2020-01-24 07:16:51'),
(42, 70, 3, NULL, '2020-01-24 07:19:26', '2020-01-24 07:19:26'),
(43, 70, 4, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(44, 70, 5, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(45, 70, 6, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(46, 70, 7, NULL, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(47, 70, NULL, 4, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(48, 70, NULL, 5, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(49, 70, NULL, 6, '2020-01-24 07:19:27', '2020-01-24 07:19:27'),
(65, 78, 3, NULL, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(66, 78, 4, NULL, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(67, 78, 5, NULL, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(68, 78, NULL, 4, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(69, 78, NULL, 5, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(70, 78, NULL, 6, '2020-01-27 02:01:26', '2020-01-27 02:01:26'),
(71, 79, 3, NULL, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(72, 79, 4, NULL, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(73, 79, 5, NULL, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(74, 79, NULL, 4, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(75, 79, NULL, 5, '2020-01-27 02:02:32', '2020-01-27 02:02:32'),
(76, 80, 4, NULL, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(77, 80, 5, NULL, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(78, 80, 6, NULL, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(79, 80, NULL, 4, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(80, 80, NULL, 5, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(81, 80, NULL, 7, '2020-01-27 02:03:39', '2020-01-27 02:03:39'),
(82, 81, 3, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(83, 81, 4, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(84, 81, 5, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(85, 81, 6, NULL, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(86, 81, NULL, 4, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(87, 81, NULL, 5, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(88, 81, NULL, 6, '2020-01-27 02:04:27', '2020-01-27 02:04:27'),
(89, 82, 3, NULL, '2020-01-27 02:05:25', '2020-01-27 02:05:25'),
(90, 82, 4, NULL, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(91, 82, 5, NULL, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(92, 82, NULL, 4, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(93, 82, NULL, 5, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(94, 82, NULL, 6, '2020-01-27 02:05:26', '2020-01-27 02:05:26'),
(95, 83, 4, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(96, 83, 5, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(97, 83, 6, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(98, 83, 7, NULL, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(99, 83, NULL, 5, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(100, 83, NULL, 6, '2020-01-27 02:06:52', '2020-01-27 02:06:52'),
(101, 83, NULL, 7, '2020-01-27 02:06:53', '2020-01-27 02:06:53'),
(102, 84, 3, NULL, '2020-01-27 02:07:38', '2020-01-27 02:07:38'),
(103, 84, 4, NULL, '2020-01-27 02:07:38', '2020-01-27 02:07:38'),
(104, 84, 5, NULL, '2020-01-27 02:07:38', '2020-01-27 02:07:38'),
(105, 84, NULL, 4, '2020-01-27 02:07:39', '2020-01-27 02:07:39'),
(106, 84, NULL, 5, '2020-01-27 02:07:39', '2020-01-27 02:07:39'),
(107, 84, NULL, 6, '2020-01-27 02:07:39', '2020-01-27 02:07:39'),
(108, 85, 3, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(109, 85, 4, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(110, 85, 5, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(111, 85, 7, NULL, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(112, 85, NULL, 4, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(113, 85, NULL, 5, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(114, 85, NULL, 6, '2020-01-27 02:08:23', '2020-01-27 02:08:23'),
(115, 86, 3, NULL, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(116, 86, 4, NULL, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(117, 86, 5, NULL, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(118, 86, NULL, 4, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(119, 86, NULL, 5, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(120, 86, NULL, 7, '2020-01-27 02:10:10', '2020-01-27 02:10:10'),
(121, 87, 4, NULL, '2020-01-27 02:11:50', '2020-01-27 02:11:50'),
(122, 87, 5, NULL, '2020-01-27 02:11:50', '2020-01-27 02:11:50'),
(123, 87, 6, NULL, '2020-01-27 02:11:50', '2020-01-27 02:11:50'),
(124, 87, NULL, 4, '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(125, 87, NULL, 5, '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(126, 87, NULL, 6, '2020-01-27 02:11:51', '2020-01-27 02:11:51'),
(127, 88, 3, NULL, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(128, 88, 4, NULL, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(129, 88, 5, NULL, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(130, 88, NULL, 4, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(131, 88, NULL, 5, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(132, 88, NULL, 6, '2020-01-27 02:13:15', '2020-01-27 02:13:15'),
(133, 89, 3, NULL, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(134, 89, 4, NULL, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(135, 89, 5, NULL, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(136, 89, NULL, 4, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(137, 89, NULL, 5, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(138, 89, NULL, 6, '2020-01-27 02:14:25', '2020-01-27 02:14:25'),
(139, 90, 3, NULL, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(140, 90, 4, NULL, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(141, 90, 5, NULL, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(142, 90, NULL, 4, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(143, 90, NULL, 5, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(144, 90, NULL, 6, '2020-01-27 02:15:16', '2020-01-27 02:15:16'),
(145, 91, 3, NULL, '2020-01-27 02:16:00', '2020-01-27 02:16:00'),
(146, 91, 4, NULL, '2020-01-27 02:16:00', '2020-01-27 02:16:00'),
(147, 91, 5, NULL, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(148, 91, 6, NULL, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(149, 91, NULL, 4, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(150, 91, NULL, 5, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(151, 91, NULL, 6, '2020-01-27 02:16:01', '2020-01-27 02:16:01'),
(152, 92, 3, NULL, '2020-01-27 02:17:00', '2020-01-27 02:17:00'),
(153, 92, 4, NULL, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(154, 92, 5, NULL, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(155, 92, 6, NULL, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(156, 92, NULL, 4, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(157, 92, NULL, 5, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(158, 92, NULL, 6, '2020-01-27 02:17:01', '2020-01-27 02:17:01'),
(159, 93, 3, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(160, 93, 4, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(161, 93, 5, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(162, 93, 6, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(163, 93, 7, NULL, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(164, 93, NULL, 4, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(165, 93, NULL, 5, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(166, 93, NULL, 6, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(167, 93, NULL, 7, '2020-01-27 02:17:44', '2020-01-27 02:17:44'),
(168, 94, 3, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(169, 94, 4, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(170, 94, 6, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(171, 94, 7, NULL, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(172, 94, NULL, 5, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(173, 94, NULL, 6, '2020-01-27 02:18:29', '2020-01-27 02:18:29'),
(174, 95, 3, NULL, '2020-01-27 02:19:16', '2020-01-27 02:19:16'),
(175, 95, 4, NULL, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(176, 95, 5, NULL, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(177, 95, NULL, 4, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(178, 95, NULL, 5, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(179, 95, NULL, 6, '2020-01-27 02:19:17', '2020-01-27 02:19:17'),
(180, 96, 3, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(181, 96, 4, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(182, 96, 5, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(183, 96, 6, NULL, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(184, 96, NULL, 4, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(185, 96, NULL, 5, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(186, 96, NULL, 6, '2020-01-27 02:20:06', '2020-01-27 02:20:06'),
(187, 97, 3, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(188, 97, 4, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(189, 97, 5, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(190, 97, 6, NULL, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(191, 97, NULL, 4, '2020-01-27 02:20:55', '2020-01-27 02:20:55'),
(192, 97, NULL, 5, '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(193, 97, NULL, 6, '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(194, 97, NULL, 7, '2020-01-27 02:20:56', '2020-01-27 02:20:56'),
(195, 98, 3, NULL, '2020-01-27 02:21:46', '2020-01-27 02:21:46'),
(196, 98, 4, NULL, '2020-01-27 02:21:46', '2020-01-27 02:21:46'),
(197, 98, 5, NULL, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(198, 98, 6, NULL, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(199, 98, NULL, 4, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(200, 98, NULL, 5, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(201, 98, NULL, 6, '2020-01-27 02:21:47', '2020-01-27 02:21:47'),
(202, 99, 3, NULL, '2020-01-27 02:22:40', '2020-01-27 02:22:40'),
(203, 99, 4, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(204, 99, 5, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(205, 99, 6, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(206, 99, 7, NULL, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(207, 99, NULL, 4, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(208, 99, NULL, 5, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(209, 99, NULL, 6, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(210, 99, NULL, 7, '2020-01-27 02:22:41', '2020-01-27 02:22:41'),
(211, 100, 3, NULL, '2020-01-27 02:23:31', '2020-01-27 02:23:31'),
(212, 100, 4, NULL, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(213, 100, 5, NULL, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(214, 100, 7, NULL, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(215, 100, NULL, 4, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(216, 100, NULL, 5, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(217, 100, NULL, 6, '2020-01-27 02:23:32', '2020-01-27 02:23:32'),
(218, 101, 3, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(219, 101, 4, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(220, 101, 5, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(221, 101, 6, NULL, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(222, 101, NULL, 4, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(223, 101, NULL, 5, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(224, 101, NULL, 6, '2020-01-27 02:24:30', '2020-01-27 02:24:30'),
(225, 102, 3, NULL, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(226, 102, 4, NULL, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(227, 102, NULL, 4, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(228, 102, NULL, 5, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(229, 102, NULL, 7, '2020-01-27 02:25:25', '2020-01-27 02:25:25'),
(230, 103, 3, NULL, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(231, 103, 4, NULL, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(232, 103, NULL, 4, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(233, 103, NULL, 5, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(234, 103, NULL, 6, '2020-01-27 02:26:17', '2020-01-27 02:26:17'),
(235, 104, 3, NULL, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(236, 104, 4, NULL, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(237, 104, 5, NULL, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(238, 104, NULL, 4, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(239, 104, NULL, 5, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(240, 104, NULL, 6, '2020-01-27 02:27:31', '2020-01-27 02:27:31'),
(241, 105, 5, NULL, '2020-01-27 02:28:25', '2020-01-27 02:28:25'),
(242, 105, 6, NULL, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(243, 105, 7, NULL, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(244, 105, NULL, 5, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(245, 105, NULL, 6, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(246, 105, NULL, 7, '2020-01-27 02:28:26', '2020-01-27 02:28:26'),
(247, 106, 3, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(248, 106, 4, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(249, 106, 5, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(250, 106, 6, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(251, 106, 7, NULL, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(252, 106, NULL, 4, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(253, 106, NULL, 5, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(254, 106, NULL, 6, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(255, 106, NULL, 7, '2020-01-27 02:29:53', '2020-01-27 02:29:53'),
(256, 107, 4, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(257, 107, 5, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(258, 107, 6, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(259, 107, 7, NULL, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(260, 107, NULL, 4, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(261, 107, NULL, 5, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(262, 107, NULL, 6, '2020-01-27 02:30:56', '2020-01-27 02:30:56'),
(263, 108, 3, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(264, 108, 5, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(265, 108, 6, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(266, 108, 7, NULL, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(267, 108, NULL, 4, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(268, 108, NULL, 5, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(269, 108, NULL, 6, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(270, 108, NULL, 7, '2020-01-27 02:31:45', '2020-01-27 02:31:45'),
(271, 109, 4, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(272, 109, 5, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(273, 109, 6, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(274, 109, 7, NULL, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(275, 109, NULL, 4, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(276, 109, NULL, 5, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(277, 109, NULL, 6, '2020-01-27 02:33:11', '2020-01-27 02:33:11'),
(278, 110, 3, NULL, '2020-01-27 02:34:55', '2020-01-27 02:34:55'),
(279, 110, 4, NULL, '2020-01-27 02:34:55', '2020-01-27 02:34:55'),
(280, 110, 5, NULL, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(281, 110, 6, NULL, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(282, 110, 7, NULL, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(283, 110, NULL, 4, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(284, 110, NULL, 5, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(285, 110, NULL, 6, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(286, 110, NULL, 7, '2020-01-27 02:34:56', '2020-01-27 02:34:56'),
(287, 111, 3, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(288, 111, 4, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(289, 111, 5, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(290, 111, 6, NULL, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(291, 111, NULL, 4, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(292, 111, NULL, 5, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(293, 111, NULL, 6, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(294, 111, NULL, 7, '2020-01-27 02:35:48', '2020-01-27 02:35:48'),
(295, 112, 3, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(296, 112, 4, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(297, 112, 5, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(298, 112, 6, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(299, 112, 7, NULL, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(300, 112, NULL, 4, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(301, 112, NULL, 5, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(302, 112, NULL, 6, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(303, 112, NULL, 7, '2020-01-27 02:36:35', '2020-01-27 02:36:35'),
(304, 113, 3, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(305, 113, 4, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(306, 113, 5, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(307, 113, 6, NULL, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(308, 113, NULL, 4, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(309, 113, NULL, 5, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(310, 113, NULL, 6, '2020-01-27 02:37:19', '2020-01-27 02:37:19'),
(311, 114, 3, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(312, 114, 4, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(313, 114, 5, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(314, 114, 6, NULL, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(315, 114, NULL, 4, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(316, 114, NULL, 5, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(317, 114, NULL, 6, '2020-01-27 02:38:05', '2020-01-27 02:38:05'),
(318, 115, 3, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(319, 115, 4, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(320, 115, 6, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(321, 115, 7, NULL, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(322, 115, NULL, 4, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(323, 115, NULL, 5, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(324, 115, NULL, 6, '2020-01-27 02:38:56', '2020-01-27 02:38:56'),
(325, 116, 3, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(326, 116, 4, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(327, 116, 5, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(328, 116, 6, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(329, 116, 7, NULL, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(330, 116, NULL, 4, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(331, 116, NULL, 5, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(332, 116, NULL, 6, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(333, 116, NULL, 7, '2020-01-27 02:39:44', '2020-01-27 02:39:44'),
(334, 117, 3, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(335, 117, 4, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(336, 117, 5, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(337, 117, 6, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(338, 117, 7, NULL, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(339, 117, NULL, 4, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(340, 117, NULL, 5, '2020-01-27 02:40:26', '2020-01-27 02:40:26'),
(341, 117, NULL, 6, '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(342, 117, NULL, 7, '2020-01-27 02:40:27', '2020-01-27 02:40:27'),
(343, 118, 3, NULL, '2020-01-27 02:41:14', '2020-01-27 02:41:14'),
(344, 118, 4, NULL, '2020-01-27 02:41:14', '2020-01-27 02:41:14'),
(345, 118, 5, NULL, '2020-01-27 02:41:14', '2020-01-27 02:41:14'),
(346, 118, 6, NULL, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(347, 118, 7, NULL, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(348, 118, NULL, 4, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(349, 118, NULL, 5, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(350, 118, NULL, 6, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(351, 118, NULL, 7, '2020-01-27 02:41:15', '2020-01-27 02:41:15'),
(352, 119, 4, NULL, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(353, 119, 5, NULL, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(354, 119, NULL, 4, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(355, 119, NULL, 5, '2020-01-28 12:59:04', '2020-01-28 12:59:04'),
(356, 120, 3, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(357, 120, 4, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(358, 120, 5, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(359, 120, 6, NULL, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(360, 120, NULL, 4, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(361, 120, NULL, 5, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(362, 120, NULL, 6, '2020-01-29 05:34:49', '2020-01-29 05:34:49'),
(363, 121, 3, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(364, 121, 4, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(365, 121, 6, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(366, 121, 7, NULL, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(367, 121, NULL, 4, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(368, 121, NULL, 5, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(369, 121, NULL, 6, '2020-02-08 10:50:09', '2020-02-08 10:50:09'),
(370, 122, 6, NULL, '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(371, 122, NULL, 4, '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(372, 122, NULL, 5, '2020-02-17 13:18:06', '2020-02-17 13:18:06'),
(373, 79, 4, NULL, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(374, 79, 6, NULL, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(375, 79, 7, NULL, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(376, 79, NULL, 5, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(377, 79, NULL, 7, '2020-02-20 16:41:50', '2020-02-20 16:41:50'),
(378, 124, 3, NULL, '2020-02-20 16:44:16', '2020-02-20 16:44:16'),
(379, 124, NULL, 4, '2020-02-20 16:44:16', '2020-02-20 16:44:16'),
(380, 124, NULL, 5, '2020-02-20 16:44:16', '2020-02-20 16:44:16');

-- --------------------------------------------------------

--
-- Table structure for table `tour_type_names`
--

CREATE TABLE `tour_type_names` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_enabled` tinyint(11) DEFAULT NULL,
  `tourtypeName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tourtypeDes` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tour_type_names`
--

INSERT INTO `tour_type_names` (`id`, `is_enabled`, `tourtypeName`, `tourtypeDes`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Services ', 'Services Available', '2020-01-20 09:00:11', '2020-01-20 09:00:11', NULL),
(5, 0, 'Desert Safari', 'The land of the Desert and its mystics – an experience that’ll takes you to the heart of Dubai with Lama Tours.\r\n\r\nYou must have seen the famous seesha in pictures, the men of the desert smoking away in the evenings, a distant song playing in the background while they enjoy a King’s meal. \r\n\r\nThat’s what we want you to experience. To feel like you know this place from before. And when you go back, you have a handful of stories to share with your friends and relatives. And we haven’t even started yet with the experience. But if you trust your gut, this trip, from the beginning till the end is going to be enthralling. Through the sand dunes, the mighty sun setting behind the mountains of sand, while you rest too, like a King, looking at the beautiful view. Accompanied by music and dance, good food and good company, a trip that’s worth all the planning is waiting for you to say ‘YES’.', '2020-01-24 07:08:30', '2020-02-20 16:28:39', NULL),
(6, 0, 'Sand Ski Dubai', 'Enjoy Sand ski in the middle of Dubai desert with our Licensed Safari Guide, We recommend basic knowledge of ski but our safari guide could help you with easy techniques. You will have an hour or two until you get exhausted with you ski adventure. Our safari driver will assist you bring back on top of the dune.\r\nPickup: 8:00 AM or 10:00 AM', '2020-01-24 07:09:42', '2020-02-20 16:28:44', NULL),
(7, 0, 'Jet Ski', 'Passengers under the age of 16 should be accompanying a parent or guardian on the jet ski\r\nExperience the warm water of the Arabian Gulf with a high-powered Jet Ski tour from Lama Tours! The water activity is ideal for both professionals and beginners. \r\nWe\'ll escort you from our docks, out of the harbor, where you\'ll be free to ride off the coastline, enjoying the beautiful waters and wildlife of the Arabian Gulf.', '2020-01-24 07:10:16', '2020-02-20 16:28:50', NULL),
(9, 0, 'Burj Khalifa Tour - At The Top 124 Floor', 'Behold Dubai from the tallest skyscraper on the planet, Burj Khalifa\r\n\r\nSoaring high at 555 meters. At the Top, Burj Khalifa is the most iconic destination of Dubai. Welcome to the highest outdoor observatory in the world. \r\n\r\nTransfers are Available for guest, who wishes to be picked up and drop off there hotel.\r\n\r\nWitness unforgettable, panoramic views over Dubai from the observation deck of the iconic Burj Khalifa, the world\'s tallest building. Let your jaw hit the floor as you are elevated up 125 floors to fantastic 360-degree views over the Arabian Gulf.', '2020-01-24 07:11:39', '2020-02-20 16:28:55', NULL),
(10, 0, 'Friends Tour', 'This is Friend Tour', '2020-02-01 05:01:13', '2020-02-03 06:31:57', NULL),
(11, 1, 'Umrah', 'abc', '2020-02-20 16:30:21', '2020-02-20 16:30:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_user` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `comp_id` int(11) DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `address`, `city`, `state`, `country`, `profile_image`, `post_code`, `admin_user`, `password`, `remember_token`, `created_at`, `updated_at`, `comp_id`, `is_enabled`) VALUES
(1, 'Admin', 'Khan', 'info@rapidfiresol.com', '(908) 214 1', 'House# 243, Street # 4', 'Test City', 'Alabama', 'United States', 'user-1.jpg', '3476', 1, '$2y$10$qMJeXM23/T6ebNLZvdiLget4hIRkHNCHTJ.49ngQl4BYGnL6b3UNu', 'nBBPIg0ClnuZltq3CU3MjW5U6O3qdGTnMW7mvKK3Ant3hNcHONfV7hBTS5Pi', NULL, '2020-02-11 11:15:46', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attractions`
--
ALTER TABLE `attractions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attraction_imgs`
--
ALTER TABLE `attraction_imgs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_infornations`
--
ALTER TABLE `company_infornations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_infornations_comp_email_unique` (`Comp_Email`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_exes`
--
ALTER TABLE `package_exes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_ins`
--
ALTER TABLE `package_ins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_requests`
--
ALTER TABLE `post_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_requests_email_unique` (`email`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tourimages`
--
ALTER TABLE `tourimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_packages`
--
ALTER TABLE `tour_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_type_names`
--
ALTER TABLE `tour_type_names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attractions`
--
ALTER TABLE `attractions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `attraction_imgs`
--
ALTER TABLE `attraction_imgs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company_infornations`
--
ALTER TABLE `company_infornations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `components`
--
ALTER TABLE `components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `package_exes`
--
ALTER TABLE `package_exes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `package_ins`
--
ALTER TABLE `package_ins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `post_requests`
--
ALTER TABLE `post_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tourimages`
--
ALTER TABLE `tourimages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=397;

--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `tour_packages`
--
ALTER TABLE `tour_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `tour_type_names`
--
ALTER TABLE `tour_type_names`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
