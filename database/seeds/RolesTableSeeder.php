<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        \App\Role::create(array(
            'role_title' => 'Admin',
            'role_slug' => 'admin',
            'role_description' => 'This is test',
        ));

        \App\Role::create(array(
            'role_title' => 'Guest User',
            'role_slug' => 'customer',
            'role_description' => 'This is test',
        ));
    }
}
