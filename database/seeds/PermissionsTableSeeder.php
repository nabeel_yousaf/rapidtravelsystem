<?php

use Illuminate\Database\Seeder;
use \App\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();

        Permission::create(array(
            'permission_title' => 'Create Page',
            'permission_slug' => 'create-page',
            'permission_description' => 'This is used to create a page'
        ));

        Permission::create(array(
            'permission_title' => 'Update Page',
            'permission_slug' => 'update-page',
            'permission_description' => 'This is used to update a page'
        ));
    }
}
