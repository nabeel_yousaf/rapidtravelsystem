<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersTableSeeder');
        $this->command->info('Users table seeded!');
        $this->call('RolesTableSeeder');
        $this->command->info('Roles table seeded!');
        $this->call('RoleUserTableSeeder');
        $this->command->info('Role user table seeded!');
        $this->call('PermissionsTableSeeder');
        $this->command->info('Permissions table seeded!');
        $this->call('PermissionsRoleTableSeeder');
        $this->command->info('Permissions role table seeded!');

    }
}
