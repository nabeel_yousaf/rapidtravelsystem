<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        \App\User::insert(array(
            'email' => 'admin@msj.com',
            'first_name' => 'Admin',
            'last_name' => 'Khan',
            'admin_user' => '1',
            'phone' => '(908) 214 1',
            'address' => 'House# 243, Street # 4 ',
            'city' => 'Test City',
            'state' => 'Alabama',
            'country' => 'United States',
            'profile_image' => 'plan2.png',
            'post_code' => '3476',
            'password' => bcrypt('admin123'),
        ));
        \App\User::insert(array(
            'email' => 'member@email.com',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'admin_user' => '0',
            'password' => bcrypt('user123'),
        ));

    }
}
