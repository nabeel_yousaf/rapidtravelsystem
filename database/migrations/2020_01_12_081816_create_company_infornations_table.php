<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyInfornationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_infornations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comp_name');
            $table->string('Comp_live_dte')->date();
            $table->string('Comp_Reg_Num');
            $table->string('Comp_City_name');
            $table->string('Comp_Stat_name');
            $table->string('Comp_Country_name');
            $table->string('Comp_Address');
            $table->string('Comp_Phone');
            $table->string('mobile');
            $table->string('Comp_Tax_num');
            $table->string('Comp_Email')->unique();;
            $table->boolean('is_enabled')->default(0);
            $table->string('CreatedBy');
            $table->string('Logo_URL');
            $table->string('Aboutus_Desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_infornations');
    }
}
