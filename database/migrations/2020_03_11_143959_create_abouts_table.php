<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('comp_id')->unsigned();
            $table->string('title');
            $table->string('slug')->unique();
            $table->integer('comp_id');
            $table->boolean('is_enabled')->default(0);
            $table->string('description'); 
          
            $table->timestamps();

            //  $table->foreign('comp_id')->references('id')->on('company_infornations');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}