<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comp_id');
            $table->string('Tour_name');
            $table->string('Tour_Desc');
            $table->string('profile_image');
            $table->integer('Tour_Adult_price');
            $table->integer('Tour_Adult_disc_price');
            $table->integer('Tour_Child_price');
            $table->integer('Tour_Child_disc_price');
            $table->integer('Tour_type_name');
            $table->integer('TourType_Code');
            $table->boolean('is_enabled')->default(0);
            $table->integer('Tour_Duration');
            $table->string('includes', 2500);
            $table->string('excludes', 2500);
            $table->string('currency');
            $table->integer('CreatedBy');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
