<?php
/*  
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/***************************************Safari Tours Route ***********************
*******************/
Route::get('/holiday/{id}', 'GenralController@holiday');
Route::get('/viewtours/{id}', 'GenralController@viewtours');
/***************************************PostRequest Route ***********************
*******************/
Route::post('/request/', 'PostRequestController@store');
Route::post('/thank-you/', 'PostRequestController@store');
Route::get('/view-booking/', 'PostRequestController@index');
Route::get('/send/email', 'PostRequestController@mail');
/***************************************services Route ***********************
*******************/
Route::get('/groups/', 'GenralController@groups');
Route::get('/car/', 'GenralController@carhiring');
/***************************************NavBarr Route **********************
********************/
Route::get('/', 'GenralController@index');
Route::get('/service/', 'GenralController@service');
Route::get('/safari/', 'GenralController@safari');

Route::get('/aboutus/', 'GenralController@aboutus');
Route::get('/contactus/', 'GenralController@contactus');
Route::get('/attractions/', 'GenralController@attraction');
/***************************************Attractions Route **************************
****************/
Route::get('/view-attraction/{id}', 'GenralController@viewAttraction');
Route::get('/view-attraction/{slug}', 'GenralController@cmsPages');
// Route::get('/ajman/', 'GenralController@Ajman');
// Route::get('/dubai/', 'GenralController@Dubai');
// Route::get('/fujairah/', 'GenralController@Fujairah');
// Route::get('/sharjah/', 'GenralController@Sharjah');
// Route::get('/khaimah/', 'GenralController@Khaimah');
//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');



/************************************************************************
           ************************************Admin Routes*************************
            ***********************************              ************************/



//Admin Panel Routes'middleware' => 'auth'
Route::group(array('namespace'=> 'Admin', 'prefix'=> 'admin'), function () {
Route:: get('/', array('as'=> 'admin.login', 'uses'=> 'LoginController@index'));
Route::get('/password/reset/{token?}', array('as'=> 'admin.passwordReset', 'uses'=> 'Auth\PasswordController@showResetForm'));
Route:: post('/password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route:: post('/password/reset', 'Auth\PasswordController@reset');
Route::group(array('middleware'=> 'throttle:5,10'), function () {
Route:: post('/postLogin', array('as'=> 'admin.postLogin', 'uses'=> 'LoginController@postLogin'));
});
Route::group(array('middleware'=> 'auth'), function () {
/***************************************User Dashboard & logout ******************************************/
Route:: get('/dashboard', array('as'=> 'admin.dashboard', 'uses'=> 'DashboardController@index'));
Route:: get('/logout', array('as'=> 'admin.logout', 'uses'=> 'LoginController@getLogout'));

 

/**********************************About routes*****************************************
*****************************************/
 Route::get('/create-about', array('as'=> 'admin.about', 'uses'=> 'AboutController@create'));
 Route::get('/view-about', array('as'=> 'admin.view-about', 'uses'=> 'AboutController@index'));
Route::post('/add-about', array('as'=> 'admin.add', 'uses' =>'AboutController@store'));
Route::get('/edit-about/{id}', array('as' => 'admin.edit','uses'=> 'AboutController@edit'));
Route::post('/update-about/{id}',array('as'=>'admin.update','uses'=> 'AboutController@update'));
Route::get('/delete-about/{id}',array('as'=>'admin.delete','uses'=> 'AboutController@destroy'));






/**********************************Countries routes*****************************************
*****************************************/
Route::get('/create-country', array('as' => 'admin.addCountry','uses' => 'CountryController@create'));
Route::post('/store-country', array('as' =>'admin.storeCountry', 'uses' => 'CountryController@store'));
Route::get('/list-country',  array('as' => 'admin.listCountry', 'uses' => 'CountryController@index'));
Route::get('/edit-country/{id}', array('as' => 'admin.editCountry','uses'=> 'CountryController@edit'));
Route::post('/update-country/{id}',array('as'=>'admin.updateCountry','uses'=>'CountryController@update'));
Route::get('/delete-country/{id}',array('as'=>'admin.deleteCountry','uses'=>'CountryController@destroy'));
/**********************************States routes*****************************************
*****************************************/
Route::get('/create-state',  array('as' => 'admin.addState', 'uses' => 'StateController@create'));
Route::post('/store-state', array('as' => 'admin.storeState', 'uses' => 'StateController@store'));
Route::get('/list-state',    array('as' => 'admin.listState', 'uses' => 'StateController@index'));
Route::get('/edit-state/{id}', array('as' => 'admin.editState', 'uses' =>'StateController@edit'));
Route::post('/update-state/{id}', array('as' =>'admin.updateState', 'uses'=>'StateController@update'));
Route::get('/delete-state/{id}', array('as' =>'admin.deleteState', 'uses'=>'StateController@destroy'));
/**********************************Cities routes*****************************************
*****************************************/
Route::get('/create-city',  array('as' => 'admin.addCity', 'uses' => 'CityController@create'));
Route::post('/store-city', array('as' => 'admin.storeCity', 'uses' => 'CityController@store'));
Route::get('/list-city',    array('as' => 'admin.listCity', 'uses' => 'CityController@index'));
Route::get('/edit-city/{id}', array('as' => 'admin.editCity', 'uses' =>'CityController@edit'));
Route::post('/update-city/{id}', array('as' => 'admin.updateCity', 'uses' =>'CityController@update'));
Route::get('/delete-city/{id}', array('as' => 'admin.deleteCity', 'uses' =>'CityController@destroy'));
Route::get('/country-id/{id}', array('as' => 'admin.country-id', 'uses' =>'CityController@get'));
Route::get('/city-id/{id}', array('as' => 'admin.city-id', 'uses' =>'CityController@getcity'));
/***************************************Company Route ******************************************/
Route:: get('/list-compinfo', array('as'=> 'admin.listCompinfo', 'uses'=> 'CompanyInfornationController@index'));
Route:: get('/create-compinfo', array('as'=> 'admin.addCompinfo', 'uses'=> 'CompanyInfornationController@getCreate'));
Route:: post('/create-compinfo', array('as'=> 'admin.storeCompinfo', 'uses'=> 'CompanyInfornationController@postStore'));
Route::get('/edit-compinfo/{id}', array('as'=> 'admin.editCompinfo', 'uses'=> 'CompanyInfornationController@getEdit'));
Route::get('/delete-compinfo/{id}', array('as'=> 'admin.deleteCompinfo', 'uses'=> 'CompanyInfornationController@getDelete'));
Route::post('/update-compinfo/{id}', array('as'=> 'admin.updateCompinfo', 'uses'=> 'CompanyInfornationController@putUpdate'));
Route::get('/destroy-logo/{id}', array('as'=> 'admin.destroyLogo', 'uses'=> 'CompanyInfornationController@destroypic'));
//Route::get('/state-id/{id}', 'CreatadsController@getstate');
//Route::get('/city-id/{id}', 'CreatadsController@getcity');
/***************************************User Route ******************************************/
Route:: get('/list-users', array('as'=> 'admin.listUsers', 'uses'=> 'UserController@index'));
Route:: get('/create-user', array('as'=> 'admin.addUser', 'uses'=> 'UserController@getCreate'));
Route:: post('/create-user', array('as'=> 'admin.storeUser', 'uses'=> 'UserController@postStore'));
Route::get('/edit-user/{id}', array('as'=> 'admin.editUser', 'uses'=> 'UserController@getEdit'));
Route::get('/delete-user/{id}', array('as'=> 'admin.deleteUser', 'uses'=> 'UserController@getDelete'));
Route::put('/update-user/{id}', array('as'=> 'admin.updateUser', 'uses'=> 'UserController@putUpdate'));
/***************************************Tour Type Route ******************************************/
Route:: get('/list-tourtype', array('as'=> 'admin.listTourType', 'uses'=> 'TourTypeNameController@index'));
Route:: get('/create-tourtype', array('as'=> 'admin.addTourType', 'uses'=> 'TourTypeNameController@getCreate'));
Route:: post('/create-tourtype', array('as'=> 'admin.storeTourType', 'uses'=> 'TourTypeNameController@postStore'));
Route::get('/edit-tourtype/{id}', array('as'=> 'admin.editTourType', 'uses'=> 'TourTypeNameController@getEdit'));
Route::get('/delete-tourtype/{id}', array('as'=> 'admin.deleteTourType', 'uses'=> 'TourTypeNameController@getDelete'));
Route::post('/update-tourtype/{id}', array('as'=> 'admin.updateTourType', 'uses'=> 'TourTypeNameController@putUpdate'));
/*************************************** INclude Route ******************************************/
Route:: get('/list-include', array('as'=> 'admin.listInclude', 'uses'=> 'PackageInController@index'));
Route:: get('/create-include', array('as'=> 'admin.addInclude', 'uses'=> 'PackageInController@getCreate'));
Route:: post('/create-include', array('as'=> 'admin.storeInclude', 'uses'=> 'PackageInController@postStore'));
Route::get('/edit-include/{id}', array('as'=> 'admin.editInclude', 'uses'=> 'PackageInController@getEdit'));
Route::get('/delete-include/{id}', array('as'=> 'admin.deleteInclude', 'uses'=> 'PackageInController@getDelete'));
Route::post('/update-include/{id}', array('as'=> 'admin.updateInclude', 'uses'=> 'PackageInController@putUpdate'));
/*************************************** EXclude Route ******************************************/
Route:: get('/list-exclude', array('as'=> 'admin.listExclude', 'uses'=> 'PackageExController@index'));
Route:: get('/create-exclude', array('as'=> 'admin.addExclude', 'uses'=> 'PackageExController@getCreate'));
Route:: post('/create-exclude', array('as'=> 'admin.storeExclude', 'uses'=> 'PackageExController@postStore'));
Route::get('/edit-exclude/{id}', array('as'=> 'admin.editExclude', 'uses'=> 'PackageExController@getEdit'));
Route::get('/delete-exclude/{id}', array('as'=> 'admin.deleteExclude', 'uses'=> 'PackageExController@getDelete'));
Route::post('/update-exclude/{id}', array('as'=> 'admin.updateExclude', 'uses'=> 'PackageExController@putUpdate'));
/***************************************Tours Route ******************************************/
Route:: get('/list-tours', array('as'=> 'admin.listTours', 'uses'=> 'TourController@index'));
Route:: get('/create-tour', array('as'=> 'admin.addTours', 'uses'=> 'TourController@getCreate'));
Route:: post('/create-tour', array('as'=> 'admin.storeTours', 'uses'=> 'TourController@postStore'));
Route::get('/edit-tour/{id}', array('as'=> 'admin.editTours', 'uses'=> 'TourController@getEdit'));
Route::get('/delete-tour/{id}', array('as'=> 'admin.deleteTours', 'uses'=> 'TourController@getDelete'));
Route::post('/update-tour/{id}', array('as'=> 'admin.updateTours', 'uses'=> 'TourController@putUpdate'));
Route::get('/destroy-pic/{id}', array('as'=> 'admin.destroyTours', 'uses'=> 'TourController@destroypic'));
Route::post('/delete-imgs/{id}', array('as'=> 'admin.deleteImgs', 'uses'=> 'TourController@deleteimgs'));
Route::post('/Multiple-update-imgs/{id}', array('as'=> 'admin.updateTours', 'uses'=> 'TourController@updateimgs'));
/***************************************Roles Route *****************************************/
Route:: get('/list-roles', array('as'=> 'admin.listRoles', 'uses'=> 'RoleController@index'));
Route:: get('/create-role', array('as'=> 'admin.addRole', 'uses'=> 'RoleController@getCreate'));
Route:: post('/create-role', array('as'=> 'admin.storeRole', 'uses'=> 'RoleController@postStore'));
Route::get('/edit-role/{id}', array('as'=> 'admin.editRole', 'uses'=> 'RoleController@getEdit'));
Route::get('/delete-role/{id}', array('as'=> 'admin.deleteRole', 'uses'=> 'RoleController@getDelete'));
Route::put('/update-role/{id}', array('as'=> 'admin.updateRole', 'uses'=> 'RoleController@putUpdate'));
/***************************************Permissions Route *****************************************/
Route:: get('/list-permissions', array('as'=> 'admin.listPermissions', 'uses'=> 'PermissionController@index'));
Route:: get('/create-permission', array('as'=> 'admin.addPermission', 'uses'=> 'PermissionController@getCreate'));
Route:: post('/create-permission', array('as'=> 'admin.storePermission', 'uses'=> 'PermissionController@postStore'));
Route::get('/edit-permission/{id}', array('as'=> 'admin.editPermission', 'uses'=> 'PermissionController@getEdit'));
Route::get('/delete-permission/{id}', array('as'=> 'admin.deletePermission', 'uses'=> 'PermissionController@getDelete'));
Route::put('/update-permission/{id}', array('as'=> 'admin.updatePermission', 'uses'=> 'PermissionController@putUpdate'));
/**********************************Components routes*****************************************
*****************************************/
Route::get('/create-component', array('as' => 'admin.addComponent','uses' => 'ComponentController@create'));
Route::post('/store-component', array('as' =>'admin.storeComponent', 'uses' => 'ComponentController@store'));
Route::get('/list-component',  array('as' => 'admin.listComponent', 'uses' => 'ComponentController@index'));
Route::get('/edit-component/{id}', array('as' => 'admin.editComponent','uses'=> 'ComponentController@edit'));
Route::post('/update-component/{id}',array('as'=>'admin.updateComponent','uses'=>'ComponentController@update'));
Route::get('/delete-component/{id}',array('as'=>'admin.deleteComponent','uses'=>'ComponentController@destroy'));
/**********************************Attractions routes*****************************************
*****************************************/
Route::get('/create-attraction', array('as' => 'admin.addAttraction','uses' => 'AttractionController@getCreate'));
Route::post('/store-attraction', array('as' =>'admin.storeAttraction', 'uses' => 'AttractionController@postStore'));
Route::get('/list-attraction',  array('as' => 'admin.listAttraction', 'uses' => 'AttractionController@index'));
Route::get('/edit-attraction/{id}', array('as' => 'admin.editAttraction','uses'=> 'AttractionController@getEdit'));
Route::post('/update-attraction/{id}',array('as'=>'admin.updateAttraction','uses'=>'AttractionController@putUpdate'));
Route::get('/delete-attraction/{id}',array('as'=>'admin.deleteAttraction','uses'=>'AttractionController@getDelete'));
Route::get('/destroy-img/{id}', array('as'=> 'admin.destroyLogo', 'uses'=> 'AttractionController@destroypic'));
Route::post('/delete-img/{id}', array('as'=> 'admin.deleteImgs', 'uses'=> 'AttractionController@deleteimgs'));
Route::post('/Multiple-update-img/{id}', array('as'=> 'admin.updateAttraction', 'uses'=> 'AttractionController@updateimgs'));
});
});