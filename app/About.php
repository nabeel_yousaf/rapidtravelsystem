<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
   protected $fillable = [
       'title',
       'slug',
       'is_enabled',
       'description',
       
   ];
//    public function Aboutfun(){
//        return 'slug';
//    }
   public function sluggable()
   {
       return [
           'slug' => [
               'source' => 'title'
           ]
       ];
   }

   public function company_information(){
    return $this->belongsTo('App\CompanyInfornation'); 
}
}