<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tour extends Model{

        // use SoftDeletes;
        // protected $table = 'tours';
        // protected $dates = ['created_at','updated_at','deleted_at'];

 
	public function tourimg(){
		return $this->hasMany('App\tourimages','tour_id');
	}
 
	/* public function tourType(){
        return $this->hasOne('App\TourTypeName', 'tourtypeName');
    }*/
 
     public function Tour_packages(){      
        return $this->hasMany('App\Tour_packages','tour_id');
    }

}
