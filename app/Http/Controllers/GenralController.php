<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour;
use App\CompanyInfornation;
use App\Http\Controllers\Controller;
use App\tourimages;
use App\TourTypeName;
use App\Tour_packages;
use App\attraction;
use App\component; 
use App\attraction_imgs;
use Validator;
use DB;
 use App\About;
class GenralController extends Controller
{
    
    public function index()
    {
        //$companies = CompanyInfornation::all();
        $components = component::all();
        $tr = DB::table('tours')->where('Tour_type_name','>','1')->where('is_enabled','1')->take(8)->get();
        $attractions = DB::table('attractions')->where('is_enabled',  '1')->paginate(12);
        $abouts = DB::table('abouts')->get();

         //dd($tr); die();
         return view('index', compact('tr', 'attractions','components','abouts'));
    }
 
    
    public function holiday($id)
    {  
 
        $tour = Tour::find($id);
        //$tours = $tour->tourimg;
        //$t= $tours->take(1);
        $details =CompanyInfornation::all();
       
        //     foreach ($tour->tourimg as $ti) {
        //         echo "<pre>";
        //         print_r($ti->tour_images);
        //     }
        
        // die();

    //echo "<pre>"; print_r($tours); die();
        


        return view('client/holiday', compact('tour','details'));
    }

    public function safari()
    {
      $components = component::all();
      // $tours = DB::table('tours')->where('Tour_type_name', '>', '1')->where('is_enabled','1')->paginate(12);
      $total = DB::table('tour_type_names')->where('id', '>', '1')->get()->toArray();
      $cattours = DB::table('tours')->where('Tour_type_name', '>', 1)->where('is_enabled','1')->get();
      $groups = $cattours->groupBy('Tour_type_name');
     //dd($tours); die();
       return view ('client/DiscoverTOUR/safari', compact('groups','components'));
    }
    
 
     public function viewtours($id){
 
        $tour = Tour::find($id);
        $components = component::all();
        $details =CompanyInfornation::all();
        

        return view('client/DiscoverTOUR/viewsafari', compact('tour','details','components'));
    }

     public function service(){
        
        $ser = DB::table('tours')->where('Tour_type_name',  '1')->paginate(12);
        $components = component::all();
        return view ('client/services',compact('ser','components'));
    }
    
     
    public function groups(){
        return view ('client/Services/groups');
    }


    public function carhiring(){
       
        return view('client/Services/car');
    }


    public function attraction(){
       
        $attractions = DB::table('attractions')->where('is_enabled',  '1')->paginate(12);

       return view('client.Attractions.discover', compact('attractions'));

    }


    public function viewAttraction($id){
       $attractions = attraction::find($id);
      // $previous = attraction::where('id', '>', $attractions->id)->max('id');
       $views =attraction::all();
       //dd($all); die();
       return view('client.Attractions.view', compact('attractions','views'));

    }

    
    public function aboutus(){
        //echo "car"; die();
        $abouts = About::all();
        return view('client.about', compact('abouts'));
    }


    public function contactus(){
        $details =CompanyInfornation::all();
        return view('client.contact', compact('details'));
    }

    public function cmsPages($slug)
    {
        $pages = About::find($slug);

        return view('client.about', compact('pages'));
    }

    
}
