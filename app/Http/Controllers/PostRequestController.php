<?php

namespace App\Http\Controllers;

use App\PostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Validator;
use Redirect;
use App\Tour;

class PostRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $bookings = PostRequest::all();
       // /dd($bookings); die();
       return view('admin/booking/view', compact('bookings'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         
         $post = new PostRequest();
         //$post = $request->all();
         $post->booking_date = $request->input('booking_date');
         $post->email = $request->input('asd');
         $post->name = $request->input('ctl00$ContentPlaceHolder1$Tour_Details$txt_Name');
         $post->nationality = $request->input('nationality');
         $post->phone =$request->input('ctl00$ContentPlaceHolder1$Tour_Details$txt_Phone');
         $post->persons =$request->input('ctl00$ContentPlaceHolder1$Tour_Details$txt_Persons');
         $post->childs = $request->input('childs');
         $post->request =$request->input('ctl00$ContentPlaceHolder1$Tour_Details$txt_Request');
         $post->transport = $request->input('transport');
         $post->pickup = $request->input('pickup');
         $post->dropoff = $request->input('dropoff');

          if (!$post->email) {
            return redirect('/safari/');

        } 
         //dd($post); die();
          if ($post->save()){
            $tour = $request->input('tourname');
            $data = [
                'tour' => $tour,
                'booking_date' => $post->booking_date,
                'email' => $post->email,
                'name' => $post->name,
                'nationality' => $post->nationality,
                'phone' => $post->phone,
                'persons' => $post->persons,
                'childs' => $post->childs,
                'message' => $post->request,
                'transport' => $post->transport,
                'pickup' => $post->pickup,
                'dropoff' => $post->dropoff
            ];
            Mail::send('admin.email.customer',['data1' => $data], function($message) use ($request)
            {
                $message->from(siteEmail(), sitename());
                $message->subject("Welcome to ".sitename());
                $message->to($request['asd']);
                // $message->bcc(siteEmail());                
                // $message->to('info@mynexttriptourism.ae');
            });
            Mail::send('admin.email.view',['data1' => $data], function($message) use ($request)
            {
                $message->from(siteEmail(), sitename());
                $message->subject("New booking for ".sitename());
                // $message->to($request['asd']);
                $message->to(siteEmail());                
                // $message->to('info@mynexttriptourism.ae');
            }); return view('admin.email.thank-you');
         }
     

        
         // /dd($post); die();
         
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PostRequest  $postRequest
     * @return \Illuminate\Http\Response
     */
    public function mail(PostRequest $postRequest)
    {
        //dd('ali'); die();
         // $name = 'waqar';
         //  Mail::to('waqarhashmi42@gmail.com')->send(new SendMail($name));
   
         //    return 'Email was sent';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostRequest  $postRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(PostRequest $postRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostRequest  $postRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostRequest $postRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostRequest  $postRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostRequest $postRequest)
    {
        //
    }
}
