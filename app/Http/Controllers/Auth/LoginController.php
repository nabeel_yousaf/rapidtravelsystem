<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login()
    {
        //dd(request()->all());
        $validator = Validator::make(request()->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required|max:255',
        ]);
    //dd(request()->all());
        if ($validator->passes()) {
            $email = Input::get('email');
            $password = Input::get('password');
            $remember = Input::get('remember');
            if($remember=='on'){
                $remember = 1;
            }
            if (Auth::attempt(array('email' => $email, 'password' => $password, 'admin_user' => 0),$remember)) {

                return redirect('/home');

            } else {
                return redirect('/login')->with('error','Please provide the valid credentials');
            }
        } else {
            // dd($validator);
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        }
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }


}
