<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\PermissionRole;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Middleware\CheckPermission;
use DB;

class PermissionController extends Controller
{

    /****************List Roles**************/
    public function index()
    {
        $permissions = Permission::all();
        return view('admin.permissions.list')->with('permissions', $permissions);
    }

    /******* Add new Permission************/
    public function getCreate()
    {
        return view('admin.permissions.add');
    }

    /**************Add new Permission ***************/
    public function postStore()
    {
        //dd(request()->all());
        $validator = Validator::make(request()->all(), [
            'permission_title' => 'required|max:255',
            'permission_slug' => 'required|unique:permissions|max:255',
        ]);

        if ($validator->passes()) {
            $permission = new Permission();
            $permission->permission_title = request()->get('permission_title');
            $permission->permission_slug = request()->get('permission_slug');
            $permission->permission_description = request()->get('permission_description');
            $permission->save();
            return redirect()->route('admin.listPermissions')->with('message', 'Permission is added successfully');
        } else {
            return redirect()->route('admin.storePermission')
                ->withErrors($validator->getMessageBag()->toArray())
                ->withInput();
        }

    }

    /**********************update show edit Form **************/
    public function getEdit($id)
    {
        $permission = Permission::find($id);
        //dd($page);
        // show the edit form and pass the id
        return view('admin.permissions.edit')->with('permission', $permission);

    }

    /********* update Role Form ***************/
    public function putUpdate($id)
    {
        // dd(request()->all());
        $validator = Validator::make(request()->all(), [
            'permission_title' => 'required|max:255',
            'permission_slug' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            $permission = Permission::find($id);
            $permission->permission_title = request()->get('permission_title');
            $permission->permission_slug = request()->get('permission_slug');
            $permission->permission_description = request()->get('permission_description');
            $permission->save();

            return redirect()->route('admin.listPermissions')->with('message', 'Permission updated successfully');
        } else {
            return redirect()->route('admin.updateRole')
                ->withErrors($validator->getMessageBag()->toArray())
                ->withInput();
        }

    }

    /********* Delete Role ***************/

    // public function getDelete($id)
    // {
    //     // delete
    //     $permission = Permission::find($id);
    //     $permission->delete();

    //     // redirect
    //     return redirect()->route('admin.listPermissions')->with('message', 'Permission deleted successfully');
    // }
}
