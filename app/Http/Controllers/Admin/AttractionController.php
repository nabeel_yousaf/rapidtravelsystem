<?php

namespace App\Http\Controllers\Admin;

use App\attraction;
use App\attraction_imgs;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;


class AttractionController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
       $attractions =  attraction::all();
    //dd($attractions); die();
        return view('admin.attraction.view', compact('attractions'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
       return view('admin.attraction.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request){
       
        $attractions = new attraction();
        $attractions->name = request()->get('name');
        $attractions->description = request()->get('description');
        $attractions->is_enabled = request()->get('radio');

        //*image-code*/ 
                        if ($request->hasFile('image')){
                                $file = $request->file('image');
                                //dd($file); die();
                                $extension = $file->getClientOriginalExtension();
                                $fileName = time().'.'.$extension;
                                $path = 'images/attractions';
                                $uplaod = $file->move($path,$fileName);
                                 if ($request->hasFile('image')){ 
                                    $attractions->logo  = $fileName;
                                }
                          }
        $attractions->save();  
                            if ($request->hasFile('multiple_images')) 
                                            
                            {
                                $files = $request->file('multiple_images');
                             //dd($files); die();
                                 foreach($files as $file)
                                 {
                                     $extension = $file->getClientOriginalExtension();
                                     $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
                                     $path = 'images/logo/attractions';
                                        $file->move($path , $fileName);
                                         //dd($file); die();

                                                $attraction = new attraction_imgs();
                                                $attraction->attraction_imgs = $fileName;
                                                $attraction->attraction_id = $attractions->id;
                                                $attraction->save();
                                        //dd($galleryz); die();  
                                    }     
                                }
    
        return redirect()->route('admin.listAttraction')->with('message', 'Attraction is added successfully');
      } 
    

    /**
     * Display the specified resource.
     *
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
    public function show(attraction $attraction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
       $attractions = attraction::find($id);
       $imgs = $attractions->imgs;

     //echo "<pre>"; print_r($imgs); die();
        return view('admin/attraction/edit', compact('attractions', 'imgs'));
    }

     public function destroypic($id)
    {
             $attractions = attraction::find($id);
             //dd($attractions); die();
               $filename = public_path().'/images/attractions/'.$attractions->logo;
               //dd($filename); die();
               
               if (file_exists($filename)) {
                unlink($filename);
               }

               $attractions->logo = "";
               $attractions->save();
                return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
   public function putUpdate(Request $request,$id)
    {
        // /dd($id);
        $attractions = attraction::find($id);
        $attractions->name = request()->get('name');
        $attractions->description = request()->get('description');
        $attractions->is_enabled = request()->get('radio');

        //*image-code*/ 
                        if ($request->hasFile('logo')){
                                $file = $request->file('logo');
                                //dd($file); die();
                                $extension = $file->getClientOriginalExtension();
                                $fileName = time().'.'.$extension;
                                $path = 'images/attractions';
                                $uplaod = $file->move($path,$fileName);
                                 if ($request->hasFile('logo')){ 
                                    $attractions->logo  = $fileName;
                                }
                          }
        $attractions->save();  
       
    
        return redirect()->route('admin.listAttraction')->with('message', 'attractions is updated successfully');
      
        
    }

     public function deleteimgs($id,Request $request )
     { 
        //dd($id); die();

                 $checkbox = $request->Input('image_gal');
            //echo "<pre>"; print_r($checkbox); die();

                 foreach ($checkbox as $id) {
                    //dd($checkbox); die();
                    attraction_imgs::where('id', $id)->delete();
                    
                 }
                    
         
           return redirect()->back();
                                    
                                   
           
                        
     }

      public function updateimgs(Request $request, $id){
            //echo "ali"; die();
            $attractions = attraction::find($id);
            //echo "<pre>"; print_r($tours); die();
              if ($request->hasFile('multiple_images')){
                   $files = $request->file('multiple_images');
                             //dd($files); die();
                    foreach($files as $file){
                        $extension = $file->getClientOriginalExtension();
                        $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
                        $path = 'images/logo/attractions';
                        $file->move($path , $fileName);
                                        // /dd($path); die();
                        $imgs = new attraction_imgs();
                           $imgs->attraction_imgs = $fileName;
                           $imgs->attraction_id = $attractions->id;
                        
                           //dd($tourimgs); die();
                           $imgs->save();                                            
                    } 
                    return redirect()->back();                     
                }        
                                    

            }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
    // public function getDelete($id)
    // {
    //     $excludes = attraction::destroy($id);

    //     return redirect()->route('admin.listExclude')->with('message', 'package_Exclude is Deleted successfully');
    // }
}
