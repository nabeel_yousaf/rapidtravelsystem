<?php

namespace App\Http\Controllers\Admin;
use Validator;
use App\PackageEx;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackageExController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $excludes =  PackageEx::all();
        return view('admin.exclude.view', compact('excludes'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
       return view('admin.exclude.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
       {
       $validator = Validator::make(request()->all(), [
            'Exclude' => 'required',
            
            
        ]);
          if ($validator->passes()) {
        $excludes = new PackageEx();
        $excludes->package_exclude = $request['Exclude'];
       

        $excludes->save(); 
        return redirect()->route('admin.listExclude')->with('message', 'package Exclude is added successfully');
      } else {
              return redirect()->route('admin.storeExclude')
                 ->withErrors($validator->getMessageBag()->toArray())
                 ->withInput();
               }
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
    public function show(PackageEx $PackageEx)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
       $excludes = PackageEx::find($id);
       // echo "<pre>"; print_r($include); die();
        return view('admin/exclude/edit', compact('excludes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
   public function putUpdate(Request $request,$id)
    {
         {
       $validator = Validator::make(request()->all(), [
            'Exclude' => 'required',
            
            
        ]);
          if ($validator->passes()) {
        $excludes =  PackageEx::find($id);
        $excludes->package_exclude = $request['Exclude'];
       

        $excludes->save(); 
        return redirect()->route('admin.listExclude')->with('message', 'package Exclude is added successfully');
      } else {
              return redirect()->route('admin.updateExclude')
                 ->withErrors($validator->getMessageBag()->toArray())
                 ->withInput();
               }
       }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageIn  $packageIn
     * @return \Illuminate\Http\Response
     */
    // public function getDelete($id)
    // {
    //     $excludes = PackageEx::destroy($id);

    //     return redirect()->route('admin.listExclude')->with('message', 'package_Exclude is Deleted successfully');
    // }
}
