<?php

namespace App\Http\Controllers\Admin;

use App\CompanyInfornation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\country;
use App\state;
use App\city;
use Validator;
use DB;

 
class CompanyInfornationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = CompanyInfornation::all();
       // dd($comp); die();
        return view('admin.companyinfo.view')->with('companies', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        $countries = country::all();
        $states = state::all();
        $cities = city::all();
        return view('admin.companyinfo.add',compact('countries','states','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {   

            $validator = Validator::make(request()->all(), [
            'comp_name' => 'required',
            'address' => 'required',
            'email' => 'required|unique:users|max:255',
            'Comp_Reg_Num' => 'required',
            'taxnum' => 'required',
            
        ]);

        if ($validator->passes()) {
            DB::transaction(function () {
               
//dd(request()->get('country'));
                $comp = new CompanyInfornation();
                $comp->comp_name = request()->get('comp_name');
                $comp->Comp_live_dte = date('Y-m-d H:i:s');
                $comp->Comp_Email = request()->get('email');
                $comp->Comp_Address = request()->get('address');
                $comp->Comp_Country_name =request()->get('country_name');
                $comp->Comp_Stat_name = request()->get('state_name');
                $comp->Comp_City_name = request()->get('city');
                $comp->Aboutus_Desc = request()->get('Aboutus_Desc');
                $comp->Comp_Reg_Num = request()->get('Comp_Reg_Num');
                $comp->Comp_Tax_num = request()->get('taxnum');
                $comp->Comp_Phone = request()->get('phone');
                $comp->Comp_Phone = request()->get('mobile');
                $comp->CreatedBy =  request()->get('createdby');
                $comp->is_enabled = request()->get('radio');
                $comp->facebook = request()->get('facebook');
                $comp->twitter = request()->get('twitter');
                $comp->youtube = request()->get('youtube');
                $comp->tiktok =  request()->get('tikTook');
                $comp->instagram =  request()->get('instagram');
               
                 $comp->save();

               
               $destinationPath = 'images/logo/'; // upload path
                if (request()->file('logo')->isValid()) {

                    //dd($destinationPath);
                    $extension = request()->file('logo')->getClientOriginalExtension();
                    $fileName = 'Logo_URL-' . $comp->id . '.' . $extension;
                    //dd($fileName);
                    request()->file('logo')->move($destinationPath, $fileName);
                }
                $userUpdate = DB::table('company_infornations')->where('id',$comp->id)
                    ->update(['Logo_URL' => $fileName]);
                  
               
               
               
                }); return redirect()->route('admin.listCompinfo')->with('message', 'CompanyInfornation added successfully');
             } else {
            return redirect()->route('admin.storeCompinfo')
                ->withErrors($validator->getMessageBag()->toArray())
                ->withInput();
        }
         
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyInfornation  $companyInfornation
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyInfornation $companyInfornation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyInfornation  $companyInfornation
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
          $companies = CompanyInfornation::find($id);
          $countries = country::all();
          $states = state::all();
          $cities = city::all();
         return view('admin/companyinfo/edit', compact('companies','countries','states','cities'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyInfornation  $companyInfornation
     * @return \Illuminate\Http\Response
     */
    public function putUpdate($id)
    {
           

                $comp = CompanyInfornation::find($id);
                $comp->comp_name = request()->get('comp_name');
                $comp->Comp_live_dte = date('Y-m-d H:i:s');
                $comp->Comp_Email = request()->get('email');
                $comp->Comp_Address = request()->get('address');
                $comp->Comp_Country_name =request()->get('country_name');
                $comp->Comp_Stat_name = request()->get('state_name');
                $comp->Comp_City_name = request()->get('city_name');
                $comp->Aboutus_Desc = request()->get('description');
                $comp->Comp_Reg_Num = request()->get('Comp_Reg_Num');
                $comp->Comp_Tax_num = request()->get('taxnum');
                $comp->Comp_Phone = request()->get('phone');
                $comp->mobile = request()->get('mobile');
                $comp->CreatedBy =  request()->get('createdby');
                $comp->is_enabled = request()->get('radio');
                $comp->facebook = request()->get('facebook');
                $comp->twitter = request()->get('twitter');
                $comp->youtube = request()->get('youtube');
                $comp->tiktok =  request()->get('tikTook');
                $comp->instagram =  request()->get('instagram');
                $comp->save();

               if (empty($comp->Logo_URL)) {
                   $destinationPath = 'images/logo/'; // upload path
                    if (request()->hasFile('logo')) {

                        if (request()->file('logo')->isValid()) {
                            //dd($destinationPath);
                            $extension = request()->file('logo')->getClientOriginalExtension();
                            $fileName = 'Logo_URL-' . $comp->id . '.' . $extension;
                            //dd($fileName);
                            request()->file('logo')->move($destinationPath, $fileName);
                        }

                    }
                    $userUpdate = DB::table('company_infornations')->where('id',$comp->id)
                        ->update(['Logo_URL' => $fileName]);
               }
               
                  
               
               
               
                return redirect()->route('admin.listCompinfo')->with('message', 'CompanyInfornation Update successfully');
             
    }

    public function destroypic($id){
             $comp = CompanyInfornation::find($id);
               $filename = public_path().'images/logo/'.$comp->Logo_URL;
               //dd($filename);
               
               if (file_exists($filename)) {
                unlink($filename);
               }

               $comp->Logo_URL = "";
               $comp->save();
                return redirect()->back();
    }
    

    // public function getDelete($id){
    //     $companies = CompanyInfornation::find($id);
        
    //     $companies->delete();

    //     // redirect
    //     return redirect()->route('admin.listCompinfo')->with('message', 'Company deleted successfully');
    // }

    public function getstate($id)
        {
                $states = state::where('country_id', $id)->get();

                return response()->json($states);
        }

        public function getcity($id)
        {
                $citys = city::where('state_id', $id)->get();

                return response()->json($citys);
        }

}
