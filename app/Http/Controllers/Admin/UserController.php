<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\RoleUser;
use App\User;
use DougSisk\CountryState\CountryState;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /****************List Pages**************/
    public function index()
    {

        // $users = User::all();

        // $users = User::find($id);
        if(Auth::user()->roles->pluck('role_slug')[0] =='admin'){
            $users = User::all();
        } else{
            $users = User::where('admin_user','!=', 1)->get();
           // dd($roles);
        }
         //if(count($users) > 0)
        return view('admin.users.list')->with('users', $users);
    }

    /******* Add new Page************/
    public function getCreate()
    {

        if(Auth::user()->roles->pluck('role_slug')[0] =='admin'){
            $roles = Role::all();
        } else{
            $roles = Role::where('role_slug','!=','admin')->get();
             //dd($roles); die();
        }
         //dd($roles); die();
        
        return view('admin.users.add',array(
            'roles'=>$roles,
            
        ));
    }

    /**************Add new Page ***************/
    public function postStore()
    {

        $validator = Validator::make(request()->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|max:255',
            'confirm_password' => 'required|max:255',
            'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',

        ]);

        if ($validator->passes()) {

            DB::transaction(function()
            {
               /* $countriesObj = new CountryState();
                $countries = $countriesObj->getCountries();

                $states = $countriesObj->getStates(request()->get('country')); */
//dd(request()->get('country'));
                $user = new User();
                $user->first_name = request()->get('first_name');
                $user->last_name = request()->get('last_name');
                $user->email = request()->get('email');
                $user->address = request()->get('address');
                //$user->city = request()->get('city');
                $user->post_code = request()->get('post_code');
                $user->phone = request()->get('phone');
                $user->admin_user = 2;
                $user->password = Hash::make(request()->get('password'));
                //$user->CompId = 1;
               // $user->IsEnabled = request()->get('radio');
               // $user->CreatedBy = request()->get('createdby');
              //echo "<pre>"; print_r($user); die();
                $user->save();

                $roleUser = RoleUser::create([
                    'user_id' => $user->id,
                    'role_id' => request()->get('role'),
                ]);
               
                $destinationPath = 'public/media/images/users/'; // upload path
                if (request()->file('profile_image')->isValid()) {

                    //dd($destinationPath);
                    $extension = request()->file('profile_image')->getClientOriginalExtension();
                    $fileName = 'user-' . $user->id . '.' . $extension;
                    request()->file('profile_image')->move($destinationPath, $fileName);
                }
                $userUpdate = DB::table('users')->where('id',$user->id)
                    ->update(['profile_image' => $fileName]);

                if( !$roleUser )
                {
                    throw new \Exception('User not created, An error occurred during creating the account');
                }
            });

            return redirect()->route('admin.listUsers')->with('message', 'User added successfully');
        } else {
            return redirect()->route('admin.storeUser')
                ->withErrors($validator->getMessageBag()->toArray())
                ->withInput();
        }

    }

    /**********************update show edit Form **************/
    public function getEdit($id)
    {
        $user = User::find($id);
        if(Auth::user()->roles->pluck('role_slug')[0] =='admin'){
            $roles = Role::all();
        } else{
            $roles = Role::where('role_slug','!=','admin')->get();
           // dd($roles);
        }

        $currentRole = $user->roles->toArray();
        $countriesObj = new CountryState();
        $countries = $countriesObj->getCountries();
        $userCountry = array_keys($countries,$user->country);
        $states = empty($userCountry) ? '' : $countriesObj->getStates($userCountry[0]);

        // show the edit form and pass the nerd
        return view('admin.users.edit',array(
            'user'=> $user,
            'roles' => $roles,
            'currentRole'=>$currentRole,
            'countries'=>$countries,
            'states'=>$states,
        ));

    }

    /********* update Page Form ***************/
    public function putUpdate($id)
    {
        //dd(request()->all());
        $validator = Validator::make(request()->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
       //     'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            // 'email' => 'required|unique:users|max:255',
        ]);

        if ($validator->passes()) {

            // $countriesObj = new CountryState();
            // $countries = $countriesObj->getCountries();

            // $states = $countriesObj->getStates(request()->get('country'));

            $user = User::find($id);
            $user->first_name = request()->get('first_name');
            $user->last_name = request()->get('last_name');
            $user->email = request()->get('email');
            $user->address = request()->get('address');
            // $user->country = $countries[request()->get('country')];
            // $user->state = $states[request()->get('state')];
            // $user->city = request()->get('city');
            $user->post_code = request()->get('post_code');
            $user->phone = request()->get('phone');
            $user->admin_user = 2;
            if(!empty(request()->get('password'))) {
                $user->password = Hash::make(request()->get('password'));
            }
            $user->save();
            $destinationPath = 'public/media/images/users/'; // upload path
            if(!empty(request()->file('profile_image'))){
            if (request()->file('profile_image')->isValid()) {

                //dd($destinationPath);
                $extension = request()->file('profile_image')->getClientOriginalExtension();
                $fileName = 'user-' . $user->id . '.' . $extension;
                request()->file('profile_image')->move($destinationPath, $fileName);
            }
            $userUpdate = DB::table('users')->where('id',$user->id)
                ->update(['profile_image' => $fileName]);
            }

            $roleUser =  RoleUser::where('user_id', $id)
                ->update(['role_id' => request()->get('role')]);

            if( !$roleUser )
            {
                throw new \Exception('User not created, An error occurred during creating the account');
            }
            return redirect()->route('admin.listUsers')->with('message', 'User updated successfully');
        } else {
            return redirect()->route('admin.updateUser')
                ->withErrors($validator->getMessageBag()->toArray())
                ->withInput();
        }

    }

    /********* Delete Page ***************/

    public function getDelete($id)
    {
        // delete
        $user = User::find($id);
        $user->delete();

        // redirect
        return redirect()->route('admin.listUsers')->with('message', 'User deleted successfully');
    }
}
