<?php

namespace App\Http\Controllers\Admin;

use App\country;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries= country::all();
        return view('admin/country/list', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/country/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $validator = Validator::make(request()->all(), [
            'country' => 'required|max:255',
            
        ]);
          if ($validator->passes()) {
        $countries = new country();
        $countries->name = $request['country'];

        $countries->save(); 
        return redirect()->route('admin.listCountry')->with('message', 'Country is added successfully');
      } else {
              return redirect()->route('admin.storeCountry')
                 ->withErrors($validator->getMessageBag()->toArray())
                 ->withInput();
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(country $country)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $countries = country::find($id);
        //echo "<pre>"; print_r($countries); die();
        return view('admin/country/edit', compact('countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $countries = country::find($id);

        //echo"<pre>"; print_r($categories); die();
        $countries->name = $request->get('country');
        
        $countries->save();
        return redirect()->route('admin.listCountry')->with('message', 'Country is edit successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $countries = country::destroy($id);

    //     return redirect()->route('admin.listCountry')->with('message', 'Country is Deleted successfully');
    // }
}
