<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\PermissionRole;
use App\Role;
use App\RoleUser;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class RoleController extends Controller
{
    /****************List Roles**************/
    public function index()
    {
        $roles = Role::where('id', '<>', 1)->get();
        return view('admin.roles.list')->with('roles', $roles);
    }

    /******* Add new Page************/
    public function getCreate()
    {
        $permissions = Permission::all();
        return view('admin.roles.add')->with('permissions', $permissions);
    }

    /**************Add new Role ***************/
    public function postStore()
    {
        //dd(request()->all());
        $validator = Validator::make(request()->all(), [
            'role_title' => 'required|max:255',
            'role_slug' => 'required|unique:roles|max:255',
        ]);

        if ($validator->passes()) {
            DB::transaction(function () {
                $role = new Role();
                $role->role_title = request()->get('role_title');
                $role->role_slug = request()->get('role_slug');
                $role->role_description = request()->get('role_description');
            // /dd($role); die();
                $role->save();
                if (!empty(request()->get('permissions'))) {
                    foreach (request()->get('permissions') as $permission) {
                        //dd($permission);
                        $permissionRole = PermissionRole::create([
                            'role_id' => $role->id,
                            'permission_id' => $permission,
                        ]);

                        //dd($permissionRole->id);
                    }

                    if (!$permissionRole) {
                        throw new \Exception('Role is not created, An error occurred during creating the Role');
                    }
                }
            });
            return redirect()->route('admin.listRoles')->with('message', 'Role is added successfully');
        } else {
            return redirect()->route('admin.storeRole')
                ->withErrors($validator->getMessageBag()->toArray())
                ->withInput();
        }

    }

    /**********************update show edit Form **************/
    public function getEdit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        // show the edit form and pass the id
        return view('admin.roles.edit', array('role' => $role, 'permissions' => $permissions));

    }

    /********* update Role Form ***************/
    public function putUpdate($id)
    {
        // dd(request()->all());
        $validator = Validator::make(request()->all(), [
            'role_title' => 'required|max:255',
            'role_slug' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            $role = Role::find($id);
            $role->role_title = request()->get('role_title');
            $role->role_slug = request()->get('role_slug');
            $role->role_description = request()->get('role_description');
            $role->save();
            $permissionRole = PermissionRole::where('role_id', $id)->delete();
            if (!empty(request()->get('permissions'))) {
                foreach (request()->get('permissions') as $permission) {
                    //dd($permission);
                    $permissionRole = PermissionRole::create([
                        'role_id' => $role->id,
                        'permission_id' => $permission,
                    ]);

                    // dd($permissionRole->id);
                }

                if (!$permissionRole ) {
                    throw new \Exception('Role is not created, An error occurred during creating the Role');
                }
            }

            return redirect()->route('admin.listRoles')->with('message', 'Role updated successfully');
        } else {
            return redirect()->route('admin.updateRole')
                ->withErrors($validator->getMessageBag()->toArray())
                ->withInput();
        }

    }

    /********* Delete Role ***************/

    // public function getDelete($id)
    // {
    //     // delete
    //     $role = Role::find($id);
    //     $role->delete();

    //     // redirect
    //     return redirect()->route('admin.listRoles')->with('message', 'Role deleted successfully');
    // }
}
