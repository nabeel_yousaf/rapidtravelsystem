<?php

namespace App\Http\Controllers\Admin;

use App\state;
use App\country;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $countries = country::all();
         //dd($countries); die;
         $states = state::all();
       // echo "<pre>"; print_r($states); die();
       return view('admin/state/list', compact('countries','states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
           $countries =country::all();
        return view('admin/state/create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'state' => 'required|max:255',
            'name' => 'required',
            
        ]);
          if ($validator->passes()) {
        $states = new state();
        $states->name = $request['state'];
        $states->country_id = $request['name'];
   //dd($subcategories); die();
        $states->save(); 
        return redirect()->route('admin.listState')->with('message', 'State is added successfully');
      } else {
              return redirect()->route('admin.storeState')
                 ->withErrors($validator->getMessageBag()->toArray())
                 ->withInput();
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\state  $state
     * @return \Illuminate\Http\Response
     */
    public function show(state $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\state  $state
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $states = state::find($id);
         $countries = country::all();
        //dd($countries); die();
        return view('admin/state/edit', compact('states','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\state  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $states = state::find($id);
        
        #echo"<pre>"; print_r($state); die();
        $states->name = $request->get('state');
        $states->country_id = $request->get('name');

        $states->save();
        return redirect()->route('admin.listState')->with('message', 'State is edit successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\state  $state
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //      $states = state::destroy($id);
    //     return redirect()->route('admin.listState')->with('message', 'State is Deleted successfully');
    // }
}
