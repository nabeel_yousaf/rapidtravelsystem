<?php

namespace App\Http\Controllers\Admin;

use App\Tour;
use App\CompanyInfornation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\tourimages;
use App\TourTypeName;
use App\PackageEx;
use App\PackageIn;
use App\Tour_packages;
use Validator;
 
use DB;

class TourController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){  
      
        $compy = CompanyInfornation::all();
        $tourtypes = TourTypeName::all();
        $tours = Tour::all();

        
        // $includes = PackageIn::all();
        // $excludes = PackageEx::all();
       
        // echo "<pre>";
        // print_r($tours[0]->Tour_packages[0]->PackageIn);die;

        // foreach ($tours as $tour) {
        //     foreach ($tour->Tour_packages as $tp) {
        //         echo "<pre>";
        //         print_r($tp->PackageIn);
        //     }
        // }
        // die();

        return view('admin.tour.view', compact('tours','compy','tourtypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(){
        $tourtypes = TourTypeName::all();
        $includes = PackageIn::all();
        $excludes = PackageEx::all();
        //dd($excludes);die();
        $companies = CompanyInfornation::all();

          return view('admin.tour.add', compact('companies','tourtypes','includes','excludes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       public function postStore(Request $request){   

                $tour = new Tour();
                $tour->comp_id = request()->get('comp_name');
                //dd($tour); die();
                $tour->Tour_name = request()->get('tour_name');
                $tour->Tour_Desc = request()->get('tour_Desc');
                $tour->Tour_Adult_price = request()->get('tour_adult_price');
                $tour->Tour_Adult_disc_price =request()->get('Tour_Adult_disc_price');
                $tour->Tour_Child_price = request()->get('Tour_Child_price');
                $tour->Tour_Child_disc_price = request()->get('Tour_Child_disc_price');
                $tour->Tour_type_name = request()->get('Tour_type_name');
                $tour->Tour_Duration = request()->get('Tour_Duration');
                $tour->includes = request()->get('excludes');
                $tour->currency = request()->get('currency');            
                $tour->CreatedBy =  request()->get('createdby');
                $tour->is_enabled = request()->get('radio');
                $tour->currency = request()->get('currency');
               //dd($tour); die();
                $tour->save(); 
            
                 //$packgs = new Tour_packages();
                 //$packgs->tour_id =$tour->id;
                // $packgs->save();
                // if ($request->input('Include')){
                //     $includes =$request->input('Include');
                //       foreach($includes as $include){
                //         $pkg = new Tour_packages();
                //         $pkg->pkg_include_id = $include;
                //         $pkg->tour_id =$tour->id;
                //         $pkg->save();
                //     }    
                // }

                // if ($request->input('Exclude')){
                //     $excludes =$request->input('Exclude');
                //       foreach($excludes as $exclude){
                //         $pkgs = new Tour_packages();
                //         $pkgs->pkg_exclude_id = $exclude;
                //         $pkgs->tour_id =$tour->id;
                //         $pkgs->save();
                //        }
                        
                // } 
                    //*image-code*//
                         if ($request->hasFile('tour_image')){
                                $file = $request->file('tour_image');
                                $extension = $file->getClientOriginalExtension();
                                $fileName = time().'.'.$extension;
                                $path = 'images/logo/';
                                $uplaod = $file->move($path,$fileName);
                                //dd($path); die();
                                 if ($request->hasFile('tour_image')){ 
                                    $tour->profile_image  = $fileName;
                             }
                         }
                             $tour->save();  

                     //******************multiple-imgs*******************//  

                if ($request->hasFile('multiple_images')){
                   $files = $request->file('multiple_images');
                             //dd($files); die();
                    foreach($files as $file){
                        $extension = $file->getClientOriginalExtension();
                        $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
                        $path = 'images/logo/tourimgs';
                        $file->move($path , $fileName);
                                        // dd($path); die();
                        $tourimgs = new tourimages();
                           $tourimgs->tour_images = $fileName;
                           $tourimgs->tour_id = $tour->id;
                           $tourimgs->comp_id = $tour->comp_id;
                           $tourimgs->save();                                            
                    } 
                                    
                }                   
                  return redirect()->route('admin.listTours')->with('message', 'Tours added successfully');
        }

         public function updateimgs(Request $request, $id){
            //echo "ali"; die();
            $tours = Tour::find($id);
            //echo "<pre>"; print_r($tours); die();
              if ($request->hasFile('multiple_images')){
                   $files = $request->file('multiple_images');
                             //dd($files); die();
                    foreach($files as $file){
                        $extension = $file->getClientOriginalExtension();
                        $fileName = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
                        $path = 'images/logo/tourimgs';
                        $file->move($path , $fileName);
                                        // /dd($path); die();
                        $tourimgs = new tourimages();
                           $tourimgs->tour_images = $fileName;
                           $tourimgs->tour_id = $tours->id;
                           $tourimgs->comp_id = $tours->comp_id;
                           //dd($tourimgs); die();
                           $tourimgs->save();                                            
                    } 
                    return redirect()->back();                     
                }        
                                    

            }


    public function getEdit($id)
    {
        $tours = Tour::find($id);
       
        $tourimgs = tourimages::where('tour_id', $tours->id)->get();
        $tourtypes = TourTypeName::all();
        $companies = CompanyInfornation::all();
        $includes = PackageIn::all();
        $excludes = PackageEx::all();
        return view('admin.tour.edit', compact('tours','companies','tourimgs','tourtypes','includes','excludes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function putUpdate(Request $request, $id)
    {
        //dd($id); die();
                $tour = Tour::find($id);
                $tour->comp_id = request()->get('comp_name');
                //dd($tour); die();
                $tour->Tour_name = request()->get('tour_name');
                $tour->Tour_Desc = request()->get('tour_Desc');
                $tour->Tour_Adult_price = request()->get('tour_adult_price');
                $tour->Tour_Adult_disc_price =request()->get('Tour_Adult_disc_price');
                $tour->Tour_Child_price = request()->get('Tour_Child_price');
                $tour->Tour_Child_disc_price = request()->get('Tour_Child_disc_price');
                $tour->Tour_type_name = request()->get('Tour_type_name');
               
                $tour->Tour_Duration = request()->get('Tour_Duration');
                $tour->includes = request()->get('includes');
                $tour->includes = request()->get('excludes');
                $tour->currency = request()->get('currency');            
                $tour->CreatedBy =  request()->get('createdby');
                $tour->is_enabled = request()->get('radio');
               //dd($tour); die();
                // if ($request->input('Include')){
                //     $includes =$request->input('Include');
                //       foreach($includes as $include){
                //         $pkg = new Tour_packages();
                //         $pkg->pkg_include_id = $include;
                //         $pkg->tour_id =$tour->id;
                //         $pkg->save();
                //     }    
                // }

                // if ($request->input('Exclude')){
                //     $excludes =$request->input('Exclude');
                //       foreach($excludes as $exclude){
                //         $pkgs = new Tour_packages();
                //         $pkgs->pkg_exclude_id = $exclude;
                //         $pkgs->tour_id =$tour->id;
                //         $pkgs->save();
                //        }
                        
                // } 
                
                    //*image-code*//
                         if ($request->hasFile('tour_image'))
                         {
                                $file = $request->file('tour_image');
                                $extension = $file->getClientOriginalExtension();
                                $fileName = time().'.'.$extension;
                                $path = 'images/logo/';
                                $uplaod = $file->move($path,$fileName);
                                //dd($path); die();
                                 if ($request->hasFile('tour_image'))
                             { 
                                    $tour->profile_image  = $fileName;

                             }
                         }
                   $tour->save();          
                            return redirect()->route('admin.listTours')->with('message', 'Tour Updated successfully');
    }

   

        // public function getDelete($id)
        //  {
        //      $tour = Tour::find($id);
        //      $tour->delete();

        // // redirect
        //     return redirect()->route('admin.listTours')->with('message', 'Tour deleted successfully');
        //  }


    
     
      public function destroypic($id)
    {
             $tour = Tour::find($id);
               $filename = 'images/logo/'.$tour->profile_image;
               //dd($filename); die();
               
               if (file_exists($filename)) {
                unlink($filename);
               }

               $tour->profile_image = "";
               $tour->save();
                return redirect()->back();
    }


      public function deleteimgs($id,Request $request )
     { 
        // /dd($id); die();

                 $checkbox = $request->Input('image_gal');
             //echo "<pre>"; print_r($checkbox); die();

                 foreach ($checkbox as $id) {
                     //dd($checkbox); die();
                    tourimages::where('id', $id)->delete();
                    
                 }
                    
         
           return redirect()->back();
                                    
                                   
           
                        
     }
}
