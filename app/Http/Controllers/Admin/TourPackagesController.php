<?php

namespace App\Http\Controllers;

use App\Tour_packages;
use Illuminate\Http\Request;

class TourPackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tour_packages  $tour_packages
     * @return \Illuminate\Http\Response
     */
    public function show(Tour_packages $tour_packages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tour_packages  $tour_packages
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour_packages $tour_packages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tour_packages  $tour_packages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour_packages $tour_packages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tour_packages  $tour_packages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour_packages $tour_packages)
    {
        //
    }
}
