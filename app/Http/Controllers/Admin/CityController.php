<?php

namespace App\Http\Controllers\Admin;

use App\city;
use App\country;
use App\state;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         /*$states = state::all();
       foreach ($states as $state) {
         echo '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'. 
                $state->name .'<br>';
              foreach ($state->cities as $city) {
              echo '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'. 
              $city->name .'<br>';
             }

           
       }
       die();*/

        $cities = city::all();
         $states = state::all();
         $countries = country::all();

        return view('admin/city/list', compact('cities','states','countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $countries = country::all();

         $states = state::all();
         //dd($states); die();
        return view('admin/city/create', compact('countries','states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = Validator::make(request()->all(), [
            'name' => 'required|max:255',
           
            
        ]);
          if ($validator->passes()) {
        $cities = new city();

        $cities->name = $request['name'];
        $cities->country_id = $request['country_name'];
        $cities->state_id = $request['state_name'];
   //dd($subcategories); die();
        $cities->save(); 
        return redirect()->route('admin.listCity')->with('message', 'City is added successfully');
      } else {
              return redirect()->route('admin.storeCity')
                 ->withErrors($validator->getMessageBag()->toArray())
                 ->withInput();
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\city  $city
     * @return \Illuminate\Http\Response
     */
    public function show(city $city)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\city  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(city $city, $id)
    {
        $cities = city::find($id);
        $states = state::all();
        $countries = country::all();

        return view('admin/city/edit', compact('cities','states','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\city  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cities = city::find($id);
        $cities->name = $request->get('name');
        $cities->country_id = $request->get('country_name');
        $cities->state_id = $request->get('state_name');
        $cities->save();
         return redirect()->route('admin.listCity')->with('message', 'City is Edit successfully');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\city  $city
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $cities = city::destroy($id);

    //     return redirect()->route('admin.listCity')->with('message', 'City is Deleted successfully');
    // }


    public function get($id)
    {
        
       $states= state::where('country_id', $id)->get();
      //echo "<pre>"; print_r($states); die();
       
          return response()->json($states);

    }

   public function getcity($id)
        {
                $citys = city::where('state_id', $id)->get();

                return response()->json($citys);
        }


}
