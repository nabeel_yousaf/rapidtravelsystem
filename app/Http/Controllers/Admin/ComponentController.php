<?php

namespace App\Http\Controllers\Admin;

use App\component; 
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentController extends Controller
{
   public function index()
    {
        $components= component::all();
        return view('admin/component/list', compact('components'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/component/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $validator = Validator::make(request()->all(), [
            'home' => 'required|max:255',
            
        ]);
          if ($validator->passes()) {
        $components = new component();
        $components->home = $request['home'];
        $components->tour = $request['tour'];
        $components->safari = $request['safari'];
        $components->service = $request['service'];
        $components->guide = $request['guide'];
        $components->contact = $request['contact'];
        $components->about = $request['about'];
        $components->travel = $request['travel'];


        $components->save(); 
        return redirect()->route('admin.listComponent')->with('message', 'Component is added successfully');
      } else {
              return redirect()->route('admin.storeComponent')
                 ->withErrors($validator->getMessageBag()->toArray())
                 ->withInput();
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(component $component)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $components = component::find($id);
        //echo "<pre>"; print_r($countries); die();
        return view('admin/component/edit', compact('components'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $components = component::find($id);

        //echo"<pre>"; print_r($categories); die();
       
        $components->home = $request['home'];
        $components->tour = $request['tour'];
        $components->safari = $request['safari'];
        $components->service = $request['service'];
        $components->guide = $request['guide'];
        $components->contact = $request['contact'];
        $components->about = $request['about'];
        $components->travel = $request['travel'];
        
        $components->save();
        return redirect()->route('admin.listComponent')->with('message', 'Component is edit successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $components = component::destroy($id);

    //     return redirect()->route('admin.listComponent')->with('message', 'Component is Deleted successfully');
    // }
}
