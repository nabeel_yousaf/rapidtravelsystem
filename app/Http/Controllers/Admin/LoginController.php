<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    

    /*************Admin Login View************/

    public function index()
    {

        if (Auth::check()){
           //dd(Auth::check());
            return redirect()->route('admin.dashboard');
        }
        return View::make('admin.login');
    }

    /**************************Post Login *******************/

    public function postLogin()
    { 
        //dd('ali'); die();
        //dd(request()->all());
        $email = Input::get('email');
        $password = Input::get('password');
        //dd($email); die();
        $remember = Input::get('remember');
        if($remember=='on'){
            $remember = 1;
        }
        if (Auth::attempt(array('email' => $email, 'password' => $password, 'admin_user' => 1))) {

          // /  return 'admin.dashboard';
            return response()->json(['error' => 'false'], 200);

        } 
         if (Auth::attempt(array('email' => $email, 'password' => $password, 'admin_user' => 2),$remember)) {

                return response()->json(['error' => 'false'], 200);
        } else {
            return response()->json(['error' => 'true', 'message' => 'Please provide the valid credentials'],200);
        }
    }

    /******************password Reset***************************/

    public function passwordReset($token = null)
   {
       if (is_null($token)) {
           throw new NotFoundHttpException;
       }


        return view('admin.passwordReset')->with('token', $token);;
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('admin.login');
    }

}