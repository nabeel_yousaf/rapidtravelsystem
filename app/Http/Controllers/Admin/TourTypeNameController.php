<?php

namespace App\Http\Controllers\Admin;
use Validator;
use App\TourTypeName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TourTypeNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $tourtypes = TourTypeName::all();
        return view('admin.tourtype.list', compact('tourtypes'));
        //dd($tourtype);die();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        return view('admin.tourtype.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
       $validator = Validator::make(request()->all(), [
            'tourtype' => 'required',
            'description' => 'required',
            
        ]);
          if ($validator->passes()) {
        $tourtypes = new TourTypeName();
        $tourtypes->tourtypeName = $request['tourtype'];
        $tourtypes->tourtypeDes = $request['description'];
        $tourtypes->is_enabled = $request['radio'];

        $tourtypes->save(); 
        return redirect()->route('admin.listTourType')->with('message', 'tourtypes is added successfully');
      } else {
              return redirect()->route('admin.storeTourType')
                 ->withErrors($validator->getMessageBag()->toArray())
                 ->withInput();
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TourTypeName  $tourTypeName
     * @return \Illuminate\Http\Response
     */
    public function show(TourTypeName $tourTypeName)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TourTypeName  $tourTypeName
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
       $tourtypes = TourTypeName::find($id);
        //echo "<pre>"; print_r($countries); die();
        return view('admin/tourtype/edit', compact('tourtypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TourTypeName  $tourTypeName
     * @return \Illuminate\Http\Response
     */
    public function putUpdate(Request $request,$id)
    {
         $tourtypes = TourTypeName::find($id);

        //echo"<pre>"; print_r($categories); die();
         $tourtypes->tourtypeName = $request->get('tourtypes');
         $tourtypes->tourtypeDes = $request->get('description');
         $tourtypes->is_enabled = $request->get('radio');
        
        $tourtypes->save();
        return redirect()->route('admin.listTourType')->with('message', 'tour types is edit successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TourTypeName  $tourTypeName
     * @return \Illuminate\Http\Response
     */
    // public function getDelete($id)
    // {
    //     $tourtypes = TourTypeName::destroy($id);

    //     return redirect()->route('admin.listTourType')->with('message', 'Country is Deleted successfully');
    // }
}
