<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\PaymentHistory;
use App\Models\Plan;
use Illuminate\Http\Request;
use App\Tour;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\PostRequest;

class DashboardController extends Controller
{
    /***************Index Method Showing Desktop **************/
    public function index()
    {
       $user = Auth::user();
       $tours = Tour::all();
       $users = User::all();
       $bookings = PostRequest::all();
       
    //dd(auth()->check() && auth()->user()-> == 1);
        // /dd(Auth::user()->isAdminUser());

        return view('admin.home.index',compact('tours','users','bookings'));

    }
}
