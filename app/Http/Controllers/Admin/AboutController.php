<?php

namespace App\Http\Controllers\Admin;
use Validator;
use App\About;
// use App\About\Aboutfun;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;


class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
   
        {
            // $about = About::orderBy('id', 'DESC')->pagination(5);
        //     $about = DB::table('abouts')->paginate(5);
        //   return view('admin.about.view-about',['about' => $about]);
          $abouts = About::all();
          // dd($comp); die();
          return view('admin.about.view-about',compact('abouts', $abouts));
        }
    
      
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
        
        $validator = Validator::make(request()->all(), [
            'title' => 'required',
            'slug' => 'required|unique:abouts',
            'description' => 'required',
            
            
        ]);
        if ($validator->passes()) {
            $about = new About();
            $about->title = $request['title'];
            $about->is_enabled = $request->get('radio');           
            $about->slug = Str_slug(request()->title);  
            $about->description = request()->get('description');
            //  $about->description =strip_tags(request()->description);
    
            $about->save(); 
             return redirect()->route('admin.view-about')->with('message', 'Data added successfully');
           
        
          } 
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(country $country)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
              $about = About::findOrFail($id);        
        return view('admin.about.update', compact('about'));
       

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(request()->all(), [
            'title' => 'required',
            'slug' => 'required',
            'description' => 'required'
            
        ]);
        if ($validator->passes()) {
            $about = About::find($id);
            $about->title = request()->get('title');
            // $about->slug = str_slug($request->title);
        $about->is_enabled = request()->get('radio');

            $about->slug = Str_slug(request()->get('title'));
            $about->description = request()->get('description');
    
            $about->save(); 
             return redirect()->route('admin.view-about')->with('message', 'Data Updated successfully');
           
        
          } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\country  $country
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $countries = country::destroy($id);

    //     return redirect()->route('admin.listCountry')->with('message', 'Country is Deleted successfully');
    // }
    public function destroy($id)
    {
        $abouts = About::destroy($id);

             return redirect()->route('admin.view-about')->with('message', 'Data Deleted successfully');
    }

}
