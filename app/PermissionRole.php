<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model
{
    protected $table = 'permission_role';
    protected $fillable=[
        'id',
        'role_id',
        'permission_id',
        'created_at',
        'updated_at'
    ];

    public function role(){
        return $this->belongsTo('App\RoleUser','role_id'); 
    }
}
