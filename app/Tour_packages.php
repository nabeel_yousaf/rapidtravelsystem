<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tour_packages extends Model{
	
    public function tour(){
        return $this->belongsTo('App\Tour','tour_id'); 
    }

    public function PackageIn(){
        return $this->belongsTo('App\PackageIn','pkg_include_id'); 
    }

    public function PackageEx(){
        return $this->belongsTo('App\PackageEx','pkg_exclude_id'); 
    }
}
