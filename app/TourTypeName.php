<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TourTypeName extends Model{
    // use SoftDeletes;
   
    // protected $dates = ['deleted_at'];


    public function tour(){
		return $this->belongsTo('App\TourTypeName');
	}

	 
}
