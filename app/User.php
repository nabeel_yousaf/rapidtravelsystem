<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'phone' ,'address',
        'city' ,
        'state' ,
        'country' ,
        'profile_image' ,
        'post_code', 'username', 'email', 'password','admin_user'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    // public function isAdminUser()
    // {
    //     return $this->admin_user;
    // }

    /*
    |--------------------------------------------------------------------------
    | ACL Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Checks a Permission
     *
     * @param  String permission Slug of a permission (i.e: manage_user)
     * @return Boolean true if has permission, otherwise false
     */
    public function hasAccess($permission)
    {
        // dd(!is_null($permission));
        if(!is_null($permission)){
            return  $this->checkPermission($permission);
        } else {

            return 0;
        }

    }

    /**
     * Check if the permission matches with any permission user has
     *
     * @param  String permission slug of a permission
     * @return Boolean true if permission exists, otherwise false
     */
    protected function checkPermission($perm)
    {
        $permissions = $this->getAllPernissionsFormAllRoles();
        $permissionArray = is_array($perm) ? $perm : [$perm];
        return count(array_intersect($permissions, $permissionArray));
    }

    /**
     * Get all permission slugs from all permissions of all roles
     *
     * @return Array of permission slugs
     */
    protected function getAllPernissionsFormAllRoles()
    {
        $permissionsArray = [];
        $permissions = $this->roles->load('permissions')->toArray()[0]['permissions'];
        //dd($permissions);
        return array_map('strtolower', array_unique(array_flatten(array_map(function ($permission) {

            return array_get($permission, 'permission_slug');

        }, $permissions))));
    }

    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */

    /**
     * Many-To-Many Relationship Method for accessing the User->roles
     *
     * @return QueryBuilder Object
     */
    public function roles(){
        return $this->belongsToMany('App\Role');
    }


     public function roleUser(){ 
     //echo "ali"; die();     
        return $this->hasMany('App\RoleUser','user_id');
    }

    public function Admin() 
    {
       return $this->roles()->where('role_id', 1);
    }

    // public function rolePermission()
    // {
    //     $user = $this->roleUser;
    //     foreach($user as $us){
    //         $get = $us->id;
    //     }

    //     return $get;
    // }


}
