<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageIn extends Model
{
    public function Tour_packages(){
        return $this->hasMany('App\Tour_packages','pkg_include_id'); 
    }
}
