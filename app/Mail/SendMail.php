<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;
      public $data1;
    /**
     * Create a new data1 instance.
     *
     * @return void
     */
    public function __construct($data1)
    {
        $this->data1 = $data1;
    }
    /**
     * Build the data1.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.email.view')->with('data1', $this->data1);
    }
}
