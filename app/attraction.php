<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attraction extends Model
{
   public function imgs(){
		return $this->hasMany('App\attraction_imgs','attraction_id');
	}
}
