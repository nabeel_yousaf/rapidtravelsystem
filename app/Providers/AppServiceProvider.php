<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
            view()->composer('layout.layout', function ($view) {
            $companyData = \DB::table('company_infornations')->where('id', 2)->get();
            $components = \DB::table('components')->get();
  //dd($module); die();
// /dd($companyData[0]->Logo_URL); die();
            $view->companyData = $companyData;
            $view->components = $components;
            
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
