<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PermissionRoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return voidd
     */
    public function boot()
    {
        view()->composer('admin.common.mainSidebar', function ($view) {
            $rolz = \DB::table('role_user')->get();
//dd($companyData[0]->Logo_URL); die();
            $view->rolz = $rolz;
             
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
