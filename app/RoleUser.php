<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_user';
    protected $fillable=[
        'id',
        'role_id',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function users(){
        return $this->belongsTo('App\User'); 
    } 

    public function permission(){
        return $this->hasMany('App\PermissionRole','role_id'); 
    }
}
