<?php
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Support\Facades\DB;



function sitename()
{
	$companies = DB::table('company_infornations')->select('comp_name')->where('id', '=', 2)->get();
    $getname = $companies[0];
    $print = $getname->comp_name;
	$name = $print;
	return $name;
}

function siteEmail()
{
	$companies = DB::table('company_infornations')->select('Comp_Email')->where('id', '=', 2)->get();
    $getemail = $companies[0];
    $email = $getemail->Comp_Email;
	return $email;
}

function mobile()
{
	$companies = DB::table('company_infornations')->select('mobile')->where('id', '=', 2)->get();
    $getmobile = $companies[0];
    $mobile = $getmobile->mobile;
	return $mobile;
}

function phone()
{
	$companies = DB::table('company_infornations')->select('Comp_Phone')->where('id', '=', 2)->get();
    $getphone = $companies[0];
    $phone = $getphone->Comp_Phone;
	return $phone;
}

?>