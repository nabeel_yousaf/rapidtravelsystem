<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInfornation extends Model
{
    public function abouts(){
        return $this->hasOne('App\About'); 
    }
}